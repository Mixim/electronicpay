using System;
namespace AdminsGUIApplication
{
	/*
		Энумераторы должны иметь тип-значение byte, т.к. в методах, где они используются, происходит их перевод 
		непосредственное в этот тип (Convert.ToByte) и отправка\прием\дешифровка осуществляется по числовому 
		представлению
	*/
	
	#region НЕОБХОДИМЫЕ ЭНУМЕРАТОРЫ
		//команды, отправляемые администраторской машиной на терминал 
		public enum AdminsCommands : byte {Reboot=0x01,
											TurnOff=0x02,
											UpdateConfiguration=0x03,
											LetsConnect=0xFF};
		
				
		//ответы, отправляемые с терминала на администраторскую машину
		public enum ClientsResponses : byte {Ack=0x01,
												CommandExecuted=0x02,
												CommandsExecutionsIsFailed=0x03,
												InvalidCommand=0x04,
												ReceivedCorruptedData=0x05,
												ThereIsNoCommandHandler=0x06,
												Resend=0x07,
												ClientDidNotSendResponse=0xFE,
												InvalidHashCodeOfClientsResponse=0xFF};
	
		//результаты исполнения команд на обоих сторонах. Например, если неудалось отправить сообщение по сети или что-то в этом роде
		//никуда не отправляется, обрабатывается на текущей стороне
		public enum ResultOfCommandExecution : byte {CommandSuccessfullyExecuted,
														RecipientDoesNotAnswered};
	#endregion
	
	
	
	#region Класс, который представляет результат разбора команд от сервера на стороне клиента
		public class ParsedAdminsCommandAndArgs
		{
			#region НЕОБХОДИМЫЕ КОНСТАНТЫ КЛАССА ParsedAdminsCommandAndArgs
				protected const String FieldsFormatOfClass="Command='{0}'; Args='{1}'";
			#endregion
		
			#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА ParsedAdminsCommandAndArgs
				public AdminsCommands Command{get;set;}
				public Object Args{get;set;}
			#endregion
			
			#region Конструктор класса ParsedAdminsCommandAndArgs[с параметрами]
				public ParsedAdminsCommandAndArgs(AdminsCommands Command, Object Args)
				{
					this.Command=Command;
					this.Args=Args;
				}
			#endregion
		
			#region Метод для перевода текущего класса в строку
				public override String ToString()
				{
					String returnedValue;
			
					returnedValue=String.Format(ParsedAdminsCommandAndArgs.FieldsFormatOfClass, this.Command, this.Args);
			
					return returnedValue;
				}
			#endregion
		}
	#endregion
	
	#region Класс, который представляет результат разбора ответов от клиента на стороне сервера
		public class ParsedClientsResponseAndComment
		{
			public ClientsResponses Response{get;set;}
			public String Comment{get;set;}
		
			public ParsedClientsResponseAndComment(ClientsResponses Response, String Comment)
			{
				this.Response=Response;
				this.Comment=Comment;
			}
		}
	#endregion
}

