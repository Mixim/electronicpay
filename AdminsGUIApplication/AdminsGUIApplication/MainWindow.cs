using System;
using Gtk;
using System.Net;


namespace AdminsGUIApplication
{
	public partial class MainWindow : Gtk.Window
	{
		#region НЕОБХОДИМЫЕ КОНСТАНТЫ КЛАССА MainWindow
			protected const String AttemptToConnectToTerminal_Format="'{0}': Предпринимается попытка подключения к терминалу с IP-адресом: '{1}' и портом: '{2}'\n";
			protected const String ResultOfAttemptsToConnectToTerminal_Format="'{0}': В результате попытки подключения получен результат: '{1}' и комментарий: '{2}'";
			protected const String ExceptionOfConnect_Format="'{0}': При попытке подключения к терминалу с IP-адресом: '{1}' и портом: '{2}' произошло исключение: '{3}'\n";
			protected const String AttemptToDisconnectFromTerminal_Format="'{0}': Предпринимается попытка отключиться от терминала";
			protected const String ResultOfAttemptsToDisconnectFromTerminal_Flrmat="'{0}': Попытка отключения от терминала выполнена успешно";
			protected const String ExceptionOfDisconnect_Format="'{0}': При попытке отключения от терминала получили исключение: '{1}'";
		
			protected const String InvalidCommand_Format="'{0}': Попытка передать на терминал неизвестную команду: '{1}'\n";
			protected const String AttemptToSendCommandToTerminal_Format="'{0}': Попытка передать команду '{1}' на терминал с IP-адресом: '{2}' и портом: '{3}'\n";
			protected const String ResultOfAttemptsToSendCommandToTerminal_Format="'{0}: В результате попытки выполнения команды '{1}' на терминале с IP-адресом: '{2}' и портом: '{3}' получен результат: '{4}' и комментарий: '{5}'";
			protected const String ExceptionOfSendCommand_Format="'{0}': При попытке передать команду: '{1}' терминалу с IP-адресом: '{2}' и портом: '{3}' произошло исключение: '{4}'\n";
		
			protected const String ConnectToTerminalButtonText="<span size='15000'>Подключиться к терминалу</span>";
			protected const String DisconnectFromTerminalButtonText="<span size='15000'>Отключиться от терминала</span>";
		#endregion
		
		#region НЕОБХОДИМЫЕ ПЕРЕМЕННЫЕ
			protected AdminClass admin=new AdminClass();
			protected ParsedClientsResponseAndComment clientResponse;
		#endregion
	
		#region Конструктор класса MainWindow
			public MainWindow () : base(Gtk.WindowType.Toplevel)
			{
				Build ();
			}
		#endregion

		#region Действия при удалении окна
			protected void OnDeleteEvent (object sender, DeleteEventArgs a)
			{
				Application.Quit ();
				a.RetVal = true;
			}
		#endregion
	
		#region Действия при нажатии на кнопку connectToTerminalOrDisconnect_button
			protected virtual void OnConnectToTerminalButtonClicked (object sender, System.EventArgs e)
			{
				try
				{
					if(admin.Server.Connected==false)
					{
						admin.ConnectToClient(IPAddress.Parse(ipAddress_entry.Text), Convert.ToInt32(port_spinbutton.Value));
						networkLog_textview.Buffer.Text+=String.Format(MainWindow.AttemptToConnectToTerminal_Format, DateTime.Now.ToString(), ipAddress_entry.Text, port_spinbutton.Value);
						clientResponse=admin.SendCommandToClient(AdminsCommands.LetsConnect);
				
						networkLog_textview.Buffer.Text+=String.Format(MainWindow.ResultOfAttemptsToConnectToTerminal_Format, DateTime.Now.ToString(), clientResponse.Response, clientResponse.Comment);
					
						connectToTerminalOrDisconnect_label.LabelProp=MainWindow.DisconnectFromTerminalButtonText;
					}
					else
					{
						networkLog_textview.Buffer.Text+=String.Format(MainWindow.AttemptToDisconnectFromTerminal_Format, DateTime.Now.ToString());	
						admin.DisconnectFromClient();
					
						networkLog_textview.Buffer.Text+=String.Format(MainWindow.ResultOfAttemptsToDisconnectFromTerminal_Flrmat, DateTime.Now.ToString());
					
						connectToTerminalOrDisconnect_label.LabelProp=MainWindow.ConnectToTerminalButtonText;
					}
				}
				catch(Exception exception)
				{
					if(admin.Server.Connected==false)
					{
						networkLog_textview.Buffer.Text+=String.Format(MainWindow.ExceptionOfConnect_Format, DateTime.Now, ipAddress_entry.Text, port_spinbutton.Value, exception.Message);
					}
					else
					{
						networkLog_textview.Buffer.Text+=String.Format(MainWindow.ExceptionOfDisconnect_Format, DateTime.Now.ToString(), exception.Message);
					}
				}
			}
		#endregion
	
		#region Действия при изменении выбранного элемента в command_combobox	
			protected virtual void OnCommandComboboxChanged (object sender, System.EventArgs e)
			{
				if(command_combobox.Active==2)
				{
					argumentsForCommand_entry.Sensitive=true;
				}
				else
				{
					argumentsForCommand_entry.Sensitive=false;
				}
			}
		#endregion
			
		#region Действия при нажатии на кнопку "Отправить команду на терминал"
			protected virtual void OnSendCommandButtonClicked (object sender, System.EventArgs e)
			{	
				try
				{
					switch(command_combobox.ActiveText)
					{
						case("Выключить терминал"):
						{
							networkLog_textview.Buffer.Text+=String.Format(MainWindow.AttemptToSendCommandToTerminal_Format, DateTime.Now.ToString(), AdminsCommands.TurnOff, ipAddress_entry.Text, port_spinbutton.Value);
							
								ParsedClientsResponseAndComment response=admin.SendCommandToClient(AdminsCommands.TurnOff);
						
							networkLog_textview.Buffer.Text+=String.Format(MainWindow.ResultOfAttemptsToSendCommandToTerminal_Format, DateTime.Now.ToString(), AdminsCommands.TurnOff, ipAddress_entry.Text, port_spinbutton.Value, response.Response, response.Comment);
				
							break;
						}

						case("Перезагрузить терминал"):
						{
							networkLog_textview.Buffer.Text+=String.Format(MainWindow.AttemptToSendCommandToTerminal_Format, DateTime.Now.ToString(), AdminsCommands.Reboot, ipAddress_entry.Text, port_spinbutton.Value);
							
								ParsedClientsResponseAndComment response=admin.SendCommandToClient(AdminsCommands.Reboot);
						
							networkLog_textview.Buffer.Text+=String.Format(MainWindow.ResultOfAttemptsToSendCommandToTerminal_Format, DateTime.Now.ToString(), AdminsCommands.Reboot, ipAddress_entry.Text, port_spinbutton.Value, response.Response, response.Comment);
				
							break;
						}

						case("Обновить конфигурацию"):
						{
							networkLog_textview.Buffer.Text+=String.Format(MainWindow.AttemptToSendCommandToTerminal_Format, DateTime.Now.ToString(), AdminsCommands.UpdateConfiguration, ipAddress_entry.Text, port_spinbutton.Value);
							
								ParsedClientsResponseAndComment response=admin.SendCommandToClient(AdminsCommands.UpdateConfiguration);
						
							networkLog_textview.Buffer.Text+=String.Format(MainWindow.ResultOfAttemptsToSendCommandToTerminal_Format, DateTime.Now.ToString(), AdminsCommands.UpdateConfiguration, ipAddress_entry.Text, port_spinbutton.Value, response.Response, response.Comment);
				
							break;
						}
						default:
						{
							networkLog_textview.Buffer.Text+=String.Format(MainWindow.InvalidCommand_Format, DateTime.Now.ToString(), command_combobox.ActiveText);
							break;
						}
					}
				}
				catch(Exception exception)
				{
					networkLog_textview.Buffer.Text+=String.Format(MainWindow.ExceptionOfSendCommand_Format, DateTime.Now.ToString(), command_combobox.ActiveText, ipAddress_entry.Text, port_spinbutton.Value, exception.Message);
				}
			}
		#endregion
			

		
		
		
		
		
	}	
	
	
	
	
}

