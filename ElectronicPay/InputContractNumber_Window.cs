using System;
using System.Configuration;
using Gtk;
using System.Timers;
using System.IO;



namespace ElectronicPay
{
	public partial class InputContractNumber_Window : Gtk.Window
	{	
		#region НЕОБХОДИМЫЕ КОНСТАНТЫ
			protected const String ContractNumberForAccessAsManager="000000000";
			protected const String InvalidContractNumberMessage_Format="<span size='20000'>Клиент с номером лицевого счета: '{0}' не найден</span>";
		#endregion
		
		#region НЕОБХОДИМЫЕ ПЕРЕМЕННЫЕ
			protected System.Timers.Timer idlingTimer;
		#endregion
		
		#region Конструктор для класса InputContractNumber_Window
			public InputContractNumber_Window () : 
					base(Gtk.WindowType.Toplevel)
			{
				this.Build ();
				
				//изменяем шрифт в contractNumber_entry
					using (Pango.FontDescription desc = new Pango.FontDescription ()) 
					{		
						desc.AbsoluteSize = ConstantClass.FontSizeForContractNumberEntry;						
						contractNumber_entry.ModifyFont (desc);		
					}
				
				
				//подгружаем из файла настроек длинну лицевого счета клиента
					contractNumber_entry.MaxLength=Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.LengthOfContractNumber_ValueName]);
			
				//изменяем размер шрифта	 в label'ах кнопок									
					ServiceClass.ChangeFontSize(Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.DigitsFontSize_ValueName]), oneButton_label, twoButton_label, threeButton_label, fourButton_label, fiveButton_label, sixButton_label, sevenButton_label, eightButton_label, nineButton_label, zeroButton_label, backspaceButton_label);
				//изменяем размер шрифта в других label'ах
					ServiceClass.ChangeFontSize(Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.InputContractNumberFontSize_ValueName]), inputContractNumber_GtkLabel);
			
				this.idlingTimer=new System.Timers.Timer(Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.MaxIdlingTime_ValueName]));
					this.idlingTimer.Elapsed+=OnIdlingTimer_Elapsed;
					this.idlingTimer.Start();
			}
		#endregion
		
		#region Обработчик события Changed для personalNumber_entry
			protected void OnContractNumberEntry_Changed (object sender, EventArgs e)
			{
				this.idlingTimer.Stop();
				String tmp = ServiceClass.GetRegularString (contractNumber_entry.Text, ServiceClass.DigitPattern);
				if (contractNumber_entry.Text != tmp) 
				{
					contractNumber_entry.Text = tmp;
				}
				else 
				{
					if (contractNumber_entry.Text.Length == contractNumber_entry.MaxLength) 
					{
						nextStep_button.Sensitive = true;
					}
					else 
					{
						nextStep_button.Sensitive = false;
					}
				}
				
				this.idlingTimer.Start();
			}
		#endregion




		#region Действия при нажатии на кнопку "0"
			protected void OnZeroButtonClicked (object sender, EventArgs e)
			{				
				Int32 pos = contractNumber_entry.CursorPosition;

				contractNumber_entry.InsertText (ConstantClass.Zero, ref pos);

				contractNumber_entry.Position = pos;
			}
		#endregion

		#region Действия при нажатии на кнопку "1"
			protected void OnOneButtonClicked (object sender, EventArgs e)
			{
				Int32 pos = contractNumber_entry.CursorPosition;

				contractNumber_entry.InsertText (ConstantClass.One, ref pos);

				contractNumber_entry.Position = pos;
			}		
		#endregion

		#region Действия при нажатии на кнопку "2"
			protected void OnTwoButtonClicked (object sender, EventArgs e)
			{
				Int32 pos = contractNumber_entry.CursorPosition;

				contractNumber_entry.InsertText (ConstantClass.Two, ref pos);

				contractNumber_entry.Position = pos;
			}
		#endregion

		#region Действия при нажатии на кнопку "3"
			protected void OnThreeButtonClicked (object sender, EventArgs e)
			{
				Int32 pos = contractNumber_entry.CursorPosition;

				contractNumber_entry.InsertText (ConstantClass.Three, ref pos);

				contractNumber_entry.Position = pos;
			}
		#endregion

		#region Действия при нажатии на кнопку "4"
			protected void OnFourButtonClicked (object sender, EventArgs e)
			{
				Int32 pos = contractNumber_entry.CursorPosition;
	
				contractNumber_entry.InsertText (ConstantClass.Four, ref pos);

				contractNumber_entry.Position = pos;
			}
		#endregion

		#region Действия при нажатии на кнопку "5"
			protected void OnFiveButtonClicked (object sender, EventArgs e)
			{
				Int32 pos = contractNumber_entry.CursorPosition;

				contractNumber_entry.InsertText (ConstantClass.Five, ref pos);

				contractNumber_entry.Position = pos;
			}
		#endregion

		#region Действия при нажатии на кнопку "6"
			protected void OnSixButtonClicked (object sender, EventArgs e)
			{
				Int32 pos = contractNumber_entry.CursorPosition;

				contractNumber_entry.InsertText (ConstantClass.Six, ref pos);

				contractNumber_entry.Position = pos;
			}
		#endregion

		#region Действия при нажатии на кнопку "7"
			protected void OnSevenButtonClicked (object sender, EventArgs e)
			{
				Int32 pos = contractNumber_entry.CursorPosition;

				contractNumber_entry.InsertText (ConstantClass.Seven, ref pos);

				contractNumber_entry.Position = pos;
			}
		#endregion

		#region Действия при нажатии на кнопку "8"
			protected void OnEightButtonClicked (object sender, EventArgs e)
			{
				Int32 pos = contractNumber_entry.CursorPosition;

				contractNumber_entry.InsertText (ConstantClass.Eight, ref pos);

				contractNumber_entry.Position = pos;
			}		
		#endregion

		#region Действия при нажатии на кнопку "9"
			protected void OnNineButtonClicked (object sender, EventArgs e)
			{
				Int32 pos = contractNumber_entry.CursorPosition;

				contractNumber_entry.InsertText (ConstantClass.Nine, ref pos);

				contractNumber_entry.Position = pos;
			}
		#endregion

		#region Действия при нажатии на кнопку backspace_button
			protected void OnBackspaceButtonClicked (object sender, EventArgs e)
			{
				if (contractNumber_entry.CursorPosition > 0) 
				{					
					contractNumber_entry.DeleteText (contractNumber_entry.CursorPosition - 1, contractNumber_entry.CursorPosition);
				}
			}
		#endregion




		#region Действия при нажатии на кнопку "Назад"
			protected void OnBackStepButtonClicked (object sender, EventArgs e)
			{				
				this.Destroy();
			}
		#endregion

		#region Действия при нажатии на кнопку "Вперед"
			protected void OnNextStepButtonClicked (object sender, EventArgs e)
			{
				try
				{
					if(contractNumber_entry.Text!=InputContractNumber_Window.ContractNumberForAccessAsManager)
					{
						String txn_id=OSMPClass.GenerateTxnId(20, Convert.ToUInt16(ConfigurationManager.AppSettings[ConstantClass.NumberOfTerminal_ValueName]));
						ServerResponse checkResult;
				
						//обращаемся к серверу, чтобы проверить валидность номера лицевого счета
							checkResult=OSMPClass.CheckAccount(ConfigurationManager.AppSettings[ConstantClass.PaymentServerAddress_ValueName],
						                                   		Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.PaymentServerPort_ValueName]),
				    		                               		ConfigurationManager.AppSettings[ConstantClass.LoginForPaymentServer_ValueName],
					    		                             	ConfigurationManager.AppSettings[ConstantClass.PasswordForPaymentServer_ValueName],
			    	        		                       		ConfigurationManager.AppSettings[ConstantClass.ApplicationOfPaymentServer_ValueName],
				    	        		                    	ConfigurationManager.AppSettings[ConstantClass.CheckCommand_ValueName],
			            	        		               		txn_id, contractNumber_entry.Text, null);
						//если проверка прошла, открываем окно для внесения денег, а текущее закрываем
							if(checkResult.IsSuccessfullyExecuted==true)
							{
								PutMoneyIntoTerminal_Window putMoneyIntoTerminal_Window=new PutMoneyIntoTerminal_Window(contractNumber_entry.Text, txn_id, checkResult.Comment);
								putMoneyIntoTerminal_Window.Fullscreen();
										
								this.Destroy ();
							}
							else
							{
								//сообщить пользователю, что есть проблемы
								resultOfCheckCommand_label.LabelProp=String.Format(InputContractNumber_Window.InvalidContractNumberMessage_Format, contractNumber_entry.Text);
							}
					}
					else
					{
						AccessToProgram_Window accessToProgram = new AccessToProgram_Window(ConstantClass.ManagerTypeOfAccess);
							accessToProgram.Show();
					}
				}
				catch(Exception exception)
				{
					MessageToUser_Dialog message=new MessageToUser_Dialog("Не удалось связаться с биллингом. Попробуйте через 5 минут", 2);
						message.Fullscreen();
						message.Run();
					using(StreamWriter errorWriter=new StreamWriter(ConstantClass.PathToErrorFile, true))
					{
						errorWriter.WriteLine(String.Format(ConstantClass.ErrorOutputFormat, DateTime.Now.ToString(), exception.Message));
					}
					this.Destroy();
				}
			}
		#endregion					
		
		#region Метод, который срабатывает при истечении времени в таймере idlingTimer
		    protected void OnIdlingTimer_Elapsed(object source, ElapsedEventArgs e)
		    {
        		(source as System.Timers.Timer).Enabled=false;
				this.Destroy();
    		}
		#endregion








	}
}

