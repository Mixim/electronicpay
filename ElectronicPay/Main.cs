using System;
using Gtk;
using System.Configuration;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace ElectronicPay
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();

			//====для отладки=====
				/*String data=ConfigurationManager.AppSettings[ConstantClass.BaudRateCashRegister_ValueName];
				String data2=ConfigurationManager.AppSettings[ConstantClass.LoginForPaymentServer_ValueName];*/
			//====================
			
			//инициализируем словарь с кодами ошибок фискального аппарата
				//CashRegisterErrorLog.LoadErrorDictionaryFromFile(ConstantClass.PathToCashRegisterErrorDictionary, ConstantClass.TypeOfNumberSystemForCashRegisterErrorDictionary);
			
			//НЕОБХОДИМО ВЫНЕСТИ В КОНФИГОВСКИЙ ФАЙЛ
			string AdminAccess = "AdminAccess";
			//======================================

			if (args.Length != 0) 
			{
				if (args [0] == AdminAccess) 
				{
					AccessToProgram_Window accessToProgram_Window = new AccessToProgram_Window (ConstantClass.AdminTypeOfAccess);
					accessToProgram_Window.Show ();
					Application.Run ();
				}
				else
				{
					MessageDialog errorDialog = new MessageDialog (
						null,
						DialogFlags.NoSeparator,
						MessageType.Error,
						ButtonsType.Ok,
						"Предпринята попытка запустить программу с неизвестными значениями аргумента"
					);
					errorDialog.Run ();
					errorDialog.Destroy ();

				}
			} 
			else 
			{			
				MainWindow win=new MainWindow();
					win.Fullscreen();
					
					
	
				//инициализируем поток, который необходим для прослушивания сети, получения и обработки удаленных команд от администратора
					IPAddress acceptedAddress=IPAddress.Any;
					Int32 port=5300;
					
					ClientClass client=new ClientClass(acceptedAddress, port, MainClass.ClientsServerResponse, MainClass.ProblemsProcessor);
				
					Thread networkThread=new Thread(client.ListeningOfNetwork);					
						networkThread.Start(ConstantClass.TimeoutOfWaitResponse);
						networkThread.IsBackground=true;
				//========================================================================================================================

				
				Application.Run ();
				
			}
			
			
			
		}
		
		
		
		#region Метод для обработки команд, полученных со стороны сервера
			protected static ClientsResponses ClientsServerResponse(ClientClass Sender, TcpClient Admin, ParsedAdminsCommandAndArgs CommandAndArgs)
			{
				ClientsResponses returnedValue;
			
				//вставляем обработчик исключительных ситуаций
				try
				{
					//выполняем ту команду, которая была передана на клиент
					switch(CommandAndArgs.Command)
					{
						case(AdminsCommands.LetsConnect):
						{
							returnedValue=ClientsResponses.Ack;
							
							break;
						}
					
						case AdminsCommands.Reboot:
						{							
							System.Diagnostics.Process.Start(ConstantClass.RebootProgramFileName, ConstantClass.RebootProgramArguments);
							
							returnedValue=ClientsResponses.CommandExecuted;
							break;
						}
												
						case AdminsCommands.TurnOff:
						{
							System.Diagnostics.Process.Start(ConstantClass.TurnOffProgramFileName, ConstantClass.TurnOffProgramArguments);
						
					
							returnedValue=ClientsResponses.CommandExecuted;
							break;
						}
					
						//команда 'UpdateConfiguration' предназначена для удаленного изменения конфигурации 
						//оборудования (загрузка нового файла с информацией о биллинговой системе, оборудовании и т.д.), 
						//сигнал об ее успешном выполнении может быть отправлен уже после фактического исполнения
						case AdminsCommands.UpdateConfiguration:
						{							
							String filename=Sender.ReceiveFileFromNetwork(Admin, "");
									Configuration currentConfig=ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
									
								Configuration newConfiguration=Sender.ConvertFileToConfiguration(filename);
					
								Sender.UpdateConfiguration(currentConfig, newConfiguration);
					
							returnedValue=ClientsResponses.CommandExecuted;
							break;
						}
					
						//если полученная команда не покрыта блоком кода switch...case, 
						//то команда является не корректной (клиент ее не знает) и мы должны вернуть соответствующее сообщение
						default:
						{
							returnedValue=ClientsResponses.InvalidCommand;
							break;
						}
					}
				}
				//если поймали некоторое исключение, то команда однозначно не выполнилась и нам нужно передать 
				//на администраторскую машину сообщение о том, что произошел крах при выполнении команды - ее 
				//выполнить невозможно и коментарии о возникшем исключении
				catch(Exception)
				{
					
				
					returnedValue=ClientsResponses.CommandsExecutionsIsFailed;
				}
			
			
			
				return returnedValue;
			}
		#endregion
		
		
		#region Метод, необходимый для представления обработчика ошибок
			protected static void ProblemsProcessor(String ProblemDescription)
			{
				using(StreamWriter writer = new StreamWriter(ConstantClass.PathToErrorFile, true))
				{
					writer.WriteLine(ConstantClass.ThereIsSomeProblemFormat, ProblemDescription);
				}
			}
		#endregion
	
		
	}
}
