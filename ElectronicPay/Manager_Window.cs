using System;

using System.Configuration;
using Gtk;

namespace ElectronicPay
{
	public partial class Manager_Window : Gtk.Window
	{
		#region НЕОБХОДИМЫЕ ПЕРЕМЕННЫЕ
			protected CashRegisterClass cashRegister;		
		#endregion
		
		#region Конструктор класса Manager_Window[ПУСТОЙ]
			public Manager_Window () : 
					base(Gtk.WindowType.Toplevel)
			{
				this.Build ();

				//заполняем имеющиеся в окне текстовые поля

					//на вкладке "Проведение платежей"
						paymentServerAddress_entry.Text = ConfigurationManager.AppSettings[ConstantClass.PaymentServerAddress_ValueName];
						paymentServerPort_entry.Text=ConfigurationManager.AppSettings[ConstantClass.PaymentServerPort_ValueName];
						applicationOfPaymentServer_entry.Text=ConfigurationManager.AppSettings[ConstantClass.ApplicationOfPaymentServer_ValueName];
						payCommand_entry.Text=ConfigurationManager.AppSettings[ConstantClass.PayCommand_ValueName];
						loginForPaymentServer_entry.Text = ConfigurationManager.AppSettings[ConstantClass.LoginForPaymentServer_ValueName];
						passwordForPaymentServer_entry.Text = ConfigurationManager.AppSettings[ConstantClass.PasswordForPaymentServer_ValueName];
						lengthOfTxnId_entry.Text=ConfigurationManager.AppSettings[ConstantClass.LengthOfTxnId_ValueName];
					//===============================

					//на вкладке "Дополнительные настройки"
						loginForAccessToProgram_entry.Text = ConfigurationManager.AppSettings[ConstantClass.LoginForAccessToProgram_ValueName];
						passwordForAccessToProgram_entry.Text = ConfigurationManager.AppSettings[ConstantClass.PasswordForAccessToProgram_ValueName];
						numberOfTerminal_entry.Text=ConfigurationManager.AppSettings[ConstantClass.NumberOfTerminal_ValueName];
						lengthOfContractNumber_entry.Text=ConfigurationManager.AppSettings[ConstantClass.LengthOfContractNumber_ValueName];
						portNameBillAcceptor_entry.Text=ConfigurationManager.AppSettings[ConstantClass.PortNameBillAcceptor_ValueName];
						baudRateBillAcceptor_entry.Text=ConfigurationManager.AppSettings[ConstantClass.BaudRateBillAcceptor_ValueName];
						portNameCashRegister_entry.Text=ConfigurationManager.AppSettings[ConstantClass.PortNameCashRegister_ValueName];
						baudRateCashRegister_entry.Text=ConfigurationManager.AppSettings[ConstantClass.BaudRateCashRegister_ValueName];
						digitsFontSize_entry.Text=ConfigurationManager.AppSettings[ConstantClass.DigitsFontSize_ValueName];
						inputContractNumberFontSize_entry.Text=ConfigurationManager.AppSettings[ConstantClass.InputContractNumberFontSize_ValueName];
					//===============================

				//==========================================
			
				//инициализируем фискальный регистратора
					this.InitOfCashRegister(ref this.cashRegister);
			}
		#endregion

		#region Действия при удалении окна
			protected void OnDeleteEvent (object sender, DeleteEventArgs a)
			{			
				Application.Quit ();
				a.RetVal = true;
			}
		#endregion
		
		#region Действия при нажатии на кнопку "Сохранить настройки проведения платежей"
			protected void OnSaveMakingPaymentsSettingsButtonClicked (object sender, EventArgs e)
			{	
				MessageDialog acceptOfSavingSettings = new MessageDialog(this, DialogFlags.Modal, MessageType.Question, ButtonsType.YesNo, 
				    	                                                            "Уверены в сохранении настроек проведения платежей?"); 
					switch ((Gtk.ResponseType)acceptOfSavingSettings.Run ()) 
					{
						case (Gtk.ResponseType.Yes):
						{
							Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
							
							//изменяем адрес сервера
								config.AppSettings.Settings.Remove(ConstantClass.PaymentServerAddress_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.PaymentServerAddress_ValueName, paymentServerAddress_entry.Text);
							//изменяем порт сервера
								config.AppSettings.Settings.Remove(ConstantClass.PaymentServerPort_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.PaymentServerPort_ValueName, paymentServerPort_entry.Text);
							//изменяем приложение для проведения платежей на сервере
								config.AppSettings.Settings.Remove(ConstantClass.ApplicationOfPaymentServer_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.ApplicationOfPaymentServer_ValueName, applicationOfPaymentServer_entry.Text);
							//изменяем команду для проведения платежей на сервер
								config.AppSettings.Settings.Remove(ConstantClass.PayCommand_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.PayCommand_ValueName, payCommand_entry.Text);
							//изменяем логин для доступа к серверу
								config.AppSettings.Settings.Remove(ConstantClass.LoginForPaymentServer_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.LoginForPaymentServer_ValueName, loginForPaymentServer_entry.Text);
							//изменяем пароль для доступа к серверу
								config.AppSettings.Settings.Remove(ConstantClass.PasswordForPaymentServer_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.PasswordForPaymentServer_ValueName, passwordForPaymentServer_entry.Text);
							//изменяем длинну идентификатора платежа в системе ОСМП
								config.AppSettings.Settings.Remove(ConstantClass.LengthOfTxnId_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.LengthOfTxnId_ValueName, lengthOfTxnId_entry.Text);
							
				
							config.Save(ConfigurationSaveMode.Full);
							ConfigurationManager.RefreshSection("appSettings");	
							break;
						}
						
						default:
						{
							break;
						}
					}
				acceptOfSavingSettings.Destroy();
			}
		#endregion

		#region Действия при нажатии на кнопку "Сохранить дополнительные настройки"
			protected virtual void OnSaveAdditionalSettingsButtonClicked (object sender, System.EventArgs e)
			{
				MessageDialog acceptOfSavingSettings = new MessageDialog(this, DialogFlags.Modal, MessageType.Question, ButtonsType.YesNo, 
				    	                                                            "Уверены в сохранении настроек доступа к программе?");
					switch ((Gtk.ResponseType)acceptOfSavingSettings.Run ()) 
					{
						case (Gtk.ResponseType.Yes):
						{
							Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
							//изменяем логин для доступа к программе
								config.AppSettings.Settings.Remove(ConstantClass.LoginForAccessToProgram_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.LoginForAccessToProgram_ValueName, loginForAccessToProgram_entry.Text);				
							//изменяем пароль для доступа к программе
								config.AppSettings.Settings.Remove(ConstantClass.PasswordForAccessToProgram_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.PasswordForAccessToProgram_ValueName, passwordForAccessToProgram_entry.Text);
							//изменяем номер терминала
								config.AppSettings.Settings.Remove(ConstantClass.NumberOfTerminal_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.NumberOfTerminal_ValueName, numberOfTerminal_entry.Text);
							//изменяем длинну лицевого счета
								config.AppSettings.Settings.Remove(ConstantClass.LengthOfContractNumber_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.LengthOfContractNumber_ValueName, lengthOfContractNumber_entry.Text);
							//изменяем порт купюроприемника
								config.AppSettings.Settings.Remove(ConstantClass.PortNameBillAcceptor_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.PortNameBillAcceptor_ValueName, portNameBillAcceptor_entry.Text);
							//изменяем скорость передачи данных для купюроприемника
								config.AppSettings.Settings.Remove(ConstantClass.BaudRateBillAcceptor_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.BaudRateBillAcceptor_ValueName, baudRateBillAcceptor_entry.Text);
							//изменяем порт фискального регистратора
								config.AppSettings.Settings.Remove(ConstantClass.PortNameCashRegister_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.PortNameCashRegister_ValueName, portNameCashRegister_entry.Text);
							//изменяем скорость передачи данных фискального регистратора
								config.AppSettings.Settings.Remove(ConstantClass.BaudRateCashRegister_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.BaudRateCashRegister_ValueName, baudRateCashRegister_entry.Text);
							//изменяем размер шрифта в числовых кнопках
								config.AppSettings.Settings.Remove(ConstantClass.DigitsFontSize_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.DigitsFontSize_ValueName, digitsFontSize_entry.Text);
							//изменяем размер шрифта на форме для ввода лицевого счета
								config.AppSettings.Settings.Remove(ConstantClass.InputContractNumberFontSize_ValueName);
									config.AppSettings.Settings.Add(ConstantClass.InputContractNumberFontSize_ValueName, inputContractNumberFontSize_entry.Text);
							
					
							config.Save(ConfigurationSaveMode.Full);
							ConfigurationManager.RefreshSection("appSettings");	
							
							
							
					
							break;
						}
						
						default:
						{
							
							break;
						}
					}
				
				acceptOfSavingSettings.Destroy();
			}
		#endregion
		
		#region Метод, выполняющий вывод необходимого отчета
			protected virtual void OnMakeReportButtonClicked (object sender, System.EventArgs e)
			{
				try
				{
					switch(typeOfReport_combobox.Active)
					{
						//x-отчет
						case 0:
						{
							this.cashRegister.CashRegister_SerialPort.Open();
								this.cashRegister.MakeXReport();
							this.cashRegister.CashRegister_SerialPort.Close();
				
							break;
						}
					
						//z-отчет
						case 1:
						{
							this.cashRegister.CashRegister_SerialPort.Open();
								this.cashRegister.MakeZReport();
							this.cashRegister.CashRegister_SerialPort.Close();
				
							break;
						}
					}
				}
				catch(Exception exception)
				{
					MessageDialog messageDialog=new MessageDialog(this, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, exception.Message);
						messageDialog.Run();
				}
			}
		#endregion
		
		
			

		#region Метод для инициализации фискального регистратора
			protected void InitOfCashRegister(ref CashRegisterClass CurrentCashRegister)
			{
				CurrentCashRegister=new CashRegisterClass(ConfigurationManager.AppSettings[ConstantClass.PortNameCashRegister_ValueName],
	                                                    	Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.BaudRateCashRegister_ValueName]));
			}
		#endregion
		
		
		
		


	}
}

