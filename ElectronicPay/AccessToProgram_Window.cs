using System;
using System.Configuration;
using Gtk;

namespace ElectronicPay
{
	public partial class AccessToProgram_Window : Gtk.Window
	{
		#region НЕОБХОДИМЫЕ ПЕРЕМЕННЫЕ
			protected readonly String typeOfAccess;
		#endregion
		
		#region Констурктор класса AccessToProgram_Window
			public AccessToProgram_Window () : 
					base(Gtk.WindowType.Toplevel)
			{
				this.Build ();
			}
		#endregion
		
		#region Конструктор класса AccessToProgram_Window [с параметрами]
			public AccessToProgram_Window(String TypeOfAccess) :
					base(Gtk.WindowType.Toplevel)
			{
				this.Build();
			
				this.typeOfAccess=TypeOfAccess;
			}
		#endregion
		
		#region Действия при нажатии на кнопку "Получить доступ"
			protected void OnGetAccessButtonClicked (object sender, EventArgs e)
			{
				switch(this.typeOfAccess)
				{
					case(ConstantClass.AdminTypeOfAccess):
					{
						if (
								login_entry.Text == ConfigurationManager.AppSettings[ConstantClass.LoginForAccessToProgram_ValueName]
					    															&&
					    		password_entry.Text == ConfigurationManager.AppSettings[ConstantClass.PasswordForAccessToProgram_ValueName]
			    			) 
						{
							Manager_Window manager_Window = new Manager_Window ();
							manager_Window.Show ();
							this.Destroy();
						}
						else 
						{
							using(Gtk.MessageDialog errorMessage= new Gtk.MessageDialog(this, Gtk.DialogFlags.Modal, Gtk.MessageType.Error, Gtk.ButtonsType.Ok, ConstantClass.InvalidLoginOrPassword))
							{
								errorMessage.Run();
							}
						}
						
						break;
					}
				
					case(ConstantClass.ManagerTypeOfAccess):
					{
						if (
								login_entry.Text == ConfigurationManager.AppSettings[ConstantClass.ManagerLogin_ValueName]
					    															&&
					    		password_entry.Text == ConfigurationManager.AppSettings[ConstantClass.ManagerPassword_ValueName]
			    			) 
						{
							CreateReport_Window createReport_Window = new CreateReport_Window ();
							createReport_Window.Show ();
							this.Destroy();
						}
						else 
						{
							using(Gtk.MessageDialog errorMessage= new Gtk.MessageDialog(this, Gtk.DialogFlags.Modal, Gtk.MessageType.Error, Gtk.ButtonsType.Ok, ConstantClass.InvalidLoginOrPassword))
							{
								errorMessage.Run();
							}
						}
				
						break;
					}
					
				}

			}
		#endregion

	}
}

