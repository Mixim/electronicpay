using System;
using System.IO;
using System.Security.Cryptography;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;


/*
	Формат отсылаемых и получаемых пакетов:
		<LNG(8)><CMD(1)><Data(N)><Hash(64)>
 
 */






namespace ElectronicPay
{		
	#region Абстрактный класс, необходимый для реализации возможности удаленного администрирования
		public abstract class RemoteAdministrationBase
		{				
			#region НЕОБХОДИМЫЕ КОНСТАНТЫ И СТАТИЧЕСКИЕ ПОЛЯ КЛАССА RemoteAdministrationBase
				//количество байт в пакете, которые будут использоваться для указания его длинны
					protected const Int32 CountOfBytesForPartOfLength=4;
				//количество байт в пакете, которые будут использоваться для указания значения хеша (64 байта - это длинна хеша, который получается при использовании методов класса HMACSHA512)
					protected const Int32 CountOfHashBytes=64;
				//статическое поле для сериализации/десериализации объектов
					protected static readonly BinaryFormatter Formatter=new BinaryFormatter();
				//ключ для генерации хеш-кода
					protected static Byte[] KeyForHashCalculation=new Byte[5]{1, 2, 3, 4, 5};
				
				//максимальное количество попыток запроса при получении некорректного ответа либо при его отсутствии заданное время
					protected const UInt16 MaximumNumberOfRequestsAttempts=5;
		
				//коментарий, отправляемый по сети на другую сторону соединения, когда хеш-код не сошелся
					protected const String ReceivedInvalidHashCode_Comment="Хеш-код полученного сообщения отличается от ожидаемого, возможно сообщения было повреждено в процессе транспортировки";
		
				//константа, которая используется при создании буфера для приема\передачи файла
					protected const Int32 BufferSizeForSendAndReceive=2048;
			#endregion
		
			#region Метод, который будет использоваться для подсчета хеша
				public Byte[] CalculationOfHash(Byte[] Data)
				{
					Byte[] returnedValue;
			
			
					using(HMACSHA512 sha512=new HMACSHA512(RemoteAdministrationBase.KeyForHashCalculation))
					{
						returnedValue=sha512.ComputeHash(Data);
					}
							
			
					return returnedValue;
				}
			#endregion
		
			#region Метод, используемый для сравнения массивов
				protected Boolean ArrayComparison<T>(T[] Array1, T[] Array2) where T:IComparable
				{
					Boolean returnedValue=true;
			
					//если передан один и тот же массив (одни и те же ссылки)
					if(Array1==Array2)
					{
						returnedValue=true;
					}
					else
					{
						//если один массив инициализирован, а другой нет
						if((Array1==null && Array2!=null) || (Array1!=null && Array2==null))
						{
							returnedValue=false;
						}
						else
						{
							//если длинна массивов различна
							if(Array1.LongLength!=Array2.LongLength)
							{
								returnedValue=false;
							}
							else
							{
								for(Int64 i=0;i!=Array1.LongLength;i++)
								{
									if(Comparer<T>.Equals(Array1[i], Array2[i])==false)
									{
										returnedValue=false;
										break;
									}
								}
							}
						}
					}
			
					return returnedValue;
				}
			#endregion
		
			#region Метод, необходимый для генерации отсылаемого пакета
				public Byte[] GenerateSendingPackage(Byte Command, Object Data)
				{
					Byte[] returnedValue;
			
					//поток, в который будет записываться весь отправляемый пакет
					using(MemoryStream totalMemoryStream=new MemoryStream())
					{
						//поток, в который будет записываться ТОЛЬКО сериализованный Command и объект Data
						using(MemoryStream significantMemoryStream=new MemoryStream())
						{												
							//записываем Command в поток со значимыми байтами, выполнив сериализацию
								RemoteAdministrationBase.Formatter.Serialize(significantMemoryStream, Command);
									
							//сериализуем Data с помощью объекта для сериализации, заданного в статическом поле RemoteAdministrationBase.Formatter
								if(Data!=null)
								{
									RemoteAdministrationBase.Formatter.Serialize(significantMemoryStream, Data);
								}

					
							//записываем длинну пакета в основной поток
								totalMemoryStream.Write(BitConverter.GetBytes(this.CalculatePackageLength(significantMemoryStream)),0,RemoteAdministrationBase.CountOfBytesForPartOfLength);
														
					
							//теперь записываем в totalMemoryStream сериализованные объекты Command и Data, которые хранятся в significantMemoryStream
								totalMemoryStream.Write(significantMemoryStream.ToArray(), 0, Convert.ToInt32(significantMemoryStream.Length));
					
							//вычисляем хеш пересылаемого пакета и записываем его в конец totalMemoryStream
								totalMemoryStream.Write(this.CalculationOfHash(significantMemoryStream.ToArray()), 0, RemoteAdministrationBase.CountOfHashBytes);
						}
				
						//конвертируем totalMemoryStream в массив
							returnedValue=totalMemoryStream.ToArray();
					}
			
					return returnedValue;
				}
			#endregion
		
			#region Метод, необходимый для разбора полученного пакета
				/*
				 	Пока что разбор никакой не предполагается, поэтому идет элементарное присвоение
				 */
				protected T ParseReceivedPackage<T>(Byte[] ReceivedPackage) where T : class
				{
					T returnedValue;

					using(MemoryStream memoryStreamConverter=new MemoryStream(ReceivedPackage))
					{
						memoryStreamConverter.Position=0;
						
						if(typeof(T) == typeof(ParsedAdminsCommandAndArgs))
						{
							AdminsCommands command=(AdminsCommands)Formatter.Deserialize(memoryStreamConverter);
						
				
							Object args=null;
							if(memoryStreamConverter.Position!=memoryStreamConverter.Length)
							{
								args=Formatter.Deserialize(memoryStreamConverter);
							}
							returnedValue=( ( new ParsedAdminsCommandAndArgs(command, args) as Object) as T);
						}
						else
						{
							if(typeof(T)==typeof(ParsedClientsResponseAndComment))
					   		{
								ClientsResponses response=(ClientsResponses)Formatter.Deserialize(memoryStreamConverter);
						
								String comment=Formatter.Deserialize(memoryStreamConverter).ToString();
								returnedValue=( (new ParsedClientsResponseAndComment(response, comment) as Object) as T);
							}
							else
							{
								throw new ArgumentException(String.Format("Некорректный тип '{0}' передан в качестве аргумента", typeof(T)));
							}
						}
					
						
						
					}
			
			
					return returnedValue;
				}
			#endregion
		
			#region Метод, необходимый для проверки хеша
				protected Boolean HashChecker(Byte[] ReceivedData, Byte[] ReceivedHashCode)
				{
					Boolean returnedValue;
					Byte[] calculatedHash;
								
					calculatedHash=this.CalculationOfHash(ReceivedData);
					
					returnedValue=this.ArrayComparison(ReceivedHashCode, calculatedHash);
			
					return returnedValue;
				}
			#endregion
		
			#region Метод, необходимый для подсчета длинны отправляемого пакета
				protected Int64 CalculatePackageLength(MemoryStream MemoryStreamOfData)
				{
					Int64 returnedValue;
			
					returnedValue=MemoryStreamOfData.Length;
			
					return returnedValue;
				}
			#endregion
		}
	#endregion
}

