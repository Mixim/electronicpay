using System;
using System.IO.Ports;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.ComponentModel;

using System.IO;



namespace ElectronicPay
{
 	public enum BillValidatorCommands : byte {RESET=0x30, GET_STATUS=0x31, SET_SECURITY=0x32, POLL=0x33, ENABLE_BILL_TYPES=0x34,
												STACK=0x35, RETURN=0x36, IDENTIFICATION=0x37, HOLD=0x38,
												SET_BARCODE_PARAMETERS=0x39, EXTRACT_BARCODE_DATA=0x3A,
												RECYCLING_CASSETTE_STATUS=0x3B, DISPENSE=0x3C, UNLOAD=0x3D,
												EXTENDED_IDENTIFICATION=0x3E, SET_RECYCLING_CASSETTE_TYPE=0x40,
												GET_BILL_TABLE=0x41, DOWNLOAD=0x50, GET_CRC32_OF_THE_CODE=0x51,
												MODULE_DOWNLOAD=0x52, MODULE_IDENTIFICATION_REQUEST=0x53,
												REQUEST_STATISTICS=0x60, REALTIME_CLOCK=0x62, POWER_RECOVERY=0x66,
												EMPTY_DISPENSER=0x67, SET_OPTIONS=0x68, GET_OPTIONS=0x69,
												EXTENDED_CASSETTE_STATUS=0x70, ACK=0x00, NAK=0xFF}

	
	public enum BillToBillReceivedCodes : byte {PowerUp=0x10, PowerUpWithBillInValidator=0x11, PowerUpWithBillInStacker=0x12,												
												Initialize=0x13, Idling=0x14, Accepting=0x15, Stacking=0x17, Returning=0x18,
												UnitDisabled=0x19, Holding=0x1A, DeviceBusy=0x1B, GenericRejectingCode=0x1C,
												InvalidCommand=0x30, DropCassetteFull=0x41, DropCassetteOutOfPosition=0x42, 
												BillValidatorJammed=0x43, CassetteJammed=0x44, Cheated=0x45, Pause=0x46, 
												GenericFailureCode=0x47,
												EscrowPosition=0x80, BillStacked=0x81, BillReturned=0x82}

	public enum GenericFailureCodes : byte {StackMotorFailure = 0x50, TransportMotorSpeedFailure=0x51, TransportMotorFailure=0x52,
											AligningMotorFailure=0x53, InitialBoxStatusFailure=0x54, OpticCanalFailure=0x55,
											MagneticCanalFailure=0x56, CapacitanceCanalFailure=0x5F}

	public enum CurrencyCodes : byte {TenRubles=0x02, FiftyRubles=0x03, OneHundredRubles=0x04, FiveHundredRubles=0x05, OneThousandRubles=0x06, FiveThousandRubles=0x07}

    public enum BillRecievedStatus {Accepted, Rejected };

    public enum BillCassetteStatus { Inplace, Removed };

	public enum ResponseType { ACK, NAK };

    // Делегат события получения банкноты
    public delegate void BillReceivedHandler(object Sender, BillReceivedEventArgs e);

    // Делегат события для контроля за кассетой
    public delegate void BillCassetteHandler(object Sender, BillCassetteEventArgs e);

    // Делегат события в процессе отправки купюры в стек
    public delegate void BillStackingHandler(object Sender, BillStackedEventArgs e);






	#region Класс для представления ошибок купюроприемника
	    public class CashCodeErroList
    	{
			#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА CashCodeErroList
				protected Dictionary<Int32, String> errors= new Dictionary<Int32, String>();
			#endregion
			
			#region Метод для доступа к полю Errors как к свойству
				public Dictionary<Int32, String> Errors 
				{
					get 
					{
						return this.errors;
					}
					protected set 
					{
						this.errors=value;
					}
				}
			#endregion

			#region Конструктор класса CashCodeErroList
	    	    public CashCodeErroList()
    	    	{
	        	    Errors.Add(100000, "Неизвестная ошибка");

    	        	Errors.Add(100010, "Ошибка открытия Com-порта");
	        	    Errors.Add(100020, "Com-порт не открыт");
    	        	Errors.Add(100030, "Ошибка отпраки команды включения купюроприемника.");
	    	        Errors.Add(100040, "Ошибка отпраки команды включения купюроприемника. От купюроприемника не получена команда POWER UP.");
    	    	    Errors.Add(100050, "Ошибка отпраки команды включения купюроприемника. От купюроприемника не получена команда ACK.");
        	    	Errors.Add(100060, "Ошибка отпраки команды включения купюроприемника. От купюроприемника не получена команда INITIALIZE.");
	            	Errors.Add(100070, "Ошибка проверки статуса купюроприемника. Cтекер снят.");
		            Errors.Add(100080, "Ошибка проверки статуса купюроприемника. Стекер переполнен.");
    		        Errors.Add(100090, "Ошибка проверки статуса купюроприемника. В валидаторе застряла купюра.");
        		    Errors.Add(100100, "Ошибка проверки статуса купюроприемника. В стекере застряла купюра.");
            		Errors.Add(100110, "Ошибка проверки статуса купюроприемника. Фальшивая купюра.");
		            Errors.Add(100120, "Ошибка проверки статуса купюроприемника. Предыдущая купюра еще не попала в стек и находится в механизме распознавания.");

    		        Errors.Add(100130, "Ошибка работы купюроприемника. Сбой при работе механизма стекера.");
        		    Errors.Add(100140, "Ошибка работы купюроприемника. Сбой в скорости передачи купюры в стекер.");
            		Errors.Add(100150, "Ошибка работы купюроприемника. Сбой передачи купюры в стекер.");
	            	Errors.Add(100160, "Ошибка работы купюроприемника. Сбой механизма выравнивания купюр.");
	    	        Errors.Add(100170, "Ошибка работы купюроприемника. Сбой в работе стекера.");
    	    	    Errors.Add(100180, "Ошибка работы купюроприемника. Сбой в работе оптических сенсоров.");
        	    	Errors.Add(100190, "Ошибка работы купюроприемника. Сбой работы канала индуктивности.");
	        	    Errors.Add(100200, "Ошибка работы купюроприемника. Сбой в работе канала проверки заполняемости стекера.");

    	        	// Ошибки распознования купюры (Стр.38)
	        	    Errors.Add(0x60, "Rejecting due to Insertion");
    	        	Errors.Add(0x61, "Rejecting due to Magnetic");
	    	        Errors.Add(0x62, "Rejecting due to Remained bill in head");
    	    	    Errors.Add(0x63, "Rejecting due to Multiplying");
        	    	Errors.Add(0x64, "Rejecting due to Conveying");
	            	Errors.Add(0x65, "Rejecting due to Identification1");
		            Errors.Add(0x66, "Rejecting due to Verification");
    		        Errors.Add(0x67, "Rejecting due to Optic");
        		    Errors.Add(0x68, "Rejecting due to Inhibit");
            		Errors.Add(0x69, "Rejecting due to Capacity");
		            Errors.Add(0x6A, "Rejecting due to Operation");
    		        Errors.Add(0x6C, "Rejecting due to Length");
					Errors.Add(0x92, "Rejecting due to unrecognised barcode");
					Errors.Add(0x6D, "Rejecting due to UV");
					Errors.Add(0x93, "Rejecting due to incorrect number of characters in barcode");
					Errors.Add(0x94, "Rejecting due to unknown barcode start sequence");
					Errors.Add(0x95, "Rejecting due to unknown barcode stop sequence");
        		}
			#endregion
    	}
	#endregion






	#region Класс для представления отсылаемого пакета
	    public class Package
    	{
			#region Необходимые константы
		        protected const Int32 POLYNOMIAL =  0x08408;     // Необходима для расчета CRC
    		    protected const Byte _Sync =      0x02;        // Бит синхронизации (фиксированный)
        		protected const Byte _Adr =       0x03;        // Переферийный адрес оборования. Для купюропиемника из документации равен 0x03
				protected const Int32 StandardPackageLength=250;
		
		
				protected const String CheckCRCValueInfo="OldCRC[0]=0x{0:X2}h;\nOldCRC[1]=0x{1:X2}h;\nNewCRC[0]=0x{2:X2}h;\nNewCRC[1]=0x{3:X2}h;";
				protected const String InvalidArgumentOfGenerateBitmapMethod_Format="Число элементов в параметре 'BitmapBitValues' метода 'GenerateBitmap' равно {0} и не может быть помещено в {1} байт, величина которого передана в качестве аргумента 'BitmapLength'";
			#endregion
		
    	    #region Поля        
		        protected Byte cmd;
        		protected Byte[] data;
        	#endregion

	        #region Конструкторы класса
	    	    public Package()
    		    {
					LogFileWriter.OutputStackTrace();
				}

        		public Package(Byte Cmd, Byte[] Data)
		        {
					LogFileWriter.OutputStackTrace();
		            this.Cmd = Cmd;
        		    this.Data = Data;
		        }
        	#endregion

	        #region Свойства
	    	    public Byte Cmd
    		    {
            		get 
					{
						LogFileWriter.OutputStackTrace();
						return this.cmd; 
					}
        		    set 
					{ 
						LogFileWriter.OutputStackTrace();
						this.cmd = value; 
					}
        		}

		        public Byte[] Data
        		{
		            get 
					{
						LogFileWriter.OutputStackTrace();
						return this.data; 
					}
		            set 
        		    {
						LogFileWriter.OutputStackTrace();
		                if (value.Length + 5 > 250)
        		        {
							throw new ArgumentException(String.Format("Попытка присвоить полю 'Data' массив с длинной {0}, которая превышает установленные пределы", value.Length));
		                }
        		        else
                		{
		                    this.data = new Byte[value.Length];
        		            this.data = value;
                		}
		            }
        		}
        	#endregion

	        #region Методы
	    	    // Возвращает массив байтов пакета
    		    public Byte[] GetBytes()
		        {
					LogFileWriter.OutputStackTrace();
		            // Буффер пакета (без 2-х байт CRC). Первые четыре байта это SYNC, ADR, LNG, CMD
		            List<Byte> Buff = new List<Byte>();
            
        		    // Байт 1: Флаг синхронизации
		            Buff.Add(_Sync);

        		    // Байт 2: адрес устройства
		            Buff.Add(_Adr);

        		    // Байт 3: длина пакета
		            // рассчитаем длину пакета
        		    Int32 result = this.GetLength();

		            // Если длина пакета вместе с байтами SYNC, ADR, LNG, CRC, CMD  больше 250
        		    if (result > Package.StandardPackageLength)
		            {
        		        // то делаем байт длины равный 0, а действительная длина сообщения будет в DATA
                		Buff.Add(0);
		            }
        		    else
		            {
        		        Buff.Add(Convert.ToByte(result));
		            }

        		    // Байт 4: Команда
		            Buff.Add(this.Cmd);

        		    // Байты с 4: Данные
		            if (this.Data != null)
        		    {
                		for (Int32 i = 0; i < this.Data.Length; i++)
		                {
							Buff.Add(this.Data[i]); 
						}
		            }

		            // Последний байт - CRC
        		    Byte[] CRC = BitConverter.GetBytes(GetCRC16(Buff.ToArray(), Buff.Count));

		            Byte[] package = new Byte[Buff.Count + CRC.Length];
        		    Buff.ToArray().CopyTo(package, 0);
		            CRC.CopyTo(package, Buff.Count);

        		    return package;
        		}

		        // Возвращает строку шестнадцатиричного представления байтов пакета
        		public String GetBytesHex()
        		{
					LogFileWriter.OutputStackTrace();
        		    Byte[] package = this.GetBytes();

		            StringBuilder hexString = new StringBuilder(package.Length);
        		    for (Int32 i = 0; i < package.Length; i++)
		            {
        		        hexString.Append(package[i].ToString("X2"));
		            }

        		    return "0x" + hexString.ToString();
		        }

		        // Длина пакета
        		public Int32 GetLength()
		        {
					LogFileWriter.OutputStackTrace();
		            return (this.Data == null ? 0 : this.Data.Length) + 6;
        		}

		        // Расчет контрольной суммы
        		protected static short GetCRC16(Byte[] BufData, Int32 SizeData)
		        {
					LogFileWriter.OutputStackTrace();
		            Int32 tmpCRC, CRC;
        		    CRC = 0;

		            for (Int32 i = 0; i < SizeData; i++)
        		    {
                		tmpCRC = CRC ^ BufData[i];

		                for (Byte j = 0; j < 8; j++)
        		        {
                		    if ((tmpCRC & 0x0001) != 0) 
							{
								tmpCRC >>= 1; 
								tmpCRC ^= Package.POLYNOMIAL; 
							}
        		            else 
							{ 
								tmpCRC >>= 1; 
							}
		                }

        		        CRC = tmpCRC;
            		}

		            return (short)CRC;
        		}

		        public static Boolean CheckCRC(Byte[] Buff)
        		{
					LogFileWriter.OutputStackTrace();
        		    Boolean result = true;

		            Byte[] OldCRC = new Byte[] { Buff[Buff.Length - 2], Buff[Buff.Length - 1]};

        		    // Два последних байта в длине убираем, так как это исходная CRC
		            Byte[] NewCRC = BitConverter.GetBytes(GetCRC16(Buff, Buff.Length - 2));

        		    for (Int32 i = 0; i < 2; i++)
		            {
        		        if (OldCRC[i] != NewCRC[i])
                		{
		                    result = false;
        		            break;
                		}
		            }
			

		            return result;
        		}

		        public static Byte[] CreateResponse(ResponseType type)
        		{
					LogFileWriter.OutputStackTrace();
        		    // Буффер пакета (без 2-х байт CRC). Первые четыре байта это SYNC, ADR, LNG, CMD
		            List<Byte> Buff = new List<Byte>();

        		    // Байт 1: Флаг синхронизации
		            Buff.Add(_Sync);

        		    // Байт 2: адрес устройства
		            Buff.Add(_Adr);

        		    // Байт 3: длина пакета, всегда 6
		            Buff.Add(0x06);
           
        		    // Байт 4: Данные
					switch(type)
					{
						case(ResponseType.ACK):
						{
							Buff.Add(0x00);	
							break;
						}
						case(ResponseType.NAK):
						{
							Buff.Add(0xFF);
							break;
						}
					}


	            // Последний байт - CRC
    	        Byte[] CRC = BitConverter.GetBytes(GetCRC16(Buff.ToArray(), Buff.Count));

        	    Byte[] package = new Byte[Buff.Count + CRC.Length];
            	Buff.ToArray().CopyTo(package, 0);
	            CRC.CopyTo(package, Buff.Count);

    	        return package;
        	}
		
			#region Метод для формирования битовой карты
				public static Byte[] GenerateBitmap(Int32 BitmapLength, params Boolean[] BitmapBitValues)
				{
					Byte[] returnedValue;
					
					if(BitmapLength*8>=BitmapBitValues.Length)
					{			
						returnedValue=new Byte[BitmapLength];
				
						for(Int32 i=0;i!=BitmapBitValues.Length;i++)
						{
							returnedValue[i/8]<<=1;
					
							if(BitmapBitValues[i]==true)
							{
								returnedValue[i/8]|=1;
							}
						}
					}
					else
					{
						throw new ArgumentOutOfRangeException("BitmapBitValues", String.Format(Package.InvalidArgumentOfGenerateBitmapMethod_Format, BitmapBitValues.Length, BitmapLength));
					}
			
					return returnedValue;
				}
			#endregion
		
        #endregion
    }
	#endregion

	#region Класс для работы с купюроприемником
	    public class CashCodeClass : IDisposable
    	{
			#region НЕОБХОДИМЫЕ КОНСТАНТЫ КЛАССА CashCodeClass		
				protected const Int32 Response_Timeout=10;
				protected const Int32 Identification_Timeout=200;
				protected const Int32 RecyclingCassetteStatus_Timeout=200;
				protected const Int32 PowerRecovery_Timeout=2000;
				protected const Int32 ExtendedIdentification_Timeout=2000;
				protected const Int32 ExtendedCassetteStatus_Timeout=2000;
				protected const Int32 BusReset_Timeout=200;//тайм-аут ожидания выполнения команды Reset на устройстве. В документации указано, что это время минимум 100мс, поэтому максимум будем ждать 200мс
				protected const Int32 NonResponse_Timeout=5000;
				protected const Int32 Poll_Timeout=200;
				protected const Int32 Free_Timeout=20;
				protected const Int32 Ack_Timeout=0;//после отправки Ack ждать не нужно
				protected const Int32 Nak_Timeout=0;
				protected const Int32 GetBillTable_Timeout=200;
				protected const Int32 GetStatus_Timeout=200;
			
		
				protected const Int32 DataBits=8;
				protected const String HexByteValueOutputFormater="Byte[{0}] = 0x{1:X2}h";
		
				protected readonly Boolean[] DefaultOption=new Boolean[]{false, true, true, false};
			#endregion

			#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА CashCodeClass        		
				protected readonly Byte[] ENABLE_BILL_TYPES_WITH_ESCROW = new Byte[6] { 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00 };

			        
    		    protected List<Byte> receivedBytes;  // Полученные байты

        		protected Int32 lastError;
		        protected Boolean Disposed;
    		    protected Boolean isConnected;
        		protected Boolean isPowerUp;
	        	protected Boolean isListening;
	    	    protected Object Locker{get;set;}

    	    	protected SerialPort comPort;
				public CashCodeErroList ErrorList{get; protected set;}

				protected System.Timers.Timer Listener{get;set;}  // Таймер прослушивания купюроприемника

				protected Boolean ReturnBill{get;set;}

				protected BillCassetteStatus cassetteStatus = BillCassetteStatus.Inplace;

        		//Событие получения купюры
					public event BillReceivedHandler BillReceived;
				//Событие изменения состояния купюроприемника
					public event BillCassetteHandler BillCassetteStatusEvent;
        		//Событие процесса отправки купюры в стек (Здесь можно делать возврат)
		        	public event BillStackingHandler BillStacking;
        	#endregion



	        #region Конструктор класса CashCodeClass(с параметрами)
		        public CashCodeClass(String PortName, Int32 BaudRate)
    		    {
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в 'CashCodeClass'");
			
	        	    this.ErrorList = new CashCodeErroList();
            
    	        	this.Disposed = false;
	    	        this.Locker = new Object();
    	    	    this.IsConnected = this.IsPowerUp = this.IsListening = this.ReturnBill = false;

        	    	// Из спецификации:
	            	//      Baud Rate:	9600 bps/19200 bps (no negotiation, hardware selectable)
		            //      Start bit:	1
    		        //      Data bit:	8 (bit 0 = LSB, bit 0 sent first)
        		    //      Parity:		Parity none 
            		//      Stop bit:	1
		            this.ComPort = new SerialPort();
					this.ComPort.PortName = PortName;
        		    this.ComPort.BaudRate = BaudRate;
            		this.ComPort.DataBits = CashCodeClass.DataBits;
	            	this.ComPort.Parity = Parity.None;
	    	        this.ComPort.StopBits = StopBits.One;
    	    	    this.ComPort.DataReceived += new SerialDataReceivedEventHandler(this.ComPort_DataReceivedEventHandler);

        	    	this.ReceivedBytes = new List<Byte>();


	    	        this.Listener = new System.Timers.Timer();
    	    	    this.Listener.Interval = CashCodeClass.Poll_Timeout;
        	    	this.Listener.Enabled = false;
	        	    this.Listener.Elapsed += new System.Timers.ElapsedEventHandler(this.Listener_ElapsedEventHandler);
							
					LogFileWriter.WriteInfoToFile("Выходим из 'CashCodeClass'");
    	    	}
        	#endregion


			#region Деструктор для финализации кода        
		        ~CashCodeClass() 
				{ 
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в '~CashCodeClass'");
			
					this.Dispose(false); 
			
					LogFileWriter.WriteInfoToFile("Выходим из '~CashCodeClass'");
				}
			#endregion

			#region Метод, реализующий интерфейс IDisposable
	    	    public void Dispose()
    	    	{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в 'Dispose()'");
			
    	    	    Dispose(true);
				 	// отменим финализацию объекта средствами GC после вызова Dispose, так как он уже освобожден
            		GC.SuppressFinalize(this);
			
					LogFileWriter.WriteInfoToFile("Выходим из 'Dispose()'");
	        	}
			#endregion

			#region Метод, который непосредственно реализует финализацию объекта
	        	// Dispose(bool disposing) выполняется по двум сценариям
	    	    // Если disposing=true, метод Dispose вызывается явно или неявно из кода пользователя
    	    	// Управляемые и неуправляемые ресурсы могут быть освобождены
	    	    // Если disposing=false, то метод может быть вызван runtime из финализатора
    	    	// В таком случае только неуправляемые ресурсы могут быть освобождены.
	        	protected void Dispose(Boolean Disposing)
		        {
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в 'Dispose(Boolean Disposing)'");
			
            		// Проверим вызывался ли уже метод Dispose
	    	        if (!this.Disposed)
    	    	    {
        	    	    // Если Disposing=true, освободим все управляемые и неуправляемые ресурсы
            	    	if (Disposing==true)
	            	    {
    	            	    // Здесь освободим управляемые ресурсы
        	            	try
	            	        {
    	            	        // Остановим таймер, если он работает
        	            	    if (this.IsListening==true)
            	            	{
	            	                this.Listener.Enabled = this.IsListening = false;
    	            	        }

        	            	    this.Listener.Dispose();

            	            	// Отправим сигнал выключения на купюроприемник
	                	        if (this.IsConnected)
    	                	    {
        	                	    this.DisableBillValidator();
	        	                }
    	        	        }
        	        	    catch 
							{
							}
    	        	    }

        	        	// Вызовем соответствующие методы для освобождения неуправляемых ресурсов
	        	        // Если disposing=false, то только следующий код буде выполнен
    	        	    try 
        	        	{
            	        	this.ComPort.Close();
                		}
		                catch 
						{
						}

            	    	this.Disposed = true;
            		}
			
					LogFileWriter.WriteInfoToFile("Выходим из 'Dispose(Boolean Disposing)'");
        		}
			#endregion

			#region Метод, необходимый для обращения к полю receivedBytes как к свойству
		        protected List<Byte> ReceivedBytes 
				{
					get 
					{ 
						return this.receivedBytes; 
					}
					set 
					{
						this.receivedBytes=value;
					}
	        	}
    	    #endregion

        	#region Метод, необходимый для обращения к полю lastError как к свойству
	        	public Int32 LastError 
				{
					get 
					{ 
						return this.lastError; 
					}
					protected set 
					{
						this.lastError=value;
					}
	        	}
    	    #endregion

        	#region Метод, необходимый для обращения к полю isConnected как к свойству
	        	public Boolean IsConnected 
				{
					get 
					{ 
						return this.isConnected; 
					}
					protected set 
					{
						this.isConnected=value;
					}
        		}
        	#endregion

			#region Метод, необходимый для обращения к полю isPowerUp как к свойству
		        protected Boolean IsPowerUp 
				{
					get 
					{ 
						return this.isPowerUp; 
					}
					set 
					{
						this.isPowerUp=value;
					}
        		}
	        #endregion

			#region Метод, необходимый для обращения к полю isListening как к свойству
	    	    protected Boolean IsListening 
				{
					get 
					{
						return this.isListening; 
					}
					set 
					{
						this.isListening=value;
					}
    	    	}
        	#endregion

			#region Метод, необходимый для обращения к полю comPort как к свойству
		        protected SerialPort ComPort 
				{
					get 
					{ 
						return this.comPort; 
					}
					set 
					{
						this.comPort=value;
					}
	        	}
    	    #endregion

			#region Метод, необходимый для обращения к полю cassetteStatus как к свойству
	        	protected BillCassetteStatus CassetteStatus
				{
					get 
					{ 
						return this.cassetteStatus; 
					}
					set 
					{
						this.cassetteStatus=value;
					}
	        	}
    	    #endregion



        
			#region Начало прослушивания купюроприемника
	        	public void StartListening()
	    	    {
        		    LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в 'StartListening'");
			
					// Если не подключен
            		if (this.IsConnected==false)
		            {
    		            this.LastError = 100020;
											
						LogFileWriter.WriteInfoToFile(String.Format("Произошла ошибка: {0}. Выходим из метода 'StartListening'", this.LastError));
				
        	    	    throw new System.IO.IOException(this.ErrorList.Errors[this.LastError]);
            		}
	            	
					if (this.IsPowerUp==false) 
					{ 
						LogFileWriter.WriteInfoToFile("Произошла ошибка: Состояние поля 'IsPowerUp' равно 'false'. Необходимо выполнить метод 'PowerUpBillValidator()' перед началом прослушивания. Выходим из метода 'StartListening'");
						throw new Exception("Состояние поля 'IsPowerUp' равно 'false'. Необходимо выполнить метод 'PowerUpBillValidator()' перед началом прослушивания");
					}

	        	    this.IsListening = true;
    	        	this.Listener.Start();
					LogFileWriter.WriteInfoToFile("Выходим из 'StartListening'");
	        	}
			#endregion


			#region Остановка прослушивания купюроприемника
	    	    public void StopListening()
    	    	{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в 'StopListening'");
			
					this.IsListening = false;
        	    	this.Listener.Stop();
	        	    this.DisableBillValidator();
			
					LogFileWriter.WriteInfoToFile("Выходим из 'StopListening'");
	    	    }
			#endregion


			#region Открытие Com-порта для работы с купюроприемником
	        	public Int32 ConnectBillValidator ()
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в 'ConnectBillValidator'");
					try 
					{
						//нужно будет удалить					
								LogFileWriter.WriteInfoToFile("Пытаемся открыть com-порт");						
						//===================
						this.ComPort.Open ();
						this.IsConnected = true;
				
						//нужно будет удалить
								LogFileWriter.WriteInfoToFile("Com-порт открыт");						
						//===================
					}
    	    	    catch(Exception exception)
        	    	{
    	    	       	this.IsConnected = false;
						this.LastError = 100010;
					
						LogFileWriter.WriteInfoToFile(String.Format("В методе 'ConnectBillValidator' имеем исключение: '{0}'. Передаем ошибку: '{0}' и выходим", exception.Message, this.LastError));
		            }
	
					LogFileWriter.WriteInfoToFile("Выходим из 'ConnectBillValidator'");
			
	    	        return this.LastError;
    	    	}
			#endregion


			#region Включение купюроприемника
		        public Int32 PowerUpBillValidator()
    		    {
        		    LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в 'PowerUpBillValidator'");
			
					List<Byte> byteResult = null;

		            // Если ком-порт не открыт
    		        if (!this.IsConnected)
        		    {				
        	    	    this.LastError = 100020;
					
						LogFileWriter.WriteInfoToFile("Com-порт неоткрыт");
						LogFileWriter.WriteInfoToFile(String.Format("Произошла ошибка. Выходим из 'PowerUpBillValidator' с передачей ошибки '{0}'", this.LastError));
					
        	    	    throw new System.IO.IOException(this.ErrorList.Errors[this.LastError]);
	        	    }
					else
					{
						LogFileWriter.WriteInfoToFile("Com-порт был открыт");
					}
	
									
			
					// POWER UP (Ok) - 1
						LogFileWriter.WriteInfoToFile("Отсылаем POOL");
    	        		byteResult = new List<Byte>(this.SendCommand(BillValidatorCommands.POLL, CashCodeClass.Poll_Timeout));
						LogFileWriter.WriteInfoToFile("Получили:");
						for(Int32 i=0;i!=byteResult.Count;i++)
						{
							LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater, i, byteResult[i]));
						}
			
			
		        	    // Проверим результат
							LogFileWriter.WriteInfoToFile("Проверяем результат");
			    	        if (CheckPollOnError(byteResult.ToArray()))
        				    {
								LogFileWriter.WriteInfoToFile("Результат содержит ошибки!\nОтсылаем на com-порт команду NAK и выбрасываем исключение");
	        			        byteResult = new List<Byte>(this.SendCommand(BillValidatorCommands.NAK, CashCodeClass.Nak_Timeout));
								for(Int32 i=0;i!=byteResult.Count;i++)
								{
									LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater, i, byteResult[i]));
								}
				
								LogFileWriter.WriteInfoToFile(String.Format("Выходим из 'PowerUpBillValidator' с генерацией ошибки {0}", this.LastError));
			            	    throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
	        			    }
							else
							{
								LogFileWriter.WriteInfoToFile("Проверка выполнена успешно");
							}

            
					//ACK ===tmp=== - 2
						LogFileWriter.WriteInfoToFile("Отправляем ACK");
    	    		    this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);						
					//=========

            		// RESET - 3
						LogFileWriter.WriteInfoToFile("Отправляем RESET");
		        	    byteResult = new List<Byte>(this.SendCommand(BillValidatorCommands.RESET, CashCodeClass.BusReset_Timeout));
							LogFileWriter.WriteInfoToFile("Получили:");
							for(Int32 i=0;i!=byteResult.Count;i++)
							{
								LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, byteResult[i]));
							}

			            //Если не получили от купюроприемника сигнала ACK
    				        if (byteResult[3] != 0x00)
			        	    {
								this.LastError = 100050;
								LogFileWriter.WriteInfoToFile(String.Format("Должны были получить в byteResult[3] ноль, а получили {0}. Выходим из 'PowerUpBillValidator' с передачей кода ошибки: {1}", byteResult[3], this.LastError));
                				
    	    			        return this.LastError;
        	    			}
							else
							{
								LogFileWriter.WriteInfoToFile("От купюроприемника в 3 байте получена ожидаемая величина");
							}

	            	// INITIALIZE - 4
		    	        // Далее снова опрашиваем купюроприемник
							LogFileWriter.WriteInfoToFile("Отправляем POLL");
				            byteResult = new List<Byte>(this.SendCommand(BillValidatorCommands.POLL, CashCodeClass.Poll_Timeout));
							LogFileWriter.WriteInfoToFile("Получили:");
							for(Int32 i=0;i!=byteResult.Count;i++)
							{
								LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, byteResult[i]));
							}
				
        			    	if (CheckPollOnError(byteResult.ToArray()))
				            {
								LogFileWriter.WriteInfoToFile(String.Format("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'PowerUpBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с ошибкой {0}", this.LastError));
					
			                	this.SendNonResponseCommand(BillValidatorCommands.NAK, CashCodeClass.Nak_Timeout);				
	        			        throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
			    	        }
							else
							{
								LogFileWriter.WriteInfoToFile("Ответ устройства на команду 'POLL' не содержит ошибок");
							}

        	    // Иначе отправляем сигнал подтверждения - 5
					LogFileWriter.WriteInfoToFile("Отправляем ACK");
    		        this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);
					
			
				//===tmp=== - 6
					LogFileWriter.WriteInfoToFile("Отправляем SET OPTIONS");
					byteResult=new List<Byte>(this.SendCommand(BillValidatorCommands.SET_OPTIONS, CashCodeClass.Response_Timeout, Package.GenerateBitmap(4, DefaultOption)));
						LogFileWriter.WriteInfoToFile("Получили:");
						for(Int32 i=0;i!=byteResult.Count;i++)
						{
							LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, byteResult[i]));
						}									
				//=========
			
				//===tmp=== - 7
					LogFileWriter.WriteInfoToFile("Отправляем POLL");
		            byteResult = new List<Byte>(this.SendCommand(BillValidatorCommands.POLL, CashCodeClass.Poll_Timeout));
						LogFileWriter.WriteInfoToFile("Получили:");
						for(Int32 i=0;i!=byteResult.Count;i++)
						{
							LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, byteResult[i]));
						}
				
        	    	if (CheckPollOnError(byteResult.ToArray()))
	        	    {
						LogFileWriter.WriteInfoToFile(String.Format("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'PowerUpBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с ошибкой {0}", this.LastError));
					
	                	this.SendNonResponseCommand(BillValidatorCommands.NAK, CashCodeClass.Nak_Timeout);				
		                throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
    		        }
					else
					{
						LogFileWriter.WriteInfoToFile("Ответ устройства на команду 'POLL' не содержит ошибок");
					}
				//=========
			
			
				//===tmp=== - 8
					LogFileWriter.WriteInfoToFile("Отправляем ACK");
		            this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);
				//=========
			
				
				//===tmp=== - 9
					LogFileWriter.WriteInfoToFile("Отправляем POLL");
		            byteResult = new List<Byte>(this.SendCommand(BillValidatorCommands.POLL, CashCodeClass.Poll_Timeout));
						LogFileWriter.WriteInfoToFile("Получили:");
						for(Int32 i=0;i!=byteResult.Count;i++)
						{
							LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, byteResult[i]));
						}
				
        	    	if (CheckPollOnError(byteResult.ToArray()))
	        	    {
						LogFileWriter.WriteInfoToFile(String.Format("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'PowerUpBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с ошибкой {0}", this.LastError));
					
	                	this.SendNonResponseCommand(BillValidatorCommands.NAK, CashCodeClass.Nak_Timeout);				
		                throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
    		        }
					else
					{
						LogFileWriter.WriteInfoToFile("Ответ устройства на команду 'POLL' не содержит ошибок");
					}
				//=========
			
				//===tmp=== - 10
					LogFileWriter.WriteInfoToFile("Отправляем ACK");
		            this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);
				//=========

	            // GET STATUS - 11
					LogFileWriter.WriteInfoToFile("Отправляем GET_STATUS");
    	        	byteResult = new List<Byte>(this.SendCommand(BillValidatorCommands.GET_STATUS, CashCodeClass.GetStatus_Timeout));
						LogFileWriter.WriteInfoToFile("Получили:");
						for(Int32 i=0;i!=byteResult.Count;i++)
						{
							LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, byteResult[i]));
						}	
			
    	        	// Команда GET STATUS возвращает 6 байт ответа. Если все равны 0, то статус ok и можно работать дальше, иначе ошибка
	        	    if (byteResult[3] != 0x00 || byteResult[4] != 0x00 || byteResult[5] != 0x00 ||
    	        	    byteResult[6] != 0x00 || byteResult[7] != 0x00 || byteResult[8] != 0x00)
	    	        {
    	    	        this.LastError = 100070;
				
						LogFileWriter.WriteInfoToFile(String.Format("GET_STATUS должна была вернуть 0, но такого не произошло. Выходим из 'PowerUpBillValidator' и генерируем исключение с кодом {0}", this.LastError));

                		throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
		            }
					else
					{
						LogFileWriter.WriteInfoToFile("Результат выполнения команды 'GET_STATUS' верен (все 6 полученных байт содержат нули)");
					}

				//ACK - 12
					LogFileWriter.WriteInfoToFile("Отправляем ACK");
    	        	this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);

				//===tmp=== - 13
					LogFileWriter.WriteInfoToFile("Отправляем GET_BILL_TABLE");
		            byteResult = new List<Byte>(this.SendCommand(BillValidatorCommands.GET_BILL_TABLE, CashCodeClass.GetBillTable_Timeout));
						LogFileWriter.WriteInfoToFile("Получили:");
						for(Int32 i=0;i!=byteResult.Count;i++)
						{
							LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, byteResult[i]));
						}
				//=========
			
				//===tmp=== - 14
					LogFileWriter.WriteInfoToFile("Отправляем ACK");
    	        	this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);	
				//=========

    	        // SET_SECURITY (в тестовом примере отправояет 3 байта (0 0 0) - 15
					LogFileWriter.WriteInfoToFile("Отправляем SET_SECURITY");
	        	    byteResult = new List<Byte>(this.SendCommand(BillValidatorCommands.SET_SECURITY, CashCodeClass.Response_Timeout, new Byte[3] { 0x00, 0x00, 0x00 }));
						LogFileWriter.WriteInfoToFile("Получили:");
						for(Int32 i=0;i!=byteResult.Count;i++)
						{
							LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, byteResult[i]));
						}	
        	    	//Если не получили от купюроприемника сигнала ACK
	            	if (byteResult[3] != 0x00)
		            {
						this.LastError = 100050;
				
						LogFileWriter.WriteInfoToFile(String.Format("Должны были получить в byteResult[3] ноль, а получили {0}. Выходим из 'PowerUpBillValidator' с передачей кода ошибки", byteResult[3], this.LastError));
                
				
                		return this.LastError;
		            }
					else
					{
						LogFileWriter.WriteInfoToFile("От купюроприемника в 3 байте получена ожидаемая величина");
					}

	   	        // IDENTIFICATION - 16
					LogFileWriter.WriteInfoToFile("Отправляем IDENTIFICATION");
		            byteResult = new List<Byte>(this.SendCommand(BillValidatorCommands.IDENTIFICATION, CashCodeClass.Identification_Timeout));
						LogFileWriter.WriteInfoToFile("Получили:");
						for(Int32 i=0;i!=byteResult.Count;i++)
						{
							LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, byteResult[i]));
						}
				
				// ACK - 17
					LogFileWriter.WriteInfoToFile("Отправляем ACK");
		            this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);


        	    // POLL -18 
    	        	// Далее снова опрашиваем купюроприемник. Должны получить команду INITIALIZE
						LogFileWriter.WriteInfoToFile("Отправляем POLL");
		        	    byteResult = new List<Byte>(this.SendCommand(BillValidatorCommands.POLL, CashCodeClass.Poll_Timeout));
							LogFileWriter.WriteInfoToFile("Получили:");
							for(Int32 i=0;i!=byteResult.Count;i++)
							{
								LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, byteResult[i]));
							}

	            		// Проверим результат
		    	        if (CheckPollOnError(byteResult.ToArray()))
        			    {
							LogFileWriter.WriteInfoToFile(String.Format("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'PowerUpBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с кодом {0}", this.LastError));
			                this.SendNonResponseCommand(BillValidatorCommands.NAK, CashCodeClass.Nak_Timeout);
				
    			            throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
        	    		}
						else
						{
							LogFileWriter.WriteInfoToFile("Ответ устройства на команду 'POLL' не содержит ошибок");
						}

	            // ACK - 19
					LogFileWriter.WriteInfoToFile("Отправляем ACK");
	            	this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);


    	        // POLL - 20
    	    	    // Далее снова опрашиваем купюроприемник. Должны получить команду UNIT DISABLE
						LogFileWriter.WriteInfoToFile("Отправляем POLL");
    	        		byteResult = new List<Byte>(this.SendCommand(BillValidatorCommands.POLL, CashCodeClass.Poll_Timeout));
							LogFileWriter.WriteInfoToFile("Получили:");
							for(Int32 i=0;i!=byteResult.Count;i++)
							{
								LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, byteResult[i]));
							}			
	
       	     		// Проверим результат
	        	    if (CheckPollOnError(byteResult.ToArray()))
    	        	{
						LogFileWriter.WriteInfoToFile(String.Format("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'PowerUpBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с кодом {0}", this.LastError));
    	    	        this.SendNonResponseCommand(BillValidatorCommands.NAK, CashCodeClass.Nak_Timeout);
	
        	    	    throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
            		}
					else
					{
						LogFileWriter.WriteInfoToFile("Ответ устройства на команду 'POLL' не содержит ошибок");	
					}

    	        // ACK - 21
					LogFileWriter.WriteInfoToFile("Отправляем ACK");
		            this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);

			
				//POLL - 22
					LogFileWriter.WriteInfoToFile("Отправляем POLL");
    		        byteResult = new List<Byte>(this.SendCommand(BillValidatorCommands.POLL, CashCodeClass.Poll_Timeout));
						LogFileWriter.WriteInfoToFile("Получили:");
						for(Int32 i=0;i!=byteResult.Count;i++)
						{
							LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, byteResult[i]));
						}			
	
    	   	     	// Проверим результат
        		    if (CheckPollOnError(byteResult.ToArray()))
            		{
						LogFileWriter.WriteInfoToFile(String.Format("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'PowerUpBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с кодом {0}", this.LastError));
    	            	this.SendNonResponseCommand(BillValidatorCommands.NAK, CashCodeClass.Nak_Timeout);
	
	        	        throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
    	        	}
					else
					{
						LogFileWriter.WriteInfoToFile("Ответ устройства на команду 'POLL' не содержит ошибок");	
					}
			
				//ACK - 23
					LogFileWriter.WriteInfoToFile("Отправляем ACK");
		            this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);
			
			
			
			
        		    this.IsPowerUp = true;
					LogFileWriter.WriteInfoToFile("Выходим из 'PowerUpBillValidator'");
    	        	return this.LastError;
        		}
			#endregion

        

			#region Включение режима приема купюр
		        public Int32 EnableBillValidator()
    		    {
        		    LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в 'EnableBillValidator'");
			
					Byte[] byteResult = null;

    	        	// Если com-порт не открыт
	    	        if (this.IsConnected==false)
    	    	    {
        	    	    this.LastError = 100020;
						LogFileWriter.WriteInfoToFile(String.Format("Выходим из 'EnableBillValidator' и генерируем исключение: '{0}'", this.ErrorList.Errors[this.LastError]));
	            	    throw new System.IO.IOException(this.ErrorList.Errors[this.LastError]);
		            }

    		        try
        		    {				
						if(IsListening==true)
						{
							LogFileWriter.WriteInfoToFile("Остановим прослушивание порта");
							this.Listener.Stop();
							this.IsListening=false;
						}
					
    	        	    lock (Locker)
        	        	{
                	    	// отправить команду ENABLE BILL TYPES (в тестовом примере отправляет 6 байт  (255 255 255 0 0 0) Функция удержания включена (Escrow)
									LogFileWriter.WriteInfoToFile("Отправляем на купюроприемник команду 'ENABLE_BILL_TYPES'");
	        	            	byteResult = this.SendCommand(BillValidatorCommands.ENABLE_BILL_TYPES, CashCodeClass.Response_Timeout, ENABLE_BILL_TYPES_WITH_ESCROW);

		                    //Если не получили от купюроприемника сигнала ACK
    		        	        if (byteResult[3] != 0x00)
        		        	    {
            		        	    this.LastError = 100050;
								
									LogFileWriter.WriteInfoToFile(String.Format("Должны были получить в byteResult[3] ноль, а получили {0}. Выходим из 'EnableBillValidator' с генерированием исключения {1}", byteResult[3], this.LastError));
						
	                	        	throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
		                    	}
								else
								{
									LogFileWriter.WriteInfoToFile("Ответ на команду 'ENABLE_BILL_TYPES' корректен");							
								}

	                	    // Далее снова опрашиваем купюроприемник
								LogFileWriter.WriteInfoToFile("Отправляем на купюроприемник команду 'POLL'");
	    	    	            byteResult = this.SendCommand(BillValidatorCommands.POLL, CashCodeClass.Poll_Timeout);
						
		                    // Проверим результат
    		            	    if (CheckPollOnError(byteResult))
		        	            {
									LogFileWriter.WriteInfoToFile(String.Format("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'EnableBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с кодом {0}", this.LastError));
						
            			            this.SendNonResponseCommand(BillValidatorCommands.NAK, CashCodeClass.Nak_Timeout);
                	    		    throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
		                    	}
								else
								{
									LogFileWriter.WriteInfoToFile("Результат выполнения команды 'POLL' не содержит ошибок");
										LogFileWriter.WriteInfoToFile("Получили следующий поток байт:");
									for(Int32 i=0;i!=byteResult.Length;i++)
									{
										LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater, i, byteResult[i]));
									}							
								}

	                    	// Иначе отправляем сигнал подтверждения
								LogFileWriter.WriteInfoToFile("Отправляем на купюроприемник команду 'ACK'");
    			                this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);
        	    	    }
	            	}
		            catch
    		        {									
        		        this.LastError = 100030;
					
						LogFileWriter.WriteInfoToFile(String.Format("В методе 'EnableBillValidator' имеем исключение. Передаем ошибку: {0} и выходим", this.LastError));
	            	}
				
			
					if(IsListening==false)
					{
						LogFileWriter.WriteInfoToFile("Возобновим прослушивание порта");
						this.Listener.Start();
						this.IsListening=true;
					}
			
					LogFileWriter.WriteInfoToFile("Выходим из 'EnableBillValidator'");
			
	    	        return this.LastError;
    	    	}
			#endregion

			#region Выключение режима приема купюр
		        public Int32 DisableBillValidator()
    		    {
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в 'DisableBillValidator'");
			
	        	    Byte[] byteResult = null;

    	        	lock (Locker)
	    	        {
    	    	        // Если Com-порт не открыт
        	    		    if (this.IsConnected==false)
            	    		{
                	    		this.LastError = 100020;
						
								LogFileWriter.WriteInfoToFile(String.Format("Порт неоткрыт. Выходим из 'DisableBillValidator' с генерированием исключения с кодом {0}", this.LastError));
					
    		                	throw new System.IO.IOException(this.ErrorList.Errors[this.LastError]);
	    		            }

        		        // отпавить команду ENABLE BILL TYPES (в тестовом примере отправляет 6 байт (0 0 0 0 0 0)
            			    byteResult = this.SendCommand(BillValidatorCommands.ENABLE_BILL_TYPES, CashCodeClass.Response_Timeout, new Byte[6] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 });
	            	}

	    	        //Если не получили от купюроприемника сигнала ACK
    	    	    if (byteResult[3] != 0x00)
        	    	{
            	    	this.LastError = 100050;
				
				
	            	    return this.LastError;
    	        	}

					LogFileWriter.WriteInfoToFile("Выходим из 'DisableBillValidator'");
			
    	    	    return this.LastError;
        		}
			#endregion





	        #region Метод для проверки полученных сообщений от купюроприемника
		        protected Boolean CheckPollOnError(Byte[] ByteResult)
    		    {
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в 'CheckPollOnError'");
			
	        	    Boolean returnedValue;
				
					switch(this.ConvertByteToEnum<BillToBillReceivedCodes>(ByteResult[3]))
					{
	        	    	//Если получили от купюроприемника третий байт равный 30Н (INVALID COMMAND )
							case(BillToBillReceivedCodes.InvalidCommand):
		    		        {
        				        this.LastError = 100040;
            				    returnedValue = true;
								break;
				            }
				      	//Если получили от купюроприемника третий байт равный 41Н (Drop Cassette Full)
							case(BillToBillReceivedCodes.DropCassetteFull):
        				    {
            				    this.LastError = 100080;
                				returnedValue = true;
								break;
				            }
    		        	//Если получили от купюроприемника третий байт равный 42Н (Drop Cassette out of position)
							case(BillToBillReceivedCodes.DropCassetteOutOfPosition):
        		    		{
                				this.LastError = 100070;
	                			returnedValue = true;
								break;
	    			        }
    	    	    	//Если получили от купюроприемника третий байт равный 43Н (Bill Validator Jammed)
							case(BillToBillReceivedCodes.BillValidatorJammed):
	    		    	    {
    	        			    this.LastError = 100090;
        	        			returnedValue = true;
								break;
	    		        	}
		            	//Если получили от купюроприемника третий байт равный 44Н (Cassette Jammed)
							case(BillToBillReceivedCodes.CassetteJammed):
        				    {
            				    this.LastError = 100100;
                				returnedValue = true;
								break;
				            }
    		        	//Если получили от купюроприемника третий байт равный 45Н (Cheated)
							case(BillToBillReceivedCodes.Cheated):
        		    		{
                				this.LastError = 100110;
	                			returnedValue = true;
								break;
	    			        }
    	    	    	//Если получили от купюроприемника третий байт равный 46Н (Pause)
							case(BillToBillReceivedCodes.Pause):
	    		    	    {
    	        			    this.LastError = 100120;
        	        			returnedValue = true;
								break;
	    		        	}
		        	    //Если получили от купюроприемника третий байт равный 47Н (Generic Failure codes)
						//при получении 47H, мы имеем Generic Failure codes - обобщенную ошибку, расширение которой находится в четвертом байте, поэтому вновь используем switch
							case(BillToBillReceivedCodes.GenericFailureCode):
        				    {
            				    switch(this.ConvertByteToEnum<GenericFailureCodes>(ByteResult[4]))
								{
									// Stack Motor Failure
										case(GenericFailureCodes.StackMotorFailure):
										{ 
											this.LastError = 100130; 
											break;
										}
							   		// Transport Motor Speed Failure
										case(GenericFailureCodes.TransportMotorSpeedFailure):
										{ 
											this.LastError = 100140; 
											break;
										}
					   				// Transport Motor Failure
										case(GenericFailureCodes.TransportMotorFailure):
										{ 
											this.LastError = 100150;
											break;
										}
									// Aligning Motor Failure
										case(GenericFailureCodes.AligningMotorFailure):
										{ 
											this.LastError = 100160; 
											break;
										}
									// Initial Box Status Failure
										case(GenericFailureCodes.InitialBoxStatusFailure):
										{ 
											this.LastError = 100170; 
											break;
										}
					   				// Optic Canal Failure
										case(GenericFailureCodes.OpticCanalFailure):
										{ 
											this.LastError = 100180; 
											break;
										}
					   				// Magnetic Canal Failure
										case(GenericFailureCodes.MagneticCanalFailure):
										{ 
											this.LastError = 100190; 
											break;
										}
					   				// Capacitance Canal Failure
										case(GenericFailureCodes.CapacitanceCanalFailure):
										{ 
											this.LastError = 100200; 
											break;
										}
								}
				
	    	            		returnedValue = true;
								break;
    	    		    	}
				
							default:
							{
								returnedValue=false;
								break;
							}
					}

					LogFileWriter.WriteInfoToFile(String.Format("Выходим из 'CheckPollOnError'. Имеется ли ошибка='{0}'. Если ошибка есть, то ее код: '{1}'", returnedValue, this.LastError));
			
	            	return returnedValue;
    	    	}
			#endregion
        

			#region Метод для отправка команды купюроприемнику с получением ответа
		    	public Byte[] SendCommand(BillValidatorCommands Command, Int32 WaitTimeout, Byte[] Data = null)
	    	    {
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в SendCommand");
					LogFileWriter.WriteInfoToFile(String.Format("Передается команда {0}", Command));
					try
					{
						Package package = new Package();
    	        		package.Cmd = Convert.ToByte(Command);

	    	        	if (Data != null) 
						{
							package.Data = Data; 
						}

		    	        Byte[] cmdBytes = package.GetBytes();
					
						LogFileWriter.WriteInfoToFile("Будем передавать следующие байты на com-порт");
						for(Int32 i=0;i!=cmdBytes.Length;i++)
						{
							LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, cmdBytes[i]));
						}
				
						LogFileWriter.WriteInfoToFile(String.Format("Открыт ли com-порт: {0}", this.ComPort.IsOpen));
	    	    	    this.ComPort.Write(cmdBytes, 0, cmdBytes.Length);

					
        	        			
				
					
						this.ReadAllAvailableData(WaitTimeout);
					
				
			            Byte[] byteResult = this.ReceivedBytes.ToArray();
						LogFileWriter.WriteInfoToFile("Получили:");
						for(Int32 i=0;i!=byteResult.Length;i++)
						{
							LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, byteResult[i]));
						}
				

    	    	        
	        	    	//проверим CRC
    		        	    if (byteResult.Length == 0 || !Package.CheckCRC(byteResult))
	        		    	{
								LogFileWriter.WriteInfoToFile(String.Format("Проверка CRC не прошла, генерируем исключение. Длинна пакета = {0}", byteResult.Length));
								LogFileWriter.WriteInfoToFile("Вышли из 'SendCommand'");
        	    		       	throw new ArgumentException("Несоответствие контрольной суммы полученного сообщения. Возможно устройство не подключено к COM-порту. Проверьте настройки подключения.");
	   			     	    }
							else
							{
								LogFileWriter.WriteInfoToFile("Проверка CRC выполнена успешно");
							}				

						LogFileWriter.WriteInfoToFile("Вышли из 'SendCommand'");
    	            	return byteResult;        	    	
					}
					catch(Exception ex)
					{
						LogFileWriter.WriteInfoToFile(String.Format("Выходим из метода 'SendCommand'. Имеем исключение: {0}\nStackTrace исключения:{1}",ex.Message,ex.StackTrace));
						throw;
					}

    	    	}
			#endregion
		
			#region Метод для отправка команды купюроприемнику без получением ответа
		    	public void SendNonResponseCommand(BillValidatorCommands Command, Int32 WaitTimeout)
	    	    {
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в SendNonResponseCommand");
					LogFileWriter.WriteInfoToFile(String.Format("Передается команда {0}", Command));
					try
					{        
						Byte[] sendingBytes = null;
		        	    switch(Command)
						{
							case(BillValidatorCommands.ACK):
							{
								sendingBytes = Package.CreateResponse(ResponseType.ACK);	
								break;
							}
							case(BillValidatorCommands.NAK):
							{
								sendingBytes = Package.CreateResponse(ResponseType.NAK);	
								break;
							}
							default:
							{
								throw new ArgumentException(String.Format("Параметр 'Command', переданный в метод 'SendNonResponseCommand' имеет значение: '{0}' при допустимых: 'ACK' или 'NAK'", Command));
							}
						}

				 		if (sendingBytes != null) 
						{
							LogFileWriter.WriteInfoToFile("Будем передавать следующие байты на com-порт");
								for(Int32 i=0;i!=sendingBytes.Length;i++)
								{
									LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, sendingBytes[i]));
								}
				
	    	            
							LogFileWriter.WriteInfoToFile(String.Format("Открыт ли com-порт: {0}", this.ComPort.IsOpen));
							this.ComPort.Write(sendingBytes, 0, sendingBytes.Length);
						}
					
						Thread.Sleep(WaitTimeout);
					                	            		
					}
					catch(Exception ex)
					{
						LogFileWriter.WriteInfoToFile(String.Format("Выходим из метода 'SendNonResponseCommand'. Имеем исключение: {0}\nStackTrace исключения:{1}",ex.Message,ex.StackTrace));
						throw;
					}
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'SendNonResponseCommand'");
    	    	}
			#endregion

			#region Метод, сопоставляющий код валюты с ее номиналом
		        protected UInt16 CurrencyCodeTable(Byte CurrencyCode)
    		    {
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в CurrencyCodeTable");
			
        	    	UInt16 returnedValue = 0;

            		switch(this.ConvertByteToEnum<CurrencyCodes>(CurrencyCode))
					{
						// 10 р.
							case(CurrencyCodes.TenRubles):
							{ 
								returnedValue = 10; 
								break;
							}     
						// 50 р.
							case(CurrencyCodes.FiftyRubles):
							{ 
								returnedValue = 50; 
								break;
							}    
						// 100 р.
							case(CurrencyCodes.OneHundredRubles):
							{
								returnedValue = 100; 
								break;
							}
						// 500 р.
							case(CurrencyCodes.FiveHundredRubles):
							{ 
								returnedValue = 500; 
								break;
							}
						// 1000 р.
							case(CurrencyCodes.OneThousandRubles):
							{ 
								returnedValue = 1000; 
								break;
							}
						// 5000 р.
							case(CurrencyCodes.FiveThousandRubles):
							{ 
								returnedValue = 5000; 
								break;
							}
							default:
							{
								LogFileWriter.WriteInfoToFile("Выходим из метода 'CurrencyCodeTable' с генерированием исключения: Невозможно распознать код купюры");
								throw new ArgumentException("Невозможно распознать код купюры");
							}
					}

            		return returnedValue;
        		}
			#endregion


			#region Метод для считывания всех доступных байтов из Com-порта
				protected void ReadAllAvailableData(Int32 ReadTimeout)
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'ReadAllAvailableData'");
			
			
					//===tmp===
						this.ReceivedBytes.Clear();
						Int32 previousReadTimeout=this.ComPort.ReadTimeout;
						this.ComPort.ReadTimeout=ReadTimeout;
						try
						{
							//пока ни сгенерируется исключение, считывать данные с com-порта
							do
							{
								this.ReceivedBytes.Add(Convert.ToByte(this.ComPort.ReadByte()));
							}
							while(true);
						}
						catch(TimeoutException)
						{
							if(this.ReceivedBytes.Count==0)
							{
								try
								{
									this.ComPort.ReadTimeout=CashCodeClass.NonResponse_Timeout;
									this.ReceivedBytes.Add(Convert.ToByte(this.ComPort.ReadByte()));
									while(this.ComPort.BytesToRead!=0)
									{
										this.ReceivedBytes.Add(Convert.ToByte(this.ComPort.ReadByte()));
									}
								}
								catch(TimeoutException)
								{
									if(this.ReceivedBytes.Count==0)
									{
										throw;
									}
								}
							}			
						}
						finally
						{
							this.ComPort.ReadTimeout=previousReadTimeout;
						}
					//=========

					LogFileWriter.WriteInfoToFile("Выходим из метода 'ReadAllAvailableData'");
				}
			#endregion
		

    	    #region Метод для вызова события BillReceived
	    	    protected void OnBillReceived(BillReceivedEventArgs e)
    	    	{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'OnBillReceived'");
			
    	    	    if (this.BillReceived != null)
        	    	{
            	    	this.BillReceived(this, new BillReceivedEventArgs(e.Status, e.Value, e.RejectedReason));
	            	}
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'OnBillReceived'");
    		    }
			#endregion

			#region Метод для вызова события BillCassetteStatusEvent
		        protected void OnBillCassetteStatus(BillCassetteEventArgs e)
    		    {
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'OnBillCassetteStatus'");
			
	        	    if (this.BillCassetteStatusEvent != null)
    	        	{
        	        	this.BillCassetteStatusEvent(this, new BillCassetteEventArgs(e.Status));
	        	    }
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'OnBillCassetteStatus'");
	    	    }
			#endregion

			#region Метод для вызова события BillStacking
	        	protected void OnBillStacking(BillStackedEventArgs e)
	    	    {
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'OnBillStacking'");
			
        		    if (this.BillStacking != null)
            		{
                		Boolean cancel = false;
		                foreach (BillStackingHandler subscriber in BillStacking.GetInvocationList())
    		            {
        		            subscriber(this, e);
						
	        	            if (e.Cancel)
    	        	        {
        	        	        cancel = true;
            	        	    break;
	                	    }
    	            	}
	
        	        	this.ReturnBill = cancel;
            		}
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'OnBillStacking'");
	        	}
    	    #endregion





        	#region Обработчик события получение данных с Com-порта
	        	protected void ComPort_DataReceivedEventHandler(Object Sender, SerialDataReceivedEventArgs E)
	    	    {
    	    		LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'ComPort_DataReceivedEventHandler'");
			
					
		            this.ReceivedBytes.Clear();

    		        
					this.ReadAllAvailableData(CashCodeClass.Response_Timeout);

    		        
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'ComPort_DataReceivedEventHandler'");
	        	}
			#endregion

			#region Обработчик события истечения времени таймера прослушки купюроприемника
		        protected void Listener_ElapsedEventHandler(Object Sender, System.Timers.ElapsedEventArgs E)
    		    {
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'Listener_ElapsedEventHandler'");
			
	        	    this.Listener.Stop();
			
				

    	        	try
	    	        {
    	    	        lock (Locker)
        	    	    {
            	    	    Byte[] byteResult = null;

                	    	// отпавить команду POLL
								LogFileWriter.WriteInfoToFile("Отправляем на купюроприемник команду 'POLL'");
        		            	byteResult = this.SendCommand(BillValidatorCommands.POLL, CashCodeClass.Poll_Timeout);
								LogFileWriter.WriteInfoToFile("От купюроприемника получили следующий поток байт:");
								for(Int32 i=0;i!=byteResult.Length;i++)
								{
									LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, byteResult[i]));
								}
						
								switch(this.ConvertByteToEnum<BillToBillReceivedCodes>(byteResult[3]))
								{						
		        		            // Если четвертый бит Idling (простой), то завершить обработку
										case(BillToBillReceivedCodes.Idling):
			        	            	{
											LogFileWriter.WriteInfoToFile("От купюроприемника в 4 байте получили Idling");
											break;
										}

			           	            // ACCEPTING
            		    	        //Если получили ответ 15H (Accepting)
										case(BillToBillReceivedCodes.Accepting):
			                        	{
											LogFileWriter.WriteInfoToFile("От купюроприемника в 4 байте получили Accepting");
											// Подтверждаем
											LogFileWriter.WriteInfoToFile("Отправляем на купюроприемник команду 'ACK'");
	        			                    this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);
											break;
			    	                    }

	        			            // REJECTING
		    	                    // Если четвертый бит 1Сh (Rejecting), то купюроприемник не распознал купюру
										case(BillToBillReceivedCodes.GenericRejectingCode):
    	        				        {
											LogFileWriter.WriteInfoToFile("От купюроприемника в 4 байте получили Rejecting");
			        	        	        // Приняли какую-то купюру
												LogFileWriter.WriteInfoToFile("Отправляем на купюроприемник команду 'ACK'");
            	        					    this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);
								
												LogFileWriter.WriteInfoToFile("Генерируем событие 'OnBillReceived'");
                					            OnBillReceived(new BillReceivedEventArgs(BillRecievedStatus.Rejected, 0, this.ErrorList.Errors[byteResult[4]]));
											break;
				                	    }

            		            	// ESCROW POSITION
	                		        // купюра распознана
										case(BillToBillReceivedCodes.EscrowPosition):
				    	    	        {
											LogFileWriter.WriteInfoToFile("От купюроприемника в 4 байте получили EscrowPosition");
											// Подтветждаем
												LogFileWriter.WriteInfoToFile("Отправляем на купюроприемник команду 'ACK'");
			        		    	    	    this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);
								

            				    	    	// Событие, что купюра в процессе отправки в стек
												LogFileWriter.WriteInfoToFile("Генерируем событие 'OnBillStacking'");
                	    	    				OnBillStacking(new BillStackedEventArgs(CurrencyCodeTable(byteResult[4])));

												LogFileWriter.WriteInfoToFile(String.Format("Значение поля 'ReturnBill'={0}", this.ReturnBill));
											// Если программа отвечает возвратом, то на возврат
	                			    	    	if (this.ReturnBill)
	    	                    				{
				        	                       // RETURN
	    	    			    	                // Если программа отказывает принимать купюру, отправим RETURN
														LogFileWriter.WriteInfoToFile("Отправляем на купюроприемник команду 'RETURN'");
	    	    	        	        			   byteResult = this.SendCommand(BillValidatorCommands.RETURN, CashCodeClass.Response_Timeout);
			    		    	            	       this.ReturnBill = false;
        					    	            }
            	    					        else
				            		    	    {
    	    			        		    	   // STACK
        	            					       // Если равпознали, отправим купюру в стек (STACK)
													LogFileWriter.WriteInfoToFile("Отправляем на купюроприемник команду 'STACK'");
			            	            	    	byteResult = this.SendCommand(BillValidatorCommands.STACK, CashCodeClass.Response_Timeout);
	        			        	        	}

												break;
										}

			    	            // STACKING
    			    	        // Если четвертый бит 17h, следовательно идет процесс отправки купюры в стек (STACKING)
										case(BillToBillReceivedCodes.Stacking):
    	    			    		    {
											LogFileWriter.WriteInfoToFile("От купюроприемника в 4 байте получили Stacking");
						
											LogFileWriter.WriteInfoToFile("Отправляем на купюроприемник команду 'ACK'");
        				        		    this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);

											break;
			            	        	}

      					        // Bill stacked
        			    	    // Если четвертый бит 81h, следовательно, купюра попала в стек
									case(BillToBillReceivedCodes.BillStacked):
		                			{
										LogFileWriter.WriteInfoToFile("От купюроприемника в 4 байте получили BillStacked");
						
										// Подтветждаем
											LogFileWriter.WriteInfoToFile("Отправляем на купюроприемник команду 'ACK'");
        		            			    this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);
									
										LogFileWriter.WriteInfoToFile("Генерируем событие 'OnBillReceived'");
        		        	            OnBillReceived(new BillReceivedEventArgs(BillRecievedStatus.Accepted, CurrencyCodeTable(byteResult[4]), ""));

										break;
				    			    }

	    				    	// RETURNING
    	    				    // Если четвертый бит 18h, следовательно идет процесс возврата
									case(BillToBillReceivedCodes.Returning):
	            		    		{
										LogFileWriter.WriteInfoToFile("От купюроприемника в 4 байте получили Returning");
						
										LogFileWriter.WriteInfoToFile("Отправляем на купюроприемник команду 'ACK'");
		    	                	    this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);

										break;
	    	    		            }

		    	 	    	    // BILL RETURNING
    		    		    	// Если четвертый бит 82h, следовательно купюра возвращена
									case(BillToBillReceivedCodes.BillReturned):
				    	    	   	{
										LogFileWriter.WriteInfoToFile("От купюроприемника в 4 байте получили BillReturned");
						
										LogFileWriter.WriteInfoToFile("Отправляем на купюроприемник команду 'ACK'");
    		    	    	    		this.SendNonResponseCommand(BillValidatorCommands.ACK, CashCodeClass.Ack_Timeout);

										break;
        				    	    }
							
	     	    			    // Drop Cassette out of position
    	    	    			// Снят купюроотстойник
									case(BillToBillReceivedCodes.DropCassetteOutOfPosition):
			            			{
										LogFileWriter.WriteInfoToFile("От купюроприемника в 4 байте получили DropCassetteOutOfPosition");
						
								
	    			               		if (this.CassetteStatus != BillCassetteStatus.Removed)
			    		    	       	{
											LogFileWriter.WriteInfoToFile(String.Format("Установим значение поля '' в значение '{0}' и сгенерируем событие '{1}'", BillCassetteStatus.Removed));
    		    			    	    	// fire event
	    		    				        	this.CassetteStatus = BillCassetteStatus.Removed;
		    		    				    	OnBillCassetteStatus(new BillCassetteEventArgs(this.CassetteStatus));
    		    		    			}
										else
										{
											LogFileWriter.WriteInfoToFile("Значение поля 'CassetteStatus' уже установлено в значение 'BillCassetteStatus.Removed'");
										}

										break;
	        			    		}
								
			        	        // Initialize
    			        	    // Кассета вставлена обратно на место
									case(BillToBillReceivedCodes.Initialize):
	    				    	    {
										LogFileWriter.WriteInfoToFile("От купюроприемника в 4 байте получили Initialize");
    	    		    				if (this.CassetteStatus == BillCassetteStatus.Removed)
					            		{
											LogFileWriter.WriteInfoToFile(String.Format("Установим значение поля '' в значение '{0}' и сгенерируем событие '{1}'", BillCassetteStatus.Inplace));
    					            		// fire event
	    			    			           	this.CassetteStatus = BillCassetteStatus.Inplace;
	    	    			       				OnBillCassetteStatus(new BillCassetteEventArgs(this.CassetteStatus));
				    	    			}
										else
										{
											LogFileWriter.WriteInfoToFile("Значение поля 'CassetteStatus' не равно значению 'BillCassetteStatus.Removed'");
										}

										break;
    				    	    	}
	       	    		        }
    	       	    	}
            		}
	            	catch(Exception exception)
    	        	{
						LogFileWriter.WriteInfoToFile(String.Format("В методе 'Listener_ElapsedEventHandler' произошло исключение: '{0}', которое было проигнорировано", exception.Message));
					}
	            	finally
	    	        {
    	    	        // Если таймер выключен, то запускаем
        		    	    if (!this.Listener.Enabled && this.IsListening)
							{
                	    		this.Listener.Start();
							}
        		   	}
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'Listener_ElapsedEventHandler'");
        		}
			#endregion


			#region Метод для преобразования байтов в Enum
				protected T ConvertByteToEnum<T> (Byte Val) where T : struct, IConvertible
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'ConvertByteToEnum'");
			
					T returnedValue;

					if (typeof(T).IsEnum == false) 
					{
						LogFileWriter.WriteInfoToFile(String.Format("В методе 'ConvertByteToEnum' генерируем исключение: 'Тип T='{0}' не является перечислителем'", typeof(T)));
				
						throw new ArgumentException (String.Format ("Тип T='{0}' не является перечислителем", typeof(T)));
					}
					else 
					{
						if(Enum.IsDefined(typeof(T), Val))
						{
							returnedValue=(T)Enum.ToObject(typeof(T), Val);
						}
						else
						{
							LogFileWriter.WriteInfoToFile(String.Format("В методе 'ConvertByteToEnum' генерируем исключение: 'Невозможно преобразовать байты: '{0}' в перечислитель '{1}''", Val, typeof(T)));
							throw new ArgumentException (String.Format ("Невозможно преобразовать байты: '{0}' в перечислитель '{1}'", Val, typeof(T)));
						}
					}
			

					LogFileWriter.WriteInfoToFile("Выходим из метода 'ConvertByteToEnum'");			
			
					return returnedValue;
				}
			#endregion

    	}
	#endregion

    /// <summary>
    /// Класс аргументов события получения купюры в купюроприемнике
    /// </summary>
    public class BillReceivedEventArgs : EventArgs
    {
		#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА BillReceivedEventArgs
	        public BillRecievedStatus Status { get; protected set; }
    	    public UInt16 Value { get; protected set; }
        	public String RejectedReason { get; protected set; }
		#endregion

		#region Конструктор класса BillReceivedEventArgs (с параметрами)
	        public BillReceivedEventArgs(BillRecievedStatus Status, UInt16 Value, String RejectedReason)
    	    {
				LogFileWriter.OutputStackTrace();
				LogFileWriter.WriteInfoToFile("Зашли в метод 'BillReceivedEventArgs'");
				
        	    this.Status = Status;
            	this.Value = Value;
	            this.RejectedReason = RejectedReason;
			
				LogFileWriter.WriteInfoToFile("Выходим из метода 'BillReceivedEventArgs'");	
    	    }
		#endregion
    }


    public class BillCassetteEventArgs : EventArgs
    {
		#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА BillCassetteEventArgs
	        public BillCassetteStatus Status { get; protected set; }
		#endregion

		#region Конструктор класса BillCassetteEventArgs (с параметрами)
	        public BillCassetteEventArgs(BillCassetteStatus Status)
    	    {
				LogFileWriter.OutputStackTrace();
				LogFileWriter.WriteInfoToFile("Зашли в метод 'BillCassetteEventArgs'");
			
        	    this.Status = Status;
				
				LogFileWriter.WriteInfoToFile("Выходим из метода 'BillCassetteEventArgs'");	
	        }
		#endregion
    }


    public class BillStackedEventArgs : CancelEventArgs
    {
		#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА BillStackedEventArgs
			public UInt16 Value { get; protected set; }
		#endregion

		#region Конструктор класса BillStackedEventArgs (с параметрами)
	        public BillStackedEventArgs(UInt16 Value)
    	    {
				LogFileWriter.OutputStackTrace();
				LogFileWriter.WriteInfoToFile("Зашли в метод 'BillStackedEventArgs'");
			
        	    this.Value = Value;
            	this.Cancel = false;
			
				LogFileWriter.WriteInfoToFile("Выходим из метода 'BillStackedEventArgs'");	
        	}
		#endregion
    }



}

