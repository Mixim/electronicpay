using System;
using System.Text.RegularExpressions;



namespace ElectronicPay
{
	public class ServiceClass
	{
		#region Необходимые константы
			public const String DigitPattern=@"\d+";//патерн для получения из строки вырезки чисел
		
			protected const UInt32 Polynomial= 0x08408;	//полином, используемый для получения CRC16-кода
		#endregion


		#region Метод для получения из строки регулярного выражения
			public static String GetRegularString (String Value, String Pattern)
			{
				String returnedValue = String.Empty;
		
				MatchCollection matches = Regex.Matches (Value, Pattern);
		
				if (matches.Count > 0) {
					foreach (Match match in matches) {
						returnedValue += match.Value;
					}
				}
		
				return returnedValue;
			}
		#endregion
		
		#region Метод для получения CRC16-кода строки
			public static UInt32 GetCRC16(Byte[] bufData, Int32 sizeData)
			{
				UInt32 returnedValue=0, i, j;
				for(i=0; i < sizeData; i++)
				{
					returnedValue ^= bufData[i];
					for(j=0; j < 8; j++)
					{
						//если последний бит равен 1
						if((returnedValue & 0x0001)==1)
						{
							returnedValue >>= 1;
							returnedValue ^= ServiceClass.Polynomial;
						}
						else 
						{
							returnedValue >>= 1;
						}
					}
				}
				return returnedValue;
			}
		#endregion
		
		#region Метод для перевода числа из Int32 в BCD-формат
			public static Byte[] ConvertInt32ToBCD(Int32 Int32Value)
			{
				const Int32 MinValueForConvertingToBCD=0;
				const Int32 MaxValueForConvertingToBCD=9999;
		        if(Int32Value<MinValueForConvertingToBCD || Int32Value>MaxValueForConvertingToBCD)
				{
    		        throw new ArgumentOutOfRangeException(String.Format("Converting value is less than {0} or more than {1}", MinValueForConvertingToBCD, MaxValueForConvertingToBCD));
				}
				else
				{
	    	    	Byte[] returnedValue;
					Int32 bcd=0;
					Int32 nibble;
			        for(Int32 i=0;i<4;i++)
					{
    		    	    nibble=Int32Value%10;
						bcd|=nibble<<(i*4);
						Int32Value/=10;
	    		    }
				
					returnedValue=new Byte[2];
						returnedValue[0]=Convert.ToByte((bcd>>8)&0xFF);
						returnedValue[1]=Convert.ToByte((bcd&0xFF));
				
        			return returnedValue;
				}
    		}
		#endregion
		
		#region Метод, необходимый для изменения размера шрифта
			public static void ChangeFontSize(Int32 FontSize, params Gtk.Label[] SomeLabels)
			{
				const String FontSizeFormat="<b><span size='{0}'>{1}</span></b>";
			
				for(Int32 i=0;i!=SomeLabels.Length;i++)
				{
					SomeLabels[i].LabelProp=String.Format(FontSizeFormat, FontSize, SomeLabels[i].LabelProp);
				}
			}
		#endregion
	}
}

