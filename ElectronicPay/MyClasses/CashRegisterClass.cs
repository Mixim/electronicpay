using System;
using System.IO.Ports;
using System.Collections.Generic;
using System.Text;

namespace ElectronicPay
{
	#region Класс, содержащий все управляющие символы протокола контрольно-кассовой машины(ККМ)
		public enum ControlledCharacterOfCashRegisterProtocol : byte	{ ENQ=0x05, ACK=0x06, STX=0x02, ETX=0x03,
																			EOT=0x04, NAK=0x15, DLE=0x10,
																			InitialValue=0
																		}
	#endregion
	
	#region Перечислитель, содержащий ВСЕ команды верхнего уровня протокола ККМ
		public enum TopLevelCommandsOfCashRegisterProtocol : byte {RequestStatus=0x3F, Annulment=0x41, LockAndUnlock=0x42,
																	DiscountAndPremium=0x43, StartReadingControlTape=0x44,
																	RequestStatusCode=0x45, ReadTable=0x46, Beep=0x47,
																	ExitFromCurrentMode=0x48, PostingCash=0x49, CloseCheckWithTapping=0x4A,
																	TimeProgramming=0x4B, PrintString=0x4C, RequestCashInBox=0x4D,
																	Reversal=0x4E, PayingOut=0x4F, ProgrammingOfTable=0x50,
																	Registration=0x52, StartBlankingControlTape=0x54, Entering=0x56,
																	Repayment=0x57, XReport=0x58, CancellationOfAllOfCheck=0x59,
																	ZReport=0x5A, EnterSerialNumber=0x61, Fiscalization=0x62,
																	RequestDateRangeAndShifts=0x63, ProgrammingOfDate=0x64, 
																	FiscalReportByDateRange=0x65, FiscalReportForRangeOfShifts=0x66,
																	StartRemovingReportWithoutBlanking=0x67, ReceiptOfNextBlockOfDataControlTape=0x68,
																	EntryDecimalPointPosition=0x69, PositioningOfSpecificCheck=0x6A,
																	TechnologicalReset=0x6B, PrintingClicheOfCheck=0x6C, EnterSecurityCode=0x6D,
																	TestRun=0x6E, InitializationTablesByInitialValues=0x71, 
																	PrintBottomOfCheck=0x73, RequestActivationOfSecurityCode=0x74,
																	CutCheck=0x75, TotalExtinction=0x77, UndoLastDiscountOrPremium=0x78,
																	OpenCashDrawer=0x80, DemonstrationRun=0x82, StartReadingSoftwareFromCashRegister=0x83,
																	ReceiptOfNextBlockOfDataSoftwareFromCashRegister=0x84, PulseOpeningOfCashDrawer=0x85,
																	GetNextRowOfPicturesByNumber=0x86, PrintFormattedString=0x87, SoundSignal=0x88,
																	DisplayToMercury140F=0x89, ClearOutAnArrayOfPictures=0x8A, AddPicturesRow=0x8B,
																	GetStatusOfPicturesArray=0x8C, PrintingPicturesByNumber=0x8D, PrintingPicturesFromPC=0x8E,
																	SendDataToPort=0x8F, ImageOptionsInArray=0x90, RegisterReading=0x91, OpenCheck=0x92,
																	PaymentByCheckWithClosing=0x99, OpenShift=0x9A, CancellationOfCheckCalculation=0x9B,
																	StartReadingDump=0x9C, ReceiveVersion=0x9D, CloseImage=0x9E, 
																	BeginPictureReadingByNumber=0x9F, GetTypeOfDevice=0xA5, ActivationOfProtectedElectronicControlTape=0xA6,
																	ClosingArchiveOfProtectedElectronicControlTape=0xA7, PrintResultsOfActivation=0xA8,
																	PrintResultsOfShiftByShiftsNumber=0xA9, PrintControlTapeByShiftsNumber=0xAA, PrintingDocumentsByPDA=0xAB,
																	ReportForDateRanges=0xAC, ReportByShiftsRanges=0xAD, GetStatusOfProtectedElectronicControlTape=0xAE,
																	ExecuteCommandOfProtectedElectronicControlTape=0xAF, SetFiscalStation=0xB0, PrintingDelayedDocuments=0xB1,
																	CompleteFormationOfDelayedDocuments=0xB2, GetLastErrorCode=0xB3, PrintingDocumentsByNumber=0xB6,
																	PrintControlStrip=0xB7,  TaxsRegistration=0xB8, CancellationOfTaxsRegistration=0xB9,
																	RegistrationDiscountOrPremium=0xBA, StatusRequestOfElectronicJournal=0xBE, FormationOfProps=0xBF, 
																	PrintDataFromExternalMedia=0xC0}
	#endregion
	
	#region Класс, необхоимый для подключения и работы с фискальным регистратором
		public class CashRegisterClass
		{
			#region НЕОБХОДИМЫЕ ЭНУМЕРАТОРЫ И ПОДКЛАССЫ КЛАССА CashRegisterClass
				protected enum MaskedBytes : byte {DLE=0x10,
													ETX=0x03};
		
				protected class Timeout{	public const Int32 T1=500;
											public const Int32 T2=2000;
											public const Int32 T3=500;
											public const Int32 T4=500;
											public const Int32 T5=10000;
											public const Int32 T6=500;
											public const Int32 T7=500;
											public const Int32 T8=1000;
										}
			#endregion
			
			#region НЕОБХОДИМЫЕ КОНСТАНТЫ И ПОЛЯ, ДОСТУПНЫЕ ТОЛЬКО ДЛЯ ЧТЕНИЯ КЛАССА CashRegisterClass
				protected const Int32 DataBits=8;
				protected const Parity ParityConst=Parity.None;
				protected const StopBits StopBitsConst=StopBits.One;
				protected const Int32 QuantityOfOverheadBytes=2;
				protected const Int32 ResponseToRequestStatusCodeLength=7;
		
				protected const String NotReceivedRequiredQuantityOfBytesFromDeviceException_Text="От устройства получено '{0}' байт вместо необходимых '{1}' за время '{2}'";
				protected const String SendingDataToPortInfo="На порт отправим следующий массив байт:";
				protected const String ReceivedDataFromPortInfo="С порта получили следующие байты:";
				protected const String ReceivedDataIsUncorrect="От устройства получен ответ: '{0}' вместо ожидаемого '{1}'";
				protected const String ReceivedByteFromPortInfo="С порта получили следующий байт:";		
				protected const String SendingControlledCharacterToPortInfo="На порт отправим следующие байты, представляющие управляющий символ протокола:";
				
				protected readonly Encoding DefaultEncoding=Encoding.Default;
				protected readonly Encoding CashRegisterEncoding=Encoding.GetEncoding(866);
			#endregion
			
			
		
			#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА CashRegisterClass
				protected SerialPort cashRegister_serialPort;
			#endregion
		
			
			#region Конструктор класса CashRegisterClass [с параметрами]
				public CashRegisterClass (String PortName, Int32 BaudRate)
				{
					this.CashRegister_SerialPort=new SerialPort(PortName, BaudRate,CashRegisterClass.ParityConst, CashRegisterClass.DataBits, CashRegisterClass.StopBitsConst);
				}
			#endregion
		
			#region Метод для доступа к полю cashRegister_serialPort как к свойству
				public SerialPort CashRegister_SerialPort
				{
					get
					{
						return this.cashRegister_serialPort;
					}
					set
					{
						this.cashRegister_serialPort=value;
					}
				}
			#endregion
		
			#region Метод для генерации пакета данных, отсылаемого на Com-порт
				protected Byte[] GenerateSendsPackage(TopLevelCommandsOfCashRegisterProtocol Command, params Byte[] Data)
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'GenerateSendsPackage'");
					
					List<Byte> returnedValue=new List<Byte>();
					Byte crc;		
			
					//добавляем Command в пакет
						returnedValue.Add(Convert.ToByte(Command));
					//если блок Data не равен null, добавляем его в List с вызовом метода для маскирования байтов						
						returnedValue.AddRange(this.MaskingBytes(Data));
						
					//добавляем в конец ETX
						returnedValue.Add(Convert.ToByte(ControlledCharacterOfCashRegisterProtocol.ETX));
					//подсчитываем CRC
						crc=this.CalculateCRC(returnedValue.ToArray(), 0, returnedValue.Count);
			
					//добавляем в НАЧАЛО STX
						returnedValue.Insert(0, Convert.ToByte(ControlledCharacterOfCashRegisterProtocol.STX));
					
					//добавляем в конец CRC
						returnedValue.Add(crc);
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'GenerateSendsPackage'");
			
					return returnedValue.ToArray();
				}
			#endregion
		
			#region Метод для генерации пакета данных, отсылаемого на Com-порт
				protected Byte[] GenerateSendsPackage(params Byte[] Data)
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'GenerateSendsPackage'");
			
					List<Byte> returnedValue=new List<Byte>();
					Byte crc;		
			
					//добавляем блок Data
						returnedValue.AddRange(this.MaskingBytes(Data));
						
					//добавляем в конец ETX
						returnedValue.Add(Convert.ToByte(ControlledCharacterOfCashRegisterProtocol.ETX));
					//подсчитываем CRC
						crc=this.CalculateCRC(returnedValue.ToArray(), 0, returnedValue.Count);
			
					//добавляем в НАЧАЛО STX
						returnedValue.Insert(0, Convert.ToByte(ControlledCharacterOfCashRegisterProtocol.STX));
					
					//добавляем в конец CRC
						returnedValue.Add(crc);
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'GenerateSendsPackage'");
			
					return returnedValue.ToArray();
				}
			#endregion
			
			#region Метод для отправки данных на Com-порт
				protected void SendDataToPort(params Byte[] Data)
				{		
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'SendDataToPort'");
			
					Byte[] sendingBytes;
					ControlledCharacterOfCashRegisterProtocol cashRegisterResponse;
			
					//запрашиваем сеанс, отправляя ENQ
						sendingBytes=new Byte[]{
														Convert.ToByte(ControlledCharacterOfCashRegisterProtocol.ENQ)
				               						};
						LogFileWriter.WriteDataToFile(CashRegisterClass.SendingDataToPortInfo, sendingBytes);
						
						this.CashRegister_SerialPort.Write(sendingBytes, 0, sendingBytes.Length);
						
						//в течении времени CashRegisterClass.Timeout.T1 ждем от устройства ответ
							this.CashRegister_SerialPort.ReadTimeout=CashRegisterClass.Timeout.T1;
							cashRegisterResponse=this.ConvertByteToEnum<ControlledCharacterOfCashRegisterProtocol>(Convert.ToByte(this.CashRegister_SerialPort.ReadByte()));
								LogFileWriter.WriteDataToFile(CashRegisterClass.ReceivedDataFromPortInfo, (Byte)cashRegisterResponse);
							//устройство должно было передать ответ ACK
							if(cashRegisterResponse!=ControlledCharacterOfCashRegisterProtocol.ACK)
							{
								LogFileWriter.WriteInfoToFile("Выходим из метода 'SendDataToPort' с генерацией исключения");
								throw new Exception(String.Format(CashRegisterClass.ReceivedDataIsUncorrect, cashRegisterResponse, ControlledCharacterOfCashRegisterProtocol.ACK));
							}
					
					//отправляем блок Data
						sendingBytes=this.GenerateSendsPackage(Data);
						LogFileWriter.WriteDataToFile(CashRegisterClass.SendingDataToPortInfo, sendingBytes);
						
						this.CashRegister_SerialPort.Write(sendingBytes, 0, sendingBytes.Length);
						
						//в течении времени CashRegisterClass.Timeout.T1 ждем от устройства ответ
							this.CashRegister_SerialPort.ReadTimeout=CashRegisterClass.Timeout.T1;
							cashRegisterResponse=this.ConvertByteToEnum<ControlledCharacterOfCashRegisterProtocol>(Convert.ToByte(this.CashRegister_SerialPort.ReadByte()));
								LogFileWriter.WriteDataToFile(CashRegisterClass.ReceivedDataFromPortInfo, (Byte)cashRegisterResponse);
							//устройство должно было передать ответ ACK
							if(cashRegisterResponse!=ControlledCharacterOfCashRegisterProtocol.ACK)
							{
								LogFileWriter.WriteInfoToFile("Выходим из метода 'SendDataToPort' с генерацией исключения");
								throw new Exception(String.Format(CashRegisterClass.ReceivedDataIsUncorrect, cashRegisterResponse, ControlledCharacterOfCashRegisterProtocol.ACK));
							}
					
					//завершаем передачу, отправляя EOT
						sendingBytes=new Byte[]{
														Convert.ToByte(ControlledCharacterOfCashRegisterProtocol.EOT)
				               						};
						LogFileWriter.WriteDataToFile(CashRegisterClass.SendingDataToPortInfo, sendingBytes);
						
						this.CashRegister_SerialPort.Write(sendingBytes, 0, sendingBytes.Length);
			
						LogFileWriter.WriteInfoToFile("Выходим из метода 'SendDataToPort'");
				}
			#endregion
		
			#region Метод для отправки команды на Com-порт (использует метод SendDataToPort)
				public void SendCommandToPort(Byte[] PasswordBCDFormat, TopLevelCommandsOfCashRegisterProtocol Command, params Byte[] Data)
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'SendCommandToPort'");
			
					List<Byte> sendingPackage=new List<Byte>();
					
										
					//в начале идет пароль доступа - два байта (0, 1) в BCD-формате
						sendingPackage.AddRange(PasswordBCDFormat);
					
					//далее, четвертом байте идет команда
						sendingPackage.Add(Convert.ToByte(Command));
			
					//затем идут данные, которые вставляются в sendingData начиная с 5 индекса
					if(Data!=null)
					{
						sendingPackage.AddRange(this.MaskingBytes(Data));
					}
					
					//шлем данные на порт
						this.SendDataToPort(sendingPackage.ToArray());
			
					//
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'SendCommandToPort'");
				}
			#endregion
		
			#region Метод для отправки на порт управляющих символов
				protected void SendControlledCharacterToPort(ControlledCharacterOfCashRegisterProtocol ControlledCharacter)
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'SendControlledCharacterToPort'");
			
					Byte[] sendingByte;
			
					sendingByte=new Byte[]{Convert.ToByte(ControlledCharacter)};
			
						LogFileWriter.WriteDataToFile(CashRegisterClass.SendingControlledCharacterToPortInfo, sendingByte);
			
					this.CashRegister_SerialPort.Write(sendingByte, 0, sendingByte.Length);
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'SendControlledCharacterToPort'");
				}
			#endregion
			
			#region Метод для получения массива данных с Com-порта
				public Byte[] ReceiveDataFromPort(Int32 AmountOfNeededData, Int32 Timeout)
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'ReceiveDataFromPort'");
			
					Byte[] returnedValue=new Byte[AmountOfNeededData];
					Byte receivedByte;
					ControlledCharacterOfCashRegisterProtocol receivedCharacter=ControlledCharacterOfCashRegisterProtocol.InitialValue;
					Boolean convertResult;
			
					//устанавливаем тайминг
						this.CashRegister_SerialPort.ReadTimeout=Timeout;
					
					//ждем получения байта ENQ, который сигнализирует о том, что фискальник хочет отослать какой-то блок данных
						do
						{
							receivedByte=this.ReceiveByteFromPort(Timeout);
							convertResult=this.TryConvertByteToEnum<ControlledCharacterOfCashRegisterProtocol>(receivedByte, ref receivedCharacter);
						}
						while((receivedCharacter!=ControlledCharacterOfCashRegisterProtocol.ENQ) || (convertResult!=true));
			
					//подтверждаем готовность получать информацию
						this.SendControlledCharacterToPort(ControlledCharacterOfCashRegisterProtocol.ACK);
			
					//пытаемся считать необходимое количество байт с com-порта
						this.CashRegister_SerialPort.Read(returnedValue, 0, AmountOfNeededData);					
			
					//подтверждаем прием
						this.SendControlledCharacterToPort(ControlledCharacterOfCashRegisterProtocol.ACK);
			
					//ждем получения EOT
						do
						{
							receivedByte=this.ReceiveByteFromPort(Timeout);
							convertResult=this.TryConvertByteToEnum<ControlledCharacterOfCashRegisterProtocol>(receivedByte, ref receivedCharacter);
						}
						while((receivedCharacter!=ControlledCharacterOfCashRegisterProtocol.EOT) || (convertResult!=true));
			
			
					//вывести полученный байт в лог
						LogFileWriter.WriteDataToFile(CashRegisterClass.ReceivedByteFromPortInfo, returnedValue);
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'ReceiveDataFromPort'");
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для получения одного байта данных с Com-порта (НУЖНО БУДЕТ НЕМНОГО ИСПРАВИТЬ ЭТОТ МЕТОД, ЧТОБЫ ОН ОСУЩЕСТВЛЯЛ ОЖИДАНИЕ ПОЛУЧЕНИЯ НЕКОТОРОГО БАЙТА)
				protected Byte ReceiveByteFromPort(Int32 Timeout)
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'ReceiveByteFromPort'");
			
					Byte returnedValue;

			
					
			
					//установим тайминг
						this.CashRegister_SerialPort.ReadTimeout=Timeout;
			
					
			
					//считаем один байт из порта
						returnedValue=Convert.ToByte(this.CashRegister_SerialPort.ReadByte());
					
					//вывести полученный байт в лог
						LogFileWriter.WriteDataToFile(CashRegisterClass.ReceivedByteFromPortInfo, returnedValue);
								
					LogFileWriter.WriteInfoToFile("Выходим из метода 'ReceiveByteFromPort'");
			
					return returnedValue;
				}
			#endregion
		
		
			#region Метод для вычисления CRC
				public Byte CalculateCRC(Byte[] Data, Int32 StartIndex, Int32 EndIndex)
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'CalculateCRC'");
			
					Byte returnedValue=0;
			
					for(Int32 i=StartIndex;i!=EndIndex;i++)
					{
						returnedValue=Convert.ToByte(returnedValue ^ Data[i]);
					}
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'CalculateCRC'");
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для маскирования байтов в отсылаемом пакете
				public Byte[] MaskingBytes(Byte[] Data)
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'MaskingBytes'");
			
					Byte[] returnedValue;
					List<Byte> maskedBytes = new List<Byte>(Data);
			
					for(Int32 i=0;i!=maskedBytes.Count;i++)
					{
						if(Enum.IsDefined(typeof(CashRegisterClass.MaskedBytes), maskedBytes[i]))
						{
							maskedBytes.Insert(i, Convert.ToByte(CashRegisterClass.MaskedBytes.DLE));
							i+=1;
						}
					}
			
					returnedValue=maskedBytes.ToArray();
			
					maskedBytes.Clear();
				
					LogFileWriter.WriteInfoToFile("Выходим из метода 'MaskingBytes'");
			
					return returnedValue;
				}
			#endregion
		
		
							
			#region Метод для преобразования байтов в Enum
				protected T ConvertByteToEnum<T> (Byte Val) where T : struct, IConvertible
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'ConvertByteToEnum'");
			
					T returnedValue;

					if (typeof(T).IsEnum == false) 
					{
						LogFileWriter.WriteInfoToFile(String.Format("В методе 'ConvertByteToEnum' генерируем исключение: 'Тип T='{0}' не является перечислителем'", typeof(T)));
				
						throw new ArgumentException (String.Format ("Тип T='{0}' не является перечислителем", typeof(T)));
					}
					else 
					{
						if(Enum.IsDefined(typeof(T), Val))
						{
							returnedValue=(T)Enum.ToObject(typeof(T), Val);
						}
						else
						{
							LogFileWriter.WriteInfoToFile(String.Format("В методе 'ConvertByteToEnum' генерируем исключение: 'Невозможно преобразовать байты: '{0}' в перечислитель '{1}''", Val, typeof(T)));
							throw new ArgumentException (String.Format ("Невозможно преобразовать байты: '{0}' в перечислитель '{1}'", Val, typeof(T)));
						}
					}
				

					LogFileWriter.WriteInfoToFile("Выходим из метода 'ConvertByteToEnum'");	
			
					return returnedValue;
				}
			#endregion
		
			#region Безопасный метод для преобразования байтов в Enum
				protected Boolean TryConvertByteToEnum<T> (Byte Val, ref T Result) where T : struct, IConvertible
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'TryConvertByteToEnum'");
					
					Boolean returnedValue;

					if (typeof(T).IsEnum == false) 
					{
						LogFileWriter.WriteInfoToFile(String.Format("В методе 'TryConvertByteToEnum' Тип T='{0}' не является перечислителем', преобразование невозможно", typeof(T)));
				
						returnedValue=false;
					}
					else 
					{
						if(Enum.IsDefined(typeof(T), Val))
						{
							Result=(T)Enum.ToObject(typeof(T), Val);
					
							returnedValue=true;
						}
						else
						{
							LogFileWriter.WriteInfoToFile(String.Format("В методе 'TryConvertByteToEnum' неудалось преобразовать байты: '{0}' в перечислитель '{1}'", Val, typeof(T)));
					
							returnedValue=false;
						}
					}
				

					LogFileWriter.WriteInfoToFile("Выходим из метода 'TryConvertByteToEnum'");
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для выполнения команды получения статуса с устройства
				public Byte[] GetStatus(Byte[] PasswordBCDFormat, Int32 CountOfByteForRead, Int32 Timeout)
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'GetStatus'");
			
					Byte[] returnedValue=null;
			
					try
					{
						LogFileWriter.WriteInfoToFile(String.Format("Порт устройства содержит {0} байт еще до отправки команды", this.CashRegister_SerialPort.BytesToRead));
					
				
						this.SendCommandToPort(PasswordBCDFormat, TopLevelCommandsOfCashRegisterProtocol.RequestStatus);
										
				
						returnedValue=this.ReceiveDataFromPort(CountOfByteForRead, Timeout);
					}
					catch(Exception exception)
					{
						LogFileWriter.WriteInfoToFile(String.Format("Получили исключение: {0}", exception.Message));
					}
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'GetStatus'");
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для вывода на печать строки из массива байтов
				protected Byte[] PrintStringByBytes(Byte[] PasswordBCDFormat, Int32 CountOfByteForRead, Int32 Timeout, Byte[] PrintingBytes)
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'PrintBytes'");
			
					Byte[] returnedValue=null;
			
			
					try
					{									
						//this.SendCommandToPort(PasswordBCDFormat, TopLevelCommandsOfCashRegisterProtocol.PrintString, this.ConvertUnicodeValue(PrintingLine, this.DefaultEncoding, this.CashRegisterEncoding));
						//===							
							LogFileWriter.WriteInfoToFile(String.Format("На порт отправляет массив байт, который в строковом представлении выглядит следующим образом:'{0}'", DefaultEncoding.GetString(Encoding.Convert(CashRegisterEncoding, DefaultEncoding, PrintingBytes))));
				       	//===
							this.SendCommandToPort(PasswordBCDFormat, TopLevelCommandsOfCashRegisterProtocol.PrintFormattedString, PrintingBytes);
						
				
					
						returnedValue=this.ReceiveDataFromPort(CountOfByteForRead, Timeout);
					}
					catch(Exception exception)
					{
						LogFileWriter.WriteInfoToFile(String.Format("Получили исключение: {0}", exception.Message));
					}
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'PrintBytes'");
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для распечатки чека (использует метод PrintingLine)
				public Byte[] PrintCashRegisterReceipt(Byte[] PasswordBCDFormat, String CashRegisterReceipt, Byte Flags, Byte Printer, Byte ModeOfCheckTape, Byte ModeOfCashRegisterTape, Int32 MaxTextLength, Int32 CountOfByteForRead, Int32 Timeout)
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'PrintCashRegisterReceipt'");
			

			
					//=========
						Byte[] printLineResult=null;
						LinkedList<Byte[]> parsedString=BytesCreatorForCashRegister.GenerateSendingBytes(CashRegisterReceipt, Flags, Printer, ModeOfCheckTape, ModeOfCashRegisterTape, MaxTextLength, DefaultEncoding, CashRegisterEncoding);
						foreach(Byte[] currentNode in parsedString)
						{							
							printLineResult=this.PrintStringByBytes(PasswordBCDFormat, CountOfByteForRead, Timeout, currentNode);
							LogFileWriter.WriteDataToFile("'PrintStringByBytes' вернул следующие данные:", printLineResult);
						}
					//=========
			
					LogFileWriter.WriteInfoToFile("Выходим из метода 'PrintCashRegisterReceipt'");
			
					return printLineResult;
				}
			#endregion
		
			#region Метод для перевода строки из одной кодировки в байты другой кодировки (судя по мануалам, фискальник понимает только кодировку ASCII)
				public Byte[] ConvertUnicodeValue(String Value, Encoding DefaultEncoding, Encoding ResultingEncoding)
				{
					Byte[] defaultBytes;					
					Byte[] returnedValue;
					
					//получаем байты из строки в кодировке DefaultEncoding
						defaultBytes=DefaultEncoding.GetBytes(Value);
			
					//производим декодирование первоначальных байтов в необходимую кодировку ResultingEncoding				
						returnedValue=Encoding.Convert(DefaultEncoding, ResultingEncoding, defaultBytes);
					
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для проерки доступности и работоспособности фискального регистратора
				public Boolean CheckAvailabilityOfCashRegister(Byte[] PasswordBCDFormat)
				{
					LogFileWriter.OutputStackTrace();
					LogFileWriter.WriteInfoToFile("Зашли в метод 'CheckAvailabilityOfCashRegister'");
					Boolean returnedValue;
					Byte[] receivedData;
					
					//===tmp===
						LinkedList<Byte> receivedBytes=new LinkedList<Byte>();
						this.CashRegister_SerialPort.ReadTimeout=5000;
						try
						{
							//пока ни сгенерируется исключение, считывать данные с com-порта
							do
							{
								receivedBytes.AddLast(Convert.ToByte(this.CashRegister_SerialPort.ReadByte()));
							}
							while(true);
						}
						catch(TimeoutException)
						{
							LogFileWriter.WriteDataToFile("Перед отправкой команды для проверки методом 'CheckAvailabilityOfCashRegister' на порту находились следующие байты:", receivedBytes);
						}
					//=========
			
					try
					{
						this.SendCommandToPort(PasswordBCDFormat, TopLevelCommandsOfCashRegisterProtocol.RequestStatusCode);
				
						receivedData=this.ReceiveDataFromPort(CashRegisterClass.ResponseToRequestStatusCodeLength, CashRegisterClass.Timeout.T5);
				
						if(receivedData[3]==0)
						{
							returnedValue=true;
						}
						else
						{
							returnedValue=false;
						}
					}
					catch(Exception exception)
					{
						LogFileWriter.WriteInfoToFile(String.Format("Получили исключение: '{0}'", exception.Message));
						returnedValue=false;
					}
					
					LogFileWriter.WriteInfoToFile(String.Format("Выходим из метода 'CheckAvailabilityOfCashRegister', который возвращает: '{0}'", returnedValue));
					return returnedValue;
				}
			#endregion
		
			#region Метод для выполнения Z-отчета на фискальном регистраторе
				public void MakeZReport()
				{
					
				}
			#endregion
		
			#region Метод для выполнения X-отчета на фискальном регистраторе
				public void MakeXReport()
				{
				}
			#endregion
		}
	#endregion
}

