using System;
using System.Text;
using System.Net;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Globalization;

namespace ElectronicPay
{
	/*
	 	Каждый платеж в системе ОСМП имеет уникальный идентификатор, который передается провайдеру
	 	в переменной txn_id - целое число длинной до 20 знаков. По этому идентификатору производится
	 	дальнейшая сверка взаиморасчетов и решение спорных вопросов
	 	
	 	Сумма платежа принимается от абонента и передается провайдеру в рублях в переменной sum - 
	 	дробное число с точностью до сотых, в качестве разделителя используется "." (точка). Если сумма 
	 	представляет целое число, оно все равно дополняется точкой и нулями, например "152.00"
	 	
	 	В запросе на добавление платежа система передает дату платежа (дату получения запроса от 
	 	клиента) в переменной txn_date в формате ГГГГММДДЧЧММСС.
	 	
	 	Провайдер идентифицирует своего абонента по уникальному идентификатору (номер лицевого счета,
	 	телефона, логин и т.д.). Перед отправкой провайдеру идентификатор проходит проверку
	 	корректности в соответствии с регулярным выражением, которое должен предоставить провайдер.
	 	Идентификатор абонента передается в переменной account - строка, содержащая буквы, цифры и 
	 	спецсимволы, длинной до 50 символов.
	 	
	 	Передача информации о платеже провайдеру производится системой в 2 этапа: проверка состояния 
	 	счета абонента и, непосредственно, проведение платежа. Тип запроса передается системой в 
	 	переменной command - строка, принимающая значения "check" и "pay". При проверке статуса 
	 	(запрос "check") провайдер должен проверить наличие в своей базе абонента с указанным 
	 	идентификатором и выполнить внутренние проверки идентификатора и суммы платежа в 
	 	соответствии с принятой логикой пополнения лицевых счетов через платежные системы. При 
	 	проведении платежа (запрос "pay") провадйер должен произвести пополнение баланса счета
	 	абонента.
	 	
	 	В случае, когда любой из запросов провайдеру завершается ошибкой, провайдер возвращает код 
	 	ошибки в соответствии с таблицей. Все ошибки имеют признак 
	 	фатальности. Для системы фатальная ошибка означает, что повторная отправка запроса с теми же
	 	параметрами приведет к 100% повторению той же ошибки, следовательно, система прекращает 
	 	обработку клиентского запроса и завершает его с ошибкой. Нефатальная ошибка означает для 
	 	системы, что повтоение запроса с теми же параметрами через некоторый промежуток времени,
	 	возможно, приведет к успеху. Система будет повторять запросы, завершающиеся нефатальной 
	 	ошибкой, постоянно увеличивая интервал, пока операция не завершится успехом или фатальной 
	 	ошибкой либо пока не истечет срок жизни запроса - 24 часа. Отсутствие связи с сервером 
	 	провайдера является нефатальной ошибкой. Отсутствие в ответе элемента <result> (некорректный 
	 	XML, страница Service temporarily unavailable и т.д.) является фатальной ошибкой. Запросы получают
	 	отказ с ошибкой 300 - Другая ошибка провайдера.
	 	
	 	В базе провайдера не должно содержаться двух успешно проведенных платежей с одним и тем же 
	 	номером txn_id. Если система повторно присылает запрос с уже существующим в базе провайдера 
	 	txn_id, провайдер должен вернуть результат обработки предыдущего запроса.
	 	
	 	Провайдер возвращает ответ на запросы системе в формате XML со следующей структурой:
		 		<?xml version="1.0" encoding="UTF-8"?>
		 		<response>
		 			<osmp_txn_id></osmp_txn_id>
		 			<prv_txn></prv_txn>
			 		<sum></sum>
		 			<result></result>
	 				<comment></comment>
			 	</response>
			
			<response> - тело ответа;
			<osmp_txn_id> - номер транзакции в системе, который передается провайдеру в переменной
			txn_id;
			<prv_txn> - уникальный номер операции пополнения баланса абонента (в базе провайдера), целое 
			число длинной до 20 знаков. Этот элемент должен возвращаться провайдером после запроса на 
			пополнение баланса (запроса "pay"). При ответе на запрос на проверку состояния счета абонента 
			(запрос "check") его возвращать не нужно - не обрабатывается;
			<sum> - сумма платежа, передаваемая провайдеру, дробное число с точностью до сотых, в качестве 
			разделителя используется "."(точка). Если сумма представляет целое число, оно все равно 
			дополняется точками и нулями, например - "152.00";
			<result> - код результата завершения запроса;
			<comment> - необязательный элемент - комментарий завершения операции.
	 */
	
	
	#region Класс, необходимый для представления разобранного ответа от сервера
		public class ServerResponse
		{
			#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА ServerResponse
				public Boolean IsComplete{get; set;}
				public Boolean IsSuccessfullyExecuted{get;set;}
				public String Result{get; set;}
				public String Comment{get; set;}
				public String Prv_Txn{get; set;}
			#endregion
		
			#region Конструктор класса ServerResponse[с параметрами]
				public ServerResponse(Boolean IsComplete, Boolean IsSuccessfullyExecuted, String Result, String Comment, String Prv_Txn)
				{
					this.IsComplete=IsComplete;
					this.IsSuccessfullyExecuted=IsSuccessfullyExecuted;
					this.Result=Result;
					this.Comment=Comment;
					this.Prv_Txn=Prv_Txn;
				}
			#endregion
			
		}
	#endregion
	
	#region Класс, необходимый для проведения платежей
		public class OSMPClass
		{
			#region НЕОБХОДИМЫЕ КОНСТАНТЫ И ПЕРЕМЕННЫЕ С ДОСТУПОМ ТОЛЬКО ДЛЯ ЧТЕНИЯ КЛАССА OSMPClass											
				//константа, необходимая для перевода числового формата представления суммы в строковый, который будет передаваться на сервер
					protected readonly static NumberFormatInfo SumNumberFormatInfo=NumberFormatInfo.InvariantInfo;
		
				//константа для генерирования запросов на проведения платежей на сервер ('{0}' - URL; '{1}' - порт; '{2}' - платежное приложение провайдера; '{3}' - команда; '{4}' - уникальный идентификатор платежа; {5} - идентификатор абонента; {6} - сумма платежа; {7} - дата и время платежа)
					protected const String PaymentRequest_Format="{0}:{1}/{2}?command={3}&txn_id={4}&txn_date={7}&account={5}&sum={6}";
				
				//константа для генерирования запросов на проверку лицевого счета абонента
					protected const String CheckRequest_Format="{0}:{1}/{2}?command={3}&txn_id={4}&account={5}&sum={6}";
		
				//символы форматирования ответа от сервера, которые необходимо удалить для верного разбора
					protected const String RemovingSymbolsFromServerResponse="[\n\t\r]";
					protected const Char PaddingCharForTxn_Id='0';
					protected const String TxnDate_Format="yyyyMMddHHmmss";
				//константы для разбора ответа сервера на запрос об оплате
		
					//имя поля с кодировкой в паттерне ResponseToPayment_Pattern
						protected const String EncodingValueName="encoding_val";
					//имя поля с номером платежа в паттерне ResponseToPayment_Pattern
						protected const String OSMPTxnIdValueName="osmpTxnId_val";
					//имя поля с уникальным номером операции пополнения баланса абонента (в базе провайдера) в паттерне ResponseToPayment_Pattern
						protected const String PrvTxnValueName="prvTxn_val";
					//имя поля с результатом проведения операции в паттерне ResponseToPayment_Pattern
						protected const String ResultValueName="result_val";
					//имя поля с комментариями в паттерне ResponseToPayment_Pattern
						protected const String CommentValueName="comment_val";
					//паттерн для разбора ответа сервера на запрос о пополнении счета
						protected const String  ResponseToPayment_Pattern="<?xml version=\"(?<encodingVersion_val>[^,]*)\" encoding=\"(?<encoding_val>[^,]*)\"\\?><response><result>(?<result_val>[0-9]*)</result><osmp_txn_id>(?<osmpTxnId_val>[0-9]*)</osmp_txn_id><prv_txn>(?<prvTxn_val>[0-9]*)</prv_txn><sum>(?<sum_val>[0-9]*\\.*[0-9]*)</sum><comment>(?<comment_val>[^,]*)</comment></response>";
					//паттерн для разбора ответа сервера на запрос о корректности будующего запроса
						protected const String ResponseToCheck_Pattern="<?xml version=\"(?<encodingVersion_val>[^,]*)\" encoding=\"(?<encoding_val>[^,]*)\"\\?><response><result>(?<result_val>[0-9]*)</result><osmp_txn_id>(?<osmpTxnId_val>[0-9]*)</osmp_txn_id><comment>(?<comment_val>[^,]*)</comment></response>";
				
				//константы с результатами запросов
		
					//константа с текстовым содержимым для Ok
						protected const String OkResponse_Value="Ok";
					//константа с ключом для Ok
						protected const Int32 OkResponse_Key=0;
					//константа с текстовым содержимым для TemporaryError
						protected const String TemporaryErrorResponse_Value="Временная ошибка. Повторите запрос позже";
					//константа с ключом для TemporaryError
						protected const Int32 TemporaryErrorResponse_Key=1;
					//константа с текстовым содержимым для InvalidFormatOfCallerID
						protected const String InvalidFormatOfCallerID_Value="Неверный формат идентификатора абонента";
					//константа с ключом для InvalidFormatOfCallerID
						protected const Int32 InvalidFormatOfCallerID_Key=4;
					//константа с текстовым содержимым для СallerIDIsNotFound
						protected const String СallerIDIsNotFound_Value="Идентификатор абонента не найден (Ошиблись номером)";
					//константа с ключом для СallerIDIsNotFound
						protected const Int32 СallerIDIsNotFound_Key=5;
					//константа с текстовым содержимым для ReceptionOfPaymentIsDeniedByProvider
						protected const String ReceptionOfPaymentIsDeniedByProvider_Value="Прием платежа запрещен провайдером";
					//константа с ключом для ReceptionOfPaymentIsDeniedByProvider
						protected const Int32 ReceptionOfPaymentIsDeniedByProvider_Key=7;
					//константа с текстовым содержимым для ReceptionOfPaymentIsDeniedForTechnicalReasons
						protected const String ReceptionOfPaymentIsDeniedForTechnicalReasons_Value="Прием платежа запрещен по техническим причинам";
					//константа с ключом для ReceptionOfPaymentIsDeniedForTechnicalReasons
						protected const Int32 ReceptionOfPaymentIsDeniedForTechnicalReasons_Key=8;
					//константа с текстовым содержимым для SubscriberAccountIsNotActive
						protected const String SubscriberAccountIsNotActive_Value="Счет абонента не активен";
					//константа с ключом для  SubscriberAccountIsNotActive
						protected const Int32  SubscriberAccountIsNotActive_Key=79;
					//константа с текстовым содержимым для EffectPaymentIsNotCompleted
						protected const String EffectPaymentIsNotCompleted_Value="Проведение платежа не окончено";
					//константа с ключом для EffectPaymentIsNotCompleted
						protected const Int32 EffectPaymentIsNotCompleted_Key=90;
					//константа с текстовым содержимым для AmountIsTooSmall
						protected const String AmountIsTooSmall_Value="Сумма слишком мала";
					//константа с ключом для AmountIsTooSmall
						protected const Int32 AmountIsTooSmall_Key=241;
					//константа с текстовым содержимым для AmountIsTooHigh
						protected const String AmountIsTooHigh_Value="Сумма слишком велика";
					//константа с ключом для AmountIsTooHigh
						protected const Int32 AmountIsTooHigh_Key=242;
					//константа с текстовым содержимым для UnableToVerifyAccountStatus
						protected const String UnableToVerifyAccountStatus_Value="Невозможно проверить состояние счета";
					//константа с ключом для UnableToVerifyAccountStatus
						protected const Int32 UnableToVerifyAccountStatus_Key=243;
					//константа с текстовым содержимым для AnotherProvidersError
						protected const String AnotherProvidersError_Value="Другая ошибка провайдера";
					//константа с ключом для AnotherProvidersError
						protected const Int32 AnotherProvidersError_Key=300;
				//константа, необходимая для конвертирования ожидаемого ответа сервера на запрос в строковое представление
					protected const String ExpectedServerResponse_Format="От сервера получен код: '{0}', который можно интерпретировать следующим образом: '{1}'";
				//константа, необходимая для конвертирования не ожидаемого ответа сервера на запрос в строковое представление
					protected const String UnexpectedServerResponse_Format="От сервера получен код: '{0}', который не поддается идентификации, возможно он не является ожидаемым";
				//константа, необходимая для представления ответа на сервера на запрос о проведении платежа, когда формат ответа не соответствует требуемому
					protected const String UncorrectServerResponse_Format="Некорректный формат ответа от сервера: '{0}'";
				//константа, необходимая для генерации ответа, когда txn_id, полученный от сервера не совпадает с передаваемым
					protected const String UncorrectTxnId_Format="От сервера получен txn_id='{0}', вместо ожидаемого: '{1}'";
		
				//исходная кодировка терминала
					protected readonly static Encoding DefaultEncoding=Encoding.Default;
				//кодировка для формирования ответов в системе
					protected readonly static Encoding BillingSystemEncoding=Encoding.UTF8;
				//максимальная скорость ответа (время, миллисекунды)
					protected const Int32 WaitResponceTimeout=60000;
				//словарь со всеми возможными результатами выполнения операции проведения платежа
					protected static Dictionary<Int32, String> CodeResultOfPayment=null;
			#endregion
		
		
		
			#region Метод для отправки сообщения на сервер и получения ответа от него
				protected static HttpWebResponse SendRequestToAndGetResponseFromServer(String Login, String Password, WebProxy Proxy, String RequestFormat, params String[] RequestArguments)
				{
					LogFileWriter.OutputStackTrace();
			
					String request;
					HttpWebRequest httpRequestToWeb;
					HttpWebResponse returnedValue;
					
					//создаем строковый запрос
						request=String.Format(RequestFormat, RequestArguments);
						LogFileWriter.WriteInfoToFile(String.Format("Строковый запрос в кодировке по-умолчанию: '{0}'", request));
			
					//изменяем кодировку строкового запроса с DefaultEncoding на BillingSystemEncoding
						request=BillingSystemEncoding.GetString(Encoding.Convert(DefaultEncoding, BillingSystemEncoding, DefaultEncoding.GetBytes(request)));
						LogFileWriter.WriteInfoToFile(String.Format("После изменения кодировки строкового запроса, получили следующую строку, которая будет представлять собой запрос в биллинг: '{0}'", request));
			
					//создаем http-запрос
						httpRequestToWeb=(WebRequest.Create(request) as HttpWebRequest);
					
					//укажем, что для запроса необходимо использовать прокси, информация о котором передана в метод (если null, то передача запроса будет осуществлена без прокси)
						httpRequestToWeb.Proxy=Proxy;
						LogFileWriter.WriteInfoToFile(String.Format("Будем использовать следующий прокси-сервер для получения доступа к биллингу: {0}", httpRequestToWeb.Proxy));
			
					//зададим переданные в метод логин и пароль для выполнения http-запроса
						httpRequestToWeb.Credentials=new NetworkCredential(Login, Password);
						LogFileWriter.WriteInfoToFile(String.Format("Для идентификации в биллинге будем использовать логин: '{0}' и пароль: '{1}'", Login, Password));
					
					//установим тайминг
						httpRequestToWeb.Timeout=10000;
			
					//исполним http-запрос
						returnedValue=(httpRequestToWeb.GetResponse() as HttpWebResponse);
			
					LogFileWriter.WriteInfoToFile("Выходим из метода: 'SendRequestToAndGetResponseFromServer'");
					
					return returnedValue;
				}
			#endregion
				
			
		
		
			#region Метод для выполнения запросов на сервер (возвращает информацию о том, прошел ли платеж или нет)
				protected static ServerResponse ExecuteCommand(String RequestFormat, String ServerURL, Int32 ServerPort, String Login, String Password, 
		                                               			String PaymentApplicationOfProvider, String Command, 
		                                               			String Txn_Id, String Txn_Date, 
		                                               			String Account, Single Sum, 
		                                               			WebProxy Proxy,
		                                               			String ResponseFormat, params String[] ArgsOfResponse)
				{
					HttpWebResponse serverWebResponse;
					ServerResponse returnedValue;
					List<Byte> readedBytes=new List<Byte>();					
					String response;
					Dictionary<String, String> valuesOfResponse=new Dictionary<String, String>();
					
			
					//делаем запрос на сервер
						serverWebResponse=OSMPClass.SendRequestToAndGetResponseFromServer(Login, Password, Proxy, RequestFormat, ServerURL, ServerPort.ToString(), PaymentApplicationOfProvider, Command, Txn_Id, Account, Sum.ToString(OSMPClass.SumNumberFormatInfo), Txn_Date);			
			
								
						//считываем все данные из потока ответа, который получили от сервера и добавляем их в список readedBytes
							readedBytes.AddRange(OSMPClass.ReadToEnd(serverWebResponse.GetResponseStream(), OSMPClass.WaitResponceTimeout));
							LogFileWriter.WriteDataToFile("В ответ на запрос в биллинг получили следующий поток байт:", readedBytes.ToArray());
			
			
					//преобразуем полученные байты в строку с кодировкой DefaultEncoding
						response=DefaultEncoding.GetString(Encoding.Convert(BillingSystemEncoding, DefaultEncoding, readedBytes.ToArray()));
						LogFileWriter.WriteInfoToFile(String.Format("В результате декодирования ответа от биллинга получили следующую строку: '{0}'", response));
					
					//удаляем из полученной от сервера декодированной строки символы форматирования
						response=Regex.Replace(response, OSMPClass.RemovingSymbolsFromServerResponse, String.Empty);
					//разбираем полученную строку
						valuesOfResponse=OSMPClass.ParseStringValue(response, ResponseFormat, ArgsOfResponse);
					//если получили null, то строка не соответствует паттерну, т.е. получен некорректный ответ - проблемы с платежом
						if(valuesOfResponse==null)
						{
							returnedValue=new ServerResponse(false, false, String.Format(OSMPClass.UncorrectServerResponse_Format, response), String.Empty, String.Empty);
						}
						else
						{
							/*если номер платежа, полученный от сервера неравен переданному в метод
							 (произошла какая-то ошибка на сервере и получили ответ, который предназанчался другому терминалу)
							 */
							if(valuesOfResponse[OSMPClass.OSMPTxnIdValueName]!=Txn_Id)
							{
								returnedValue=new ServerResponse(false, false, String.Format(OSMPClass.UncorrectTxnId_Format, valuesOfResponse[OSMPClass.OSMPTxnIdValueName], Txn_Id), String.Empty, valuesOfResponse.ContainsKey(OSMPClass.PrvTxnValueName) ? valuesOfResponse[OSMPClass.PrvTxnValueName] : String.Empty);
							}
							else
							{															
								returnedValue=new ServerResponse(valuesOfResponse[OSMPClass.ResultValueName]!=OSMPClass.OkResponse_Value,
					                                 				Convert.ToInt32(valuesOfResponse[OSMPClass.ResultValueName])==OSMPClass.OkResponse_Key,
					                                 				GenerateMessageByCode(Convert.ToInt32(valuesOfResponse[OSMPClass.ResultValueName])), 
					                                 				valuesOfResponse[CommentValueName],
					                                 				valuesOfResponse.ContainsKey(OSMPClass.PrvTxnValueName) ? valuesOfResponse[OSMPClass.PrvTxnValueName] : String.Empty
					                                 			);
							}
						}
						
					//возвращаем значение
					return returnedValue;
				}
			#endregion
			
		
			#region Метод для считывания байтов из Stream с текущей позиции до конца
				protected static Byte[] ReadToEnd(Stream CurrentStream, Int32 ReadTimeout)
				{
					Int64 originalPosition=0;
					Byte[] readBuffer=new Byte[4096];
					Int32 totalBytesRead=0;
					Int32 bytesRead;
					Byte[] returnedValue;
					
					if(CurrentStream.CanSeek==true)
					{
						originalPosition=CurrentStream.Position;
						CurrentStream.Position=0;
					}
			
					try
					{
						//установим таймуаут на считывание
						CurrentStream.ReadTimeout=ReadTimeout;
						while((bytesRead=CurrentStream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead))>0)
						{
							totalBytesRead+=bytesRead;
							
							if(totalBytesRead==readBuffer.Length)
							{
								Int32 nextByte=CurrentStream.ReadByte();
								if(nextByte!=-1)
								{
									Byte[] temp = new Byte[readBuffer.Length*2];
									Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
									Buffer.SetByte(temp, totalBytesRead, (Byte)nextByte);
									readBuffer=temp;
									totalBytesRead++;
								}
							}
						}
						returnedValue=readBuffer;
						if(readBuffer.Length!=totalBytesRead)
						{
							returnedValue=new Byte[totalBytesRead];
							Buffer.BlockCopy(readBuffer, 0, returnedValue, 0, totalBytesRead);
						}
					}
					finally
					{
						if(CurrentStream.CanSeek)
						{
							CurrentStream.Position=originalPosition;
						}
					}
				
					
			
			
					return returnedValue;
				}
			#endregion
		
		
			#region Метод для генерирования внутреннего номера платежа в системе ОСМП
				public static String GenerateTxnId(UInt16 RequiredLengthOfTxnId, UInt16 TerminalNumber)
				{
					String returnedValue;
					Int64 currentTicks;
					String tmp;
			
					//запомнить текущее время в тиках
						currentTicks=DateTime.Now.Ticks;
					
					//сгенерировать первоначальное значение номера платежа, который начинается с номера терминала, а заканчивается сохраненным временем в тиках
						tmp=TerminalNumber.ToString()+currentTicks.ToString();
			
					//если необходимая длинна номера платежа меньше длинны tmp
						if(RequiredLengthOfTxnId<tmp.Length)
						{
							//извлечем ровно RequiredLengthOfTxnId символов
							tmp=tmp.Substring(tmp.Length-RequiredLengthOfTxnId);
						}
						else
						{
							//иначе, дополним tmp слева символом OSMPClass.PaddingCharForTxn_Id
								tmp=tmp.PadLeft(RequiredLengthOfTxnId, OSMPClass.PaddingCharForTxn_Id);
						}
					
					
					returnedValue=tmp;
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для извлечения из строки значения переменных с помощью регулярных выражений, имена которых передаются в виде списка params
				//аргумент Pattern нужен для применения регулярного выражения, он должен содержать все указанные в params имена переменных
				protected static Dictionary<String, String> ParseStringValue(String ParsingString, String Pattern, params String[] ValueNames)
				{
					Dictionary<String, String> returnedValue;
							
			
					if(Regex.IsMatch(ParsingString, Pattern)==true)
					{
						returnedValue=new Dictionary<String, String>();
						Match parseResult=Regex.Match(ParsingString, Pattern);
				
						foreach(String currentNode in ValueNames)
						{
							returnedValue.Add(currentNode, parseResult.Groups[currentNode].Value);
						}
					}
					else
					{
						returnedValue=null;
					}
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для заполнения словаря со всеми возможными результатами выполнения операции проведения платежа
				public static void InitCodeResultOfPaymentDictionary()
				{
					CodeResultOfPayment=new Dictionary<Int32, String>();
			
					CodeResultOfPayment.Add(OSMPClass.OkResponse_Key, OSMPClass.OkResponse_Value);
					CodeResultOfPayment.Add(OSMPClass.TemporaryErrorResponse_Key, OSMPClass.TemporaryErrorResponse_Value);
					CodeResultOfPayment.Add(OSMPClass.InvalidFormatOfCallerID_Key, OSMPClass.InvalidFormatOfCallerID_Value);
					CodeResultOfPayment.Add(OSMPClass.СallerIDIsNotFound_Key, OSMPClass.СallerIDIsNotFound_Value);
					CodeResultOfPayment.Add(OSMPClass.ReceptionOfPaymentIsDeniedByProvider_Key, OSMPClass.ReceptionOfPaymentIsDeniedByProvider_Value);
					CodeResultOfPayment.Add(OSMPClass.ReceptionOfPaymentIsDeniedForTechnicalReasons_Key, OSMPClass.ReceptionOfPaymentIsDeniedForTechnicalReasons_Value);
					CodeResultOfPayment.Add(OSMPClass.SubscriberAccountIsNotActive_Key, OSMPClass.SubscriberAccountIsNotActive_Value);
					CodeResultOfPayment.Add(OSMPClass.EffectPaymentIsNotCompleted_Key, OSMPClass.EffectPaymentIsNotCompleted_Value);
					CodeResultOfPayment.Add(OSMPClass.AmountIsTooSmall_Key, OSMPClass.AmountIsTooSmall_Value);
					CodeResultOfPayment.Add(OSMPClass.AmountIsTooHigh_Key, OSMPClass.AmountIsTooHigh_Value);
					CodeResultOfPayment.Add(OSMPClass.UnableToVerifyAccountStatus_Key, OSMPClass.UnableToVerifyAccountStatus_Value);
					CodeResultOfPayment.Add(OSMPClass.AnotherProvidersError_Key, OSMPClass.AnotherProvidersError_Value);
					
				}
			#endregion
		
			#region Метод для генерации текстового сообщения по полученному от сервера коду
				protected static String GenerateMessageByCode(Int32 Code)
				{
					String returnedValue;
					
					if(CodeResultOfPayment==null)
					{
						InitCodeResultOfPaymentDictionary();
					}
					
					if(CodeResultOfPayment.ContainsKey(Code)==true)
					{
						returnedValue=String.Format(OSMPClass.ExpectedServerResponse_Format, Code, CodeResultOfPayment[Code]);
					}
					else
					{
						returnedValue=String.Format(OSMPClass.UnexpectedServerResponse_Format, Code);
					}
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для выполнения проверки лицевого счета
				public static ServerResponse CheckAccount(String ServerURL, Int32 ServerPort, String Login, String Password, String PaymentApplicationOfProvider, String CheckCommand, String Txn_Id, String Account, WebProxy Proxy)
				{
					Boolean returnedValue;
					ServerResponse responseToCheck;
					Single checkSum=1;		
			
					responseToCheck=OSMPClass.ExecuteCommand(OSMPClass.CheckRequest_Format, ServerURL, ServerPort, Login, Password, PaymentApplicationOfProvider, CheckCommand, Txn_Id, String.Empty, Account, checkSum, Proxy, OSMPClass.ResponseToCheck_Pattern, OSMPClass.OSMPTxnIdValueName, OSMPClass.ResultValueName, OSMPClass.CommentValueName);
					
			
					/*returnedValue=responseToCheck.IsSuccessfullyExecuted;
			
					return returnedValue;*/
					return responseToCheck;
				}
			#endregion
		
			#region Метод для проведения платежа на сервер
				public static ServerResponse MakePayment(String ServerURL, Int32 ServerPort, String Login, String Password, String PaymentApplicationOfProvider, String Command, String Txn_Id, DateTime Txn_Date, String Account, Single Sum, WebProxy Proxy)
				{
					ServerResponse returnedValue;
			
					returnedValue=OSMPClass.ExecuteCommand(OSMPClass.PaymentRequest_Format, ServerURL, ServerPort, Login, 
			                                       			Password, PaymentApplicationOfProvider, Command, Txn_Id, Txn_Date.ToString(OSMPClass.TxnDate_Format), Account, Sum, Proxy, 
			                                       			OSMPClass.ResponseToPayment_Pattern, OSMPClass.OSMPTxnIdValueName, OSMPClass.ResultValueName, OSMPClass.CommentValueName, OSMPClass.PrvTxnValueName);
			
					return returnedValue;
				}
			#endregion
		}
	#endregion
}

