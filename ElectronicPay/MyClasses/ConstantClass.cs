using System;
namespace ElectronicPay
{

	#region Класс с необходимыми константами
		public class ConstantClass
		{
			//константа для установления размера шрифта в поле ввода лицевого счета
			public const Int32 FontSizeForContractNumberEntry = 100000;

			//константы, необходимые для ввода номера лицевого счета
				public const String Zero="0";
				public const String One = "1";
				public const String Two = "2";
				public const String Three = "3";
				public const String Four = "4";
				public const String Five = "5";
				public const String Six = "6";
				public const String Seven = "7";
				public const String Eight = "8";
				public const String Nine = "9";
			//======================================================

			//имена переменных для задания настроек(для обращения к конфигурационному файлу)
				public const String PaymentServerAddress_ValueName = "PaymentServerAddress";
				public const String PaymentServerPort_ValueName="PaymentServerPort";
				public const String ApplicationOfPaymentServer_ValueName="ApplicationOfPaymentServer";
				public const String PayCommand_ValueName="Pay_Command";
				public const String CheckCommand_ValueName="Check_Command";
				public const String MaxIdlingTime_ValueName="MaxIdlingTime";
		
				public const String LoginForPaymentServer_ValueName = "LoginForPaymentServer";
				public const String PasswordForPaymentServer_ValueName = "PasswordForPaymentServer";				
				public const String NumberOfRetailFacility_ValueName="NumberOfRetailFacility";
				public const String NumberOfTerminal_ValueName="NumberOfTerminal";					
				public const String LengthOfContractNumber_ValueName="LengthOfContractNumber";
		
		
				public const String LengthOfTxnId_ValueName="LengthOfTxnId";
				public const String LoginForAccessToProgram_ValueName = "LoginForAccessToProgram";
				public const String PasswordForAccessToProgram_ValueName = "PasswordForAccessToProgram";				
				public const String PortNameBillAcceptor_ValueName="PortName_billAcceptor";
				public const String BaudRateBillAcceptor_ValueName="BaudRate_billAcceptor";						
				public const String PortNameCashRegister_ValueName="PortName_cashRegister";
				public const String BaudRateCashRegister_ValueName="BaudRate_cashRegister";																						
				public const String DigitsFontSize_ValueName="DigitsFontSize";
				public const String InputContractNumberFontSize_ValueName="InputContractNumberFontSize";
				public const String DialogMessageFontSize_ValueName="DialogMessageFontSize";
				public const String DialogButtonFontSize_ValueName="DialogButtonFontSize";

				
				public const String AdminTypeOfAccess="AdminAccess";
				public const String ManagerTypeOfAccess="ManagerAccess";
		
				public const String ManagerLogin_ValueName="ManagerLogin";
				public const String ManagerPassword_ValueName="ManagerPassword";
			//==============================================================================
		
			public const String PathToErrorFile="errorFile.txt";
				public const String ErrorOutputFormat="{0}\t{1}";
			public const String PathToLogFile="logFile.txt";
		
			public const String PathToCashRegisterErrorDictionary="CashRegisterErrorDictionary.txt";
				public const Int32 TypeOfNumberSystemForCashRegisterErrorDictionary=16;
		
		
			//строковые константы
				public const String InvalidLoginOrPassword="Неверное сочетание комбинации Логин/Пароль";
				public const String CanNotEnableBillAcceptor="Произошла ошибка: Невозможно включить купюроприемник";
				public const String ConnectBillValidatorExceptionMessageFormat="При попытке подключения к устройству по адресу '{0}' со скоростью передачи '{1}' произошла ошибка: '{2}'";
				public const String ThereIsSomeProblemFormat="Имеем проблему: '{0}'";
			//===================
		
			//константы, необходимые для удаленного управления программой
				public const Int32 TimeoutOfWaitResponse=Int32.MaxValue;
		
				public const String RebootProgramFileName="dbus-send";
					public const String RebootProgramArguments="--system --print-reply --dest=org.freedesktop.ConsoleKit /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Restart";
		
				public const String TurnOffProgramFileName="dbus-send";
					public const String TurnOffProgramArguments="--system   --dest=org.freedesktop.ConsoleKit   --type=method_call   --print-reply   --reply-timeout=2000   /org/freedesktop/ConsoleKit/Manager   org.freedesktop.ConsoleKit.Manager.Stop";
			//===========================================================
		}
	#endregion
}

