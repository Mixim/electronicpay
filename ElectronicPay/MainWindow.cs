using System;
using Gtk;


namespace ElectronicPay
{
	public partial class MainWindow: Gtk.Window
	{	
		#region НЕОБХОДИМЫЕ ПЕРЕМЕННЫЕ
			protected InputContractNumber_Window inputContractNumber=null;
		#endregion

		#region Конструктор для класса MainWindow
			public MainWindow (): base (Gtk.WindowType.Toplevel)
			{
				Build ();		
			}
		#endregion

		#region Действия при вызове Destroy окна
			protected virtual void OnDestroyEvent (object o, Gtk.DestroyEventArgs args)
			{
				Application.Quit ();
				args.RetVal=true;
			}
		#endregion

		#region Действия при нажатии на кнопку "Ввести лицевой счет"
			protected void OnInputPersonalAccountButtonClicked (object sender, EventArgs e)
			{		
				inputContractNumber=new InputContractNumber_Window();				
					inputContractNumber.Fullscreen();
			
			
		
				//this.Destroy();
			}
		#endregion
		
		#region Событие, возникающее при каком-то действии на виджете
			protected virtual void OnWidgetEvent (object o, Gtk.WidgetEventArgs args)
			{			
				this.QueueDraw();
			}
		#endregion
		
	}	
	

}
