using System;
using Gtk;
using System.Configuration;
using System.Timers;

namespace ElectronicPay
{
	public partial class Result_Window : Gtk.Window
	{
		#region НЕОБХОДИМЫЕ ПЕРЕМЕННЫЕ		
			protected System.Timers.Timer idlingTimer;
		#endregion
		
		#region Конструктор класса Result_Window[ПУСТОЙ]
			public Result_Window () : 
					base(Gtk.WindowType.Toplevel)
			{
				this.Build ();
			}
		#endregion
		
		#region Конструктор класса Result_Window[С параметрами]
			public Result_Window(String MessageResult) :
					base(Gtk.WindowType.Toplevel)
			{
				this.Build();
			
				this.message_textview.ModifyFont(Pango.FontDescription.FromString("Courier 30"));
				this.message_textview.Buffer.Text=MessageResult;
			
				this.idlingTimer=new System.Timers.Timer(Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.MaxIdlingTime_ValueName]));
					this.idlingTimer.Elapsed+=OnIdlingTimer_Elapsed;
					this.idlingTimer.Start();
			}
		#endregion

		
		#region Действия при нажатии на кнопку "Следующий платеж"
			protected void OnNextStepButtonClicked (object sender, EventArgs e)
			{
				this.Destroy();
			}
		#endregion

		#region Метод, который срабатывает при истечении времени в таймере idlingTimer
		    protected void OnIdlingTimer_Elapsed(object source, ElapsedEventArgs e)
		    {
        		(source as System.Timers.Timer).Enabled=false;
								
				this.Destroy();
    		}
		#endregion
		
	}
}

