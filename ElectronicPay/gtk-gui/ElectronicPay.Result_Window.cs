
// This file has been generated by the GUI designer. Do not modify.
namespace ElectronicPay
{
	public partial class Result_Window
	{
		private global::Gtk.Table table1;

		private global::Gtk.ScrolledWindow message_GtkScrolledWindow;

		private global::Gtk.TextView message_textview;

		private global::Gtk.Button nextStep_button;

		protected virtual void Build ()
		{
			global::Stetic.Gui.Initialize (this);
			// Widget ElectronicPay.Result_Window
			this.Name = "ElectronicPay.Result_Window";
			this.Title = global::Mono.Unix.Catalog.GetString ("Результат");
			this.WindowPosition = ((global::Gtk.WindowPosition)(3));
			this.BorderWidth = ((uint)(45));
			// Container child ElectronicPay.Result_Window.Gtk.Container+ContainerChild
			this.table1 = new global::Gtk.Table (((uint)(3)), ((uint)(3)), false);
			this.table1.Name = "table1";
			this.table1.RowSpacing = ((uint)(6));
			this.table1.ColumnSpacing = ((uint)(6));
			// Container child table1.Gtk.Table+TableChild
			this.message_GtkScrolledWindow = new global::Gtk.ScrolledWindow ();
			this.message_GtkScrolledWindow.Name = "message_GtkScrolledWindow";
			this.message_GtkScrolledWindow.VscrollbarPolicy = ((global::Gtk.PolicyType)(0));
			this.message_GtkScrolledWindow.HscrollbarPolicy = ((global::Gtk.PolicyType)(2));
			this.message_GtkScrolledWindow.ShadowType = ((global::Gtk.ShadowType)(1));
			// Container child message_GtkScrolledWindow.Gtk.Container+ContainerChild
			this.message_textview = new global::Gtk.TextView ();
			this.message_textview.CanFocus = true;
			this.message_textview.Name = "message_textview";
			this.message_textview.Editable = false;
			this.message_textview.Justification = ((global::Gtk.Justification)(3));
			this.message_textview.WrapMode = ((global::Gtk.WrapMode)(1));
			this.message_GtkScrolledWindow.Add (this.message_textview);
			this.table1.Add (this.message_GtkScrolledWindow);
			global::Gtk.Table.TableChild w2 = ((global::Gtk.Table.TableChild)(this.table1[this.message_GtkScrolledWindow]));
			w2.LeftAttach = ((uint)(1));
			w2.RightAttach = ((uint)(2));
			// Container child table1.Gtk.Table+TableChild
			this.nextStep_button = new global::Gtk.Button ();
			this.nextStep_button.CanFocus = true;
			this.nextStep_button.Name = "nextStep_button";
			this.nextStep_button.UseUnderline = true;
			// Container child nextStep_button.Gtk.Container+ContainerChild
			global::Gtk.Alignment w3 = new global::Gtk.Alignment (0.5f, 0.5f, 0f, 0f);
			// Container child GtkAlignment.Gtk.Container+ContainerChild
			global::Gtk.HBox w4 = new global::Gtk.HBox ();
			w4.Spacing = 2;
			// Container child GtkHBox.Gtk.Container+ContainerChild
			global::Gtk.Image w5 = new global::Gtk.Image ();
			w5.Pixbuf = global::Gdk.Pixbuf.LoadFromResource ("ElectronicPay.Attachments.nextStep.ico");
			w4.Add (w5);
			// Container child GtkHBox.Gtk.Container+ContainerChild
			global::Gtk.Label w7 = new global::Gtk.Label ();
			w4.Add (w7);
			w3.Add (w4);
			this.nextStep_button.Add (w3);
			this.table1.Add (this.nextStep_button);
			global::Gtk.Table.TableChild w11 = ((global::Gtk.Table.TableChild)(this.table1[this.nextStep_button]));
			w11.TopAttach = ((uint)(2));
			w11.BottomAttach = ((uint)(3));
			w11.LeftAttach = ((uint)(2));
			w11.RightAttach = ((uint)(3));
			w11.XOptions = ((global::Gtk.AttachOptions)(4));
			w11.YOptions = ((global::Gtk.AttachOptions)(4));
			this.Add (this.table1);
			if ((this.Child != null)) {
				this.Child.ShowAll ();
			}
			this.DefaultWidth = 545;
			this.DefaultHeight = 394;
			this.Show ();
			this.nextStep_button.Clicked += new global::System.EventHandler (this.OnNextStepButtonClicked);
		}
	}
}
