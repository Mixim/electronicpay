
// This file has been generated by the GUI designer. Do not modify.
namespace ElectronicPay
{
	public partial class MainWindow
	{
		private global::Gtk.Table main_table;

		private global::Gtk.Alignment logo_alignment;

		private global::Gtk.Image logo_image;

		private global::Gtk.VBox mainButton_vbox;

		private global::Gtk.Alignment inputPersonalAccount_alignment;

		private global::Gtk.Button inputPersonalAccount_button;

		private global::Gtk.Label inputPersonalAccount_label;

		protected virtual void Build ()
		{
			global::Stetic.Gui.Initialize (this);
			// Widget ElectronicPay.MainWindow
			this.Name = "ElectronicPay.MainWindow";
			this.Title = global::Mono.Unix.Catalog.GetString ("Основное окно");
			this.WindowPosition = ((global::Gtk.WindowPosition)(1));
			// Container child ElectronicPay.MainWindow.Gtk.Container+ContainerChild
			this.main_table = new global::Gtk.Table (((uint)(2)), ((uint)(3)), false);
			this.main_table.ExtensionEvents = ((global::Gdk.ExtensionMode)(1));
			this.main_table.Name = "main_table";
			// Container child main_table.Gtk.Table+TableChild
			this.logo_alignment = new global::Gtk.Alignment (0.5f, 0.5f, 1f, 1f);
			this.logo_alignment.Name = "logo_alignment";
			// Container child logo_alignment.Gtk.Container+ContainerChild
			this.logo_image = new global::Gtk.Image ();
			this.logo_image.Name = "logo_image";
			this.logo_image.Pixbuf = global::Gdk.Pixbuf.LoadFromResource ("ElectronicPay.Attachments.logoOfTTK.gif");
			this.logo_alignment.Add (this.logo_image);
			this.main_table.Add (this.logo_alignment);
			global::Gtk.Table.TableChild w2 = ((global::Gtk.Table.TableChild)(this.main_table[this.logo_alignment]));
			w2.LeftAttach = ((uint)(1));
			w2.RightAttach = ((uint)(2));
			w2.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child main_table.Gtk.Table+TableChild
			this.mainButton_vbox = new global::Gtk.VBox ();
			this.mainButton_vbox.Name = "mainButton_vbox";
			this.mainButton_vbox.Spacing = 6;
			// Container child mainButton_vbox.Gtk.Box+BoxChild
			this.inputPersonalAccount_alignment = new global::Gtk.Alignment (0.5f, 0.5f, 1f, 1f);
			this.inputPersonalAccount_alignment.Name = "inputPersonalAccount_alignment";
			this.inputPersonalAccount_alignment.LeftPadding = ((uint)(100));
			this.inputPersonalAccount_alignment.RightPadding = ((uint)(100));
			// Container child inputPersonalAccount_alignment.Gtk.Container+ContainerChild
			this.inputPersonalAccount_button = new global::Gtk.Button ();
			this.inputPersonalAccount_button.HeightRequest = 100;
			this.inputPersonalAccount_button.CanFocus = true;
			this.inputPersonalAccount_button.Name = "inputPersonalAccount_button";
			// Container child inputPersonalAccount_button.Gtk.Container+ContainerChild
			this.inputPersonalAccount_label = new global::Gtk.Label ();
			this.inputPersonalAccount_label.Name = "inputPersonalAccount_label";
			this.inputPersonalAccount_label.LabelProp = global::Mono.Unix.Catalog.GetString ("<b><span size='30000'>Ввести лицевой счет\n</span></b>");
			this.inputPersonalAccount_label.UseMarkup = true;
			this.inputPersonalAccount_button.Add (this.inputPersonalAccount_label);
			this.inputPersonalAccount_button.Label = null;
			this.inputPersonalAccount_alignment.Add (this.inputPersonalAccount_button);
			this.mainButton_vbox.Add (this.inputPersonalAccount_alignment);
			global::Gtk.Box.BoxChild w5 = ((global::Gtk.Box.BoxChild)(this.mainButton_vbox[this.inputPersonalAccount_alignment]));
			w5.Position = 1;
			w5.Fill = false;
			this.main_table.Add (this.mainButton_vbox);
			global::Gtk.Table.TableChild w6 = ((global::Gtk.Table.TableChild)(this.main_table[this.mainButton_vbox]));
			w6.TopAttach = ((uint)(1));
			w6.BottomAttach = ((uint)(2));
			w6.LeftAttach = ((uint)(1));
			w6.RightAttach = ((uint)(2));
			w6.XOptions = ((global::Gtk.AttachOptions)(4));
			this.Add (this.main_table);
			if ((this.Child != null)) {
				this.Child.ShowAll ();
			}
			this.DefaultWidth = 999;
			this.DefaultHeight = 942;
			this.Show ();
			this.DestroyEvent += new global::Gtk.DestroyEventHandler (this.OnDestroyEvent);
			this.WidgetEvent += new global::Gtk.WidgetEventHandler (this.OnWidgetEvent);
			this.inputPersonalAccount_button.Clicked += new global::System.EventHandler (this.OnInputPersonalAccountButtonClicked);
		}
	}
}
