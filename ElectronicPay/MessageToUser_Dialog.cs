using System;
using System.Configuration;
using System.Timers;

namespace ElectronicPay
{
	public partial class MessageToUser_Dialog : Gtk.Dialog
	{		
		#region НЕОБХОДИМЫЕ ПЕРЕМЕННЫЕ
			protected Timer idlingTimer;
		#endregion
		
		#region Конструктор класса MessageToUser_Dialog [пустой]
			public MessageToUser_Dialog ()
			{
				this.Build ();
			}
		#endregion
		
		#region Конструктор класса MessageToUser_Dialog [с параметрами]
			public MessageToUser_Dialog(String Message, Int32 TypeOfButton)
			{
				this.Build();
			
				//изменяем шрифт в main_textview
					using (Pango.FontDescription desc = new Pango.FontDescription ()) 
					{		
						desc.AbsoluteSize = Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.DialogMessageFontSize_ValueName]);
						main_textview.ModifyFont (desc);		
					}
				
				//задаем текст сообщения
					main_textview.Buffer.Text=Message;
			
				//изменим шрифт в метках кнопок
					ServiceClass.ChangeFontSize(Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.DialogButtonFontSize_ValueName]), yesButton_label, noButton_label);
			
					switch(TypeOfButton)
					{
						case 1:
						{
							yes_button.Visible=no_button.Visible=true;
							ok_button.Visible=false;
							break;
						}
						case 2:
						{
							yes_button.Visible=no_button.Visible=false;
							ok_button.Visible=true;
							break;
						}
					}
			
				//устанавливаем таймер для автозакрытия диалогового окна
					this.idlingTimer=new Timer(Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.MaxIdlingTime_ValueName]));
					this.idlingTimer.Elapsed+=OnIdlingTimer_Elapsed;
					this.idlingTimer.Start();
			}
		#endregion
		
		#region Действия при нажатии на кнопку no_button
			protected virtual void OnNoButtonClicked (object sender, System.EventArgs e)
			{
				this.idlingTimer.Stop();
			
				this.Destroy();
			}
		#endregion
		
		#region Действия при нажатии на кнопку yes_button
			protected virtual void OnYesButtonClicked (object sender, System.EventArgs e)
			{
				this.idlingTimer.Stop();
				
				this.Destroy();
			}
		#endregion
		
		#region Действия при нажатии на кнопку ok_button
			protected virtual void OnOkButtonClicked (object sender, System.EventArgs e)
			{
				this.idlingTimer.Stop();
				
				this.Destroy();
			}
		#endregion
		
		#region Метод, который срабатывает при истечении времени в таймере idlingTimer
		    protected void OnIdlingTimer_Elapsed(object source, ElapsedEventArgs e)
		    {
				this.idlingTimer.Stop();
							
			
				this.Destroy();
    		}
		#endregion
	}
}

