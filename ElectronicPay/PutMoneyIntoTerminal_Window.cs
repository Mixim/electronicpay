using System;
using System.Threading;
using System.ComponentModel;
using Gtk;
using System.IO;
using System.Configuration;
using System.Timers;



namespace ElectronicPay
{
	public partial class PutMoneyIntoTerminal_Window : Gtk.Window
	{
		#region НЕОБХОДИМЫЕ КОНСТАНТЫ КЛАССА
			protected const String AmountDepositedValueLabelProp_Format="<span size='20000'>{0}</span>";
			protected const String ContractNumberValueLabelProp_Format="<span size='20000'>{0}</span>";
			const String CashRegisterReceipt_Format="[size='1'][brightness='15'][b][center][doubleWidth]ТОРГОВЫЙ ОБЪЕКТ {0}[/doubleWidth][/center][/b][/brightness][/size]\n[size='1'][brightness='15'][center][doubleWidth]ДОБРО ПОЖАЛОВАТЬ![/doubleWidth][/center][/brightness][/size]\n[brightness='15']----------------------------------------[/brightness]\n\n[brightness='15']ЗАО ТТК Чита[/brightness]\n[brightness='15']Терминал {1}[/brightness]\nЧек {2} от {3}\n\nЛицевой счет {4}\nОплачено: {5}руб.\n\n\n[width]{6} ={7}руб.[/width]\n[size='1'][width]ИТОГ ={8}руб.[/width][/size]\n{9}\n{10}\n[size='1'][center]СПАСИБО[/center][/size]\n[size='1'][center]ЗА ОПЛАТУ![/center][/size]\n\n\n\n\n\n";
		#endregion
		
		#region НЕОБХОДИМЫЕ ПЕРЕМЕННЫЕ
			protected readonly String contractNumber=String.Empty;//номер лицевого счета, значение которого задается в конструкторе
		
			protected readonly String txn_id;
			
			protected UInt16 amountDeposited=0;//внесенная сумма денег

			protected Thread billAcceptorProcessor;

			protected CashCodeClass billAcceptor;
					
			protected Byte[] passwordBCDFormat;
		
			protected CashRegisterClass cashRegister;
		
			protected System.Timers.Timer idlingTimer;
		#endregion

		#region Конструктор для класса PutMoneyIntoTerminal_Window[ПУСТОЙ]
			public PutMoneyIntoTerminal_Window () : 
					base(Gtk.WindowType.Toplevel)
			{
				LogFileWriter.WriteInfoToFile(String.Format("Зашли в метод '{0}'", "PutMoneyIntoTerminal_Window()"));
				this.Build ();
							
			
				LogFileWriter.WriteInfoToFile(String.Format("Выходим из метода '{0}'", "PutMoneyIntoTerminal_Window()"));
			}
		#endregion

		#region Конструктор для класса PutMoneyIntoTerminal_Window[с параметрами]
			public PutMoneyIntoTerminal_Window (String ContractNumber, String Txn_ID, String Comment) : 
					base(Gtk.WindowType.Toplevel)
			{
				LogFileWriter.WriteInfoToFile(String.Format("Зашли в метод '{0}'", "PutMoneyIntoTerminal_Window(String)"));
				this.Build ();
			
				//сделаем текущую форму невидимой
					this.Visible=false;
			
				this.contractNumber = ContractNumber;
				this.txn_id=Txn_ID;
				
				String portName=ConfigurationManager.AppSettings[ConstantClass.PortNameBillAcceptor_ValueName];
				Int32 baudRate=Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.BaudRateBillAcceptor_ValueName]);
				
				Int32 password=0;
				this.passwordBCDFormat=ServiceClass.ConvertInt32ToBCD(password);
				
			
				try 
				{
					LogFileWriter.WriteInfoToFile("Пытаемся инициализировать класс для распечатки чеков");
					
					this.cashRegister=new CashRegisterClass(ConfigurationManager.AppSettings[ConstantClass.PortNameCashRegister_ValueName],
					                                                    	Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.BaudRateCashRegister_ValueName]));
					
					this.cashRegister.CashRegister_SerialPort.Open();
					if(cashRegister.CheckAvailabilityOfCashRegister(passwordBCDFormat)!=true)
					{
						this.cashRegister.CashRegister_SerialPort.Close();
						MessageToUser_Dialog messageToUserDialog=new MessageToUser_Dialog("Терминал не имеет возможности выдать Вам чек. Продолжить?", 1);
						if((Gtk.ResponseType)messageToUserDialog.Run()!=Gtk.ResponseType.Yes)
						{
							messageToUserDialog.Destroy();
							throw new DeviceException(String.Format("В фискальном регистраторе есть ошибки"));
						}
						messageToUserDialog.Destroy();
					}
					this.cashRegister.CashRegister_SerialPort.Close();
				
					LogFileWriter.WriteDataToFile("Trying to execute ConnectToBillAcceptor method");
					this.ConnectToBillAcceptor (portName, baudRate);
				
					//если все прошло успешно, то делаем текущую форму видимой
						this.Visible=true;
				}
				catch (DeviceException deviceException) 
				{
					using(StreamWriter errorWriter=new StreamWriter(ConstantClass.PathToErrorFile))
					{
						errorWriter.WriteLine(String.Format(ConstantClass.ErrorOutputFormat, DateTime.Now, deviceException.Message));
					}
					this.Destroy();
				}
				catch(Exception exception)
				{
					using(StreamWriter errorWriter=new StreamWriter(ConstantClass.PathToErrorFile, true))
					{
						errorWriter.WriteLine(String.Format(ConstantClass.ErrorOutputFormat, DateTime.Now, exception.Message));
					}
					this.Destroy();
				}
			
				//отображаем на форме введенный номер л\с
					this.contractNumber_label.LabelProp=String.Format(PutMoneyIntoTerminal_Window.ContractNumberValueLabelProp_Format, Comment);
			
				//устанавливаем тайминг
					this.idlingTimer=new System.Timers.Timer(Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.MaxIdlingTime_ValueName]));
						this.idlingTimer.Elapsed+=OnIdlingTimer_Elapsed;
						this.idlingTimer.Start();
			
				LogFileWriter.WriteInfoToFile(String.Format("Вышли из метода '{0}'", "PutMoneyIntoTerminal_Window(String)"));
			}
		#endregion
			
		
		#region Действия при нажатии на кнопку "Далее"
			protected void OnPaymentIsCompletedButtonClicked (object sender, EventArgs e)
			{
				try
				{
					this.idlingTimer.Stop();
			
					LogFileWriter.WriteInfoToFile(String.Format("Зашли в метод '{0}'", "OnPaymentIsCompletedButtonClicked"));			
				
					Boolean result = MakeServiceCommandAndPaymentAndPrintReceipt();
			
					LogFileWriter.WriteInfoToFile("Пытаемся создать форму с результатом");
						Result_Window result_window;					
							if(result==true)
							{
								result_window=new Result_Window(String.Format("Пополнение лицевого счета: '{0}' на сумму: {1}руб выполнено", this.contractNumber, this.amountDeposited));
							}
							else
							{
								result_window=new Result_Window(String.Format("Терминал не смог пополнить лицевой счет:'{0}' на сумму: {1}руб. Возьмите чек и обратитесь в абонентский отдел по тел.22-20-20", this.contractNumber, this.amountDeposited));
							}
							LogFileWriter.WriteInfoToFile("Пытаемся отобразить форму с результатом");
								result_window.Fullscreen();
				}
				catch(Exception exception)
				{
					using(StreamWriter errorWriter=new StreamWriter(ConstantClass.PathToErrorFile, true))
					{
						errorWriter.WriteLine(String.Format(ConstantClass.ErrorOutputFormat, DateTime.Now, exception.Message));
					}
				}
				finally
				{
					this.Destroy();
					LogFileWriter.WriteInfoToFile(String.Format("Выходим из метода '{0}'", "OnPaymentIsCompletedButtonClicked"));
				}
			}
		#endregion
		
		
		
		

		#region Метод для подключения к купюроприемнику из основного кода
			protected void ConnectToBillAcceptor (String PortName, Int32 BaudRate)
			{
				LogFileWriter.WriteInfoToFile("Зашли в метод 'ConnectToBillAcceptor'");
			
				billAcceptor = new CashCodeClass (PortName, BaudRate);
				    billAcceptor.BillReceived += new BillReceivedHandler(billAcceptor_BillReceived);
                    billAcceptor.BillStacking += new BillStackingHandler(billAcceptor_BillStacking);
                    billAcceptor.BillCassetteStatusEvent += new BillCassetteHandler(billAcceptor_BillCassetteStatusEvent);
					
				try 
				{								
					if(billAcceptor.ConnectBillValidator ()==100010)
					{
						throw new DeviceException(billAcceptor.ErrorList.Errors[100010]);
					}
				} 
				catch (Exception exception)
				{
					//billAcceptor.Dispose();

					LogFileWriter.WriteInfoToFile(String.Format("Поймали исключение: {0}", exception.Message));
					
				
					if((exception is UnauthorizedAccessException) || (exception is ArgumentOutOfRangeException) || (exception is ArgumentException) || (exception is System.IO.IOException) || (exception is InvalidOperationException) )
					{
						throw new DeviceException(String.Format(ConstantClass.ConnectBillValidatorExceptionMessageFormat, PortName, BaudRate, exception.Message));
					}
					else
					{
						throw;
					}
				}
								
				if(billAcceptor.IsConnected==true)
				{					
					//нужно будет удалить
						Int32 resultOfPowerUp;
							resultOfPowerUp=billAcceptor.PowerUpBillValidator();
							LogFileWriter.WriteInfoToFile(String.Format("'PowerUpBillValidator' выполнен и вернул '{0}'", resultOfPowerUp));
							
						
						if(resultOfPowerUp==0)
						{
							LogFileWriter.WriteInfoToFile("Пытаемся выполнить метод 'StartListening'");
							
							billAcceptor.StartListening();
							
							LogFileWriter.WriteInfoToFile("Теперь попытаемся исполнить метод 'EnableBillValidator'");
							
					
							billAcceptor.EnableBillValidator();
						}
						else
						{
							LogFileWriter.WriteInfoToFile("Неудалось включить купюроприемник. Генерируем исключение: 'DeviceException'");
								
							//billAcceptor.Dispose();
							throw new DeviceException(ConstantClass.CanNotEnableBillAcceptor);
						}
					//===================
					
				}
			
				LogFileWriter.WriteInfoToFile("Вышли из метода 'ConnectToBillAcceptor'");
			}
		#endregion
		
		#region Метод для обработки события изменения статуса купюроприемника
		    protected void billAcceptor_BillCassetteStatusEvent(object Sender, BillCassetteEventArgs e)
        	{
				this.idlingTimer.Stop();
			
				LogFileWriter.WriteInfoToFile("Зашли в метод 'billAcceptor_BillCassetteStatusEvent'");
			
            	LogFileWriter.WriteInfoToFile(e.Status.ToString());
			
				this.idlingTimer.Start();
			
				LogFileWriter.WriteInfoToFile("Вышли из метода 'billAcceptor_BillCassetteStatusEvent'");			
        	}
		#endregion
		
		#region Метод для обработки события занесения купюры в стек купюроприемника
			protected void billAcceptor_BillStacking(object Sender, System.ComponentModel.CancelEventArgs e)
    	    {
				this.idlingTimer.Stop();
			
				LogFileWriter.WriteInfoToFile("Зашли в метод 'billAcceptor_BillStacking'");
				LogFileWriter.WriteInfoToFile("Купюра в стеке");
	            
				this.idlingTimer.Start();
			
				LogFileWriter.WriteInfoToFile("Вышли из метода 'billAcceptor_BillStacking'");							
    	    }
		#endregion
		
		#region Метод для обработки события получения купюры купюроприемником
	        protected void billAcceptor_BillReceived(object Sender, BillReceivedEventArgs e)
    	    {
				this.idlingTimer.Stop();
			
				LogFileWriter.WriteInfoToFile("Зашли в метод 'billAcceptor_BillReceived'");
        	    switch(e.Status)
				{
					case(BillRecievedStatus.Rejected):
					{						
						LogFileWriter.WriteInfoToFile(e.RejectedReason);
						break;
					}
					case(BillRecievedStatus.Accepted):
					{
	            	    paymentIsCompleted_button.Sensitive=true;
						amountDeposited += e.Value;
						amountDepositedValue_label.LabelProp=String.Format(AmountDepositedValueLabelProp_Format, amountDeposited);
				
						LogFileWriter.WriteInfoToFile(String.Format("Bill accepted! " + e.Value + " руб. Общая сумму: " + amountDeposited.ToString()));
						break;
					}				
				}
			
				this.idlingTimer.Start();
			
				LogFileWriter.WriteInfoToFile("Вышли из метод 'billAcceptor_BillReceived'");
        	}
		#endregion
		
		
		

		
	
		
		#region Метод для генерации чека			
			protected String GenerateCashRegisterReceipt(String ReceiptFormat, params System.Object[] Data)
			{
				String returnedValue;
			
				returnedValue=String.Format(ReceiptFormat, Data);
			
				return returnedValue;
			}
		#endregion
		
		
		#region Метод, который срабатывает при истечении времени в таймере idlingTimer
		    protected void OnIdlingTimer_Elapsed(object source, ElapsedEventArgs e)
		    {
				this.idlingTimer.Stop();
			
				LogFileWriter.WriteInfoToFile(String.Format("Зашли в метод '{0}'", "OnIdlingTimer_Elapsed"));			
				
				MakeServiceCommandAndPaymentAndPrintReceipt();				
			
				this.Destroy();
				LogFileWriter.WriteInfoToFile(String.Format("Вышли из метода '{0}'", "OnIdlingTimer_Elapsed"));
    		}
		#endregion
		
		#region Метод формы для проведения платежа и распечатки чека (используется в кнопке "Далее" и в обработчике idlingTimer)
			protected Boolean MakeServiceCommandAndPaymentAndPrintReceipt()
			{
				LogFileWriter.WriteInfoToFile(String.Format("Зашли в метод '{0}'", "MakeServiceCommandAndPaymentAndPrintReceipt"));
			
				Boolean returnedValue=false;
				ServerResponse serverResponse=null;
			
				//некоторые параметры для проведения платежа
					DateTime txn_date=DateTime.Now;
			
				try 
				{
					LogFileWriter.WriteInfoToFile("Пытаемся остановить прослушивание купюроприемника");				
						this.billAcceptor.StopListening();
					LogFileWriter.WriteInfoToFile("Купюроприемник больше не прослушивается");
					LogFileWriter.WriteInfoToFile("Вызываем метод 'Dispose()' для класса купюроприемника");
						this.billAcceptor.Dispose();
					LogFileWriter.WriteInfoToFile("Вызов метода 'Dispose()' для класса купюроприемника прошел успешно");
															
					
					//проводим платеж
						if(amountDeposited!=0)
						{
							LogFileWriter.WriteInfoToFile("Обращаемся к биллинговому серверу");						
							serverResponse=OSMPClass.MakePayment(ConfigurationManager.AppSettings[ConstantClass.PaymentServerAddress_ValueName],
					                                              			Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.PaymentServerPort_ValueName]),
					                                 						ConfigurationManager.AppSettings[ConstantClass.LoginForPaymentServer_ValueName],
					                                 						ConfigurationManager.AppSettings[ConstantClass.PasswordForPaymentServer_ValueName],
					                                 						ConfigurationManager.AppSettings[ConstantClass.ApplicationOfPaymentServer_ValueName],
				    	                             						ConfigurationManager.AppSettings[ConstantClass.PayCommand_ValueName],
				        	                         						txn_id, txn_date, contractNumber, amountDeposited, null);
							LogFileWriter.WriteInfoToFile(String.Format("Вышли из метода 'MakePayment', который вернул следующие значения: \n\t'IsComplete'='{0}'\n\t'Result'='{1}'\n\t'Comment'='{2}'", serverResponse.IsComplete, serverResponse.Result, serverResponse.Comment));
							returnedValue=serverResponse.IsComplete;
						}
						else
						{
							returnedValue=true;
						}														
				}
				catch(Exception exception)
				{
					returnedValue=false;
				
					using(StreamWriter errorWriter=new StreamWriter(ConstantClass.PathToErrorFile))
					{
						errorWriter.WriteLine(String.Format(ConstantClass.ErrorOutputFormat, DateTime.Now.ToString(), exception.Message));
					}		
				}
				finally 
				{
					try
					{
						if(amountDeposited!=0)
						{
							Int32 countOfByteForRead=10;
							Int32 timeout=3000;
						
						
							LogFileWriter.WriteInfoToFile("генерируем чек");
						
							String receipt=this.GenerateCashRegisterReceipt(CashRegisterReceipt_Format, 
						                                                		ConfigurationManager.AppSettings[ConstantClass.NumberOfRetailFacility_ValueName], 
						                                                		ConfigurationManager.AppSettings[ConstantClass.NumberOfTerminal_ValueName], 
						                                                		txn_id, 
					    	                                            		DateTime.Now.ToString("dd.MM.yyyy"), 
					        	                                        		contractNumber, 
					            	                                    		amountDeposited, 
					                	                                		1, 
					                    	                            		amountDeposited, 
					                        	                        		amountDeposited, 
						                                                		returnedValue==true ?  String.Format("Код платежа: '{0}'", serverResponse.Prv_Txn) : String.Empty,
					                            	                    		returnedValue==true ? serverResponse.Comment: String.Format("Платеж по лицевому счету {0} на\n сумму {1} не проведен,\n обратитесь в абонентский отдел", contractNumber, amountDeposited));
					
							LogFileWriter.WriteInfoToFile("пытаемся открыть порт");	
								cashRegister.CashRegister_SerialPort.Open();
					
							LogFileWriter.WriteInfoToFile("пытаемся распечатать чек");
								cashRegister.PrintCashRegisterReceipt(passwordBCDFormat, receipt, 0, 1, 1, 1, 40, countOfByteForRead, timeout);
						
								cashRegister.SendCommandToPort(passwordBCDFormat, TopLevelCommandsOfCashRegisterProtocol.CutCheck, 0);
					
							LogFileWriter.WriteInfoToFile("пытаемся закрыть порт");
								cashRegister.CashRegister_SerialPort.Close();
							LogFileWriter.WriteInfoToFile("порт закрыт");
						}
					}
					catch(Exception exception)
					{
						using(StreamWriter errorWriter=new StreamWriter(ConstantClass.PathToErrorFile, true))
						{
							errorWriter.WriteLine(String.Format(ConstantClass.ErrorOutputFormat, DateTime.Now.ToString(), exception.Message));
						}
					}													
				}
				LogFileWriter.WriteInfoToFile(String.Format("Вышли из метода '{0}', который возвращает: '{1}'", "MakeServiceCommandAndPaymentAndPrintReceipt", returnedValue));
				return returnedValue;
			}
		#endregion

	}

	#region Класс, необходимый для обработки и генерации исключений, произошедших на устройстве
		public class DeviceException : System.Exception
		{
			public DeviceException (String Message) : base(Message)
			{
			}
		}
	#endregion



}

