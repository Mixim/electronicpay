using System;
using Gtk;
using System.Configuration;
using System.Timers;

namespace ElectronicPay
{
	public partial class MessageToUser_Window : Gtk.Window
	{
		#region НЕОБХОДИМЫЕ ПЕРЕМЕННЫЕ		
			protected System.Timers.Timer idlingTimer;
		#endregion
		
		#region Конструктор класса MessageToUser_Window[ПУСТОЙ]
			public MessageToUser_Window () : 
					base(Gtk.WindowType.Toplevel)
			{
				this.Build ();
			}
		#endregion
		
		#region Конструктор класса MessageToUser_Window[С параметрами]
			public MessageToUser_Window(String MessageResult) :
					base(Gtk.WindowType.Toplevel)
			{
				this.Build();
			
				this.message_label.LabelProp=MessageResult;
			
				this.idlingTimer=new System.Timers.Timer(Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.MaxIdlingTime_ValueName]));
					this.idlingTimer.Elapsed+=OnIdlingTimer_Elapsed;
					this.idlingTimer.Start();
			}
		#endregion

		
		#region Действия при нажатии на кнопку "Следующий платеж"
			protected void OnNextStepButtonClicked (object sender, EventArgs e)
			{
				this.Destroy();
			}
		#endregion

		#region Метод, который срабатывает при истечении времени в таймере idlingTimer
		    protected void OnIdlingTimer_Elapsed(object source, ElapsedEventArgs e)
		    {
        		(source as System.Timers.Timer).Enabled=false;
								
				this.Destroy();
    		}
		#endregion
		
	}
}

