using System;
using System.Configuration;
using Gtk;

namespace ElectronicPay
{
	public partial class CreateReport_Window : Gtk.Window
	{		
		#region НЕОБХОДИМЫЕ ПЕРЕМЕННЫЕ
			protected CashRegisterClass cashRegister;
		#endregion
		
		#region Конструктор класса CreateReport_Window
			public CreateReport_Window () : base(Gtk.WindowType.Toplevel)
			{
				try
				{
					this.Build ();
					
					//инициализируем фискальный регистратор
						this.InitOfCashRegister(ref cashRegister);
				}
				catch(Exception exception)
				{
					MessageDialog errorMessage=new MessageDialog(this, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, exception.Message);
						errorMessage.Run();
				}
			}
		#endregion
		
		#region Действия при нажатии на кнопку "Создать X-отчет"
			protected virtual void OnMakeXReportButtonClicked (object sender, System.EventArgs e)
			{
				try
				{
					this.cashRegister.MakeXReport();
				}
				catch(Exception exception)
				{
					MessageDialog errorMessage=new MessageDialog(this, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, exception.Message);
						errorMessage.Run();
				}
			}
		#endregion
			
		#region Действия при нажатии на кнопку "Создать Z-отчет"
			protected virtual void OnMakeZReportButtonClicked (object sender, System.EventArgs e)
			{
				try
				{
					this.cashRegister.MakeZReport();
				}
				catch(Exception exception)
				{
					MessageDialog errorMessage=new MessageDialog(this, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, exception.Message);
						errorMessage.Run();
				}
			}
		#endregion
		
		#region Метод для инициализации фискального регистратора
			protected void InitOfCashRegister(ref CashRegisterClass CurrentCashRegister)
			{
				CurrentCashRegister=new CashRegisterClass(ConfigurationManager.AppSettings[ConstantClass.PortNameCashRegister_ValueName],
	                                                    	Convert.ToInt32(ConfigurationManager.AppSettings[ConstantClass.BaudRateCashRegister_ValueName]));
			}
		#endregion
		
	}
}

