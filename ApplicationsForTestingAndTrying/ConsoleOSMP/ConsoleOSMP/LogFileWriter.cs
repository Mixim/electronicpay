using System;
using System.IO;
using System.Diagnostics;

namespace ConsoleOSMP
{
	public class LogFileWriter
	{	
		public static void WriteInfoToFile(String Info)
		{
			using(StreamWriter writer=new StreamWriter(ConstantClass.PathToLogFile, true))
			{
				writer.WriteLine(Info);
			}
		}
		
		
				
		
		public static void WriteDataToFile(String Info, params Object[] Data)
		{
			UInt32 bytesNumber=0;
			
			using(StreamWriter writer=new StreamWriter(ConstantClass.PathToLogFile, true))
			{
				writer.WriteLine(Info);
				
				foreach(Object currentNode in Data)
				{
					if(currentNode.GetType().IsArray)
					{
						Byte[] arr=(currentNode as Byte[]);
						for(Int32 i=0;i!=arr.Length;i++)
						{
							writer.WriteLine("Data[{0}]='0x{1:X2}h'", bytesNumber, arr[i]);
							bytesNumber+=1;
						}
					}
					else
					{
						writer.WriteLine("Data[{0}]='0x{1:X2}h'", bytesNumber, currentNode);
					}
				}
			}
		}
		
		
		
		public static void OutputStackTrace()
		{
			StackTrace stackTrace = new StackTrace();           
  			StackFrame[] stackFrames = stackTrace.GetFrames();  
			
			using(StreamWriter writer=new StreamWriter(ConstantClass.PathToLogFile, true))
			{			
				writer.WriteLine("Стек вызовов:");
				
	  			foreach (StackFrame stackFrame in stackFrames)
  				{ 
					writer.WriteLine("\t'{0}'",stackFrame.GetMethod().Name);
  				}
			}
		}
	}
}

