using System;
using System.Net;

namespace ConsoleOSMP
{
	/*
	 	Тестовый биллинг: 
	 	http://10.4.0.186:8080/bgbilling/mpsexecuter/5/15/
		login=term
		passw=123098
		
		testAccount=751004200
		
		user 
		pswd
	 */
	
	
	class MainClass
	{
		public static void Main (string[] args)
		{
			try
			{
				UInt16 terminalNumber=999;
				
				
				String serverURL="http://10.4.0.186";
				Int32 serverPort=8080;
				String paymentApplicationOfProvider=@"bgbilling/mpsexecuter/5/15";
				//String command="pay";
				String command="check";
				String txn_id=OSMPClass.GenerateTxnId(20, terminalNumber);
				//String txn_id="123";
				DateTime txn_date=DateTime.Now;
				String account="751007532";
				//String account="123456abc";
				Single sum=120f;
				
				
				//права
					String billingsLogin="term";
					String billingsPassword="123098";					
				//=====
				
			
				//serverResponse=OSMPClass.MakePayment(serverURL, serverPort, billingsLogin, billingsPassword,  paymentApplicationOfProvider, command, txn_id, txn_date, account, sum, null);
				//Console.WriteLine("Вышли из метода 'RealizationOfPayment', который вернул следующие значения: \n\t'IsComplete'='{0}'\n\t'Result'='{1}'\n\t'Comment'='{2}'", serverResponse.IsComplete, serverResponse.Result, serverResponse.Comment);
				
				
				//ServerResponse checkRes=OSMPClass.CheckAccount(serverURL, serverPort, billingsLogin, billingsPassword, paymentApplicationOfProvider, command, txn_id, account, null);
				ServerResponse payRes=OSMPClass.MakePayment(serverURL, serverPort, billingsLogin, billingsPassword, paymentApplicationOfProvider, "pay", txn_id, txn_date, account, 100, null);
				Console.WriteLine("IsComplete='{0}';\nIsSuccessfullyExecuted='{1}';\nResult='{2}';\nComment='{3}';", payRes.IsComplete, payRes.IsSuccessfullyExecuted, payRes.Result, payRes.Comment);
				
			}
			catch(Exception ex)
			{
				Console.WriteLine("Поймали исключение: '{0}'", ex.Message);
			}
		}
	}
}

