using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace ConsoleCashRegister
{
	
	/*
	 	Наличие чековой ленты в фискальном регистраторе можно проверить с помощью команды 0x45 (стр.49)
	 */
	class MainClass
	{
		public const String CashRegisterReceipt_Format="[size='1'][brightness='15'][b][center][doubleWidth]ТОРГОВЫЙ ОБЪЕКТ {0}[/doubleWidth][/center][/b][/brightness][/size]\n[size='1'][brightness='15'][center][doubleWidth]ДОБРО ПОЖАЛОВАТЬ![/doubleWidth][/center][/brightness][/size]\n[brightness='15']----------------------------------------[/brightness]\n\n[brightness='15']ЗАО ТТК Чита[/brightness]\n[brightness='15']Терминал {1}[/brightness]\nЧек {2} от {3}\n\nЛицевой счет {4}\nОплачено: {5}\n\n\n[width]{6} ={7}[/width]\n[size='1'][width]ИТОГ ={8}[/width][/size]\n{9}\n{10}\n[size='1'][center]СПАСИБО[/center][/size]\n[size='1'][center]ЗА ОПЛАТУ![/center][/size]\nДополнительно\n\nДополнительно\n\n";

		
		public static void Main (string[] args)
		{					
			Int32 password=0;
			Byte[] result;
			Int32 countOfByteForRead;
			Int32 timeout=10000;
			Byte[] passwordBCDFormat=ConvertInt32ToBCD(password, 4);

			
			
			Console.WriteLine("Введите количество байт, которое должен вернуть фискальный регистратор");
				countOfByteForRead=Convert.ToInt32(Console.ReadLine());
			
			
			CashRegisterClass cash=new CashRegisterClass("/dev/ttyS1",115200);
			//открываем порт
				cash.CashRegister_SerialPort.Open();
			
			Boolean isAvailability=cash.CheckAvailabilityOfCashRegister(passwordBCDFormat);
			
			//закрываем порт
				cash.CashRegister_SerialPort.Close();
			
			Console.WriteLine(isAvailability);
			Console.ReadKey();
		}
		
		
		
		public static Byte[] ConvertInt32ToBCD(Int32 Int32Value, Int32 SizeOfBCDValue)
		{
			const Int32 MinValueForConvertingToBCD=0;
			const Int32 MaxValueForConvertingToBCD=9999;
	        if(Int32Value<MinValueForConvertingToBCD || Int32Value>MaxValueForConvertingToBCD)
			{
    	        throw new ArgumentOutOfRangeException(String.Format("Converting value is less than {0} or more than {1}", MinValueForConvertingToBCD, MaxValueForConvertingToBCD));
			}
			else
			{
	        	Byte[] returnedValue;
				Int32 bcd=0;
				Int32 nibble;
		        for(Int32 i=0;i<4;i++)
				{
    	    	    nibble=Int32Value%10;
					bcd|=nibble<<(i*4);
					Int32Value/=10;
    		    }
				
				returnedValue=new Byte[2];
					returnedValue[0]=Convert.ToByte((bcd>>8)&0xFF);
					returnedValue[1]=Convert.ToByte((bcd&0xFF));
				
        		return returnedValue;
			}
    	}
		
		

		


	}
}

