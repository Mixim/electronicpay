using System;
using System.Text;
using System.Collections.Generic;

namespace ConsoleCashRegister
{
	#region Класс, необходимый для создания потока байт, передаваемых на фискальный регистратор
		public class BytesCreatorForCashRegister : StringParserForCashRegister
		{
			#region Метод для генерации отправляемых байт по строке
				public static LinkedList<Byte[]> GenerateSendingBytes(String StringForParse, Byte Flags, Byte Printer, Byte ModeOfCheckTape, Byte ModeOfCashRegisterTape, Int32 MaxTextLength, Encoding DefaultEncoding, Encoding RequiredEncoding)
				{
					LinkedList<Byte[]> returnedValue;
					LinkedList<Byte[]> tmpValue;
			
					
					//получить байты разобранной строк с ее форматированием
						tmpValue=BytesCreatorForCashRegister.ParseString(StringForParse, MaxTextLength, DefaultEncoding, RequiredEncoding);
			
					//зададим служебные байты
						foreach(Byte[] currentNode in tmpValue)
						{	
							//установим байт Флаги
								currentNode[BytesCreatorForCashRegister.FlagsIndex]=Flags;
							//установим байт Принтер
								currentNode[BytesCreatorForCashRegister.PrintersIndex]=Printer;
							//установим байт РежимЧЛ
								currentNode[BytesCreatorForCashRegister.ModeOfCheckTapeIndex]=ModeOfCheckTape;
							//установим байт РежимКЛ
								currentNode[BytesCreatorForCashRegister.ModeOfCashRegisterTapeIndex]=ModeOfCashRegisterTape;
						}
			
					//инициализируем возвращаемый байтовый массив значением tmpValue
						returnedValue=tmpValue;
			
					return returnedValue;
				}
			#endregion
		}
	#endregion
}

