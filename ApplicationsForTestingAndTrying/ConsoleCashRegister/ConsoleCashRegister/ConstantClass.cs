using System;
namespace ConsoleCashRegister
{

	#region Класс с необходимыми константами
		public class ConstantClass
		{
			//константа для установления размера шрифта в поле ввода лицевого счета
			public const Int32 FontSizeForContractNumberEntry = 100000;

			//константы, необходимые для ввода номера лицевого счета
				public const String Zero="0";
				public const String One = "1";
				public const String Two = "2";
				public const String Three = "3";
				public const String Four = "4";
				public const String Five = "5";
				public const String Six = "6";
				public const String Seven = "7";
				public const String Eight = "8";
				public const String Nine = "9";
			//======================================================

			//имена переменных для задания настроек(для обращения к конфигурационному файлу)
				public const String PaymentServerAddress_ValueName = "PaymentServerAddress";
				public const String LoginForPaymentServer_ValueName = "LoginForPaymentServer";
				public const String PasswordForPaymentServer_ValueName = "PasswordForPaymentServer";
		
		
				public const String LoginForAccessToProgram_ValueName = "LoginForAccessToProgram";
				public const String PasswordForAccessToProgram_ValueName = "PasswordForAccessToProgram";
		
		
				public const String PortNameBillAcceptor_ValueName="PortName_billAcceptor";
				public const String BaudRateBillAcceptor_ValueName="BaudRate_billAcceptor";
				public const String ParityBillAcceptor_ValueName="Parity_billAcceptor";
				public const String DataBitsBillAcceptor_ValueName="DataBits_billAcceptor";
				public const String StopBitsBillAcceptor_ValueName="StopBits_billAcceptor";
		
				public const String AddressByteBillToBill_ValueName="AddressByte_billToBill";
				public const String SeparatingBytesBillToBill_ValueName="SeparatingBytes_billToBill";
				
		
				
				public const String LengthOfContractNumber_ValueName="LengthOfContractNumber";
					
			//==============================================================================
		
			public const String PathToErrorFile="errorFile.txt";
				public const String ErrorOutputFormat="{0}\t{1}";
			public const String PathToLogFile="logFile.txt";
		
			public const String PathToCashRegisterErrorDictionary="CashRegisterErrorDictionary.txt";
				public const Int32 TypeOfNumberSystemForCashRegisterErrorDictionary=16;
		
		
			//строковые константы
				public const String InvalidLoginOrPassword="Неверное сочетание комбинации Логин/Пароль";
				public const String CanNotEnableBillAcceptor="Произошла ошибка: Невозможно включить купюроприемник";
				public const String ConnectBillValidatorExceptionMessageFormat="При попытке подключения к устройству по адресу '{0}' со скоростью передачи '{1}' произошла ошибка: '{2}'";
			//===================
		}
	#endregion
}

