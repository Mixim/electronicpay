using System;
namespace ConsoleCashRegisterTextFormatting
{	
	#region Энумератор, необходимый для обозначения типа действия ключевого с элементом массива
		public enum TypeOfService
		{
			//простая установка 
			Set=0,
			//применение оператора или
			//если применяется данный энумератор, это значит, что к байту должна применяться операция дизъюнкции с заранее задаными величинами
			Or=1
		}
	#endregion
	
	#region Энумератор, необходимый для определения типа выравнивания
		public enum TypeOfAlignment
		{
			//без выравнивания
			WithoutAlignment=0,
			//выравнивание по левому краю
			LeftAlignment=1,
			//выравнивание по правому краю
			RightAlignment=2,
			//выравнивание по центру
			CenterAlignment=3,
			//выравнивание по ширине
			WidthAlignment=4
		}
	#endregion
	
	#region Энумератор, необходимый для обозначения типа используемого паттерна
		public enum TypeOfPattern
		{
			//стандартный паттерн
				StandardPattern=0,
			//паттерн для поиска вхождения в начале строки
				StartingPattern=1,
			//паттерн для поиска вхождения в конце строки
				EndingPattern=2				
		}
	#endregion
	
	#region Класс, необходимый для задания элементов словаря ключевых слов и индексов массива в классе StringParserForCashRegister
		public class StringParserKeywordAndIndex
		{
			#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА StringParserKeywordAndIndex
				protected String startOfKeyword;
				protected String endOfKeyword;
				protected Int32 indexOfOverheadBytes;
				protected Byte valueForOr;//
				protected TypeOfService typeOfWordService;
			#endregion
		
			#region Метод для получения доступа к полю startOfKeyword как к свойству
				public String StartOfKeyword
				{
					get
					{
						return this.startOfKeyword;
					}
					set
					{
						this.startOfKeyword=value;
					}
				}
			#endregion
		
			#region Метод для получения доступа к полю endOfKeyword как к свойству
				public String EndOfKeyword
				{
					get
					{
						return this.endOfKeyword;
					}
					set
					{
						this.endOfKeyword=value;
					}
				}
			#endregion
		
			#region Метод для получения доступа к полю indexOfOverheadBytes как к свойству
				public Int32 IndexOfOverheadBytes
				{
					get
					{
						return this.indexOfOverheadBytes;
					}
					set
					{
						this.indexOfOverheadBytes=value;
					}
				}
			#endregion
		
			#region Метод для получения доступа к полю valueForOr как к свойству
				public Byte ValueForOr
				{
					get
					{
						return this.valueForOr;
					}
					set
					{
						this.valueForOr=value;
					}
				}
			#endregion
		
			#region Метод для получения доступа к полю typeOfWordService как к свойству
				public TypeOfService TypeOfWordService
				{
					get
					{
						return this.typeOfWordService;
					}
					set
					{
						this.typeOfWordService=value;
					}
				}
			#endregion			
		
			#region Конструктор класса StringParserKeywordAndIndex [пустой]
				public StringParserKeywordAndIndex()
				{
				}
			#endregion
		
			#region Конструктор класса StringParserKeywordsAndIndex [с параметрами]
				public StringParserKeywordAndIndex (String StartOfKeyword, String EndOfKeyword, Int32 IndexOfOverheadBytes, Byte ValueForOr, TypeOfService TypeOfWordService)
				{
					this.StartOfKeyword=StartOfKeyword;
					this.EndOfKeyword=EndOfKeyword;
					this.IndexOfOverheadBytes=IndexOfOverheadBytes;
					this.ValueForOr=ValueForOr;
					this.TypeOfWordService=TypeOfWordService;
				}
			#endregion
		}
	#endregion
}

