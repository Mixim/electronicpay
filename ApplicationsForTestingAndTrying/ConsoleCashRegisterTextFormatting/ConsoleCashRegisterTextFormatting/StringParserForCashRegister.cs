using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

namespace ConsoleCashRegisterTextFormatting
{
	/*
		Формат строки форматирования для чека представляется в следующем виде:
			[font='10'][b][spacing='2']некоторый текст[/spacing][/b][/font]
		Т.е. находящиеся в текст символы, открывающие форматирование, должны располагаться в начале строки, 
		а символы, закрывающие форматирование, должны находится в конце строки в верном порядке
	*/
	
	
	/*
	 	ВСЕ ЭЛЕМЕНТЫ МАССИВОВ НЕОБХОДИМО ВЫНЕСТИ В ОТДЕЛЬНЫЕ КОНСТАНТЫ И ЗАПОЛНЯТЬ МАССИВЫ ЭТИМИ КОНСТАНТАМИ
	 	ЭТО НЕОБХОДИМО СДЕЛАТЬ ДЛЯ ТОГО, ЧТОБЫ  МЕТОД ДЛЯ ГЕНЕРАЦИИ БАЙТОВ БЫЛ БОЛЕЕ КОРРЕКТНЫМ
	 */
	
	
	#region Класс для разбора строк, передаваемых на фискальный регистратор
		public class StringParserForCashRegister
		{
			#region НЕОБХОДИМЫЕ КОНСТАНТЫ КЛАССА StringParserForCashRegister
				//элементы для массивов StartsOfCodeFormat и EndsOfCode
					public const String StartOfFont=@"[font='{0}']";
						public const String StartOfFontSize=@"[size='{0}']";
						public const String StartOfSpacing=@"[spacing='{0}']";
						public const String StartOfBrightness=@"[brightness='{0}']";
						public const String StartOfBold=@"[b]";
						public const String StartOfUnderline=@"[u]";
						public const String StartOfInvertedPrinting=@"[invertedPrinting]";
						public const String StartOfLeftAlignment=@"[left]";
						public const String StartOfRightAlignment=@"[right]";
						public const String StartOfCenterAlignment=@"[center]";
						public const String StartOfWidthAlignment=@"[width]";
					public const String EndOfFont=@"[/font]";
						public const String EndOfFontSize=@"[/size]";
						public const String EndOfSpacing=@"[/spacing]";
						public const String EndOfBrightness=@"[/brightness]";
						public const String EndOfBold=@"[/b]";
						public const String EndOfUnderline=@"[/u]";
						public const String EndOfInvertedPrinting=@"[/invertedPrinting]";
						public const String EndOfLeftAlignment=@"[/left]";
						public const String EndOfRightAlignment=@"[/right]";
						public const String EndOfCenterAlignment=@"[/center]";
						public const String EndOfWidthAlignment=@"[/width]";
					
				//константа для словаря KeywordsAndIndexes, которая необходима для задания поля ValueForOr для тех ключей, величину которых определяет пользователь
					public const Byte UsersFormattedByte=0;
				//константа для словаря KeywordsAndIndexes, которая необходима тех параметров, значение которых не оказывают влияния на последовательность служебных байт
					public const Int32 IndexForNonOverheadByte=-1;			
		
				//словарь с ключевыми словами и индексами в массиве
					protected static Dictionary<String, StringParserKeywordAndIndex> KeywordsAndIndexes=new Dictionary<String, StringParserKeywordAndIndex>{{"Font", new StringParserKeywordAndIndex(StartOfFont, EndOfFont, FontsIndex, UsersFormattedByte, TypeOfService.Set)},
																																								{"Size", new StringParserKeywordAndIndex(StartOfFontSize, EndOfFontSize, FontsSizeIndex, UsersFormattedByte, TypeOfService.Set)},
																																								{"Spacing", new StringParserKeywordAndIndex(StartOfSpacing, EndOfSpacing, SpacingsIndex, UsersFormattedByte, TypeOfService.Set)},
																																								{"Brightness", new StringParserKeywordAndIndex(StartOfBrightness, EndOfBrightness, BrightnessIndex, UsersFormattedByte, TypeOfService.Set)},
																																								{"Bold", new StringParserKeywordAndIndex(StartOfBold, EndOfBold, FormattingIndex, 1, TypeOfService.Or)},
																																								{"Underline", new StringParserKeywordAndIndex(StartOfUnderline, EndOfUnderline, FormattingIndex, 2, TypeOfService.Or)},
																																								{"InvertedPrinting", new StringParserKeywordAndIndex(StartOfInvertedPrinting, EndOfInvertedPrinting, FormattingIndex, 4, TypeOfService.Or)},
																																								{"LeftAlignment", new StringParserKeywordAndIndex(StartOfLeftAlignment, EndOfLeftAlignment, IndexForNonOverheadByte, 0, TypeOfService.Set)},
																																								{"RightAlignment", new StringParserKeywordAndIndex(StartOfRightAlignment, EndOfRightAlignment, IndexForNonOverheadByte, 0, TypeOfService.Set)},
																																								{"CenterAlignment", new StringParserKeywordAndIndex(StartOfCenterAlignment, EndOfCenterAlignment, IndexForNonOverheadByte, 0, TypeOfService.Set)},
																																								{"WidthAlignment", new StringParserKeywordAndIndex(StartOfWidthAlignment, EndOfWidthAlignment, IndexForNonOverheadByte, 0, TypeOfService.Set)}
																																							};
		
				//элементы в массиве StartsOfCode должны идти симметрично EndsOfCode
					/*public static String[] StartsOfCode=new String[]{StringParserForCashRegister.StartOfFont,
																		StringParserForCashRegister.StartOfFontSize,
																		StringParserForCashRegister.StartOfSpacing,
																		StringParserForCashRegister.StartOfBrightness,
																		StringParserForCashRegister.StartOfBold,
																		StringParserForCashRegister.StartOfUnderline,
																		StringParserForCashRegister.StartOfInvertedPrinting
																	};
					public static String[] EndsOfCode=new String[]{StringParserForCashRegister.EndOfFont,
																		StringParserForCashRegister.EndOfFontSize,
																		StringParserForCashRegister.EndOfSpacing,
																		StringParserForCashRegister.EndOfBrightness,
																		StringParserForCashRegister.EndOfBold,
																		StringParserForCashRegister.EndOfUnderline,
																		StringParserForCashRegister.EndOfInvertedPrinting
																	};*/
				
				//элементы в StandartSymbolsOfString должны также идти симметрично элементам StandartSymbolsOfRegularExpression
					public static String[] StandartSymbolsOfString=new String[]{"{0}"};
					public static String[] StandartSymbolsOfRegularExpression=new String[]{@"\d+"};
		
				//символы, которые необходимо маскировать
					public static Char[] MaskedChars={'[',']'};
				//набор символов, с помощью которых будем выполнять маскирование
					public const String SymbolsForMasking=@"\";
		
				//ключевой символ регулярных выражений, который обозначает поиск паттерна в начале строки
					public const String SearchInStart_Format="^{0}";
				//ключевой символ регулярных выражений, который обозначает поиск паттерна в конце строки
					public const String SearchInEnd_Format="{0}$";
				
				//индексы параметров команды "Печать поля"
					public const Int32 FlagsIndex=0;	//индекс поля Флаги(1)
					public const Int32 PrintersIndex=1;	//индекс поля Принтер(1)
					public const Int32 FontsIndex=2;	//индекс поля Шрифты(1)
					public const Int32 FontsSizeIndex=3;	//индекс поля Множители(1)
					public const Int32 SpacingsIndex=4;	//индекс поля Межстрочие(1)
					public const Int32 BrightnessIndex=5;	//индекс поля Яркость(1)
					public const Int32 ModeOfCheckTape=6;	//индекс поля РежимЧЛ(1)
					public const Int32 ModeOfCashRegisterTape=7;	//индекс поля РежимКЛ(1)
					public const Int32 FormattingIndex=8;	//индекс поля Форматирование(1)
					public const Int32 ReserveIndex=9;	//индекс поля Резерв(2)
					public const Int32 TextIndex=11;	//индекс поля Текст(X)
		
				//границы значений параметров
					public static Byte[] ValidFontValue=new Byte[]{0, 1, 2, 3, 4};//0 - по настройке для данного принтера; 1..4 - соответствующие шрифты; 5..15 - зарезервированные значения (недопустимы)
					public static Byte[] ValidFontSizeValue=new Byte[]{0, 1, 3};//0 - по настройке для данного принтера; 1 - растянутый; 3 - единичный; 2, 4..15 - зарезервированные значения (недопустимы)
					public static Byte[] ValidSpacingValue=new Byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};//0 - по настройке для данного принтера; 1..15 - точно указанное количество пикселей
					public static Byte[] ValidBrightnessValue=new Byte[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};//0 - по настройке ККМ; 1 - минимальная; 2 - чуть поярче, чем 1; ...; 14 - чуть поярче, чем 13; 15 - максимальная
		
		
				//символ, показывающий что в чеке начинается новая строка
					public const Char ToNewLineSymbol='\n';
				//символ, который будет вставляться для выравнивания
					protected const Char Aligner=' ';
				//символ, который будем искать в строке и вставлять после него символ для выравнивания
					protected const Char Space=' ';
				
				//количество служебных байт - Флаги(1), Принтер(1), Шрифты(1), Множители(1), Межстрочие(1), Яркость(1), РежимЧЛ(1), РежимКЛ(1), Форматирование(1), Резерв(2)
					public const Int32 AmountOfOverheadBytes=11;
				
				
				//шаблоны для генерирования исключительных ситуаций
					public const String AssymetricalCodeBlockException_Text="Неверный формат строки \"{0}\" переданной для разбора, возможно теги открытия и закрытия размещены не симметрично либо порядок следования тегов не соответствует спецификации";
					public const String DiscrepancyOfArraysDimensionException_Text="Размерность массива \"{0}\" равна {1}, в то время как размерности массива \"{2}\" равна {3}. Размерности массивов должны совпадат";
					public const String CanNotRetrieveValueFromStringException_Text="Невозможно извлечь необходимый параметр из строки: \"{0}\"";
					public const String InvalidValueOfStringException_Text="Переданный аргумент \"{0}\" для элемента форматирования: \"{1}\" является некорректным";
					public const String InvalidLengthOfTextException_Text="Некорректная длинна текстового блока: \"{0}\" в строке: \"{1}\". Максимальная длинна составляет {2} символов";
					public const String UnreachedSwitchCaseCodeException_Text="Значение \"{0}\" не может быть обработано в пределах оператора Switch..Case метода \"{1}\"";
			#endregion


			#region Метод для проверки формата чека на корректность (чтобы все коды форматирования находились ровно там, где и должны)				
				public static Boolean IsCorrect(String CheckingString)
				{
					Boolean returnedValue=true;
					//String[] startsOfCodePattern, endsOfCodePattern;	
					String[] splittedValue;
					//необходим для нахождения всех вхождений текущего открывающего паттерна в строке splittedValue[i]
						Match startsOfCodePatternMatchResult;
					//необходим для проверки наличия закрывающего паттерна для текущего открывающего в строке splittedValue[i]
						Match endsOfCodePatternMatchResult;
																		
			
					//разбиваем полученную строку на подстроки по ключевому символу StringParserForCashRegister.ToNewLineSymbol
					splittedValue=CheckingString.Split(StringParserForCashRegister.ToNewLineSymbol);
					
				
					//проверяем соответствие блоков открытия блокам закрытия
						for(Int32 i=0; (i!=splittedValue.Length) && returnedValue;i++)
						{
							foreach(KeyValuePair<String, StringParserKeywordAndIndex> currentNode in StringParserForCashRegister.KeywordsAndIndexes)
							{														
								startsOfCodePatternMatchResult=Regex.Match(splittedValue[i], GeneratePatternForRegex(currentNode.Value.StartOfKeyword, true, TypeOfPattern.StartingPattern));
								
								//если совпадение успешно
									if(startsOfCodePatternMatchResult.Success==true)
									{																
										//ПРОВЕРЯЕМ, ЧТО ТЕКУЩИЙ ТЕГ НАЧАЛА ФОРМАТИРОВАНИЯ В СТРОКЕ ЗАКРЫВАЕТСЯ ВЕРНЫМ ТЕГОМ
										if(Regex.IsMatch(splittedValue[i], GeneratePatternForRegex(currentNode.Value.EndOfKeyword, true, TypeOfPattern.EndingPattern))==false)
										{
											returnedValue=false;
											break;
										}
										else
										{
											//удалить вхождение startsOfCodePatternMatchResult.Value из splittedValue[i]
												splittedValue[i]=splittedValue[i].Remove(splittedValue[i].IndexOf(startsOfCodePatternMatchResult.Value), startsOfCodePatternMatchResult.Value.Length);
											//определить чем именно представлен конец
												endsOfCodePatternMatchResult=Regex.Match(splittedValue[i], GeneratePatternForRegex(currentNode.Value.EndOfKeyword, true, TypeOfPattern.EndingPattern));
											//удалить вхождение endsOfCodePatternMatchResult.Value из splittedValue[i]									
												splittedValue[i]=splittedValue[i].Remove(splittedValue[i].LastIndexOf(endsOfCodePatternMatchResult.Value), endsOfCodePatternMatchResult.Value.Length);
										}
						
						
										//переходим к следующему совпадению
								    	//startsOfCodePatternMatchResult = startsOfCodePatternMatchResult.NextMatch();
									}
								
							}
						}
			
					//проверяем все ли блоки форматирования исключились
						for(Int32 i=0; (i!=splittedValue.Length) && returnedValue;i++)
						{
							foreach(KeyValuePair<String, StringParserKeywordAndIndex> currentNode in StringParserForCashRegister.KeywordsAndIndexes)
							{
								if(Regex.IsMatch(splittedValue[i], GeneratePatternForRegex(currentNode.Value.StartOfKeyword, true, TypeOfPattern.StandardPattern)) || Regex.IsMatch(splittedValue[i], GeneratePatternForRegex(currentNode.Value.StartOfKeyword, true, TypeOfPattern.StandardPattern)))
								{
									returnedValue=false;
								}
					   		}
						}
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для маскирования символов паттерна регулярных выражений
				public static String MaskingOfPattern(String Pattern)
				{
					String returnedValue=Pattern;
					//индекс символа, который будем маскировать
					Int32 indexOfMaskedSymbol=0;
					//индекс символа маскирования
					Int32 indexOfMaskingSymbol;					
					
					while((indexOfMaskedSymbol=returnedValue.IndexOfAny(StringParserForCashRegister.MaskedChars, indexOfMaskedSymbol))!=-1)
					{
						indexOfMaskingSymbol=returnedValue.IndexOf(StringParserForCashRegister.SymbolsForMasking, indexOfMaskedSymbol-StringParserForCashRegister.SymbolsForMasking.Length + 1);
						//если символ еще не замаскирован
						if((indexOfMaskingSymbol==-1) ||( indexOfMaskingSymbol>indexOfMaskedSymbol ))
						{							
							//вставляем символ маскирования
							returnedValue=returnedValue.Insert(indexOfMaskedSymbol, StringParserForCashRegister.SymbolsForMasking);
						}
						//смещаемся на длинну символа маскирования + найденный символ
							indexOfMaskedSymbol=indexOfMaskedSymbol+StringParserForCashRegister.SymbolsForMasking.Length+1;
					}
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для замены СТАНДАРТНЫХ символов форматирования String на символы регулярного выражения
				public static String Replace(String Value, String[] OldSymbols, String[] NewSymbols)
				{
					String returnedValue=Value;
					
					if(OldSymbols.Length!=NewSymbols.Length)
					{
						throw new ArgumentException(String.Format(StringParserForCashRegister.DiscrepancyOfArraysDimensionException_Text, "OldSymbold", OldSymbols.Length, "NewSymbols", NewSymbols.Length));
					}
			
					for(Int32 i=0;i!=OldSymbols.Length;i++)
					{
						returnedValue=returnedValue.Replace(OldSymbols[i], NewSymbols[i]);
					}
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для генерирования паттерна
				public static String GeneratePatternForRegex(String Val, Boolean IsMasking, TypeOfPattern PatternsType)
				{
					String returnedValue;
				
					//заменяем стандартные символы строки на символы регулярного выражения
						returnedValue=StringParserForCashRegister.Replace(Val, StringParserForCashRegister.StandartSymbolsOfString, StringParserForCashRegister.StandartSymbolsOfRegularExpression);
					
					//если необходимо выполнить маскирование
					if(IsMasking==true)
					{
						//маскируем символы для паттерна регулярного выражения
						returnedValue=StringParserForCashRegister.MaskingOfPattern(returnedValue);
					}
					
					//задаем тип поиска паттерна
					switch(PatternsType)	
					{
						case (TypeOfPattern.EndingPattern):
						{
							returnedValue=ModifyPatternsForSearchingFromEnd(returnedValue);
							break;				
						}
						case (TypeOfPattern.StartingPattern):
						{
							returnedValue=ModifyPatternForSearchingFromStart(returnedValue);
							break;
						}
					}
			
					return returnedValue;
				}
			#endregion
		
		
			#region Метод для изменения паттерна, чтобы он применялся для поиска вхождения в начале строки
				public static String ModifyPatternForSearchingFromStart(String Pattern)
				{
					String returnedValue;
			
			
					returnedValue=String.Format(StringParserForCashRegister.SearchInStart_Format, Pattern);
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для изменения паттерна, чтобы он применялся для поиска вхождения в конце строки
				public static String ModifyPatternsForSearchingFromEnd(String Pattern)
				{
					String returnedValue;
			
			
					returnedValue=String.Format(StringParserForCashRegister.SearchInEnd_Format, Pattern);
			
					return returnedValue;
				}
			#endregion
		
		
		
		
					
			#region Метод для разбора строки и генерации последовательности параметров печати
				//Flags - битовое поле: 0-й бит: режим проверки, 0 - нет, 1 - да;
					//Остальные биты не используются и должны содержать ноль
				//Printer - битовое поле: 0-й бит - печать на чековой ленте, 0 - нет, 1 - да;
					//1-й бит - печать на контрольной ленте, 0 - нет, 1 - да (бит используется только в ККМ ФЕЛИКС-Р Ф, ФЕЛИКС-02К, ТОРНАДО и Меркурий MS-K)
					//Остальные биты не используются и должны содержать ноль
				public static LinkedList<Byte[]> ParseString(String StringForParse, Byte Flags, Byte Printer, Int32 MaxTextLength, Encoding DefaultEncoding, Encoding RequiredEncoding)
				{
					LinkedList<Byte[]> returnedValue=new LinkedList<Byte[]>();
					Byte[] insertingBytes;
					
					if(IsCorrect(StringForParse)==true)
					{
						//разбиваем переданную строку на массив подстрок, которые в исходной разделены симолом StringParserForCashRegister.ToNewLineSymbol
						LinkedList<String> trimmedStringForParse=new LinkedList<String>(StringForParse.Split(StringParserForCashRegister.ToNewLineSymbol));
						foreach(String currentNode in trimmedStringForParse)
						{
							insertingBytes=StringParserForCashRegister.GenerateFormattedBytes(currentNode, MaxTextLength, DefaultEncoding, RequiredEncoding);
					
							//установим переданные флаги
								insertingBytes[StringParserForCashRegister.FlagsIndex]=Flags;
							//установим переданный принтер
								insertingBytes[StringParserForCashRegister.PrintersIndex]=Printer;
					
							returnedValue.AddLast(insertingBytes);
						}
			
						trimmedStringForParse.Clear();
					}
					else
					{
						throw new ArgumentException(String.Format(StringParserForCashRegister.AssymetricalCodeBlockException_Text, StringForParse));
					}
					
					return returnedValue;
				}
			#endregion
		
			#region Метод для установки значений байтов в массиве по определенному ключу
				protected static void SetFormatByte(ref Byte[] FormattingBytes, StringParserKeywordAndIndex SettingByteFormat, Byte SettingByte=0)
				{
					switch(SettingByteFormat.TypeOfWordService)
					{
						case (TypeOfService.Set):
						{
							FormattingBytes[SettingByteFormat.IndexOfOverheadBytes]=SettingByte;			
				
							break;
						}
						
						case (TypeOfService.Or):
						{
							FormattingBytes[SettingByteFormat.IndexOfOverheadBytes]|=SettingByteFormat.ValueForOr;
				
							break;
						}
					}
				}
			#endregion
		
			#region Метод для выравнивание текста по левому краю путем добавления символа StringParserForCashRegister.Aligner
				protected static String AlignedToLeft(String DefaultString, Int32 TotalWidth)
				{
					String returnedValue;
			
					returnedValue=DefaultString.PadLeft(TotalWidth, StringParserForCashRegister.Aligner);
			
					return returnedValue;
				}
			#endregion

			#region Метод для выравнивания текста по правому краю путем добавления символа StringParserForCashRegister.Aligner
				protected static String AlignedToRight(String DefaultString, Int32 TotalWidth)
				{
					String returnedValue;
			
					returnedValue=DefaultString.PadRight(TotalWidth, StringParserForCashRegister.Aligner);
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для выравнивания текста по центру путем добавления символа StringParserForCashRegister.Aligner по левому и правлму краям
				protected static String AlignedToCenter(String DefaultString, Int32 TotalWidth)
				{
					String returnedValue;
			
					Int32 countOfAddedSymbol=TotalWidth-DefaultString.Length;
			
					if(countOfAddedSymbol>0)
					{
						returnedValue=DefaultString.PadLeft(countOfAddedSymbol/2, StringParserForCashRegister.Aligner);
						returnedValue=returnedValue.PadRight(countOfAddedSymbol, StringParserForCashRegister.Aligner);
					}
					else
					{
						returnedValue=DefaultString;
					}
			
					return returnedValue;
				}
			#endregion	
		
			#region Метод для выравнивания текста по ширине путем добавления символа StringParserForCashRegister.Aligner между StringParserForCashRegister.Space
				protected static String AlignedToWidth(String DefaultString, Int32 TotalWidth)
				{
					String returnedValue;
			
					Int32 countOfAddedSymbol=TotalWidth-DefaultString.Length;
					Int32 indexForInsertSymbol=0;
					String searchingValue=String.Empty;
			
					returnedValue=DefaultString;
			
					if(countOfAddedSymbol>0)
					{
						if(returnedValue.IndexOf(StringParserForCashRegister.Space, indexForInsertSymbol)>0)
						{
							//пока длинна текущей строки меньше необходимой
							while(returnedValue.Length<TotalWidth)
							{						
								searchingValue+=StringParserForCashRegister.Space;
								indexForInsertSymbol=returnedValue.IndexOf(searchingValue, indexForInsertSymbol);
								do
								{							
									returnedValue=returnedValue.Insert(indexForInsertSymbol, Convert.ToString(StringParserForCashRegister.Aligner));
									indexForInsertSymbol+=2;
									indexForInsertSymbol=returnedValue.IndexOf(searchingValue, indexForInsertSymbol);
								}
								while((returnedValue.Length<TotalWidth) && (indexForInsertSymbol!=-1));
						
								//обнуляем индекс для вставки
								indexForInsertSymbol=0;
							}										
						}
						else
						{
							returnedValue=DefaultString.PadLeft(countOfAddedSymbol/2);
							returnedValue=returnedValue.PadRight(countOfAddedSymbol);
						}

					}
					else
					{
						returnedValue=DefaultString;
					}
			
					return returnedValue;
				}
			#endregion
		
			#region Метод, необходимый для применения необходимого метода для выравнивания строки
				protected static String AlignmentOfString(String StringForAlignment, TypeOfAlignment RequiredTypeOfAlignment, Int32 TotalWidth)
				{
					String returnedValue;
			
					switch(RequiredTypeOfAlignment)
					{
						case TypeOfAlignment.CenterAlignment:
						{
							returnedValue=AlignedToCenter(StringForAlignment, TotalWidth);
							break;
						}
						case TypeOfAlignment.LeftAlignment:
						{
							returnedValue=AlignedToLeft(StringForAlignment, TotalWidth);
							break;
						}
						case TypeOfAlignment.RightAlignment:
						{
							returnedValue=AlignedToRight(StringForAlignment, TotalWidth);
							break;
						}
						case TypeOfAlignment.WidthAlignment:
						{
							returnedValue=AlignedToWidth(StringForAlignment, TotalWidth);
							break;
						}
						case TypeOfAlignment.WithoutAlignment:
						{
							returnedValue=StringForAlignment;
							break;
						}
				
						default:
						{
							throw new ArgumentException(String.Format(StringParserForCashRegister.UnreachedSwitchCaseCodeException_Text, RequiredTypeOfAlignment, "AlignmentOfString"));
						}
					}
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для генерации массива байт для форматированной строки
				public static Byte[] GenerateFormattedBytes(String StringForFormatted, Int32 MaxTextLength, Encoding DefaultEncoding, Encoding RequiredEncoding)
				{
					List<Byte> returnedValue;
					Byte[] overheadBytes=new Byte[StringParserForCashRegister.AmountOfOverheadBytes];
					String parsedString;						
					Match matchResult;
					Match valueParseResult;
					Byte settingByte;
					TypeOfAlignment requiredTypeOfAlignment=TypeOfAlignment.WithoutAlignment;
					
			
					//инициализируем список для байтов
						returnedValue=new List<Byte>();
						
			
					//разобьем строку на подстроки
						parsedString=StringForFormatted;
					
					//начинаем разбор
						foreach(KeyValuePair<String, StringParserKeywordAndIndex> currentNode in StringParserForCashRegister.KeywordsAndIndexes)
						{
							matchResult=Regex.Match(parsedString, GeneratePatternForRegex(currentNode.Value.StartOfKeyword, true, TypeOfPattern.StartingPattern));
							if(matchResult.Success==true)
							{
								if(currentNode.Value.TypeOfWordService==TypeOfService.Set)
								{
									if(currentNode.Value.IndexOfOverheadBytes!=StringParserForCashRegister.IndexForNonOverheadByte)
									{
										//получаем необходимую переменную из строки, которая определяет параметр печати, теперь необходимо проверить успешность извлечения этой переменной и конвертировать ее в байт
											valueParseResult=Regex.Match(matchResult.Value, StringParserForCashRegister.StandartSymbolsOfRegularExpression[0]);
											if(valueParseResult.Success==false)
											{
												throw new ArgumentException(String.Format(StringParserForCashRegister.CanNotRetrieveValueFromStringException_Text, parsedString));	
											}
											else
											{
												if(Byte.TryParse(valueParseResult.Value, out settingByte)==true)
												{
													StringParserForCashRegister.SetFormatByte(ref overheadBytes, currentNode.Value, settingByte);
									        	}
												else
												{
													throw new ArgumentException(String.Format(StringParserForCashRegister.InvalidValueOfStringException_Text, valueParseResult, matchResult.Value));
												}
											}
									}
									else
									{
										switch(currentNode.Value.StartOfKeyword)
										{
											case StartOfLeftAlignment:
											{
												requiredTypeOfAlignment=TypeOfAlignment.LeftAlignment;	
												break;
											}
											case StartOfRightAlignment:
											{
												requiredTypeOfAlignment=TypeOfAlignment.RightAlignment;	
												break;
											}
											case StartOfCenterAlignment:
											{
												requiredTypeOfAlignment=TypeOfAlignment.CenterAlignment;	
												break;
											}
											case StartOfWidthAlignment:
											{
												requiredTypeOfAlignment=TypeOfAlignment.WidthAlignment;	
												break;
											}
										}
									}
									//удаляем из строки начало блока форматирования
										parsedString=parsedString.Remove(parsedString.IndexOf(matchResult.Value), matchResult.Value.Length);
									//удаляем из строки конец блока форматирования
										parsedString=parsedString.Remove(parsedString.IndexOf(currentNode.Value.EndOfKeyword), currentNode.Value.EndOfKeyword.Length);																								
								}
								else
								{
									StringParserForCashRegister.SetFormatByte(ref overheadBytes, currentNode.Value);
										
									//удаляем из строки начало блока форматирования
										parsedString=parsedString.Remove(parsedString.IndexOf(matchResult.Value), matchResult.Value.Length);
									//удаляем из строки конец блока форматирования
										parsedString=parsedString.Remove(parsedString.IndexOf(currentNode.Value.EndOfKeyword), currentNode.Value.EndOfKeyword.Length);
						
								}
							}							
						
						}
					
					//вставляем байты заголовка (служебные байты)
						returnedValue.AddRange(overheadBytes);
					//если непосредственная длинна текста превышает максимальную
					if(parsedString.Length>MaxTextLength)
					{
						throw new ArgumentException(String.Format(StringParserForCashRegister.InvalidLengthOfTextException_Text, parsedString, StringForFormatted, MaxTextLength));
					}
					else
					{
						String str=AlignmentOfString(parsedString, requiredTypeOfAlignment, MaxTextLength);
						returnedValue.AddRange(Encoding.Convert(DefaultEncoding, RequiredEncoding, DefaultEncoding.GetBytes(str)));
					}
					
			
					return returnedValue.ToArray();
				}
			#endregion
		
		}
	#endregion
}

