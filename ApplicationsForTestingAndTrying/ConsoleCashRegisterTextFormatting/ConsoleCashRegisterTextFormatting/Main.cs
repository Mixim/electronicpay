using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Text;

namespace ConsoleCashRegisterTextFormatting
{
	class MainClass
	{
		const String TextFormat="[size='1'][u][center]ТОРГОВЫЙ ОБЪЕКТ {0}[/center][/u][/size]\n[size='1']ДОБРО ПОЖАЛОВАТЬ![/size]\n\nЗАО ТТК Чита\nТерминал {1}\nЧек {2} от {3}\n\nЛицевой счет {4}\nОплачено {5}\n\n\n{6} ={7}\n[size='1']ИТОГ ={8}[/size]\n{9}\n{10}\n[size='1']СПАСИБО[/size]\n[size='1']ЗА ПОКУПКУ![/size]";
		//const String TextFormat="[size='1'][brightness='15'][b]123456789[/b][/brightness][/size]";
		
		
		
		//печать чисел от 1 до 9 крупным жирным шрифтом
		//нулевой байт - флаги
		//первый байт - принтер (1 - печать на чековой ленте да)
		//второй байт - шрифты (0 - по настройке для принтера)
		//третий байт - множители (1 - растянутый)
		//четвертый байт - межстрочие (0 - по настройке для принтера)
		//пятый байт - яркость (15 - максимальная)
		//шестой байт - РежимЧЛ
		//седьмой байт - РежимКЛ
		//восьмой байт - форматирование
		//девятый и десятый байт - резерв
		//Byte[] data=new Byte[20]{0, 1, 0, 1, 0, 15, 1, 1, 1, 0, 0,    49, 50, 51, 52, 53, 54, 55, 56, 57};
		
		public static void Main (string[] args)
		{
			/*Int32 tradingObject=10;
			Int32 terminalNumber=11;
			Int32 checkNumber=12;
			DateTime date=DateTime.Now;
			Int32 personalAccount=13;
			Int32 sum=14;
			Int32 any1=15;	
			Int32 any2=16;
			String checkInfo="checkInfo";
			String kkmInfo="kkmInfo";
			
			
			String check=String.Format(MainClass.TextFormat, tradingObject, terminalNumber, checkNumber, date, personalAccount, sum, any1, any2, sum, checkInfo, kkmInfo);
					
			
			try
			{
				LinkedList<Byte[]> tmp=StringParserForCashRegister.ParseString(check, (byte)1, (byte)2, 50, Encoding.Default, Encoding.GetEncoding(866));
				foreach(Byte[] node in tmp)
				{
					Console.WriteLine("======");
					for(Int32 i=0;i!=node.Length;i++)
					{
						Console.WriteLine(node[i]);
					}
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
			}*/
			LinkedList<Byte[]> tmp=StringParserForCashRegister.ParseString(TextFormat, (byte)0, (byte)1, 50, Encoding.Default, Encoding.GetEncoding(866));			
			
			foreach(Byte[] currentNode in tmp)
			{
				Console.WriteLine("Текущий элемент списка:");
				for(Int32 i=0;i!=currentNode.Length;i++)
				{
					Console.WriteLine("currentNode[{0}]={1}", i, currentNode[i]);
				}
			}
		}
		
		

		


	}
}

