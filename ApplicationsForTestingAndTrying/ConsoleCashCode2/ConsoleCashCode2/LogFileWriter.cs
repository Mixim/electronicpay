using System;
using System.IO;
using System.Diagnostics;

namespace ConsoleCashCode2
{
	public class LogFileWriter
	{
		public const String PathToLogFile="logFile.txt";
		
		public static void WriteInfoToFile(String Info)
		{
			using(StreamWriter writer=new StreamWriter(LogFileWriter.PathToLogFile, true))
			{
				writer.WriteLine(Info);
			}
		}
		
		
		public static void WriteBytesToFile(String Info, Byte[] Bytes)
		{
			using(StreamWriter writer=new StreamWriter(LogFileWriter.PathToLogFile, true))
			{
				writer.WriteLine(Info);
				
				for(Int32 i=0;i!=Bytes.Length;i++)
				{
					writer.WriteLine("Byte[{0}]='0x{1:X2}h'", i, Bytes[i]);
				}
			}
		}
		
		
		
		public static void OutputStackTrace()
		{
			StackTrace stackTrace = new StackTrace();           
  			StackFrame[] stackFrames = stackTrace.GetFrames();  
			
			using(StreamWriter writer=new StreamWriter(LogFileWriter.PathToLogFile, true))
			{			
				writer.WriteLine("Стек вызовов:");
				
	  			foreach (StackFrame stackFrame in stackFrames)
  				{ 
					writer.WriteLine("\t'{0}'",stackFrame.GetMethod().Name);
  				}
			}
		}
	}
}

