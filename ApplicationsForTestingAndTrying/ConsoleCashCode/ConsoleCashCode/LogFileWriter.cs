using System;
using System.IO;

namespace ConsoleCashCodeTesting
{
	public class LogFileWriter
	{
		public const String PathToLogFile="logFile.txt";
		
		public static void WriteInfoToFile(String Info)
		{
			using(StreamWriter writer=new StreamWriter(LogFileWriter.PathToLogFile, true))
			{
				writer.WriteLine(Info);
			}
		}
		
		
		
		public static void WriteByteToFile(String Text, String Format, Byte[] Bytes)
		{
			using(StreamWriter writer=new StreamWriter(LogFileWriter.PathToLogFile, true))
			{
				writer.WriteLine(Text);
				
				for(Int32 i=0;i!=Bytes.Length;i++)
				{
					writer.WriteLine(Format, i, Bytes[i]);
				}
			}
		}
	}
}

