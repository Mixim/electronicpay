using System;


namespace ConsoleCashCodeTesting
{
	class MainClass
	{
		static int Sum = 0;

        static void Main(string[] args)
        {
            try
            {
				//купюроприемник в /dev/ttyS2
				
				Console.WriteLine("Запускаем");
                using (CashCodeBillValidator c = new CashCodeBillValidator("/dev/ttyS2", 9600))
                {	
					/*Console.WriteLine("Rts='{0}'; Dtr='{1}'", c._ComPort.RtsEnable, c._ComPort.DtrEnable);
					c._ComPort.RtsEnable=true;
					c._ComPort.DtrEnable=true;
					Console.WriteLine("Rts='{0}'; Dtr='{1}'", c._ComPort.RtsEnable, c._ComPort.DtrEnable);*/
					
					Console.WriteLine("Зашли в using");	
                    //c.BillReceived += new BillReceivedHandler(c_BillReceived);
                    //c.BillStacking += new BillStackingHandler(c_BillStacking);
                    //c.BillCassetteStatusEvent += new BillCassetteHandler(c_BillCassetteStatusEvent);
					
					
                    c.ConnectBillValidator();

					Console.WriteLine("Выполнили попытку подключиться и итог={0}", c.IsConnected);
					LogFileWriter.WriteInfoToFile(String.Format("Выполнили попытку подключиться и итог={0}", c.IsConnected));
					
                    if (c.IsConnected)
                    {
						Console.WriteLine("Подаем питание");					
						LogFileWriter.WriteInfoToFile("Подаем питание");
                        c.PowerUpBillValidator();
						Console.WriteLine("Начинаем слушать");
						LogFileWriter.WriteInfoToFile("Начинаем слушать");
                        c.StartListening();

						Console.WriteLine("Включаем режим приема купюр");
						LogFileWriter.WriteInfoToFile("Включаем режим приема купюр");
                        c.EnableBillValidator();
						Console.WriteLine("Нажмите любую клавишу");
						LogFileWriter.WriteInfoToFile("Нажмите любую клавишу");
                        Console.ReadKey();
						Console.WriteLine("Отключаем режим приема купюр");
						LogFileWriter.WriteInfoToFile("Отключаем режим приема купюр");
                        c.DisableBillValidator();
						Console.WriteLine("Нажмите любую клавишу");
						LogFileWriter.WriteInfoToFile("Нажмите любую клавишу");
                        Console.ReadKey();
						Console.WriteLine("Вновь включаем режим приема купюр");
						LogFileWriter.WriteInfoToFile("Вновь включаем режим приема купюр");
                        c.EnableBillValidator();
						Console.WriteLine("Нажмите любую клавишу");
						LogFileWriter.WriteInfoToFile("Нажмите любую клавишу");
                        Console.ReadKey();
						Console.WriteLine("Останавливаем прослушивание");
						LogFileWriter.WriteInfoToFile("Останавливаем прослушивание");
                        c.StopListening();
                    }
					else
					{
						CashCodeErroList errorList=new CashCodeErroList();
						Console.WriteLine("При подключении произошла ошибка {0}: '{1}'", c._LastError, errorList.Errors[c._LastError]);
						
						LogFileWriter.WriteInfoToFile(String.Format("При подключении произошла ошибка {0}", c._LastError));
					}

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
				LogFileWriter.WriteInfoToFile(String.Format("В main-код поймали исключение:{0}", ex.Message));
            }
        }

        static void c_BillCassetteStatusEvent(object Sender, BillCassetteEventArgs e)
        {
            Console.WriteLine(e.Status.ToString());
        }

        static void c_BillStacking(object Sender, System.ComponentModel.CancelEventArgs e)
        {
            Console.WriteLine("Купюра в стеке");
			LogFileWriter.WriteInfoToFile("Купюра в стеке");
            if (Sum > 100)
            {
                //e.Cancel = true;
                Console.WriteLine("Превышен лимит единовременной оплаты");
				LogFileWriter.WriteInfoToFile("Превышен лимит единовременной оплаты");
            }
        }

        static void c_BillReceived(object Sender, BillReceivedEventArgs e)
        {
            if (e.Status == BillRecievedStatus.Rejected)
            {
                Console.WriteLine(e.RejectedReason);
            }
            else if (e.Status == BillRecievedStatus.Accepted)
            {
                Sum += e.Value;
                Console.WriteLine("Bill accepted! " + e.Value + " руб. Общая сумму: " + Sum.ToString());
				LogFileWriter.WriteInfoToFile(String.Format("Bill accepted! " + e.Value + " руб. Общая сумму: " + Sum.ToString()));
            }
        }
	}
}
