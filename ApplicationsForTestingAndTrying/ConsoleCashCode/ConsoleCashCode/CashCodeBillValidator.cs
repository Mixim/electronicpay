﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Text;

namespace ConsoleCashCodeTesting
{
    public enum BillValidatorCommands { ACK=0x00, NAK=0xFF, POLL=0x33, RESET=0x30, GET_STATUS=0x31, SET_SECURITY=0x32,
                                        IDENTIFICATION=0x37, ENABLE_BILL_TYPES=0x34, STACK=0x35, RETURN=0x36, HOLD=0x38}

    public enum BillRecievedStatus {Accepted, Rejected };

    public enum BillCassetteStatus { Inplace, Removed };

    // Делегат события получения банкноты
    public delegate void BillReceivedHandler(object Sender, BillReceivedEventArgs e);

    // Делегат события для контроля за кассетой
    public delegate void BillCassetteHandler(object Sender, BillCassetteEventArgs e);

    // Делегат события в процессе отправки купюры в стек (Здесь можно делать возврат)
    public delegate void BillStackingHandler(object Sender, BillStackedEventArgs e);

    public sealed class CashCodeBillValidator : IDisposable
    {
		#region Необходимые константы
	        private const int POLL_TIMEOUT = 200;    // Тайм-аут ожидания ответа от считывателя
    	    private const int EVENT_WAIT_HANDLER_TIMEOUT = 10000; // Тайм-аут ожидания снятия блокировки
			protected const String HexByteValueOutputFormater="Byte[{0}] = 0x{1:X2}h";
		#endregion
		
        #region Закрытые члены        
	        private byte[] ENABLE_BILL_TYPES_WITH_ESCROW = new byte[6] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

    	    private EventWaitHandle _SynchCom;     // Переменная синхронизации отправки и считывания данных с ком порта
        	private List<byte> _ReceivedBytes;  // Полученные байты

	        public int _LastError;
    	    private bool _Disposed;
        	private string _ComPortName;
	        private bool _IsConnected;
    	    private int _BaudRate;
        	private bool _IsPowerUp;
	        private bool _IsListening;
    	    private bool _IsEnableBills;
        	private object _Locker;

	        public SerialPort _ComPort;
    	    private CashCodeErroList _ErrorList;

        	private System.Timers.Timer _Listener;  // Таймер прослушивания купюроприемника

	        bool _ReturnBill;

    	    BillCassetteStatus _cassettestatus = BillCassetteStatus.Inplace;			
        #endregion
		
		#region События
			/// <summary>
	        /// Событие получения купюры
    	    /// </summary>
        	public event BillReceivedHandler BillReceived;
		
			/// <summary>
	        /// Событие процесса отправки купюры в стек (Здесь можно делать возврат)
    	    /// </summary>
        	public event BillStackingHandler BillStacking;
		
			public event BillCassetteHandler BillCassetteStatusEvent;
		#endregion
		

        #region Конструкторы

        public CashCodeBillValidator(string PortName, int BaudRate)
        {
            this._ErrorList = new CashCodeErroList();
            
            this._Disposed = false;
            this._IsEnableBills = false;
            this._ComPortName = "";
            this._Locker = new object();
            this._IsConnected = this._IsPowerUp = this._IsListening = this._ReturnBill = false;

            // Из спецификации:
            //      Baud Rate:	9600 bps/19200 bps (no negotiation, hardware selectable)
            //      Start bit:	1
            //      Data bit:	8 (bit 0 = LSB, bit 0 sent first)
            //      Parity:		Parity none 
            //      Stop bit:	1
            this._ComPort = new SerialPort();
            this._ComPort.PortName = this._ComPortName = PortName;
            this._ComPort.BaudRate = this._BaudRate = BaudRate;
            this._ComPort.DataBits = 8;
            this._ComPort.Parity = Parity.None;
            this._ComPort.StopBits = StopBits.One;
            this._ComPort.DataReceived += new SerialDataReceivedEventHandler(_ComPort_DataReceived);

            this._ReceivedBytes = new List<byte>();
            this._SynchCom = new EventWaitHandle(false, EventResetMode.AutoReset);

            this._Listener = new System.Timers.Timer();
            this._Listener.Interval = POLL_TIMEOUT;
            this._Listener.Enabled = false;
            this._Listener.Elapsed += new System.Timers.ElapsedEventHandler(_Listener_Elapsed);
        }

        #endregion

        #region Деструктор

        // Деструктор для финализации кода
        ~CashCodeBillValidator() { Dispose(false); }

        // Реализует интерфейс IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this); // Прикажем GC не финализировать объект после вызова Dispose, так как он уже освобожден
        }

        // Dispose(bool disposing) выполняется по двум сценариям
        // Если disposing=true, метод Dispose вызывается явно или неявно из кода пользователя
        // Управляемые и неуправляемые ресурсы могут быть освобождены
        // Если disposing=false, то метод может быть вызван runtime из финализатора
        // В таком случае только неуправляемые ресурсы могут быть освобождены.
        private void Dispose(bool disposing)
        {
            // Проверим вызывался ли уже метод Dispose
            if (!this._Disposed)
            {
                // Если disposing=true, освободим все управляемые и неуправляемые ресурсы
                if (disposing)
                {
                    // Здесь освободим управляемые ресурсы
                    try
                    {
                        // Останови таймер, если он работает
                        if (this._IsListening)
                        {
                            this._Listener.Enabled = this._IsListening = false;
                        }

                        this._Listener.Dispose();

                        // Отприм сигнал выключения на купюроприемник
                        if (this._IsConnected)
                        {
                            this.DisableBillValidator();
                        }
                    }
                    catch { }
                }

                // Высовем соответствующие методы для освобождения неуправляемых ресурсов
                // Если disposing=false, то только следующий код буде выполнен
                try 
                {
                    this._ComPort.Close();
                }
                catch { }

                _Disposed = true;
            }
        }

        #endregion

        #region Свойства
        
        public bool IsConnected
        {
            get { return _IsConnected; }
        }

        #endregion

        #region Открытые методы

        /// <summary>
        /// Начало прослушки купюроприемника
        /// </summary>
        public void StartListening()
        {
			Console.WriteLine("Зашли в 'StartListening'");
			LogFileWriter.WriteInfoToFile("Зашли в 'StartListening'");
            // Если не подключен
            if (!this._IsConnected)
            {
                this._LastError = 100020;
                throw new System.IO.IOException(this._ErrorList.Errors[this._LastError]);
            }

            // Если отсутствует энергия, то включим
            if (!this._IsPowerUp) { this.PowerUpBillValidator(); }

            this._IsListening = true;
            this._Listener.Start();
			Console.WriteLine("Вышли из 'StartListening'");
			LogFileWriter.WriteInfoToFile("Вышли из 'StartListening'");
        }

        /// <summary>
        /// Остановк прослушки купюроприемника
        /// </summary>
        public void StopListening()
        {
			Console.WriteLine("Зашли из 'StopListening'");
			LogFileWriter.WriteInfoToFile("Зашли из 'StopListening'");
            this._IsListening = false;
            this._Listener.Stop();
            this.DisableBillValidator();
			Console.WriteLine("Вышли из 'StopListening'");
			LogFileWriter.WriteInfoToFile("Вышли из 'StopListening'");
        }

        /// <summary>
        /// Открытие Ком-порта для работы с купюроприемником
        /// </summary>
        /// <returns></returns>
        public int ConnectBillValidator()
        {
			Console.WriteLine("Зашли из 'ConnectBillValidator'");
			LogFileWriter.WriteInfoToFile("Зашли из 'ConnectBillValidator'");
            try
            {
                this._ComPort.Open();
                this._IsConnected = true;
            }
            catch
            {
                this._IsConnected = false;
               this._LastError = 100010;
            }
			Console.WriteLine("Вышли из 'ConnectBillValidator'");
			LogFileWriter.WriteInfoToFile("Вышли из 'ConnectBillValidator'");
            return this._LastError;
        }

        // Включение купюроприемника
        public int PowerUpBillValidator()
        {
            Console.WriteLine("Зашли из 'PowerUpBillValidator'");
			LogFileWriter.WriteInfoToFile("Зашли из 'PowerUpBillValidator'");
			List<byte> ByteResult = null;

            // Если ком-порт не открыт
            if (!this._IsConnected)
            {
				Console.WriteLine("Com-порт неоткрыт");
				LogFileWriter.WriteInfoToFile("Com-порт неоткрыт");
				Console.WriteLine("Вышли из 'PowerUpBillValidator' с передачей ошибки '100020'");
				LogFileWriter.WriteInfoToFile("Вышли из 'PowerUpBillValidator' с передачей ошибки '100020'");
				
                this._LastError = 100020;
                throw new System.IO.IOException(this._ErrorList.Errors[this._LastError]);
            }
			else
			{
				Console.WriteLine("Com-порт был открыт");
				LogFileWriter.WriteInfoToFile("Com-порт был открыт");
			}

            //RESET (Ok)
			Console.WriteLine("Отсылаем RESET");
			LogFileWriter.WriteInfoToFile("Отсылаем RESET");
			this.SendCommand(BillValidatorCommands.RESET);
				Console.WriteLine("Подождем");
				LogFileWriter.WriteInfoToFile("Подождем");
                this._SynchCom.WaitOne(100);
                this._SynchCom.Reset();
			
			
			
			// POWER UP (Ok)
			Console.WriteLine("Отсылаем POOL");
			LogFileWriter.WriteInfoToFile("Отсылаем POOL");
            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.POLL));
			Console.WriteLine("Получили:");
			LogFileWriter.WriteInfoToFile("Получили:");
			for(Int32 i=0;i!=ByteResult.Count;i++)
			{
				Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]);
				LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]));
			}
			
			
            // Проверим результат
			Console.WriteLine("Проверяем результат");
			LogFileWriter.WriteInfoToFile("Проверяем результат");
            if (CheckPollOnError(ByteResult.ToArray()))
            {
				Console.WriteLine("Результат содержит ошибки!\nОтсылаем на com-порт команду NAK и выбрасываем исключение");	
				LogFileWriter.WriteInfoToFile("Результат содержит ошибки!\nОтсылаем на com-порт команду NAK и выбрасываем исключение");
                ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.NAK));
				Console.WriteLine("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]));
				}
				
				Console.WriteLine("Вышли из 'PowerUpBillValidator' с генерацией ошибки {0}", this._LastError);
				LogFileWriter.WriteInfoToFile(String.Format("Вышли из 'PowerUpBillValidator' с генерацией ошибки {0}", this._LastError));
                throw new System.ArgumentException(this._ErrorList.Errors[this._LastError]);
            }
			else
			{
				Console.WriteLine("Проверка выполнена успешно");
				LogFileWriter.WriteInfoToFile("Проверка выполнена успешно");
			}

            // Иначе отправляем сигнал подтверждения
			Console.WriteLine("Отправляем ACK");
			LogFileWriter.WriteInfoToFile("Отправляем ACK");
            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.ACK));
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]));
				}

            // RESET
			Console.WriteLine("Отправляем RESET");
			LogFileWriter.WriteInfoToFile("Отправляем RESET");
            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.RESET));
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]));
				}

            //Если не получили от купюроприемника сигнала ACK
            if (ByteResult[3] != 0x00)
            {
				Console.WriteLine("Должны были получить в ByteResult[3] ноль, а получили {0}. Выходим с передачей кода ошибки", ByteResult[3]);
				LogFileWriter.WriteInfoToFile(String.Format("Должны были получить в ByteResult[3] ноль, а получили {0}. Выходим с передачей кода ошибки", ByteResult[3]));
                this._LastError = 100050;
				Console.WriteLine("Вышли из 'PowerUpBillValidator' с передачей ошибки 100050");
				LogFileWriter.WriteInfoToFile("Вышли из 'PowerUpBillValidator' с передачей ошибки 100050");
                return this._LastError;
            }
			else
			{
				Console.WriteLine("От купюроприемника в 3 байте получена ожидаемая величина");
				LogFileWriter.WriteInfoToFile("От купюроприемника в 3 байте получена ожидаемая величина");
			}

            // INITIALIZE
            // Далее снова опрашиваем купюроприемник
			Console.WriteLine("Отправляем POLL");
			LogFileWriter.WriteInfoToFile("Отправляем POLL");
            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.POLL));
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]));
				}
			
            if (CheckPollOnError(ByteResult.ToArray()))
            {
				Console.WriteLine("Имеем ошибку при проверке методом CheckPollOnError. Выходим с отправкой на com-порт сообщения NAK и генерированием исключения");
				LogFileWriter.WriteInfoToFile(String.Format("Имеем ошибку при проверке методом CheckPollOnError. Выходим с отправкой на com-порт сообщения NAK и генерированием исключения с ошибкой {0}", this._LastError));
                this.SendCommand(BillValidatorCommands.NAK);
				Console.WriteLine("Вышли из 'PowerUpBillValidator'");
				LogFileWriter.WriteInfoToFile("Вышли из 'PowerUpBillValidator'");
                throw new System.ArgumentException(this._ErrorList.Errors[this._LastError]);
            }
			else
			{
				Console.WriteLine("Ответ устройства на команду 'POLL' не содержит ошибок");	
				LogFileWriter.WriteInfoToFile("Ответ устройства на команду 'POLL' не содержит ошибок");
			}

            // Иначе отправляем сигнал подтверждения
			Console.WriteLine("Отправляем ACK");
			LogFileWriter.WriteInfoToFile("Отправляем ACK");
            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.ACK));
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]));
				}
			

            // GET STATUS
			Console.WriteLine("Отправляем GET_STATUS");
			LogFileWriter.WriteInfoToFile("Отправляем GET_STATUS");
            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.GET_STATUS));
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]));
				}	
			
            // Команда GET STATUS возвращает 6 байт ответа. Если все равны 0, то статус ok и можно работать дальше, иначе ошибка
            if (ByteResult[3] != 0x00 || ByteResult[4] != 0x00 || ByteResult[5] != 0x00 ||
                ByteResult[6] != 0x00 || ByteResult[7] != 0x00 || ByteResult[8] != 0x00)
            {
                Console.WriteLine("GET_STATUS должна была вернуть 0, но такого не произошло. Генерируем исключение с кодом 100070");
				LogFileWriter.WriteInfoToFile("GET_STATUS должна была вернуть 0, но такого не произошло. Генерируем исключение с кодом 100070");
				this._LastError = 100070;
				Console.WriteLine("Вышли из 'PowerUpBillValidator'");
				LogFileWriter.WriteInfoToFile("Вышли из 'PowerUpBillValidator'");
                throw new System.ArgumentException(this._ErrorList.Errors[this._LastError]);
            }
			else
			{
				Console.WriteLine("Результат выполнения команды 'GET_STATUS' верен (все 6 полученных байт содержат нули)");
				LogFileWriter.WriteInfoToFile("Результат выполнения команды 'GET_STATUS' верен (все 6 полученных байт содержат нули)");
			}

			
			Console.WriteLine("Вновь отправляем ACK");
			LogFileWriter.WriteInfoToFile("Отправляем ACK");
            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.ACK));
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]));
				}

            // SET_SECURITY (в тестовом примере отправояет 3 байта (0 0 0)
			Console.WriteLine("Отправляем SET_SECURITY");
			LogFileWriter.WriteInfoToFile("Отправляем SET_SECURITY");
            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.SET_SECURITY, new byte[3] { 0x00, 0x00, 0x00 }));
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]));
				}	
            //Если не получили от купюроприемника сигнала ACK
            if (ByteResult[3] != 0x00)
            {
				Console.WriteLine("Должны были получить в ByteResult[3] ноль, а получили {0}. Выходим с передачей кода ошибки {1}", ByteResult[3], 100050);
				LogFileWriter.WriteInfoToFile(String.Format("Должны были получить в ByteResult[3] ноль, а получили {0}. Выходим с передачей кода ошибки", ByteResult[3], 100050));
                this._LastError = 100050;
				Console.WriteLine("Вышли из 'PowerUpBillValidator' с передачей кода ошибки {0}", this._LastError);
				LogFileWriter.WriteInfoToFile(String.Format("Вышли из 'PowerUpBillValidator' с передачей кода ошибки {0}", this._LastError));
                return this._LastError;
            }
			else
			{
				Console.WriteLine("От купюроприемника в 3 байте получена ожидаемая величина");
				LogFileWriter.WriteInfoToFile("От купюроприемника в 3 байте получена ожидаемая величина");
			}

            // IDENTIFICATION
			Console.WriteLine("Отправляем IDENTIFICATION");
			LogFileWriter.WriteInfoToFile("Отправляем IDENTIFICATION");
            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.IDENTIFICATION));
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]));
				}
			Console.WriteLine("Отправляем ACK");
			LogFileWriter.WriteInfoToFile("Отправляем ACK");
            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.ACK));
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]));
				}


            // POLL
            // Далее снова опрашиваем купюроприемник. Должны получить команду INITIALIZE
			Console.WriteLine("Отправляем POLL");
			LogFileWriter.WriteInfoToFile("Отправляем POLL");
            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.POLL));
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]));
				}

            // Проверим результат
            if (CheckPollOnError(ByteResult.ToArray()))
            {
				Console.WriteLine("Имеем ошибку при проверке методом CheckPollOnError. Выходим с отправкой на com-порт сообщения NAK и генерированием исключения");
				LogFileWriter.WriteInfoToFile("Имеем ошибку при проверке методом CheckPollOnError. Выходим с отправкой на com-порт сообщения NAK и генерированием исключения");
                this.SendCommand(BillValidatorCommands.NAK);
				Console.WriteLine("Вышли из 'PowerUpBillValidator' с генерирование исключения {0}", this._LastError);
				LogFileWriter.WriteInfoToFile(String.Format("Вышли из 'PowerUpBillValidator' с генерирование исключения {0}", this._LastError));
                throw new System.ArgumentException(this._ErrorList.Errors[this._LastError]);
            }
			else
			{
				Console.WriteLine("Ответ устройства на команду 'POLL' не содержит ошибок");	
				LogFileWriter.WriteInfoToFile("Ответ устройства на команду 'POLL' не содержит ошибок");
			}

            // Иначе отправляем сигнал подтверждения
			Console.WriteLine("Отправляем ACK");
			LogFileWriter.WriteInfoToFile("Отправляем ACK");
            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.ACK));
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]));
				}

            // POLL
            // Далее снова опрашиваем купюроприемник. Должны получить команду UNIT DISABLE
			Console.WriteLine("Отправляем POLL");
			LogFileWriter.WriteInfoToFile("Отправляем POLL");
            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.POLL));
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]));
				}			

            // Проверим результат
            if (CheckPollOnError(ByteResult.ToArray()))
            {
				Console.WriteLine("Имеем ошибку при проверке методом CheckPollOnError. Выходим с отправкой на com-порт сообщения NAK и генерированием исключения");
				LogFileWriter.WriteInfoToFile("Имеем ошибку при проверке методом CheckPollOnError. Выходим с отправкой на com-порт сообщения NAK и генерированием исключения");
                this.SendCommand(BillValidatorCommands.NAK);
				Console.WriteLine("Вышли из 'PowerUpBillValidator' с передачей кода {0}",this._LastError);
				LogFileWriter.WriteInfoToFile(String.Format("Вышли из 'PowerUpBillValidator' с передачей кода {0}",this._LastError));
                throw new System.ArgumentException(this._ErrorList.Errors[this._LastError]);
            }
			else
			{
				Console.WriteLine("Ответ устройства на команду 'POLL' не содержит ошибок");	
				LogFileWriter.WriteInfoToFile("Ответ устройства на команду 'POLL' не содержит ошибок");	
			}

            // Иначе отправляем сигнал подтверждения
			Console.WriteLine("Отправляем ACK");
			LogFileWriter.WriteInfoToFile("Отправляем ACK");
            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.ACK));
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]));
				}	
			
            this._IsPowerUp = true;
			Console.WriteLine("Вышли из 'PowerUpBillValidator'");
			LogFileWriter.WriteInfoToFile("Вышли из 'PowerUpBillValidator'");
            return this._LastError;
        }

        // Включение режима приема купюр
        public int EnableBillValidator()
        {
            Console.WriteLine("Зашли из 'EnableBillValidator'");
			LogFileWriter.WriteInfoToFile("Зашли из 'EnableBillValidator'");
			List<byte> ByteResult = null;

            // Если ком-порт не открыт
            if (!this._IsConnected)
            {
                this._LastError = 100020;
				Console.WriteLine("Вышли из 'EnableBillValidator' с передачей кода ошибки {0}", this._LastError);
				LogFileWriter.WriteInfoToFile(String.Format("Вышли из 'EnableBillValidator' с передачей кода ошибки {0}", this._LastError));
                throw new System.IO.IOException(this._ErrorList.Errors[this._LastError]);
            }

            try
            {
                if (!_IsListening)
                {
					Console.WriteLine("Вышли из 'EnableBillValidator'");
					LogFileWriter.WriteInfoToFile("Вышли из 'EnableBillValidator'");
                    throw new InvalidOperationException("Ошибка метода включения приема купюр. Необходимо вызвать метод StartListening.");
					LogFileWriter.WriteInfoToFile("Ошибка метода включения приема купюр. Необходимо вызвать метод StartListening.");
                }

                lock (_Locker)
                {
                    _IsEnableBills = true;

                    // отпавить команду ENABLE BILL TYPES (в тестовом примере отправляет 6 байт  (255 255 255 0 0 0) Функция удержания включена (Escrow)
                    ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.ENABLE_BILL_TYPES, ENABLE_BILL_TYPES_WITH_ESCROW));

                    //Если не получили от купюроприемника сигнала ACK
                    if (ByteResult[3] != 0x00)
                    {
                        this._LastError = 100050;
						Console.WriteLine("Вышли из 'EnableBillValidator' с передачей кода ошибки {0}", this._LastError);
						LogFileWriter.WriteInfoToFile(String.Format("Вышли из 'EnableBillValidator' с передачей кода ошибки {0}", this._LastError));
                        throw new System.ArgumentException(this._ErrorList.Errors[this._LastError]);
                    }
					else
					{
						Console.WriteLine("От купюроприемника в 3 байте получена ожидаемая величина");
						LogFileWriter.WriteInfoToFile("От купюроприемника в 3 байте получена ожидаемая величина");
					}

                    // Далее снова опрашиваем купюроприемник
                    ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.POLL));

                    // Проверим результат
                    if (CheckPollOnError(ByteResult.ToArray()))
                    {
                        this.SendCommand(BillValidatorCommands.NAK);
						Console.WriteLine("Вышли из 'EnableBillValidator' с передачей кода ошибки {0}", this._LastError);
						LogFileWriter.WriteInfoToFile(String.Format("Вышли из 'EnableBillValidator' с передачей кода ошибки {0}", this._LastError));
                        throw new System.ArgumentException(this._ErrorList.Errors[this._LastError]);
                    }
					else
					{
						Console.WriteLine("Ответ устройства на команду 'POLL' не содержит ошибок");	
						LogFileWriter.WriteInfoToFile("Ответ устройства на команду 'POLL' не содержит ошибок");	
					}

                    // Иначе отправляем сигнал подтверждения
                    this.SendCommand(BillValidatorCommands.ACK);
                }
            }
            catch
            {
                this._LastError = 100030;
				LogFileWriter.WriteInfoToFile(String.Format("Получили ошибку {0}", this._LastError));	
            }
			
  			Console.WriteLine("Вышли из 'EnableBillValidator' с передачей кода ошибки {0}", this._LastError);
			LogFileWriter.WriteInfoToFile(String.Format("Вышли из 'EnableBillValidator' с передачей кода ошибки {0}", this._LastError));
			return this._LastError;
        }

        // Выключение режима приема купюр
        public int DisableBillValidator()
        {
            Console.WriteLine("Зашли из 'DisableBillValidator'");
			LogFileWriter.WriteInfoToFile("Зашли из 'DisableBillValidator'");
			List<byte> ByteResult = null;

            lock (_Locker)
            {
                // Если ком-порт не открыт
                if (!this._IsConnected)
                {
                    this._LastError = 100020;
					Console.WriteLine("Вышли из 'DisableBillValidator' с передачей ошибки {0}", this._LastError);
					LogFileWriter.WriteInfoToFile(String.Format("Вышли из 'DisableBillValidator' с передачей ошибки {0}", this._LastError));
                    throw new System.IO.IOException(this._ErrorList.Errors[this._LastError]);
                }

                _IsEnableBills = false;

                // отпавить команду ENABLE BILL TYPES (в тестовом примере отправояет 6 байт (0 0 0 0 0 0)
                ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.ENABLE_BILL_TYPES, new byte[6] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }));
            }

            //Если не получили от купюроприемника сигнала ACK
            if (ByteResult[3] != 0x00)
            {
                this._LastError = 100050;
				Console.WriteLine("Вышли из 'DisableBillValidator' с передачей ошибки {0}", this._LastError);
					LogFileWriter.WriteInfoToFile(String.Format("Вышли из 'DisableBillValidator' с передачей ошибки {0}", this._LastError));
                return this._LastError;
            }
			else
			{
				Console.WriteLine("От купюроприемника в 3 байте получена ожидаемая величина");
				LogFileWriter.WriteInfoToFile(String.Format("От купюроприемника в 3 байте получена ожидаемая величина"));
			}
			
  			Console.WriteLine("Вышли из 'DisableBillValidator'");
			LogFileWriter.WriteInfoToFile("Вышли из 'DisableBillValidator'");
			return this._LastError;
        }

        #endregion

        #region Закрытые методы

        private bool CheckPollOnError(byte[] ByteResult)
        {
            Console.WriteLine("Зашли из 'CheckPollOnError'");
			LogFileWriter.WriteInfoToFile("Зашли из 'CheckPollOnError'");
			bool IsError = false;

            //Если не получили от купюроприемника третий байт равный 30Н (ILLEGAL COMMAND )
            if (ByteResult[3] == 0x30)
            {
                this._LastError = 100040;
                IsError = true;
            }
            //Если не получили от купюроприемника третий байт равный 41Н (Drop Cassette Full)
            else if (ByteResult[3] == 0x41)
            {
                this._LastError = 100080;
                IsError = true;
            }
            //Если не получили от купюроприемника третий байт равный 42Н (Drop Cassette out of position)
            else if (ByteResult[3] == 0x42)
            {
                this._LastError = 100070;
                IsError = true;
            }
            //Если не получили от купюроприемника третий байт равный 43Н (Validator Jammed)
            else if (ByteResult[3] == 0x43)
            {
                this._LastError = 100090;
                IsError = true;
            }
            //Если не получили от купюроприемника третий байт равный 44Н (Drop Cassette Jammed)
            else if (ByteResult[3] == 0x44)
            {
                this._LastError = 100100;
                IsError = true;
            }
            //Если не получили от купюроприемника третий байт равный 45Н (Cheated)
            else if (ByteResult[3] == 0x45)
            {
                this._LastError = 100110;
                IsError = true;
            }
            //Если не получили от купюроприемника третий байт равный 46Н (Pause)
            else if (ByteResult[3] == 0x46)
            {
                this._LastError = 100120;
                IsError = true;
            }
            //Если не получили от купюроприемника третий байт равный 47Н (Generic Failure codes)
            else if (ByteResult[3] == 0x47)
            {
                if (ByteResult[4] == 0x50) { this._LastError = 100130; }        // Stack Motor Failure
                else if (ByteResult[4] == 0x51) { this._LastError = 100140; }   // Transport Motor Speed Failure
                else if (ByteResult[4] == 0x52) { this._LastError = 100150; }   // Transport Motor Failure
                else if (ByteResult[4] == 0x53) { this._LastError = 100160; }   // Aligning Motor Failure
                else if (ByteResult[4] == 0x54) { this._LastError = 100170; }   // Initial Cassette Status Failure
                else if (ByteResult[4] == 0x55) { this._LastError = 100180; }   // Optic Canal Failure
                else if (ByteResult[4] == 0x56) { this._LastError = 100190; }   // Magnetic Canal Failure
                else if (ByteResult[4] == 0x5F) { this._LastError = 100200; }   // Capacitance Canal Failure
                IsError = true;
            }
			
  			Console.WriteLine("Вышли из 'CheckPollOnError' с передачей состояние об ошибке {0} и ошибкой {1}", IsError, this._LastError);
			LogFileWriter.WriteInfoToFile(String.Format("Вышли из 'CheckPollOnError' с передачей состояние об ошибке {0} и ошибкой {1}", IsError, this._LastError));
			
			return IsError;
        }

        

        // Отправка команды купюроприемнику
        private byte[] SendCommand(BillValidatorCommands cmd, byte[] Data = null)
        {
            Console.WriteLine("Зашли в SendCommand");
			LogFileWriter.WriteInfoToFile("Зашли в SendCommand");
			Console.WriteLine("Передается команда {0}", cmd);
			LogFileWriter.WriteInfoToFile(String.Format("Передается команда {0}", cmd));
			if (cmd == BillValidatorCommands.ACK || cmd == BillValidatorCommands.NAK || cmd==BillValidatorCommands.RESET)
            {                
				byte[] bytes = null;
                
                if (cmd == BillValidatorCommands.ACK) 
				{ 
					bytes = Package.CreateResponse(ResponseType.ACK); 
				}
				else
	                if (cmd == BillValidatorCommands.NAK) 
					{ 
						bytes = Package.CreateResponse(ResponseType.NAK); 
					}
					else
						if(cmd==BillValidatorCommands.RESET)
						{
							Package package = new Package();
            		    	package.Cmd = (byte)cmd;
							bytes = package.GetBytes();	
						}
				
			 	
				Console.WriteLine("Будем передавать следующие байты на com-порт");
				LogFileWriter.WriteInfoToFile("Будем передавать следующие байты на com-порт");
				for(Int32 i=0;i!=bytes.Length;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, bytes[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, bytes[i]));
				}
				
                if (bytes != null) {this._ComPort.Write(bytes, 0, bytes.Length);}

                return null;
            }
            else
            {
                Package package = new Package();
                package.Cmd = (byte)cmd;

                if (Data != null) { package.Data = Data; }

                byte[] CmdBytes = package.GetBytes();
				
				Console.WriteLine("Будем передавать следующие байты на com-порт");
				LogFileWriter.WriteInfoToFile("Будем передавать следующие байты на com-порт");
				for(Int32 i=0;i!=CmdBytes.Length;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, CmdBytes[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, CmdBytes[i]));
				}
				
                this._ComPort.Write(CmdBytes, 0, CmdBytes.Length);

				
                // Подождем пока получим данные с ком-порта
				Console.WriteLine("Подождем");
				LogFileWriter.WriteInfoToFile("Подождем");
                this._SynchCom.WaitOne(EVENT_WAIT_HANDLER_TIMEOUT);
                this._SynchCom.Reset();
				
				//tmp
					/*String receivedString=this._ComPort.ReadExisting();
						Console.WriteLine("С com-порта получили строку '{0}'",receivedString);
					System.Text.ASCIIEncoding  encoding=new System.Text.ASCIIEncoding();
					
					this._ReceivedBytes=new List<byte>(encoding.GetBytes(receivedString));*/
				//===
				
				//tmp again
					this.ReadAllAvailableData();
				//=========
				
                byte[] ByteResult = this._ReceivedBytes.ToArray();
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Length;i++)
				{
					Console.WriteLine(CashCodeBillValidator.HexByteValueOutputFormater, i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeBillValidator.HexByteValueOutputFormater,i, ByteResult[i]));
				}
				

                // Если CRC ок, то проверим четвертый бит с результатом
                // Должны уже получить данные с ком-порта, поэтому проверим CRC
                if (ByteResult.Length == 0 || !Package.CheckCRC(ByteResult))
                {
					Console.WriteLine("Проверка CRC не прошла, генерируем исключение. Длинна пакета = {0}", ByteResult.Length);
					LogFileWriter.WriteInfoToFile(String.Format("Проверка CRC не прошла, генерируем исключение. Длинна пакета = {0}", ByteResult.Length));
					Console.WriteLine("Вышли из 'SendCommand'");
					LogFileWriter.WriteInfoToFile("Вышли из 'SendCommand'");
                    throw new ArgumentException("Несоответствие контрольной суммы полученного сообщения. Возможно устройство не подключено к COM-порту. Проверьте настройки подключения.");
                }
				else
				{
					Console.WriteLine("Проверка CRC выполнена успешно");
					LogFileWriter.WriteInfoToFile("Проверка CRC выполнена успешно");
				}

				Console.WriteLine("Вышли из 'SendCommand'");
				LogFileWriter.WriteInfoToFile("Вышли из 'SendCommand'");
                return ByteResult;
            }

        }

        // Таблица кодов валют
        private int CashCodeTable(byte code)
        {
            Console.WriteLine("Зашли из 'CashCodeTable'");
			LogFileWriter.WriteInfoToFile("Зашли из 'CashCodeTable'");
			int result = 0;

            if (code == 0x02) { result = 10; }          // 10 р.
            else if (code == 0x03) { result = 50; }     // 50 р.
            else if (code == 0x04) { result = 100; }    // 100 р.
            else if (code == 0x05) { result = 500; }    // 500 р.
            else if (code == 0x06) { result = 1000; }   // 1000 р.
            else if (code == 0x07) { result = 5000; }   // 5000 р.

			Console.WriteLine("Вышли из 'CashCodeTable'");
			LogFileWriter.WriteInfoToFile("Вышли из 'CashCodeTable'");
            return result;
        }

        #endregion

        #region События



        private void OnBillReceived(BillReceivedEventArgs e)
        {
			Console.WriteLine("Зашли из 'OnBillReceived'");
			LogFileWriter.WriteInfoToFile("Зашли из 'OnBillReceived'");
            if (BillReceived != null)
            {
                BillReceived(this, new BillReceivedEventArgs(e.Status, e.Value, e.RejectedReason));
            }
			Console.WriteLine("Вышли из 'OnBillReceived'");
			LogFileWriter.WriteInfoToFile("Вышли из 'OnBillReceived'");
        }

        
        private void OnBillCassetteStatus(BillCassetteEventArgs e)
        {
			Console.WriteLine("Зашли из 'OnBillCassetteStatus'");
			LogFileWriter.WriteInfoToFile("Зашли из 'OnBillCassetteStatus'");
            if (BillCassetteStatusEvent != null)
            {
                BillCassetteStatusEvent(this, new BillCassetteEventArgs(e.Status));
            }
			Console.WriteLine("Вышли из 'StartListening'");
			LogFileWriter.WriteInfoToFile("Вышли из 'StartListening'");
        }




        private void OnBillStacking(BillStackedEventArgs e)
        {
			
            if (BillStacking != null)
            {
                bool cancel = false;
                foreach (BillStackingHandler subscriber in BillStacking.GetInvocationList())
                {
                    subscriber(this, e);

                    if (e.Cancel)
                    {
                        cancel = true;
                        break;
                    }
                }

                _ReturnBill = cancel;
            }
        }

        #endregion

		#region Метод для считывания всех доступных байтов из Com-порта
			protected void ReadAllAvailableData()
			{
				Console.WriteLine("Зашли в метод 'ReadAllAvailableData'");
				LogFileWriter.WriteInfoToFile("Зашли в метод 'ReadAllAvailableData'");
			
				// Заснем на 100 мс, дабы дать программе получить все данные с ком-порта
    	        Thread.Sleep(100);
        	    this._ReceivedBytes.Clear();

            	// Читаем байты
	            while (_ComPort.BytesToRead > 0)
    	        {
        	        this._ReceivedBytes.Add(Convert.ToByte(this._ComPort.ReadByte()));
            	}

				Console.WriteLine("Выходим из метода 'ReadAllAvailableData'");
				LogFileWriter.WriteInfoToFile("Выходим из метода 'ReadAllAvailableData'");
			}
		#endregion
		
		
        #region Обработчики событий

        // Получение данных с ком-порта
        private void _ComPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Console.WriteLine("Зашли в метод '_ComPort_DataReceived'");
			LogFileWriter.WriteInfoToFile("Зашли в метод '_ComPort_DataReceived'");
			
			// Заснем на 100 мс, дабы дать программе получить все данные с ком-порта
            Thread.Sleep(100);
            this._ReceivedBytes.Clear();

            // Читаем байты
            while (_ComPort.BytesToRead > 0)
            {
                this._ReceivedBytes.Add((byte)_ComPort.ReadByte());
            }

            // Снимаем блокировку
            this._SynchCom.Set();
			Console.WriteLine("Выходим из метода '_ComPort_DataReceived'");
			LogFileWriter.WriteInfoToFile("Выходим из метода '_ComPort_DataReceived'");
        }

        // Таймер прослушки купюроприемника
        private void _Listener_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this._Listener.Stop();

            try
            {
                lock (_Locker)
                {
                    List<byte> ByteResult = null;

                    // отпавить команду POLL
                    ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.POLL));

                    // Если четвертый бит не Idling (незанятый), то идем дальше
                    if (ByteResult[3] != 0x14)
                    {
                        // ACCEPTING
                        //Если получили ответ 15H (Accepting)
                        if (ByteResult[3] == 0x15)
                        {
                            // Подтверждаем
                            this.SendCommand(BillValidatorCommands.ACK);
                        }

                        // ESCROW POSITION  
                        // Если четвертый бит 1Сh (Rejecting), то купюроприемник не распознал купюру
                        else if (ByteResult[3] == 0x1C)
                        {
                            // Принялии какую-то купюру
                            this.SendCommand(BillValidatorCommands.ACK);

                            OnBillReceived(new BillReceivedEventArgs(BillRecievedStatus.Rejected, 0, this._ErrorList.Errors[ByteResult[4]]));
                        }

                        // ESCROW POSITION
                        // купюра распознана
                        else if (ByteResult[3] == 0x80)
                        {
                            // Подтветждаем
                            this.SendCommand(BillValidatorCommands.ACK);

                            // Событие, что купюра в процессе отправки в стек
                            OnBillStacking(new BillStackedEventArgs(CashCodeTable(ByteResult[4])));

                            // Если программа отвечает возвратом, то на возврат
                            if (this._ReturnBill)
                            {
                                // RETURN
                                // Если программа отказывает принимать купюру, отправим RETURN
                                ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.RETURN));
                                this._ReturnBill = false;
                            }
                            else
                            {
                                // STACK
                                // Если равпознали, отправим купюру в стек (STACK)
                                ByteResult =new List<byte>(this.SendCommand(BillValidatorCommands.STACK));
                            }
                        }

                        // STACKING
                        // Если четвертый бит 17h, следовательно идет процесс отправки купюры в стек (STACKING)
                        else if (ByteResult[3] == 0x17)
                        {
                            this.SendCommand(BillValidatorCommands.ACK);
                        }

                        // Bill stacked
                        // Если четвертый бит 81h, следовательно, купюра попала в стек
                        else if (ByteResult[3] == 0x81)
                        {
                            // Подтветждаем
                            this.SendCommand(BillValidatorCommands.ACK);

                            OnBillReceived(new BillReceivedEventArgs(BillRecievedStatus.Accepted, CashCodeTable(ByteResult[4]), ""));
                        }

                        // RETURNING
                        // Если четвертый бит 18h, следовательно идет процесс возврата
                        else if (ByteResult[3] == 0x18)
                        {
                            this.SendCommand(BillValidatorCommands.ACK);
                        }

                        // BILL RETURNING
                        // Если четвертый бит 82h, следовательно купюра возвращена
                        else if (ByteResult[3] == 0x82)
                        {
                            this.SendCommand(BillValidatorCommands.ACK);
                        }

                        // Drop Cassette out of position
                        // Снят купюроотстойник
                        else if (ByteResult[3] == 0x42)
                        {
                            if (_cassettestatus != BillCassetteStatus.Removed)
                            {
                                // fire event
                                _cassettestatus = BillCassetteStatus.Removed;
                                OnBillCassetteStatus(new BillCassetteEventArgs(_cassettestatus));

                            }
                        }

                        // Initialize
                        // Кассета вставлена обратно на место
                        else if (ByteResult[3] == 0x13)
                        {
                            if (_cassettestatus == BillCassetteStatus.Removed)
                            {
                                // fire event
                                _cassettestatus = BillCassetteStatus.Inplace;
                                OnBillCassetteStatus(new BillCassetteEventArgs(_cassettestatus));
                            }
                        }
                    }
                }
            }
            catch
            {}
            finally
            {
                // Если таймер выключен, то запускаем
                if (!this._Listener.Enabled && this._IsListening)
                    this._Listener.Start();
            }

        }

        #endregion
    }

    /// <summary>
    /// Класс аргументов события получения купюры в купюроприемнике
    /// </summary>
    public class BillReceivedEventArgs : EventArgs
    {

        public BillRecievedStatus Status { get; private set; }
        public int Value { get; private set; }
        public string RejectedReason { get; private set; }

        public BillReceivedEventArgs(BillRecievedStatus status, int value, string rejectedReason)
        {
            this.Status = status;
            this.Value = value;
            this.RejectedReason = rejectedReason;
        }
    }

    public class BillCassetteEventArgs : EventArgs
    {

        public BillCassetteStatus Status { get; private set; }

        public BillCassetteEventArgs(BillCassetteStatus status)
        {
            this.Status = status;
        }
    }

    public class BillStackedEventArgs : CancelEventArgs
    {
        public int Value { get; private set; }

        public BillStackedEventArgs(int value)
        {
            this.Value = value;
            this.Cancel = false;
        }
    }
}

