using System;
using System.IO.Ports;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.ComponentModel;

using System.IO;



namespace ConsoleCashCode2
{
 	public enum BillValidatorCommands : byte { ACK=0x00, NAK=0xFF, POLL=0x33, RESET=0x30, GET_STATUS=0x31, SET_SECURITY=0x32,
                                        IDENTIFICATION=0x37, ENABLE_BILL_TYPES=0x34, STACK=0x35, RETURN=0x36, HOLD=0x38}


	public enum BillToBillReceivedCodes : byte {Initialize=0x13, Idling=0x14, Accepting=0x15, Stacking=0x17, Returning=0x18,
												Rejecting=0x1C, EscrowPosition=0x80, BillStacked=0x81, BillReturned=0x82,
												InvalidCommand=0x30, DropCassetteFull=0x41, DropCassetteOutOfPosition=0x42, 
												BillValidatorJammed=0x43, CassetteJammed=0x44, Cheated=0x45, Pause=0x46, GenericFailureCode=0x47}

	public enum GenericFailureCodes : byte {StackMotorFailure = 0x50, TransportMotorSpeedFailure=0x51, TransportMotorFailure=0x52,
											AligningMotorFailure=0x53, InitialBoxStatusFailure=0x54, OpticCanalFailure=0x55,
											MagneticCanalFailure=0x56, CapacitanceCanalFailure=0x5F}

	public enum CurrencyCodes : byte {TenRubles=0x02, FiftyRubles=0x03, OneHundredRubles=0x04, FiveHundredRubles=0x05, OneThousandRubles=0x06, FiveThousandRubles=0x07}

    public enum BillRecievedStatus {Accepted, Rejected };

    public enum BillCassetteStatus { Inplace, Removed };

	public enum ResponseType { ACK, NAK };

    // Делегат события получения банкноты
    public delegate void BillReceivedHandler(object Sender, BillReceivedEventArgs e);

    // Делегат события для контроля за кассетой
    public delegate void BillCassetteHandler(object Sender, BillCassetteEventArgs e);

    // Делегат события в процессе отправки купюры в стек
    public delegate void BillStackingHandler(object Sender, BillStackedEventArgs e);






	#region Класс для представления ошибок купюроприемника
	    public class CashCodeErroList
    	{
			#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА CashCodeErroList
				protected Dictionary<Int32, String> errors= new Dictionary<Int32, String>();
			#endregion
			
			#region Метод для доступа к полю Errors как к свойству
				public Dictionary<Int32, String> Errors 
				{
					get 
					{
						return this.errors;
					}
					protected set 
					{
						this.errors=value;
					}
				}
			#endregion

			#region Конструктор класса CashCodeErroList
	    	    public CashCodeErroList()
    	    	{
	        	    Errors.Add(100000, "Неизвестная ошибка");

    	        	Errors.Add(100010, "Ошибка открытия Com-порта");
	        	    Errors.Add(100020, "Com-порт не открыт");
    	        	Errors.Add(100030, "Ошибка отпраки команды включения купюроприемника.");
	    	        Errors.Add(100040, "Ошибка отпраки команды включения купюроприемника. От купюроприемника не получена команда POWER UP.");
    	    	    Errors.Add(100050, "Ошибка отпраки команды включения купюроприемника. От купюроприемника не получена команда ACK.");
        	    	Errors.Add(100060, "Ошибка отпраки команды включения купюроприемника. От купюроприемника не получена команда INITIALIZE.");
	            	Errors.Add(100070, "Ошибка проверки статуса купюроприемника. Cтекер снят.");
		            Errors.Add(100080, "Ошибка проверки статуса купюроприемника. Стекер переполнен.");
    		        Errors.Add(100090, "Ошибка проверки статуса купюроприемника. В валидаторе застряла купюра.");
        		    Errors.Add(100100, "Ошибка проверки статуса купюроприемника. В стекере застряла купюра.");
            		Errors.Add(100110, "Ошибка проверки статуса купюроприемника. Фальшивая купюра.");
		            Errors.Add(100120, "Ошибка проверки статуса купюроприемника. Предыдущая купюра еще не попала в стек и находится в механизме распознавания.");

    		        Errors.Add(100130, "Ошибка работы купюроприемника. Сбой при работе механизма стекера.");
        		    Errors.Add(100140, "Ошибка работы купюроприемника. Сбой в скорости передачи купюры в стекер.");
            		Errors.Add(100150, "Ошибка работы купюроприемника. Сбой передачи купюры в стекер.");
	            	Errors.Add(100160, "Ошибка работы купюроприемника. Сбой механизма выравнивания купюр.");
	    	        Errors.Add(100170, "Ошибка работы купюроприемника. Сбой в работе стекера.");
    	    	    Errors.Add(100180, "Ошибка работы купюроприемника. Сбой в работе оптических сенсоров.");
        	    	Errors.Add(100190, "Ошибка работы купюроприемника. Сбой работы канала индуктивности.");
	        	    Errors.Add(100200, "Ошибка работы купюроприемника. Сбой в работе канала проверки заполняемости стекера.");

    	        	// Ошибки распознования купюры
	        	    Errors.Add(0x60, "Rejecting due to Insertion");
    	        	Errors.Add(0x61, "Rejecting due to Magnetic");
	    	        Errors.Add(0x62, "Rejecting due to Remained bill in head");
    	    	    Errors.Add(0x63, "Rejecting due to Multiplying");
        	    	Errors.Add(0x64, "Rejecting due to Conveying");
	            	Errors.Add(0x65, "Rejecting due to Identification1");
		            Errors.Add(0x66, "Rejecting due to Verification");
    		        Errors.Add(0x67, "Rejecting due to Optic");
        		    Errors.Add(0x68, "Rejecting due to Inhibit");
            		Errors.Add(0x69, "Rejecting due to Capacity");
		            Errors.Add(0x6A, "Rejecting due to Operation");
    		        Errors.Add(0x6C, "Rejecting due to Length");
        		}
			#endregion
    	}
	#endregion






	#region Класс для представления отсылаемого пакета
	    public sealed class Package
    	{
			#region Необходимые константы
		        private const Int32 POLYNOMIAL =  0x08408;     // Необходима для расчета CRC
    		    private const Byte _Sync =      0x02;        // Бит синхронизации (фиксированный)
        		private const Byte _Adr =       0x03;        // Переферийный адрес оборования. Для купюропиемника из документации равен 0x03			
		
		
				protected const String CheckCRCValueInfo="OldCRC[0]=0x{0:X2}h;\nOldCRC[1]=0x{1:X2}h;\nNewCRC[0]=0x{2:X2}h;\nNewCRC[1]=0x{3:X2}h;";
			#endregion
		
    	    #region Поля        
		        private Byte _Cmd;
        		private Byte[] _Data;
        	#endregion

	        #region Конструкторы класса
	    	    public Package()
    		    {
					LogFileWriter.OutputStackTrace();
				}

        		public Package(byte cmd, byte[] data)
		        {
					LogFileWriter.OutputStackTrace();
		            this._Cmd = cmd;
        		    this.Data = data;
		        }
        	#endregion

	        #region Свойства
	    	    public byte Cmd
    		    {
            		get 
					{
						LogFileWriter.OutputStackTrace();
						return _Cmd; 
					}
        		    set 
					{ 
						LogFileWriter.OutputStackTrace();
						_Cmd = value; 
					}
        		}

		        public byte[] Data
        		{
		            get 
					{
						LogFileWriter.OutputStackTrace();
						return _Data; 
					}
		            set 
        		    {
						LogFileWriter.OutputStackTrace();
		                if (value.Length + 5 > 250)
        		        {
							throw new ArgumentException(String.Format("Попытка присвоить полю 'Data' массив с длинной {0}, которая превышает установленные пределы", value.Length));
		                }
        		        else
                		{
		                    _Data = new byte[value.Length];
        		            _Data = value;
                		}
		            }
        		}
        	#endregion

	        #region Методы
	    	    // Возвращает массив байтов пакета
    		    public byte[] GetBytes()
		        {
					LogFileWriter.OutputStackTrace();
		            // Буффер пакета (без 2-х байт CRC). Первые четыре байта это SYNC, ADR, LNG, CMD
		            List<byte> Buff = new List<byte>();
            
        		    // Байт 1: Флаг синхронизации
		            Buff.Add(_Sync);

        		    // Байт 2: адрес устройства
		            Buff.Add(_Adr);

        		    // Байт 3: длина пакета
		            // рассчитаем длину пакета
        		    int result = this.GetLength();

		            // Если длина пакета вместе с байтами SYNC, ADR, LNG, CRC, CMD  больше 250
        		    if (result > 250)
		            {
        		        // то делаем байт длины равный 0, а действительная длина сообщения будет в DATA
                		Buff.Add(0);
		            }
        		    else
		            {
        		        Buff.Add(Convert.ToByte(result));
		            }

        		    // Байт 4: Команда
		            Buff.Add(this._Cmd);

        		    // Байты с 4: Данные
		            if (this._Data != null)
        		    {
                		for (int i = 0; i < _Data.Length; i++)
		                {
							Buff.Add(this._Data[i]); 
						}
		            }

		            // Последний байт - CRC
        		    byte[] CRC = BitConverter.GetBytes(GetCRC16(Buff.ToArray(), Buff.Count));

		            byte[] package = new byte[Buff.Count + CRC.Length];
        		    Buff.ToArray().CopyTo(package, 0);
		            CRC.CopyTo(package, Buff.Count);

        		    return package;
        		}

		        // Возвращает строку шестнадцатиричного представления байтов пакета
        		public string GetBytesHex()
        		{
					LogFileWriter.OutputStackTrace();
        		    byte[] package = GetBytes();

		            StringBuilder hexString = new StringBuilder(package.Length);
        		    for (int i = 0; i < package.Length; i++)
		            {
        		        hexString.Append(package[i].ToString("X2"));
		            }

        		    return "0x" + hexString.ToString();
		        }

		        // Длина пакета
        		public int GetLength()
		        {
					LogFileWriter.OutputStackTrace();
		            return (this._Data == null ? 0 : this._Data.Length) + 6;
        		}

		        // Расчет контрольной суммы
        		private static short GetCRC16(byte[] BufData, int SizeData)
		        {
					LogFileWriter.OutputStackTrace();
		            int TmpCRC, CRC;
        		    CRC = 0;

		            for (int i = 0; i < SizeData; i++)
        		    {
                		TmpCRC = CRC ^ BufData[i];

		                for (byte j = 0; j < 8; j++)
        		        {
                		    if ((TmpCRC & 0x0001) != 0) 
							{
								TmpCRC >>= 1; 
								TmpCRC ^= POLYNOMIAL; 
							}
        		            else 
							{ 
								TmpCRC >>= 1; 
							}
		                }

        		        CRC = TmpCRC;
            		}

		            return (short)CRC;
        		}

		        public static bool CheckCRC(byte[] Buff)
        		{
					LogFileWriter.OutputStackTrace();
        		    bool result = true;

		            byte[] OldCRC = new byte[] { Buff[Buff.Length - 2], Buff[Buff.Length - 1]};

        		    // Два последних байта в длине убираем, так как это исходная CRC
		            byte[] NewCRC = BitConverter.GetBytes(GetCRC16(Buff, Buff.Length - 2));

        		    for (int i = 0; i < 2; i++)
		            {
        		        if (OldCRC[i] != NewCRC[i])
                		{
		                    result = false;
        		            break;
                		}
		            }
			
					Console.WriteLine(Package.CheckCRCValueInfo, OldCRC[0], OldCRC[1], NewCRC[0], NewCRC[1]);

		            return result;
        		}

		        public static byte[] CreateResponse(ResponseType type)
        		{
					LogFileWriter.OutputStackTrace();
        		    // Буффер пакета (без 2-х байт CRC). Первые четыре байта это SYNC, ADR, LNG, CMD
		            List<byte> Buff = new List<byte>();

        		    // Байт 1: Флаг синхронизации
		            Buff.Add(_Sync);

        		    // Байт 2: адрес устройства
		            Buff.Add(_Adr);

        		    // Байт 3: длина пакета, всегда 6
		            Buff.Add(0x06);
           
        		    // Байт 4: Данные
					switch(type)
					{
						case(ResponseType.ACK):
						{
							Buff.Add(0x00);	
							break;
						}
						case(ResponseType.NAK):
						{
							Buff.Add(0xFF);
							break;
						}
					}


            // Последний байт - CRC
            byte[] CRC = BitConverter.GetBytes(GetCRC16(Buff.ToArray(), Buff.Count));

            byte[] package = new byte[Buff.Count + CRC.Length];
            Buff.ToArray().CopyTo(package, 0);
            CRC.CopyTo(package, Buff.Count);

            return package;
        }

        #endregion
    }
	#endregion

    public class CashCodeClass : IDisposable
    {
		#region НЕОБХОДИМЫЕ КОНСТАНТЫ КЛАССА CashCodeClass
			protected const Int32 POLL_TIMEOUT = 200;    // Тайм-аут ожидания ответа от считывателя
    	    protected const Int32 EVENT_WAIT_HANDLER_TIMEOUT = 10000; // Тайм-аут ожидания снятия блокировки
			protected const Int32 DataBits=8;
			protected const String HexByteValueOutputFormater="Byte[{0}] = 0x{1:X2}h";
		#endregion

		#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА CashCodeClass        
        	protected Byte[] ENABLE_BILL_TYPES_WITH_ESCROW = new Byte[6] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

	        protected EventWaitHandle synchCom;     // Переменная синхронизации отправки и считывания данных с com-порта
    	    protected List<Byte> receivedBytes;  // Полученные байты

        	protected Int32 lastError;
	        protected Boolean Disposed;
    	    protected Boolean isConnected;
        	protected Boolean isPowerUp;
	        protected Boolean isListening;
    	    protected Object Locker;

        	protected SerialPort comPort;
			protected CashCodeErroList ErrorList{get;set;}

			protected System.Timers.Timer Listener{get;set;}  // Таймер прослушивания купюроприемника

			protected Boolean ReturnBill{get;set;}

			protected BillCassetteStatus cassetteStatus = BillCassetteStatus.Inplace;

        	//Событие получения купюры
			public event BillReceivedHandler BillReceived;
			//Событие изменения состояния купюроприемника
			public event BillCassetteHandler BillCassetteStatusEvent;
        	//Событие процесса отправки купюры в стек (Здесь можно делать возврат)
        	public event BillStackingHandler BillStacking;
        #endregion



        #region Конструктор класса CashCodeClass(с параметрами)
	        public CashCodeClass(String PortName, Int32 BaudRate)
    	    {
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в 'CashCodeClass'");
				LogFileWriter.WriteInfoToFile("Зашли в 'CashCodeClass'");
			
        	    this.ErrorList = new CashCodeErroList();
            
            	this.Disposed = false;
	            this.Locker = new Object();
    	        this.IsConnected = this.IsPowerUp = this.IsListening = this.ReturnBill = false;

        	    // Из спецификации:
            	//      Baud Rate:	9600 bps/19200 bps (no negotiation, hardware selectable)
	            //      Start bit:	1
    	        //      Data bit:	8 (bit 0 = LSB, bit 0 sent first)
        	    //      Parity:		Parity none 
            	//      Stop bit:	1
	            this.ComPort = new SerialPort();
				this.ComPort.PortName = PortName;
        	    this.ComPort.BaudRate = BaudRate;
            	this.ComPort.DataBits = CashCodeClass.DataBits;
	            this.ComPort.Parity = Parity.None;
    	        this.ComPort.StopBits = StopBits.One;
        	    this.ComPort.DataReceived += new SerialDataReceivedEventHandler(this.ComPort_DataReceivedEventHandler);

            	this.ReceivedBytes = new List<byte>();
	            this.SynchCom = new EventWaitHandle(false, EventResetMode.AutoReset);

    	        this.Listener = new System.Timers.Timer();
        	    this.Listener.Interval = POLL_TIMEOUT;
            	this.Listener.Enabled = false;
	            this.Listener.Elapsed += new System.Timers.ElapsedEventHandler(this.Listener_ElapsedEventHandler);
			
				Console.WriteLine("Выходим из 'CashCodeClass'");
				LogFileWriter.WriteInfoToFile("Выходим из 'CashCodeClass'");
    	    }
        #endregion


		#region Деструктор для финализации кода        
	        ~CashCodeClass() 
			{ 
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в '~CashCodeClass'");
				LogFileWriter.WriteInfoToFile("Зашли в '~CashCodeClass'");
			
				this.Dispose(false); 
			
				Console.WriteLine("Выходим из '~CashCodeClass'");
				LogFileWriter.WriteInfoToFile("Выходим из '~CashCodeClass'");
			}
		#endregion

		#region Метод, реализующий интерфейс IDisposable
	        public void Dispose()
    	    {
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в 'Dispose()'");
				LogFileWriter.WriteInfoToFile("Зашли в 'Dispose()'");
			
        	    Dispose(true);
			 	// отменим финализацию объекта средствами GC после вызова Dispose, так как он уже освобожден
            	GC.SuppressFinalize(this);
			
				Console.WriteLine("Выходим из 'Dispose()'");
				LogFileWriter.WriteInfoToFile("Выходим из 'Dispose()'");
        	}
		#endregion

		#region Метод, который непосредственно реализует финализацию объекта
	        // Dispose(bool disposing) выполняется по двум сценариям
    	    // Если disposing=true, метод Dispose вызывается явно или неявно из кода пользователя
        	// Управляемые и неуправляемые ресурсы могут быть освобождены
	        // Если disposing=false, то метод может быть вызван runtime из финализатора
    	    // В таком случае только неуправляемые ресурсы могут быть освобождены.
        	protected void Dispose(Boolean Disposing)
	        {
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в 'Dispose(Boolean Disposing)'");
				LogFileWriter.WriteInfoToFile("Зашли в 'Dispose(Boolean Disposing)'");
			
            	// Проверим вызывался ли уже метод Dispose
    	        if (!this.Disposed)
        	    {
            	    // Если Disposing=true, освободим все управляемые и неуправляемые ресурсы
                	if (Disposing==true)
	                {
    	                // Здесь освободим управляемые ресурсы
        	            try
            	        {
                	        // Остановим таймер, если он работает
                    	    if (this.IsListening==true)
                        	{
	                            this.Listener.Enabled = this.IsListening = false;
    	                    }

        	                this.Listener.Dispose();

            	            // Отправим сигнал выключения на купюроприемник
                	        if (this.IsConnected)
                    	    {
                        	    this.DisableBillValidator();
	                        }
    	                }
        	            catch 
						{
						}
            	    }

                	// Вызовем соответствующие методы для освобождения неуправляемых ресурсов
	                // Если disposing=false, то только следующий код буде выполнен
    	            try 
        	        {
            	        this.ComPort.Close();
                	}
	                catch 
					{
					}

                	this.Disposed = true;
            	}
			
				Console.WriteLine("Выходим из 'Dispose(Boolean Disposing)'");
				LogFileWriter.WriteInfoToFile("Выходим из 'Dispose(Boolean Disposing)'");
        	}
		#endregion


		#region Метод, необходимый для обращения к полю synchCom как к свойству
	        protected EventWaitHandle SynchCom 
			{
				get 
				{ 					
					return this.synchCom; 
				}
				set 
				{
					this.synchCom=value;
				}
        	}
        #endregion

		#region Метод, необходимый для обращения к полю receivedBytes как к свойству
	        protected List<Byte> ReceivedBytes 
			{
				get 
				{ 
					return this.receivedBytes; 
				}
				set 
				{
					this.receivedBytes=value;
				}
        	}
        #endregion

        #region Метод, необходимый для обращения к полю lastError как к свойству
	        public Int32 LastError 
			{
				get 
				{ 
					return this.lastError; 
				}
				protected set 
				{
					this.lastError=value;
				}
        	}
        #endregion

        #region Метод, необходимый для обращения к полю isConnected как к свойству
	        public Boolean IsConnected 
			{
				get 
				{ 
					return this.isConnected; 
				}
				protected set 
				{
					this.isConnected=value;
				}
        	}
        #endregion

		#region Метод, необходимый для обращения к полю isPowerUp как к свойству
	        protected Boolean IsPowerUp 
			{
				get 
				{ 
					return this.isPowerUp; 
				}
				set 
				{
					this.isPowerUp=value;
				}
        	}
        #endregion

		#region Метод, необходимый для обращения к полю isListening как к свойству
	        protected Boolean IsListening 
			{
				get 
				{ 
					return this.isListening; 
				}
				set 
				{
					this.isListening=value;
				}
        	}
        #endregion

		#region Метод, необходимый для обращения к полю comPort как к свойству
	        protected SerialPort ComPort 
			{
				get 
				{ 
					return this.comPort; 
				}
				set 
				{
					this.comPort=value;
				}
        	}
        #endregion

		#region Метод, необходимый для обращения к полю cassetteStatus как к свойству
	        protected BillCassetteStatus CassetteStatus
			{
				get 
				{ 
					return this.cassetteStatus; 
				}
				set 
				{
					this.cassetteStatus=value;
				}
        	}
        #endregion



        
		#region Начало прослушивания купюроприемника
	        public void StartListening()
    	    {
        	    LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в 'StartListening'");
				LogFileWriter.WriteInfoToFile("Зашли в 'StartListening'");
			
				// Если не подключен
            	if (this.IsConnected==false)
	            {
    	            this.LastError = 100020;
										
					Console.WriteLine("Произошла ошибка: {0}. Выходим из метода 'StartListening'", this.LastError);
					LogFileWriter.WriteInfoToFile(String.Format("Произошла ошибка: {0}. Выходим из метода 'StartListening'", this.LastError));
				
        	        throw new System.IO.IOException(this.ErrorList.Errors[this.LastError]);
            	}

	            // Если отсутствует энергия, то включим
    	        if (this.IsPowerUp==false) 
				{ 
					this.PowerUpBillValidator(); 
				}

        	    this.IsListening = true;
            	this.Listener.Start();
				Console.WriteLine("Выходим из 'StartListening'");
				LogFileWriter.WriteInfoToFile("Выходим из 'StartListening'");
	        }
		#endregion


		#region Остановка прослушивания купюроприемника
	        public void StopListening()
    	    {
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в 'StopListening'");
				LogFileWriter.WriteInfoToFile("Зашли в 'StopListening'");
			
        	    this.IsListening = false;
            	this.Listener.Stop();
	            this.DisableBillValidator();
			
				Console.WriteLine("Выходим из 'StopListening'");
				LogFileWriter.WriteInfoToFile("Выходим из 'StopListening'");
    	    }
		#endregion


		#region Открытие Com-порта для работы с купюроприемником
	        public Int32 ConnectBillValidator ()
			{
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в 'ConnectBillValidator'");
				LogFileWriter.WriteInfoToFile("Зашли в 'ConnectBillValidator'");
				try 
				{
					//нужно будет удалить					
							LogFileWriter.WriteInfoToFile("Пытаемся открыть com-порт");						
					//===================
					this.ComPort.Open ();//валимся скорее всего здесь
					this.IsConnected = true;
				
					//нужно будет удалить
							LogFileWriter.WriteInfoToFile("Com-порт открыт");						
					//===================
				}
        	    catch
            	{
    	           	this.IsConnected = false;
					this.LastError = 100010;
				
					Console.WriteLine("В методе 'ConnectBillValidator' имеем исключение. Передаем ошибку: {0} и выходим", this.LastError);
					LogFileWriter.WriteInfoToFile(String.Format("В методе 'ConnectBillValidator' имеем исключение. Передаем ошибку: {0} и выходим", this.LastError));
	            }
	
				Console.WriteLine("Выходим из 'ConnectBillValidator'");
				LogFileWriter.WriteInfoToFile("Выходим из 'ConnectBillValidator'");
			
    	        return this.LastError;
        	}
		#endregion


		#region Включение купюроприемника
	        public Int32 PowerUpBillValidator()
    	    {
        	    LogFileWriter.OutputStackTrace();
	            Console.WriteLine("Зашли в 'PowerUpBillValidator'");
				LogFileWriter.WriteInfoToFile("Зашли в 'PowerUpBillValidator'");
			
				List<byte> ByteResult = null;

	            // Если ком-порт не открыт
    	        if (!this.IsConnected)
        	    {				
        	        this.LastError = 100020;
				
					Console.WriteLine("Com-порт неоткрыт");
					LogFileWriter.WriteInfoToFile("Com-порт неоткрыт");
					Console.WriteLine("Произошла ошибка. Выходим из 'PowerUpBillValidator' с передачей ошибки '{0}'", this.LastError);
					LogFileWriter.WriteInfoToFile(String.Format("Произошла ошибка. Выходим из 'PowerUpBillValidator' с передачей ошибки '{0}'", this.LastError));
				
            	    throw new System.IO.IOException(this.ErrorList.Errors[this.LastError]);
	            }
				else
				{
					Console.WriteLine("Com-порт был открыт");
					LogFileWriter.WriteInfoToFile("Com-порт был открыт");
				}

    	        //RESET (Ok)
				Console.WriteLine("Отсылаем RESET");
				LogFileWriter.WriteInfoToFile("Отсылаем RESET");
				this.SendCommand(BillValidatorCommands.RESET);
					Console.WriteLine("Подождем");
					LogFileWriter.WriteInfoToFile("Подождем");
            	    this.SynchCom.WaitOne(100);
                	this.SynchCom.Reset();
			
			
			
				// POWER UP (Ok)
				Console.WriteLine("Отсылаем POOL");
				LogFileWriter.WriteInfoToFile("Отсылаем POOL");
            	ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.POLL));
				Console.WriteLine("Получили:");
				LogFileWriter.WriteInfoToFile("Получили:");
				for(Int32 i=0;i!=ByteResult.Count;i++)
				{
					Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]);
					LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]));
				}
			
			
        	    // Проверим результат
				Console.WriteLine("Проверяем результат");
				LogFileWriter.WriteInfoToFile("Проверяем результат");
    	        if (CheckPollOnError(ByteResult.ToArray()))
        	    {
					Console.WriteLine("Результат содержит ошибки!\nОтсылаем на com-порт команду NAK и выбрасываем исключение");	
					LogFileWriter.WriteInfoToFile("Результат содержит ошибки!\nОтсылаем на com-порт команду NAK и выбрасываем исключение");
	                ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.NAK));
					Console.WriteLine("Получили:");
					for(Int32 i=0;i!=ByteResult.Count;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]));
					}
				
					Console.WriteLine("Выходим из 'PowerUpBillValidator' с генерацией ошибки {0}", this.LastError);
					LogFileWriter.WriteInfoToFile(String.Format("Выходим из 'PowerUpBillValidator' с генерацией ошибки {0}", this.LastError));
            	    throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
	            }
				else
				{
					Console.WriteLine("Проверка выполнена успешно");
					LogFileWriter.WriteInfoToFile("Проверка выполнена успешно");
				}

            
				//===tmp===
					Console.WriteLine("Отправляем ACK");
					LogFileWriter.WriteInfoToFile("Отправляем ACK");
        		    this.SendCommand(BillValidatorCommands.ACK);
						Console.WriteLine("Подождем");
						LogFileWriter.WriteInfoToFile("Подождем");
	    	            this.SynchCom.WaitOne(100);
    	    	        this.SynchCom.Reset();
				//=========

            	// RESET
				Console.WriteLine("Отправляем RESET");
				LogFileWriter.WriteInfoToFile("Отправляем RESET");
        	    ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.RESET));
					Console.WriteLine("Получили:");
					LogFileWriter.WriteInfoToFile("Получили:");
					for(Int32 i=0;i!=ByteResult.Count;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]));
					}

	            //Если не получили от купюроприемника сигнала ACK
    	        if (ByteResult[3] != 0x00)
        	    {
					this.LastError = 100050;
					Console.WriteLine("Должны были получить в ByteResult[3] ноль, а получили {0}. Выходим из 'PowerUpBillValidator' с передачей кода ошибки: {1}", ByteResult[3], this.LastError);
					LogFileWriter.WriteInfoToFile(String.Format("Должны были получить в ByteResult[3] ноль, а получили {0}. Выходим из 'PowerUpBillValidator' с передачей кода ошибки: {1}", ByteResult[3], this.LastError));
                				
    	            return this.LastError;
        	    }
				else
				{
					Console.WriteLine("От купюроприемника в 3 байте получена ожидаемая величина");
					LogFileWriter.WriteInfoToFile("От купюроприемника в 3 байте получена ожидаемая величина");
				}

	            // INITIALIZE
    	        // Далее снова опрашиваем купюроприемник
				Console.WriteLine("Отправляем POLL");
				LogFileWriter.WriteInfoToFile("Отправляем POLL");
	            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.POLL));
					Console.WriteLine("Получили:");
					LogFileWriter.WriteInfoToFile("Получили:");
					for(Int32 i=0;i!=ByteResult.Count;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]));
					}
				
            	if (CheckPollOnError(ByteResult.ToArray()))
	            {
					Console.WriteLine("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'PowerUpBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с кодом: '{0}'", this.LastError);
					LogFileWriter.WriteInfoToFile(String.Format("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'PowerUpBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с ошибкой {0}", this.LastError));
					
                	this.SendCommand(BillValidatorCommands.NAK);				
	                throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
    	        }
				else
				{
					Console.WriteLine("Ответ устройства на команду 'POLL' не содержит ошибок");	
					LogFileWriter.WriteInfoToFile("Ответ устройства на команду 'POLL' не содержит ошибок");
				}

        	    // Иначе отправляем сигнал подтверждения
				Console.WriteLine("Отправляем ACK");
				LogFileWriter.WriteInfoToFile("Отправляем ACK");
    	        ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.ACK));
					Console.WriteLine("Получили:");
					LogFileWriter.WriteInfoToFile("Получили:");
					for(Int32 i=0;i!=ByteResult.Count;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]));
					}
			

	            // GET STATUS
				Console.WriteLine("Отправляем GET_STATUS");
				LogFileWriter.WriteInfoToFile("Отправляем GET_STATUS");
            	ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.GET_STATUS));
					Console.WriteLine("Получили:");
					LogFileWriter.WriteInfoToFile("Получили:");
					for(Int32 i=0;i!=ByteResult.Count;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]));
					}	
			
    	        // Команда GET STATUS возвращает 6 байт ответа. Если все равны 0, то статус ok и можно работать дальше, иначе ошибка
        	    if (ByteResult[3] != 0x00 || ByteResult[4] != 0x00 || ByteResult[5] != 0x00 ||
            	    ByteResult[6] != 0x00 || ByteResult[7] != 0x00 || ByteResult[8] != 0x00)
	            {
    	            this.LastError = 100070;
				
					Console.WriteLine("GET_STATUS должна была вернуть 0, но такого не произошло. Выходим из 'PowerUpBillValidator' и генерируем исключение с кодом {0}", this.LastError);
					LogFileWriter.WriteInfoToFile(String.Format("GET_STATUS должна была вернуть 0, но такого не произошло. Выходим из 'PowerUpBillValidator' и генерируем исключение с кодом {0}", this.LastError));

                	throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
	            }
				else
				{
					Console.WriteLine("Результат выполнения команды 'GET_STATUS' верен (все 6 полученных байт содержат нули)");
					LogFileWriter.WriteInfoToFile("Результат выполнения команды 'GET_STATUS' верен (все 6 полученных байт содержат нули)");
				}

			
				Console.WriteLine("Вновь отправляем ACK");
				LogFileWriter.WriteInfoToFile("Отправляем ACK");
            	ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.ACK));
					Console.WriteLine("Получили:");
					LogFileWriter.WriteInfoToFile("Получили:");
					for(Int32 i=0;i!=ByteResult.Count;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]));
					}

    	        // SET_SECURITY (в тестовом примере отправояет 3 байта (0 0 0)
				Console.WriteLine("Отправляем SET_SECURITY");
				LogFileWriter.WriteInfoToFile("Отправляем SET_SECURITY");
	            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.SET_SECURITY, new byte[3] { 0x00, 0x00, 0x00 }));
					Console.WriteLine("Получили:");
					LogFileWriter.WriteInfoToFile("Получили:");
					for(Int32 i=0;i!=ByteResult.Count;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]));
					}	
        	    //Если не получили от купюроприемника сигнала ACK
            	if (ByteResult[3] != 0x00)
	            {
					this.LastError = 100050;
				
					Console.WriteLine("Должны были получить в ByteResult[3] ноль, а получили {0}. Выходим из 'PowerUpBillValidator' с передачей кода ошибки {1}", ByteResult[3], this.LastError);
					LogFileWriter.WriteInfoToFile(String.Format("Должны были получить в ByteResult[3] ноль, а получили {0}. Выходим из 'PowerUpBillValidator' с передачей кода ошибки", ByteResult[3], this.LastError));
                
				
                	return this.LastError;
	            }
				else
				{
					Console.WriteLine("От купюроприемника в 3 байте получена ожидаемая величина");
					LogFileWriter.WriteInfoToFile("От купюроприемника в 3 байте получена ожидаемая величина");
				}

    	        // IDENTIFICATION
				Console.WriteLine("Отправляем IDENTIFICATION");
				LogFileWriter.WriteInfoToFile("Отправляем IDENTIFICATION");
	            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.IDENTIFICATION));
					Console.WriteLine("Получили:");
					LogFileWriter.WriteInfoToFile("Получили:");
					for(Int32 i=0;i!=ByteResult.Count;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]));
					}
			
				Console.WriteLine("Отправляем ACK");
				LogFileWriter.WriteInfoToFile("Отправляем ACK");
	            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.ACK));
					Console.WriteLine("Получили:");
					LogFileWriter.WriteInfoToFile("Получили:");
					for(Int32 i=0;i!=ByteResult.Count;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]));
					}


        	    // POLL
            	// Далее снова опрашиваем купюроприемник. Должны получить команду INITIALIZE
				Console.WriteLine("Отправляем POLL");
				LogFileWriter.WriteInfoToFile("Отправляем POLL");
        	    ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.POLL));
					Console.WriteLine("Получили:");
					LogFileWriter.WriteInfoToFile("Получили:");
					for(Int32 i=0;i!=ByteResult.Count;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]));
					}

	            // Проверим результат
    	        if (CheckPollOnError(ByteResult.ToArray()))
        	    {
					Console.WriteLine("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'PowerUpBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с кодом {0}", this.LastError);
					LogFileWriter.WriteInfoToFile(String.Format("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'PowerUpBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с кодом {0}", this.LastError));
	                this.SendCommand(BillValidatorCommands.NAK);
				
    	            throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
        	    }
				else
				{
					Console.WriteLine("Ответ устройства на команду 'POLL' не содержит ошибок");	
					LogFileWriter.WriteInfoToFile("Ответ устройства на команду 'POLL' не содержит ошибок");
				}

	            // Иначе отправляем сигнал подтверждения
				Console.WriteLine("Отправляем ACK");
				LogFileWriter.WriteInfoToFile("Отправляем ACK");
            	ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.ACK));
					Console.WriteLine("Получили:");
					LogFileWriter.WriteInfoToFile("Получили:");
					for(Int32 i=0;i!=ByteResult.Count;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]));
					}

    	        // POLL
        	    // Далее снова опрашиваем купюроприемник. Должны получить команду UNIT DISABLE
				Console.WriteLine("Отправляем POLL");
				LogFileWriter.WriteInfoToFile("Отправляем POLL");
    	        ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.POLL));
					Console.WriteLine("Получили:");
					LogFileWriter.WriteInfoToFile("Получили:");
					for(Int32 i=0;i!=ByteResult.Count;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]));
					}			
	
       	     // Проверим результат
        	    if (CheckPollOnError(ByteResult.ToArray()))
            	{
					Console.WriteLine("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'PowerUpBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с кодом {0}", this.LastError);
					LogFileWriter.WriteInfoToFile(String.Format("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'PowerUpBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с кодом {0}", this.LastError));
    	            this.SendCommand(BillValidatorCommands.NAK);
	
        	        throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
            	}
				else
				{
					Console.WriteLine("Ответ устройства на команду 'POLL' не содержит ошибок");	
					LogFileWriter.WriteInfoToFile("Ответ устройства на команду 'POLL' не содержит ошибок");	
				}

    	        // Иначе отправляем сигнал подтверждения
				Console.WriteLine("Отправляем ACK");
				LogFileWriter.WriteInfoToFile("Отправляем ACK");
	            ByteResult = new List<byte>(this.SendCommand(BillValidatorCommands.ACK));
					Console.WriteLine("Получили:");
					LogFileWriter.WriteInfoToFile("Получили:");
					for(Int32 i=0;i!=ByteResult.Count;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]));
					}	
			
        	    this.IsPowerUp = true;
				Console.WriteLine("Выходим из 'PowerUpBillValidator'");
				LogFileWriter.WriteInfoToFile("Выходим из 'PowerUpBillValidator'");
    	        return this.LastError;
        	}
		#endregion

        

		#region Включение режима приема купюр
	        public Int32 EnableBillValidator()
    	    {
        	    LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в 'EnableBillValidator'");
				LogFileWriter.WriteInfoToFile("Зашли в 'EnableBillValidator'");
			
				Byte[] byteResult = null;

            	// Если com-порт не открыт
	            if (this.IsConnected==false)
    	        {
        	        this.LastError = 100020;
            	    throw new System.IO.IOException(this.ErrorList.Errors[this.LastError]);
	            }

    	        try
        	    {
            	    if (!IsListening)
                	{
						Console.WriteLine("Выходим из 'EnableBillValidator' и генерируем исключение: Ошибка метода включения приема купюр. Необходимо вызвать метод StartListening.");
						LogFileWriter.WriteInfoToFile("Выходим из 'EnableBillValidator' и генерируем исключение: Ошибка метода включения приема купюр. Необходимо вызвать метод StartListening.");
                    	throw new InvalidOperationException("Ошибка метода включения приема купюр. Необходимо вызвать метод StartListening.");
	                }

    	            lock (Locker)
        	        {
                	    // отправить команду ENABLE BILL TYPES (в тестовом примере отправляет 6 байт  (255 255 255 0 0 0) Функция удержания включена (Escrow)
                    	byteResult = this.SendCommand(BillValidatorCommands.ENABLE_BILL_TYPES, ENABLE_BILL_TYPES_WITH_ESCROW);

	                    //Если не получили от купюроприемника сигнала ACK
    	                if (byteResult[3] != 0x00)
        	            {
            	            this.LastError = 100050;
							
							Console.WriteLine("Должны были получить в byteResult[3] ноль, а получили {0}. Выходим из 'EnableBillValidator' с генерированием исключения {1}", byteResult[3], this.LastError);
							LogFileWriter.WriteInfoToFile(String.Format("Должны были получить в byteResult[3] ноль, а получили {0}. Выходим из 'EnableBillValidator' с генерированием исключения {1}", byteResult[3], this.LastError));
						
                	        throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
                    	}

	                    // Далее снова опрашиваем купюроприемник
    	                byteResult = this.SendCommand(BillValidatorCommands.POLL);
						
	                    // Проверим результат
    	                if (CheckPollOnError(byteResult))
        	            {
							Console.WriteLine("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'EnableBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с кодом {0}", this.LastError);
							LogFileWriter.WriteInfoToFile(String.Format("Имеем ошибку при проверке методом CheckPollOnError. Выходим из 'EnableBillValidator' с отправкой на com-порт сообщения NAK и генерированием исключения с кодом {0}", this.LastError));
						
            	            this.SendCommand(BillValidatorCommands.NAK);
                	        throw new System.ArgumentException(this.ErrorList.Errors[this.LastError]);
                    	}

	                    // Иначе отправляем сигнал подтверждения
    	                this.SendCommand(BillValidatorCommands.ACK);
        	        }
            	}
	            catch
    	        {									
        	        this.LastError = 100030;
				
					Console.WriteLine("В методе 'EnableBillValidator' имеем исключение. Передаем ошибку: {0} и выходим", this.LastError);
					LogFileWriter.WriteInfoToFile(String.Format("В методе 'EnableBillValidator' имеем исключение. Передаем ошибку: {0} и выходим", this.LastError));
            	}

				Console.WriteLine("Выходим из 'EnableBillValidator'");
				LogFileWriter.WriteInfoToFile("Выходим из 'EnableBillValidator'");
			
	            return this.LastError;
    	    }
		#endregion

		#region Выключение режима приема купюр
	        public Int32 DisableBillValidator()
    	    {
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в 'DisableBillValidator'");
				LogFileWriter.WriteInfoToFile("Зашли в 'DisableBillValidator'");
			
        	    Byte[] byteResult = null;

            	lock (Locker)
	            {
    	            // Если Com-порт не открыт
        	        if (this.IsConnected==false)
            	    {
                	    this.LastError = 100020;
						
						Console.WriteLine("Порт неоткрыт. Выходим из 'DisableBillValidator' с генерированием исключения с кодом {0}", this.LastError);
						LogFileWriter.WriteInfoToFile(String.Format("Порт неоткрыт. Выходим из 'DisableBillValidator' с генерированием исключения с кодом {0}", this.LastError));
					
                    	throw new System.IO.IOException(this.ErrorList.Errors[this.LastError]);
	                }

        	        // отпавить команду ENABLE BILL TYPES (в тестовом примере отправляет 6 байт (0 0 0 0 0 0)
            	    byteResult = this.SendCommand(BillValidatorCommands.ENABLE_BILL_TYPES, new Byte[6] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 });
	            }

    	        //Если не получили от купюроприемника сигнала ACK
        	    if (byteResult[3] != 0x00)
            	{
                	this.LastError = 100050;
				
					Console.WriteLine("Должны были получить в byteResult[3] ноль, а получили {0}. Выходим из 'DisableBillValidator' с генерированием исключения {1}", byteResult[3], this.LastError);
				
	                return this.LastError;
    	        }

				Console.WriteLine("Выходим из 'DisableBillValidator'");
				LogFileWriter.WriteInfoToFile("Выходим из 'DisableBillValidator'");
			
        	    return this.LastError;
        	}
		#endregion





        #region Метод для проверки полученных сообщений от купюроприемника
	        protected Boolean CheckPollOnError(Byte[] ByteResult)
    	    {
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в 'CheckPollOnError'");
				LogFileWriter.WriteInfoToFile("Зашли в 'CheckPollOnError'");
			
        	    Boolean returnedValue;
				
				switch(this.ConvertByteToEnum<BillToBillReceivedCodes>(ByteResult[3]))
				{
	            	//Если получили от купюроприемника третий байт равный 30Н (INVALID COMMAND )
					case(BillToBillReceivedCodes.InvalidCommand):
    		        {
        		        this.LastError = 100040;
            		    returnedValue = true;
						break;
		            }
		            //Если получили от купюроприемника третий байт равный 41Н (Drop Cassette Full)
					case(BillToBillReceivedCodes.DropCassetteFull):
        		    {
            		    this.LastError = 100080;
                		returnedValue = true;
						break;
		            }
    		        //Если получили от купюроприемника третий байт равный 42Н (Drop Cassette out of position)
					case(BillToBillReceivedCodes.DropCassetteOutOfPosition):
            		{
                		this.LastError = 100070;
	                	returnedValue = true;
						break;
	    	        }
    	    	    //Если получили от купюроприемника третий байт равный 43Н (Bill Validator Jammed)
					case(BillToBillReceivedCodes.BillValidatorJammed):
	        	    {
    	        	    this.LastError = 100090;
        	        	returnedValue = true;
						break;
	            	}
		            //Если получили от купюроприемника третий байт равный 44Н (Cassette Jammed)
					case(BillToBillReceivedCodes.CassetteJammed):
        		    {
            		    this.LastError = 100100;
                		returnedValue = true;
						break;
		            }
    		        //Если получили от купюроприемника третий байт равный 45Н (Cheated)
					case(BillToBillReceivedCodes.Cheated):
            		{
                		this.LastError = 100110;
	                	returnedValue = true;
						break;
	    	        }
    	    	    //Если получили от купюроприемника третий байт равный 46Н (Pause)
					case(BillToBillReceivedCodes.Pause):
	        	    {
    	        	    this.LastError = 100120;
        	        	returnedValue = true;
						break;
	            	}
		            //Если получили от купюроприемника третий байт равный 47Н (Generic Failure codes)
					//при получении 47H, мы имеем Generic Failure codes - обобщенную ошибку, расширение которой находится в четвертом байте, поэтому вновь используем switch
					case(BillToBillReceivedCodes.GenericFailureCode):
        		    {
            		    switch(this.ConvertByteToEnum<GenericFailureCodes>(ByteResult[4]))
						{
							// Stack Motor Failure
							case(GenericFailureCodes.StackMotorFailure):
							{ 
								this.LastError = 100130; 
								break;
							}
					   		// Transport Motor Speed Failure
							case(GenericFailureCodes.TransportMotorSpeedFailure):
							{ 
								this.LastError = 100140; 
								break;
							}
					   		// Transport Motor Failure
							case(GenericFailureCodes.TransportMotorFailure):
							{ 
								this.LastError = 100150;
								break;
							}
							// Aligning Motor Failure
							case(GenericFailureCodes.AligningMotorFailure):
							{ 
								this.LastError = 100160; 
								break;
							}
							// Initial Box Status Failure
							case(GenericFailureCodes.InitialBoxStatusFailure):
							{ 
								this.LastError = 100170; 
								break;
							}
					   		// Optic Canal Failure
							case(GenericFailureCodes.OpticCanalFailure):
							{ 
								this.LastError = 100180; 
								break;
							}
					   		// Magnetic Canal Failure
							case(GenericFailureCodes.MagneticCanalFailure):
							{ 
								this.LastError = 100190; 
								break;
							}
					   		// Capacitance Canal Failure
							case(GenericFailureCodes.CapacitanceCanalFailure):
							{ 
								this.LastError = 100200; 
								break;
							}
						}
	    	            returnedValue = true;
						break;
    	    	    }
					default:
					{
						returnedValue=false;
						break;
					}
				}

				Console.WriteLine("Выходим из 'CheckPollOnError'. Имеется ли ошибка='{0}'. Если ошибка есть, то ее код: '{1}'", returnedValue, this.LastError);
				LogFileWriter.WriteInfoToFile(String.Format("Выходим из 'CheckPollOnError'. Имеется ли ошибка='{0}'. Если ошибка есть, то ее код: '{1}'", returnedValue, this.LastError));
			
            	return returnedValue;
        	}
		#endregion
        

		#region Отправка команды купюроприемнику
	    	private byte[] SendCommand(BillValidatorCommands cmd, byte[] Data = null)
	        {
				LogFileWriter.OutputStackTrace();
        	    Console.WriteLine("Зашли в SendCommand");
				LogFileWriter.WriteInfoToFile("Зашли в SendCommand");
				Console.WriteLine("Передается команда {0}", cmd);
				LogFileWriter.WriteInfoToFile(String.Format("Передается команда {0}", cmd));
				try
				{
					if (cmd == BillValidatorCommands.ACK || cmd == BillValidatorCommands.NAK || cmd==BillValidatorCommands.RESET)
	    	        {                
						byte[] bytes = null;
                switch(cmd)
					{
					case(BillValidatorCommands.ACK):
					{
						break;
						}
					}
        	    	    if (cmd == BillValidatorCommands.ACK) 
						{ 
							bytes = Package.CreateResponse(ResponseType.ACK); 
					
						}
					else
	    	            if (cmd == BillValidatorCommands.NAK) 
						{ 
							bytes = Package.CreateResponse(ResponseType.NAK); 
						}
						else
							if(cmd==BillValidatorCommands.RESET)
							{
								Package package = new Package();
            			    	package.Cmd = (byte)cmd;
								bytes = package.GetBytes();	
							}
				
			 	
					Console.WriteLine("Будем передавать следующие байты на com-порт");
					LogFileWriter.WriteInfoToFile("Будем передавать следующие байты на com-порт");
					for(Int32 i=0;i!=bytes.Length;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, bytes[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, bytes[i]));
					}
				
    	            if (bytes != null) 
					{
						LogFileWriter.WriteInfoToFile(String.Format("Открыт ли com-порт: {0}", this.ComPort.IsOpen));
						this.ComPort.Write(bytes, 0, bytes.Length);
					}

                	return null;
            	}
	            else
    	        {
        	        Package package = new Package();
            	    package.Cmd = (byte)cmd;

	                if (Data != null) 
					{ 
						package.Data = Data; 
					}

	                byte[] CmdBytes = package.GetBytes();
					
					Console.WriteLine("Будем передавать следующие байты на com-порт");
					LogFileWriter.WriteInfoToFile("Будем передавать следующие байты на com-порт");
					for(Int32 i=0;i!=CmdBytes.Length;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, CmdBytes[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, CmdBytes[i]));
					}
				
					LogFileWriter.WriteInfoToFile(String.Format("Открыт ли com-порт: {0}", this.ComPort.IsOpen));
	                this.ComPort.Write(CmdBytes, 0, CmdBytes.Length);

					
        	        // Подождем пока получим данные с ком-порта
					Console.WriteLine("Подождем");
					LogFileWriter.WriteInfoToFile("Подождем");
	                this.SynchCom.WaitOne(EVENT_WAIT_HANDLER_TIMEOUT);
    	            this.SynchCom.Reset();
				
				
					//tmp again
						this.ReadAllAvailableData();
					//=========
				
	                byte[] ByteResult = this.ReceivedBytes.ToArray();
					Console.WriteLine("Получили:");
					LogFileWriter.WriteInfoToFile("Получили:");
					for(Int32 i=0;i!=ByteResult.Length;i++)
					{
						Console.WriteLine(CashCodeClass.HexByteValueOutputFormater, i, ByteResult[i]);
						LogFileWriter.WriteInfoToFile(String.Format(CashCodeClass.HexByteValueOutputFormater,i, ByteResult[i]));
					}
				

        	        // Если CRC ок, то проверим четвертый бит с результатом
            	    // Должны уже получить данные с ком-порта, поэтому проверим CRC
                	if (ByteResult.Length == 0 || !Package.CheckCRC(ByteResult))
	                {
						Console.WriteLine("Проверка CRC не прошла, генерируем исключение. Длинна пакета = {0}", ByteResult.Length);
						LogFileWriter.WriteInfoToFile(String.Format("Проверка CRC не прошла, генерируем исключение. Длинна пакета = {0}", ByteResult.Length));
						Console.WriteLine("Вышли из 'SendCommand'");
						LogFileWriter.WriteInfoToFile("Вышли из 'SendCommand'");
                    	throw new ArgumentException("Несоответствие контрольной суммы полученного сообщения. Возможно устройство не подключено к COM-порту. Проверьте настройки подключения.");
	                }
					else
					{
						Console.WriteLine("Проверка CRC выполнена успешно");
						LogFileWriter.WriteInfoToFile("Проверка CRC выполнена успешно");
					}				

					Console.WriteLine("Вышли из 'SendCommand'");
					LogFileWriter.WriteInfoToFile("Вышли из 'SendCommand'");
                	return ByteResult;
            	}
			}
			catch(Exception ex)
			{
				Console.WriteLine("Выходим из метода 'SendCommand'. Имеем исключение: {0}\nStackTrace исключения:{1}",ex.Message,ex.StackTrace);
				LogFileWriter.WriteInfoToFile(String.Format("Выходим из метода 'SendCommand'. Имеем исключение: {0}\nStackTrace исключения:{1}",ex.Message,ex.StackTrace));
				throw;
			}

        }
		#endregion

		#region Метод, сопоставляющий код валюты с ее номиналом
	        protected UInt16 CurrencyCodeTable(Byte CurrencyCode)
    	    {
				LogFileWriter.OutputStackTrace();
        	    Console.WriteLine("Зашли в CurrencyCodeTable");
				LogFileWriter.WriteInfoToFile("Зашли в CurrencyCodeTable");
			
        	    UInt16 returnedValue = 0;

            	switch(this.ConvertByteToEnum<CurrencyCodes>(CurrencyCode))
				{
					// 10 р.
					case(CurrencyCodes.TenRubles):
					{ 
						returnedValue = 10; 
						break;
					}     
					// 50 р.
					case(CurrencyCodes.FiftyRubles):
					{ 
						returnedValue = 50; 
						break;
					}    
					// 100 р.
					case(CurrencyCodes.OneHundredRubles):
					{ 
						returnedValue = 100; 
						break;
					}    
					// 500 р.
					case(CurrencyCodes.FiveHundredRubles):
					{ 
						returnedValue = 500; 
						break;
					}   
					// 1000 р.
					case(CurrencyCodes.OneThousandRubles):
					{ 
						returnedValue = 1000; 
						break;
					}   
					// 5000 р.
					case(CurrencyCodes.FiveThousandRubles):
					{ 
						returnedValue = 5000; 
						break;
					}
					default:
					{
						Console.WriteLine("Выходим из метода 'CurrencyCodeTable' с генерированием исключения: Невозможно распознать код купюры");
						LogFileWriter.WriteInfoToFile("Выходим из метода 'CurrencyCodeTable' с генерированием исключения: Невозможно распознать код купюры");
						throw new ArgumentException("Невозможно распознать код купюры");
					}
				}

            	return returnedValue;
        	}
		#endregion


		#region Метод для считывания всех доступных байтов из Com-порта
			protected void ReadAllAvailableData()
			{
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в метод 'ReadAllAvailableData'");
				LogFileWriter.WriteInfoToFile("Зашли в метод 'ReadAllAvailableData'");
			
				// Заснем на 100 мс, дабы дать программе получить все данные с ком-порта
    	        Thread.Sleep(100);
        	    this.ReceivedBytes.Clear();

            	// Читаем байты
	            while (ComPort.BytesToRead > 0)
    	        {
        	        this.ReceivedBytes.Add(Convert.ToByte(this.ComPort.ReadByte()));
            	}

				Console.WriteLine("Выходим из метода 'ReadAllAvailableData'");
				LogFileWriter.WriteInfoToFile("Выходим из метода 'ReadAllAvailableData'");
			}
		#endregion
		

        #region Метод для вызова события BillReceived
	        protected void OnBillReceived(BillReceivedEventArgs e)
    	    {
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в метод 'OnBillReceived'");
				LogFileWriter.WriteInfoToFile("Зашли в метод 'OnBillReceived'");
			
        	    if (this.BillReceived != null)
            	{
                	this.BillReceived(this, new BillReceivedEventArgs(e.Status, e.Value, e.RejectedReason));
	            }
			
				Console.WriteLine("Выходим из метода 'OnBillReceived'");
				LogFileWriter.WriteInfoToFile("Выходим из метода 'OnBillReceived'");
    	    }
		#endregion

		#region Метод для вызова события BillCassetteStatusEvent
	        protected void OnBillCassetteStatus(BillCassetteEventArgs e)
    	    {
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в метод 'OnBillCassetteStatus'");
				LogFileWriter.WriteInfoToFile("Зашли в метод 'OnBillCassetteStatus'");
			
        	    if (this.BillCassetteStatusEvent != null)
            	{
                	this.BillCassetteStatusEvent(this, new BillCassetteEventArgs(e.Status));
	            }
			
				Console.WriteLine("Выходим из метода 'OnBillCassetteStatus'");
				LogFileWriter.WriteInfoToFile("Выходим из метода 'OnBillCassetteStatus'");
    	    }
		#endregion

		#region Метод для вызова события BillStacking
	        protected void OnBillStacking(BillStackedEventArgs e)
    	    {
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в метод 'OnBillStacking'");
				LogFileWriter.WriteInfoToFile("Зашли в метод 'OnBillStacking'");
			
        	    if (this.BillStacking != null)
            	{
                	Boolean cancel = false;
	                foreach (BillStackingHandler subscriber in BillStacking.GetInvocationList())
    	            {
        	            subscriber(this, e);
						
	                    if (e.Cancel)
    	                {
        	                cancel = true;
            	            break;
                	    }
                	}

                	this.ReturnBill = cancel;
            	}
			
				Console.WriteLine("Выходим из метода 'OnBillStacking'");
				LogFileWriter.WriteInfoToFile("Выходим из метода 'OnBillStacking'");
        	}
        #endregion





        #region Обработчик события получение данных с Com-порта
	        protected void ComPort_DataReceivedEventHandler(Object Sender, SerialDataReceivedEventArgs E)
    	    {
        		LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в метод 'ComPort_DataReceivedEventHandler'");
				LogFileWriter.WriteInfoToFile("Зашли в метод 'ComPort_DataReceivedEventHandler'");
			
				// Заснем на 100 мс, дабы дать программе получить все данные с ком-порта
            	Thread.Sleep(100);
	            this.ReceivedBytes.Clear();

    	        // Читаем байты
        	    while (ComPort.BytesToRead > 0)
            	{
                	this.ReceivedBytes.Add(Convert.ToByte(ComPort.ReadByte()));
	            }

    	        // Снимаем блокировку
        	    this.SynchCom.Set();
			
				Console.WriteLine("Выходим из метода 'ComPort_DataReceivedEventHandler'");
				LogFileWriter.WriteInfoToFile("Выходим из метода 'ComPort_DataReceivedEventHandler'");
        	}
		#endregion

		#region Обработчик события истечения времени таймера прослушки купюроприемника
	        protected void Listener_ElapsedEventHandler(Object Sender, System.Timers.ElapsedEventArgs E)
    	    {
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в метод 'Listener_ElapsedEventHandler'");
				LogFileWriter.WriteInfoToFile("Зашли в метод 'Listener_ElapsedEventHandler'");
			
        	    this.Listener.Stop();

            	try
	            {
    	            lock (Locker)
        	        {
            	        Byte[] byteResult = null;

                	    // отпавить команду POLL
                    	byteResult = this.SendCommand(BillValidatorCommands.POLL);
						
						switch(this.ConvertByteToEnum<BillToBillReceivedCodes>(byteResult[3]))
						{						
		                    // Если четвертый бит Idling (простой), то завершить обработку
							case(BillToBillReceivedCodes.Idling):
        	            	{
								break;
							}

            	            // ACCEPTING
                	        //Если получили ответ 15H (Accepting)
							case(BillToBillReceivedCodes.Accepting):
                        	{
                            	// Подтверждаем
	                            this.SendCommand(BillValidatorCommands.ACK);
								break;
    	                    }

	                        // REJECTING
    	                    // Если четвертый бит 1Сh (Rejecting), то купюроприемник не распознал купюру
							case(BillToBillReceivedCodes.Rejecting):
    	        	        {
        	        	        // Принялии какую-то купюру
            	        	    this.SendCommand(BillValidatorCommands.ACK);

                	            OnBillReceived(new BillReceivedEventArgs(BillRecievedStatus.Rejected, 0, this.ErrorList.Errors[byteResult[4]]));
								break;
	                	    }

                        	// ESCROW POSITION
	                        // купюра распознана
							case(BillToBillReceivedCodes.EscrowPosition):
	    	    	        {
    	    	    	        // Подтветждаем
        	    	    	    this.SendCommand(BillValidatorCommands.ACK);

            	    	    	// Событие, что купюра в процессе отправки в стек
                	    	    OnBillStacking(new BillStackedEventArgs(CurrencyCodeTable(byteResult[4])));

                    	    	// Если программа отвечает возвратом, то на возврат
	                    	    if (this.ReturnBill)
    	                    	{
        	                       // RETURN
	            	                // Если программа отказывает принимать купюру, отправим RETURN
	    	            	           byteResult = this.SendCommand(BillValidatorCommands.RETURN);
    	    	            	       this.ReturnBill = false;
        	    	            }
            	    	        else
	            	    	    {
    	            	    	   // STACK
        	            	       // Если равпознали, отправим купюру в стек (STACK)
            	            	    byteResult = this.SendCommand(BillValidatorCommands.STACK);
	                	        }

								break;
    	                	}

		    	            // STACKING
    		    	        // Если четвертый бит 17h, следовательно идет процесс отправки купюры в стек (STACKING)
							case(BillToBillReceivedCodes.Stacking):
    	        		    {
        	        		    this.SendCommand(BillValidatorCommands.ACK);

								break;
            	        	}

      				        // Bill stacked
        		    	    // Если четвертый бит 81h, следовательно, купюра попала в стек
							case(BillToBillReceivedCodes.BillStacked):
		                	{
    		                	// Подтветждаем
        		                this.SendCommand(BillValidatorCommands.ACK);
									
                	            OnBillReceived(new BillReceivedEventArgs(BillRecievedStatus.Accepted, CurrencyCodeTable(byteResult[4]), ""));

								break;
				    	    }

    				    	// RETURNING
        				    // Если четвертый бит 18h, следовательно идет процесс возврата
							case(BillToBillReceivedCodes.Returning):
	                		{
    	                	    this.SendCommand(BillValidatorCommands.ACK);

								break;
	    	                }

		    	 	        // BILL RETURNING
    		    		    // Если четвертый бит 82h, следовательно купюра возвращена
							case(BillToBillReceivedCodes.BillReturned):
		    	    	   	{
    		    	    	    this.SendCommand(BillValidatorCommands.ACK);

								break;
        		    	    }
							
	     	    		    // Drop Cassette out of position
    	    	    		// Снят купюроотстойник
							case(BillToBillReceivedCodes.DropCassetteOutOfPosition):
			            	{
    			               	if (this.CassetteStatus != BillCassetteStatus.Removed)
	    		    	       	{
    	    		    	    	// fire event
	    	    		        	this.CassetteStatus = BillCassetteStatus.Removed;
		    	    		    	OnBillCassetteStatus(new BillCassetteEventArgs(this.CassetteStatus));
    		    	    		}

								break;
	        		    	}
								
		        	        // Initialize
    		        	    // Кассета вставлена обратно на место
							case(BillToBillReceivedCodes.Initialize):
	    		    	    {
    	    		    		if (this.CassetteStatus == BillCassetteStatus.Removed)
			            		{
    			            		// fire event
	    			               	this.CassetteStatus = BillCassetteStatus.Inplace;
	    	    			       	OnBillCassetteStatus(new BillCassetteEventArgs(this.CassetteStatus));
		    	    			}

								break;
    		    	    	}
	       	            }
    	       	    }
            	}
            	catch(Exception exception)
            	{
					Console.WriteLine("В методе 'Listener_ElapsedEventHandler' произошло исключение: '{0}', которое было проигнорировано", exception.Message);
					LogFileWriter.WriteInfoToFile(String.Format("В методе 'Listener_ElapsedEventHandler' произошло исключение: '{0}', которое было проигнорировано", exception.Message));
				}
	            finally
    	        {
        	        // Если таймер выключен, то запускаем
            	    if (!this.Listener.Enabled && this.IsListening)
					{
                	    this.Listener.Start();
					}
            	}
			
				Console.WriteLine("Выходим из метода 'Listener_ElapsedEventHandler'");
				LogFileWriter.WriteInfoToFile("Выходим из метода 'Listener_ElapsedEventHandler'");
        	}
		#endregion


		#region Метод для преобразования байтов в Enum
			protected T ConvertByteToEnum<T> (Byte Val) where T : struct, IConvertible
			{
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в метод 'ConvertByteToEnum'");
				LogFileWriter.WriteInfoToFile("Зашли в метод 'ConvertByteToEnum'");
			
				T returnedValue;

				if (typeof(T).IsEnum == false) 
				{
					Console.WriteLine("В методе 'ConvertByteToEnum' генерируем исключение: 'Тип T='{0}' не является перечислителем'", typeof(T));
					LogFileWriter.WriteInfoToFile(String.Format("В методе 'ConvertByteToEnum' генерируем исключение: 'Тип T='{0}' не является перечислителем'", typeof(T)));
				
					throw new ArgumentException (String.Format ("Тип T='{0}' не является перечислителем", typeof(T)));
				}
				else 
				{
					if(Enum.IsDefined(typeof(T), Val))
					{
						returnedValue=(T)Enum.ToObject(typeof(T), Val);
					}
					else
					{
						Console.WriteLine("В методе 'ConvertByteToEnum' генерируем исключение: 'Невозможно преобразовать байты: '{0}' в перечислитель '{1}''", Val, typeof(T));
						LogFileWriter.WriteInfoToFile(String.Format("В методе 'ConvertByteToEnum' генерируем исключение: 'Невозможно преобразовать байты: '{0}' в перечислитель '{1}''", Val, typeof(T)));
						throw new ArgumentException (String.Format ("Невозможно преобразовать байты: '{0}' в перечислитель '{1}'", Val, typeof(T)));
					}
				}
			

				Console.WriteLine("Выходим из метода 'ConvertByteToEnum'");
				LogFileWriter.WriteInfoToFile("Выходим из метода 'ConvertByteToEnum'");			
			
				return returnedValue;
			}
		#endregion

    }

    /// <summary>
    /// Класс аргументов события получения купюры в купюроприемнике
    /// </summary>
    public class BillReceivedEventArgs : EventArgs
    {
		#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА BillReceivedEventArgs
	        public BillRecievedStatus Status { get; protected set; }
    	    public UInt16 Value { get; protected set; }
        	public String RejectedReason { get; protected set; }
		#endregion

		#region Конструктор класса BillReceivedEventArgs (с параметрами)
	        public BillReceivedEventArgs(BillRecievedStatus Status, UInt16 Value, String RejectedReason)
    	    {
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в метод 'BillReceivedEventArgs'");
				LogFileWriter.WriteInfoToFile("Зашли в метод 'BillReceivedEventArgs'");
				
        	    this.Status = Status;
            	this.Value = Value;
	            this.RejectedReason = RejectedReason;
			
				Console.WriteLine("Выходим из метода 'BillReceivedEventArgs'");
				LogFileWriter.WriteInfoToFile("Выходим из метода 'BillReceivedEventArgs'");	
    	    }
		#endregion
    }


    public class BillCassetteEventArgs : EventArgs
    {
		#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА BillCassetteEventArgs
	        public BillCassetteStatus Status { get; protected set; }
		#endregion

		#region Конструктор класса BillCassetteEventArgs (с параметрами)
	        public BillCassetteEventArgs(BillCassetteStatus Status)
    	    {
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в метод 'BillCassetteEventArgs'");
				LogFileWriter.WriteInfoToFile("Зашли в метод 'BillCassetteEventArgs'");
			
        	    this.Status = Status;
				
				Console.WriteLine("Выходим из метода 'BillCassetteEventArgs'");
				LogFileWriter.WriteInfoToFile("Выходим из метода 'BillCassetteEventArgs'");	
	        }
		#endregion
    }


    public class BillStackedEventArgs : CancelEventArgs
    {
		#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА BillStackedEventArgs
			public UInt16 Value { get; protected set; }
		#endregion

		#region Конструктор класса BillStackedEventArgs (с параметрами)
	        public BillStackedEventArgs(UInt16 Value)
    	    {
				LogFileWriter.OutputStackTrace();
				Console.WriteLine("Зашли в метод 'BillStackedEventArgs'");
				LogFileWriter.WriteInfoToFile("Зашли в метод 'BillStackedEventArgs'");
			
        	    this.Value = Value;
            	this.Cancel = false;
			
				Console.WriteLine("Выходим из метода 'BillStackedEventArgs'");
				LogFileWriter.WriteInfoToFile("Выходим из метода 'BillStackedEventArgs'");	
        	}
		#endregion
    }



}

