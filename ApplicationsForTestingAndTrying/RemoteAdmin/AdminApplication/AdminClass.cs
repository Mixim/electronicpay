using System;
using System.Net;
using System.Text;
using System.Net.Sockets;
using System.IO;

namespace AdminApplication
{
	#region Класс, необходимый для представления функционала удаленного управления клиентским приложением
		public class AdminClass:RemoteAdministrationBase
		{
			#region НЕОБХОДИМЫЕ КОНСТАНТЫ КЛАССА AdminClass
				protected const String ReceivedInvalidHashCodeOfClientsResponse_Format="Была предпринята попытка исполнить команду: '{0}' на клиенте с Ip-адресом: '{1}' и портом: '{2}'. В ответ на это клиент вернул значение, которое не прошло проверку по хеш-коду";
				protected const String ClientDidNotSendResponse_Format="Клиент с Ip-адресом: '{0}' и портом: '{1}' не отправил ответ на запрос";
				protected const String SendFileToClientException_Format="Не удалось передать все данные клиенту с Ip-адресом: '{0} и портом: '{1}', поскольку он преждевременно разорвал установленное соединение";
				protected const String InvalidAdminsCommandValueException_Format="Переданное значение: '{0}' типа 'AdminsCommand' не является покрытым в методе '{1}'";
			#endregion
		
			#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА AdminClass
				protected TcpClient server;
			#endregion
		
			#region Метод для доступа к полю server как к свойству
				public TcpClient Server
				{
					get
					{
						return this.server;
					}
					set
					{
						this.server=value;
					}
				}
			#endregion
		
			#region Конструктор класса AdminClass [пустой]
				public AdminClass ()
				{
					this.Server=new TcpClient();
				}
			#endregion
		
			#region Конструктор класса AdminClass [с параметрами]
				/*public AdminClass(String Hostname, Int32 Port)
				{
					this.Server=new TcpClient(Hostname, Port);
				}*/
			#endregion
		
			
			#region Метод для отправки команды клиенту
				public ParsedClientsResponseAndComment SendCommandToClient(IPAddress ClientAddress, Int32 ClientPort, AdminsCommands Command, Object Data=null)
				{
					ParsedClientsResponseAndComment returnedValue;
					IPEndPoint clientsEndPoint;
					NetworkStream networkStream;
					Byte[] sendingPackage;
			
			
					
					clientsEndPoint=new IPEndPoint(ClientAddress, ClientPort);
			
					this.Server.Connect(clientsEndPoint);
					
					networkStream=this.Server.GetStream();
								
			
					switch(Command)
					{
						case(AdminsCommands.LetsConnect):
						{
							sendingPackage=this.GenerateSendingPackage(Convert.ToByte(Command), null);
								networkStream.Write(sendingPackage, 0, sendingPackage.Length);
							break;
						}
						case(AdminsCommands.Reboot):
						{
							sendingPackage=this.GenerateSendingPackage(Convert.ToByte(Command), Data);				
								networkStream.Write(sendingPackage, 0, sendingPackage.Length);
							break;
						}
						case(AdminsCommands.TurnOff):
						{
							sendingPackage=this.GenerateSendingPackage(Convert.ToByte(Command), Data);
								networkStream.Write(sendingPackage, 0, sendingPackage.Length);
							break;
						}
						case(AdminsCommands.UpdateConfiguration):
						{
							sendingPackage=this.GenerateSendingPackage(Convert.ToByte(Command), null);
								networkStream.Write(sendingPackage, 0, sendingPackage.Length);
								this.SendFileToClient(this.Server, Data.ToString());
							break;
						}
						default:
						{
							throw new ArgumentException(String.Format(AdminClass.InvalidAdminsCommandValueException_Format, Command, "SendCommandToClient"));
						}
					}
			
					
			
					//===for debug===
						Byte[] readedLNG=new Byte[RemoteAdministrationBase.CountOfBytesForPartOfLength];
						Byte[] readedResponseAndComment;
						Byte[] readedHash=new Byte[RemoteAdministrationBase.CountOfHashBytes];
						if(networkStream.Read(readedLNG, 0, RemoteAdministrationBase.CountOfBytesForPartOfLength)!=0)
						{		
							Int32 packageLength=BitConverter.ToInt32(readedLNG,0);
							//инициализируем буфер для считывания всех данных пакета
								readedResponseAndComment=new Byte[packageLength];
					
																		
							//считаем из потока количество байт, равное packageLength - блок Response + Comment
								networkStream.Read(readedResponseAndComment, 0, packageLength);
							//считаем хеш
								networkStream.Read(readedHash, 0, RemoteAdministrationBase.CountOfHashBytes);
				
							//проверяем хеш
							if(this.HashChecker(readedResponseAndComment, readedHash)==true)
							{						
								returnedValue=this.ParseReceivedPackage<ParsedClientsResponseAndComment>(readedResponseAndComment);
							}
							//если хеш ПОЛУЧЕННОГО ОТ КЛИЕНТА сообщения не сошелся, то мы должны вернуть сообщение InvalidHashCodeOfClientsResponse
							else
							{
								returnedValue=new ParsedClientsResponseAndComment(ClientsResponses.InvalidHashCodeOfClientsResponse, String.Format(AdminClass.ReceivedInvalidHashCodeOfClientsResponse_Format, Command, ClientAddress.ToString(), ClientPort));
							}
						}
						else
						{
							returnedValue=new ParsedClientsResponseAndComment(ClientsResponses.ClientDidNotSendResponse, String.Format(AdminClass.ClientDidNotSendResponse_Format, ClientAddress.ToString(), ClientPort));
						}
					//===============
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для отправки файлов клиентам
				protected void SendFileToClient(TcpClient Client, String FilePath)
				{
					Int64 length;
					Int64 totalBytes;
					Int32 readBytes;
					Byte[] buffer;
					
					
					using (FileStream inputStream = File.OpenRead(FilePath))
					{
						NetworkStream outputStream = Client.GetStream();
						
							BinaryWriter writer = new BinaryWriter(outputStream);
								length = inputStream.Length;
								totalBytes = 0;
								readBytes = 0;
								buffer = new Byte[AdminClass.BufferSizeForSendAndReceive];

								//передаем имя файла
									writer.Write(Path.GetFileName(FilePath));
								//передаем размер файла
									writer.Write(length);

								do
								{
									readBytes = inputStream.Read(buffer, 0, buffer.Length);
									outputStream.Write(buffer, 0, readBytes);
									totalBytes += readBytes;
								}
								while (Client.Connected && totalBytes < length);
						
								//если цикл был завершен по той причине, что клиент разорвал соединение, 
								//то он мог не получить все данные=>нужно сгенерировать исключение с соответствующим текстом
								if((Client.Connected==false) && (totalBytes!=length))
								{
									Console.WriteLine("dsfjasf");
									//throw new Exception(String.Format(AdminClass.SendFileToClientException_Format, ClientsEndPoint.Address.ToString(), ClientsEndPoint.Port));
								}								
					}

					//client.Close();
				}
			#endregion
		}
	#endregion
}

