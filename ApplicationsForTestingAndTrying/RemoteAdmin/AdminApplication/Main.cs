using System;
using System.Text;
using System.Net;


//<LNG(8)><CMD(1)><Data(N)><Hash(64)>

/*
 Приложение с частью проекта для администратора
 */
namespace AdminApplication
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//String pathToFile;						
			IPAddress clientsAddress=IPAddress.Parse("127.0.0.1");
			
			//Console.WriteLine("Введите путь к файлу, который Вы хотите отправить клиенту");
			//pathToFile=Console.ReadLine();
			
			AdminClass admin=new AdminClass();
			//ParsedClientsResponseAndComment res= admin.SendCommandToClient(clientsAddress, 5300, AdminsCommands.UpdateConfiguration, pathToFile);
			ParsedClientsResponseAndComment res= admin.SendCommandToClient(clientsAddress, 5300, AdminsCommands.LetsConnect);
			
			Console.WriteLine("Response='{0}';\nComment='{1}'",res.Response, res.Comment);
		}
	}
}

