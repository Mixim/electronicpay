using System;
using AdminApplication;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Configuration;



/*
 Приложение с клиентской частью проекта
 */
namespace ClientApplication
{
	class MainClass
	{
		protected const Int32 TimeoutOfWaitResponse=Int32.MaxValue;
		
		protected const String RebootProgramFileName="dbus-send";
			protected const String RebootProgramArguments="--system --print-reply --dest=org.freedesktop.ConsoleKit /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Restart";
		
		protected const String TurnOffProgramFileName="dbus-send";
			protected const String TurnOffProgramArguments="--system   --dest=org.freedesktop.ConsoleKit   --type=method_call   --print-reply   --reply-timeout=2000   /org/freedesktop/ConsoleKit/Manager   org.freedesktop.ConsoleKit.Manager.Stop";
		
		
		public static void Main (string[] args)
		{	
			IPAddress acceptedAddress=IPAddress.Any;
			Int32 port;
			
			Console.WriteLine("Введите порт");
			port=Convert.ToInt32(Console.ReadLine());
			
			
			Console.WriteLine("Пытаемся создать клиента, который будет допускать к подключению следующий адрес: '{0}' на порт: '{1}'", acceptedAddress.ToString(), port);
				ClientClass client=new ClientClass(acceptedAddress, port, MainClass.ClientsServerResponse, MainClass.ProblemsProcessor);
			
			Console.WriteLine("Начинаем слушать сеть");
			
			client.ListeningOfNetwork(MainClass.TimeoutOfWaitResponse);
			
			//client.ReceiveFileFromNetwork(
		}
		
		
		
		
		
		#region Метод для обработки команд, полученных со стороны сервера
			protected static ClientsResponses ClientsServerResponse(ClientClass Sender, TcpClient Admin, ParsedAdminsCommandAndArgs CommandAndArgs)
			{
				ClientsResponses returnedValue;
			
				//вставляем обработчик исключительных ситуаций
				try
				{
					//выполняем ту команду, которая была передана на клиент
					switch(CommandAndArgs.Command)
					{
						case(AdminsCommands.LetsConnect):
						{
							returnedValue=ClientsResponses.Ack;
							
							break;
						}
					
						case AdminsCommands.Reboot:
						{							
							//System.Diagnostics.Process.Start(MainClass.RebootProgramFileName, MainClass.RebootProgramArguments);
							
							returnedValue=ClientsResponses.CommandExecuted;
							break;
						}
												
						case AdminsCommands.TurnOff:
						{
							//System.Diagnostics.Process.Start(MainClass.TurnOffProgramFileName, MainClass.TurnOffProgramArguments);
						
					
							returnedValue=ClientsResponses.CommandExecuted;
							break;
						}
					
						//команда 'UpdateConfiguration' предназначена для удаленного изменения конфигурации 
						//оборудования (загрузка нового файла с информацией о биллинговой системе, оборудовании и т.д.), 
						//сигнал об ее успешном выполнении может быть отправлен уже после фактического исполнения
						case AdminsCommands.UpdateConfiguration:
						{						
							String filename=Sender.ReceiveFileFromNetwork(Admin, "");
								Configuration currentConfig=ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
									
								Configuration newConfiguration=Sender.ConvertFileToConfiguration(filename);
					
								Sender.UpdateConfiguration(currentConfig, newConfiguration);
					
							returnedValue=ClientsResponses.CommandExecuted;
							break;
						}
					
						//если полученная команда не покрыта блоком кода switch...case, 
						//то команда является не корректной (клиент ее не знает) и мы должны вернуть соответствующее сообщение
						default:
						{
							returnedValue=ClientsResponses.InvalidCommand;
							break;
						}
					}
				}
				//если поймали некоторое исключение, то команда однозначно не выполнилась и нам нужно передать 
				//на администраторскую машину сообщение о том, что произошел крах при выполнении команды - ее 
				//выполнить невозможно и коментарии о возникшем исключении
				catch(Exception ex)
				{
					
				
					returnedValue=ClientsResponses.CommandsExecutionsIsFailed;
				}
			
			
			
				return returnedValue;
			}
		#endregion
		
		
		#region Метод, необходимый для представления обработчика ошибок
			protected static void ProblemsProcessor(String ProblemDescription)
			{
				using(StreamWriter writer = new StreamWriter("problemsFile.txt", true))
				{
					writer.WriteLine("Имеем проблему: '{0}'", ProblemDescription);
				}
			}
		#endregion
	}
}

