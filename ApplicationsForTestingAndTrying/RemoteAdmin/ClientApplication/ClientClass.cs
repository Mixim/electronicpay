using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.IO;
using AdminApplication;
using System.Configuration;

namespace ClientApplication
{
	#region Делегат, необходимый для представления метода-обработчика получаемых команд
		public delegate ClientsResponses CommandsProcessor(ClientClass Sender, TcpClient Admin, ParsedAdminsCommandAndArgs CommandAndArgs);	
	#endregion
	
	#region Делегат, необхоимый для представления метода-обработчика ошибок отправки данных (например, сервер не ответил)
		public delegate void ProblemsProcessor(String ProblemDescription);
	#endregion
	
	#region Класс клиента, который будет удаленно управляться
		public class ClientClass:RemoteAdministrationBase
		{		
			#region НЕОБХОДИМЫЕ КОНСТАНТЫ КЛАССА ClientClass
				protected const String CommandProcessingResult_Format="После успешной проверки хеша, была предпринята попытка обработать команду: {0}";
				protected const String RecipientDoesNotAnsweredMessage_Format="Обратная сторона с адресом: '{0}' не ответила после '{1}' запросов";						
			#endregion
		
			#region	НЕОБХОДИМЫЕ ПОЛЯ КЛАССА ClientClass
				protected TcpListener client;
				protected CommandsProcessor commandProcessing;
				protected ProblemsProcessor problemProcessing;
			#endregion
		
			#region Метод для доступа к полю client как к свойству
				public TcpListener Client
				{
					get
					{
						return this.client;
					}
					set
					{
						this.client=value;
					}
				}
			#endregion
		
			#region Метод для доступа к полю commandProcessing как к свойству
				public CommandsProcessor CommandProcessing
				{
					get
					{
						return this.commandProcessing;
					}
					set
					{
						this.commandProcessing=value;
					}
				}
			#endregion
		
			#region Метод для доступа к полю problemProcessing как к свойству
				public ProblemsProcessor ProblemProcessing
				{
					get
					{
						return this.problemProcessing;
					}
					set
					{
						this.problemProcessing=value;
					}
				}
			#endregion
		
			#region Конструктор класса ClientClass [пустой]
				public ClientClass ()
				{
				}
			#endregion
		
			#region Конструктор класса ClientClass [с параметрами]
				public ClientClass(IPAddress IpAddressOfClient, Int32 PortOfClient, CommandsProcessor CommandProcessing, ProblemsProcessor ProblemProcessing)
				{
					this.Client=new TcpListener(IpAddressOfClient, PortOfClient);
			
					this.CommandProcessing=CommandProcessing;
			
					this.ProblemProcessing=ProblemProcessing;
				}
			#endregion
		
			#region Метод для прослушивания ip-адреса и порта
				/*
				 	TimeoutOfWaitResponse - это время, в течении которого должны придти данные (ответ).
				 	Если необходимый объем данных не приходит за указанный промежуток времени, то программа продолжает свою работу
				 */
				public void ListeningOfNetwork(Int32 TimeoutOfWaitResponse)
				{
					TcpClient connected;
					NetworkStream stream;
					Int32 packageLength;
					Byte[] readedLNG=new Byte[RemoteAdministrationBase.CountOfBytesForPartOfLength];
					Byte[] readedCmdAndData;
					Byte[] readedHash=new Byte[RemoteAdministrationBase.CountOfHashBytes];
					ParsedAdminsCommandAndArgs commandAndArgs;
					ClientsResponses responsesToAdmin;
			
			
					this.Client.Start();
					
			
					//запускаем бесконечный цикл
					while(true)
					{
						//ждем подключения
							connected=this.Client.AcceptTcpClient();
						
						stream=connected.GetStream();						
						stream.ReadTimeout=TimeoutOfWaitResponse;			
						while(connected.Connected==true)
						{
							//получим из потока длинну отосланного пакета LNG(8). Если метод Read вернул число равное 0, то байтов в потоке нет
								if(stream.Read(readedLNG, 0, RemoteAdministrationBase.CountOfBytesForPartOfLength)!=0)
								{		
									packageLength=BitConverter.ToInt32(readedLNG,0);
									//инициализируем буфер для считывания всех данных пакета
										readedCmdAndData=new Byte[packageLength];
					
																		
										//считаем из потока количество байт, равное packageLength - блок Command + Data
											stream.Read(readedCmdAndData, 0, packageLength);
										//считаем хеш
											stream.Read(readedHash, 0, RemoteAdministrationBase.CountOfHashBytes);
								
					
									//проверим значение подсчитанного хеша полученных данных и 	полученного хеша
										if(this.HashChecker(readedCmdAndData, readedHash)==true)
										{						
											commandAndArgs=this.ParseReceivedPackage<ParsedAdminsCommandAndArgs>(readedCmdAndData);
							
											if(this.CommandProcessing!=null)
											{
												responsesToAdmin=this.CommandProcessing(this, connected, commandAndArgs);																	
											}
											else
											{
												responsesToAdmin=ClientsResponses.ThereIsNoCommandHandler;
											}
											this.SendResponseToNetwork(connected, responsesToAdmin, String.Format(ClientClass.CommandProcessingResult_Format, commandAndArgs.ToString()));
										}
										else
										{
											ResultOfCommandExecution commandExecutionResult;
											//отправить TcpClient информацию о том, что получены поврежденные данные
											commandExecutionResult=this.SendResponseToNetwork(connected, ClientsResponses.ReceivedCorruptedData, ClientClass.ReceivedInvalidHashCode_Comment);
											if(commandExecutionResult==ResultOfCommandExecution.RecipientDoesNotAnswered)
											{
												if(this.ProblemProcessing!=null)	
												{
													this.ProblemProcessing(String.Format(ClientClass.RecipientDoesNotAnsweredMessage_Format, connected.ToString(), ClientClass.MaximumNumberOfRequestsAttempts));
												}
											}
										}
								}
						}
					}
				}
			#endregion
		
			#region Метод для отправки необходимых запросов в сеть (например, отправка результата выполнения операции на администраторскую часть приложения)
				protected ResultOfCommandExecution SendResponseToNetwork(TcpClient Recipient, ClientsResponses Response, String Comment)
				{
					ResultOfCommandExecution returnedValue;
					Byte[] sendingPackage;
					NetworkStream recipientsNetworkStream;
					UInt16 numberOfAttemptsLeft=ClientClass.MaximumNumberOfRequestsAttempts;
					Boolean dataIsSended=false;
			
					//сформируем ответ для администраторской стороны связи
						sendingPackage=this.GenerateSendingPackage(Convert.ToByte(Response), Comment);
					
					//получим поток для общения с администраторской стороной
						recipientsNetworkStream=Recipient.GetStream();
			
					//попытаемся отправить данные
						while((numberOfAttemptsLeft!=0) && (dataIsSended==false))
						{
							try
							{
								numberOfAttemptsLeft--;
								recipientsNetworkStream.Write(sendingPackage, 0, sendingPackage.Length);
								dataIsSended=true;
							}
							catch(Exception)
							{
							}
						}
					
					//укажем, как выполнена операция отправки отчета администраторскому приложению
					if(dataIsSended==true)
					{
						returnedValue=ResultOfCommandExecution.CommandSuccessfullyExecuted;
					}
					else
					{
						returnedValue=ResultOfCommandExecution.RecipientDoesNotAnswered;
					}
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для получения файла из сети
				public String ReceiveFileFromNetwork(TcpClient Admin, String FilesFolder)
				{					
					String filename;
					Int64 length;
					Int64 totalBytesCount;
					Int32 readBytesCount;
					Byte[] buffer = new Byte[ClientClass.BufferSizeForSendAndReceive];
			
					NetworkStream inputStream=Admin.GetStream();
					//нет никакой необходимости закрывать поток inputStream при выходе из текущего метода, поэтому для reader не будем использовать using
					BinaryReader reader=new BinaryReader(inputStream);
						//считываем имя файла, который нам хотят передать из сети
							filename=reader.ReadString();
						//считываем размер файла в байтах
							length=reader.ReadInt64();
						using(FileStream outputStream=File.Open(Path.Combine(FilesFolder, filename), FileMode.Create))
						{
							totalBytesCount=0;
							readBytesCount=0;
					
							while((Admin.Connected) && (totalBytesCount<length))
							{
								//получаем данные из сети
									readBytesCount=inputStream.Read(buffer, 0, buffer.Length);
								//записываем их в выходной поток, т.е. файл
									outputStream.Write(buffer, 0, readBytesCount);
								totalBytesCount+=readBytesCount;						
							}							
						}
			
					//вернем имя файла
					return filename;
				}
			#endregion

			#region Метод для получения из обычного файла конфигурационного
				public Configuration ConvertFileToConfiguration(String PathToFile)
				{
					Configuration returnedValue;			
			
					ExeConfigurationFileMap configMap=new ExeConfigurationFileMap();
						configMap.ExeConfigFilename=PathToFile;
			
			
					returnedValue=ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для обновления файла конфигурации на стороне клиента
				public ClientsResponses UpdateConfiguration(Configuration ConfigurationIsUpdated, Configuration SourceConfiguration)
				{
					ClientsResponses returnedValue;

			
					try
					{
						foreach(String currentKey in SourceConfiguration.AppSettings.Settings.AllKeys)
						{
							//если в текущем файле с параметрами приложения есть значение с точно таким же ключом (вернулся не null)
							if(ConfigurationIsUpdated.AppSettings.Settings[currentKey]!=null)
							{
								ConfigurationIsUpdated.AppSettings.Settings[currentKey].Value=SourceConfiguration.AppSettings.Settings[currentKey].Value;
							}
							else
							{
								ConfigurationIsUpdated.AppSettings.Settings.Add(currentKey, SourceConfiguration.AppSettings.Settings[currentKey].Value);
							}
						}
				
						//сохраняем конфигурационный файл
							ConfigurationIsUpdated.Save(ConfigurationSaveMode.Modified);
	
						//перегрузка измененной секции. Это делает новые значения доступными для чтения
							ConfigurationManager.RefreshSection("appSettings");
				
						//если все предыдущие операции выполнены, то команда исполнена успешно
							returnedValue=ClientsResponses.CommandExecuted;
					}					
					catch(Exception)
					{
						//если поймали какое-то исключение при обновлении конфигурации, то команда выполнилась не корректно
						returnedValue=ClientsResponses.CommandsExecutionsIsFailed;
					}
			
					return returnedValue;
				}
			#endregion
		}
	#endregion
}

