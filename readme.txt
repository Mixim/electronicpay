1. Exception from hardware don't show error message for user witn Release version. Must goto default screen 
and show "Problem with hardware bla-bla-bla". - fixed, now all error message which can see user will be written to errorFile
2. Length of account number must be controlled from preferences. - fixed, now you can set it by edit "ElectronicPay.exe.config" file. Value name is "LengthOfContractNumber"
3. All screen messages must be load from external file for simplify localization.
4. Don't forget about checking availability of printer and other device and services before user had put money


5. Don't forget to develop a GUI and port console 'AdminApplication' to 'AdminsGUIApplication' - fixed
6. I think, that there is some bug with CashRegister, because I don't open check
7. Password for cash register should load from external file (now it set by code)
