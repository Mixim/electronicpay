using System;
using System.IO.Ports;




namespace ElectronicPay
{
	#region Энумератор для представления возможных форматов пересылаемных команд
		public enum SendingType {Case1, Case2, Case3}
	#endregion
	
	#region Необходимые делегаты
		public delegate void DataReceived_Delegate(Object sender, SerialDataReceivedEventArgs e);
	#endregion
	
	#region Класс, необходимый для работы с купюроприемником марки CashCode
		public class CashCodeClass:IDisposable
		{
			#region НЕОБХОДИМЫЕ КОНСТАНТЫ КЛАССА
				//размер блока, который может быть передан за один запрос(макимальный размер одного кадра передаваемых данных)
				protected const Int32 MaxSizeOfSingleFrame=250;
				//количество байт синхронизации
				protected const Int32 QuantityOfSYNCBytes=1;
				//количество байт адреса
				protected const Int32 QuantityOfADRBytes=1;
				//количество байт разделителя
				protected const Int32 QuantityOfSeparatingByte=1;
				//количество байт длинны
				protected const Int32 QuantityOfLNGByte=1;
				//количество байт команды
				protected const Int32 QuantityOfCMDByte=1;
				//количество байт подкоманды
				protected const Int32 QuantityOfSUBCMDByte=1;
				//минимальное количество байт данных(DATA)
				protected const Int32 MinimumQuantityOfDATAByte=0;
				//максимальное количество байт данных(DATA)
				protected const Int32 MaximumQuantityOfDATAByte=250;
				//количество байт в CRC
				protected const Int32 QuantityOfCRCBytes=2;
		
				//MSB-байт, который необходимо вставлять перед LNG в случае если она имеет размер в 2 байта
				protected const Byte MSBByte=128;	//будет выполняться операция or. Число 128 в двоичном представлении это "10000000"
		
				//дефолтное значение пустых байт
				protected const Byte ByteNullValue=0;
				//смещение в массиве для записи в порт
				protected const Int32 WriteOffset=0;
				//смещение в массиве для чтения из порта
				protected const Int32 ReadOffset=0;
		
				//блок со строками для генерации исключительных ситуаций
					protected const String InvalidFormatOfSentBytes="Неверный формат передаваемых на порт данных";
				//======================================================
			#endregion
			
			#region НЕОБХОДИМЫЕ ПОЛЯ КЛАССА
				SerialPort serialPort=null;
		
				Boolean dataReceived=false;
			#endregion

			
			#region Конструктор класса CashCodeClass
				public CashCodeClass (String PortName, Int32 BaudRate=9600, Parity DeviceParity=Parity.None, Int32 DataBits=8, StopBits DeviceStopBits=StopBits.One, System.IO.Ports.SerialDataReceivedEventHandler DeviceDataReceivedEventHandler=null)
				{
					this.SerialPort = new SerialPort (PortName, BaudRate, DeviceParity, DataBits, DeviceStopBits);
			
					this.SerialPort.DataReceived+=DeviceDataReceivedEventHandler;
					
				}
			#endregion

			#region Деструктор класса CashCodeClass
				public void Dispose ()
				{
					if(this.SerialPort.IsOpen==true)
					{
						this.SerialPort.Close();
					}
			
					this.serialPort.Dispose ();
					
					GC.SuppressFinalize (this);
				}
			#endregion

			#region Метод для обращения к полю serialPort
				public SerialPort SerialPort
				{
					get
					{
						return this.serialPort;
					}
					set
					{
						this.serialPort=value;
					}
				}
			#endregion
		
			#region Метод для обращения к полю dataReceived
				public Boolean DataReceived
				{
					get
					{
						return this.dataReceived;
					}
					set
					{
						this.dataReceived=value;
					}
				}
			#endregion
		
			
			#region Метод для открытия порта
				public Boolean OpenPort()
				{
					Boolean returnedValue=false;
			
					try
					{
						//если порт уже открыт
						if(this.SerialPort.IsOpen==true)
						{
							//вернем true
							returnedValue=true;
						}
						//иначе
						else
						{	
							//предпринимаем попытку открыть порт
							this.SerialPort.Open();
							//возвращаем значение открыт ли порт
							returnedValue=this.SerialPort.IsOpen;
						}
					}
					//если произошло исключение при открытии порта(порт занят другим приложением или др.)
					catch(Exception)
					{
						//вернем false
						returnedValue=false;
					}
				
					return returnedValue;
				}
			#endregion
		
			#region Метод для закрытия порта
				public Boolean ClosePort()
				{
					Boolean returnedValue=false;
					
					try
					{
						if(this.SerialPort.IsOpen==true)
						{
							this.SerialPort.Close();
						}
						returnedValue=true;
					}
					catch(Exception)
					{
						returnedValue=false;
					}
			
					return returnedValue;
				}
			#endregion
		
			#region Метод для проверки некоторых данных на корректность(на соответствие форматов)
				protected Boolean CheckSentBytes(Byte[] DataBytes, Boolean HaveSubcommand)
				{
					Boolean returnedValue=true;
			
					//проверка массива данных
					if(
						(CashCodeClass.MinimumQuantityOfDATAByte>=DataBytes.Length)
												||
						(CashCodeClass.MaximumQuantityOfDATAByte-(HaveSubcommand ? 3 : 2)<=DataBytes.Length)	//минус 2 или 3, т.к. при общем объеме пересылаемого пакета большем 250 байт, мы должны длинну пакета занести в нулевой и первый(если нет подкоманд) либо в первый и второй(если подкоманды присутствуют)
					  )
					{
						
						returnedValue=false;
					}
					//========================
	
					return returnedValue;
				}
			#endregion
		
		
		
			#region Публичный метод-оболочка для отправки команд на порт в одном из допустимых форматов
				public void SendCommand(Byte SyncByte, Byte AddressByte, Byte SeparatingBytes, Byte[] DataBytes, SendingType SendingTypes,
		                                Byte CommandByte=CashCodeClass.ByteNullValue, Byte SubCommandByte=CashCodeClass.ByteNullValue)
				{
					//проверяем данные, которые предполагается переслать, на соответствие
					if(this.CheckSentBytes(DataBytes, SubCommandByte!=CashCodeClass.ByteNullValue)==false)
					{
						throw new ArgumentException(CashCodeClass.InvalidFormatOfSentBytes);
					}
					else
					{
						switch(SendingTypes)
						{
							//подкоманда отсутствует
							case(SendingType.Case1):
							{
								//2 LNG-байта будут вычисляться и приписываться при непосредственной отсылке команды, поэтому текущая максимальная длинна, передача которой возможна в одной пакете(фрейме) составляет CashCodeClass.MaxSizeOfSingleFrame - 2*sizeof(byte)
								if(
										System.Runtime.InteropServices.Marshal.SizeOf(SyncByte) + 
										System.Runtime.InteropServices.Marshal.SizeOf(AddressByte) +
										System.Runtime.InteropServices.Marshal.SizeOf(SeparatingBytes) +
										System.Runtime.InteropServices.Marshal.SizeOf(CommandByte) +  
										DataBytes.Length*System.Runtime.InteropServices.Marshal.SizeOf(DataBytes.GetType().GetElementType())
																		<=
										(CashCodeClass.MaxSizeOfSingleFrame - 2*sizeof(byte))
									)
								{
									this.SendCommand_Case1SMALL(SyncByte, AddressByte, SeparatingBytes, CommandByte, DataBytes); 
								}
								else
								{
									this.SendCommand_Case1LARGE(SyncByte, AddressByte, SeparatingBytes, CommandByte,DataBytes);	
								}
						
								break;
							}
							
							//присутствует и команда и подкоманда
							case(SendingType.Case2):
							{
								//2 LNG-байта будут вычисляться и приписываться при непосредственной отсылке команды, поэтому текущая максимальная длинна, передача которой возможна в одной пакете(фрейме) составляет CashCodeClass.MaxSizeOfSingleFrame - 2*sizeof(byte)
								if(
										System.Runtime.InteropServices.Marshal.SizeOf(SyncByte) + 
										System.Runtime.InteropServices.Marshal.SizeOf(AddressByte) +
										System.Runtime.InteropServices.Marshal.SizeOf(SeparatingBytes) +
										System.Runtime.InteropServices.Marshal.SizeOf(CommandByte) + 
										System.Runtime.InteropServices.Marshal.SizeOf(SubCommandByte) + 
										DataBytes.Length*System.Runtime.InteropServices.Marshal.SizeOf(DataBytes.GetType().GetElementType())
																		<=
										(CashCodeClass.MaxSizeOfSingleFrame - 2*sizeof(byte))
									)
								{
									this.SendCommand_Case2SMALL(SyncByte, AddressByte, SeparatingBytes, CommandByte, SubCommandByte, DataBytes);
								}
								else
								{
									this.SendCommand_Case2LARGE(SyncByte, AddressByte, SeparatingBytes, CommandByte, SubCommandByte, DataBytes);	
								}
					
								break;
							}
					
							//отсутствует команда и подкоманда
							case(SendingType.Case3):
							{
								//2 LNG-байта будут вычисляться и приписываться при непосредственной отсылке команды, поэтому текущая максимальная длинна, передача которой возможна в одной пакете(фрейме) составляет CashCodeClass.MaxSizeOfSingleFrame - 2*sizeof(byte)
								if(
										System.Runtime.InteropServices.Marshal.SizeOf(SyncByte) + 
										System.Runtime.InteropServices.Marshal.SizeOf(AddressByte) +
										System.Runtime.InteropServices.Marshal.SizeOf(SeparatingBytes) +
										DataBytes.Length*System.Runtime.InteropServices.Marshal.SizeOf(DataBytes.GetType().GetElementType())
																		<=
										(CashCodeClass.MaxSizeOfSingleFrame - 2*sizeof(byte))
									)
								{
									this.SendCommand_Case3SMALL(SyncByte, AddressByte, SeparatingBytes, DataBytes);
								}
								else
								{
									this.SendCommand_Case3LARGE(SyncByte, AddressByte, SeparatingBytes, DataBytes);	
								}
					
								break;
							}
							
							//передали неизвестный перечислитель
							default:
							{
								throw new ArgumentException(String.Format("передан неизвестный перечислитель {0} в качестве параметра 'SendingTypes'", SendingTypes), "SendingTypes");
							}
						}
					}
				}
			#endregion	
		

		
		
			#region Метод для отправки команды на порт в формате case 1, размером не более 250 байт(SYNC-ADR-0-CMD-LNG HIGH-LNG LOW-DATA-CRC)
				protected void SendCommand_Case1SMALL(Byte SyncByte, Byte AddressByte, Byte SeparatingByte, Byte CommandByte, Byte[] DataBytes)
				{								
					//проверка на соответствие данных требуемому формату делается в методе-оболочке SendCommand
						//создание массива байт для отправки на порт
							Byte lengthHigh=0, lengthLow;	//т.к. команда маленькая(длинна умещается в 250 байт, то старший байт длинны не нужен и поэтому инициализируем его нулем. Инициализация младшего байта будет ниже)
							Byte[] crcBytes=null;//CRC-байты представляют собой crc-код для байтов, начиная с SYNC и заканчивая концом блока DATA
				
							Byte[] buffer = null;
							Int32 bufferSize=0;
							//в buffer одназначно должны быть выделены байты для SYNC, ADR, SeparatingBytes и CRC
								bufferSize=CashCodeClass.QuantityOfSYNCBytes + 
											CashCodeClass.QuantityOfADRBytes + 
											CashCodeClass.QuantityOfSeparatingByte +
											2*CashCodeClass.QuantityOfLNGByte+
											CashCodeClass.QuantityOfCRCBytes;;
						
							if(CommandByte!=CashCodeClass.ByteNullValue)
							{
								bufferSize+=CashCodeClass.QuantityOfCMDByte;
							}
							//DataByte уже проверен на длинну и если они не соответствует, то выбросится исключение
								bufferSize+=DataBytes.Length;								
				
							//инициализируем байтовый массив и длинну команды
								buffer=new Byte[bufferSize];
								lengthLow=Convert.ToByte(bufferSize);//инициализируем младший байт длинны команды, т.к. команда маленькая, то её длинна влезет в 1 байт
							//заполняем массив
								Int32 currentByte=0;
									buffer[currentByte]=SyncByte;//заносим байт синхронизации
										currentByte+=CashCodeClass.QuantityOfSYNCBytes;
									buffer[currentByte]=AddressByte;//заносим адресный байт
										currentByte+=CashCodeClass.QuantityOfADRBytes;
									buffer[currentByte]=SeparatingByte;//заносим байт разделителя
										currentByte+=CashCodeClass.QuantityOfSeparatingByte;
									buffer[currentByte]=CommandByte;//заносим командный байт 
										currentByte+=CommandByte.Equals(CashCodeClass.ByteNullValue) ? 0 : CashCodeClass.QuantityOfCMDByte; //если командный байт присутствовал (его значение не было равно CashCodeClass.ByteNullValue), то смещаемся на CashCodeClass.QuantityOfCMDByte, в противном случае на 0
									buffer[currentByte]=lengthHigh;
										currentByte+=CashCodeClass.QuantityOfLNGByte;//размер байтов LNG одинаков и для LNG HIGH и для LNG LOW
									buffer[currentByte]=lengthLow;
										currentByte+=CashCodeClass.QuantityOfLNGByte;//размер байтов LNG одинаков и для LNG HIGH и для LNG LOW
									DataBytes.CopyTo(buffer, currentByte);//скопировать массив DataBytes в buffer по индексу currentByte
										currentByte+=DataBytes.Length;
				
								//генерируем массив crc-кода
									crcBytes=BitConverter.GetBytes(
																		ServiceClass.GetCRC16(buffer, currentByte)
																	);//получаем тип Int32 с CRC-кодом массива buffer до элемента currentByte
												
				
								crcBytes.CopyTo(buffer, currentByte);//скопировать массив crcBytes в buffer по индексу currentByte
							//массив заполнен
						//массив создан
			
						//вызываем метод для отправки байтов на порт			
							this.SendCommand_Main(buffer);
				}
			#endregion
		
			#region Метод для отправки команды на порт в формате case 1, размером превышающем 250 байт(SYNC-ADR-0-CMD-LNG HIGH-LNG LOW-DATA-CRC)
				protected void SendCommand_Case1LARGE(Byte SyncByte, Byte AddressByte, Byte SeparatingByte, Byte CommandByte, Byte[] DataBytes)
				{
						//генерируем исключение, т.к. нужно еще разбираться каким образом передавать поток байтов длинной более 250 байт
					throw new System.NotImplementedException ();
				}
			#endregion
		
		
		
			#region Метод для отправки команды на порт в формате case 2, размером не более 250 байт(SYNC-ADR-0-CMD-SYBCMD-LNG HIGH-LNG LOW-DATA-CRC)
				protected void SendCommand_Case2SMALL(Byte SyncByte, Byte AddressByte, Byte SeparatingBytes, Byte CommandByte, Byte SubCommandByte, Byte[] DataBytes)
				{												
					//проверка на соответствие данных требуемому формату делается в методе-оболочке SendCommand
						//создание массива байт для отправки на порт
							Byte[] crcBytes=null;//CRC-байты представляют собой crc-код для байтов, начиная с SYNC и заканчивая концом блока DATA
							Byte lengthHigh=0, lengthLow;	//т.к. команда маленькая(длинна умещается в 250 байт, то старший байт длинны не нужен и поэтому инициализируем его нулем. Инициализация младшего байта будет ниже)
			
							Byte[] buffer = null;
							Int32 bufferSize=0;
							//в buffer одназначно должны быть выделены байты для SYNC, ADR, SeparatingBytes и CRC
								bufferSize=CashCodeClass.QuantityOfSYNCBytes + 
											CashCodeClass.QuantityOfADRBytes + 
											CashCodeClass.QuantityOfSeparatingByte +
											2*CashCodeClass.QuantityOfLNGByte+
											CashCodeClass.QuantityOfCRCBytes;;
						
							if(CommandByte!=CashCodeClass.ByteNullValue)
							{
								bufferSize+=CashCodeClass.QuantityOfCMDByte;
									//SubCommandByte может быть не null только когда CommandByte также не null (спецификация протокола связи)
									if(SubCommandByte!=CashCodeClass.ByteNullValue)
									{
										bufferSize+=CashCodeClass.QuantityOfSUBCMDByte;
									}
							}
							//DataByte уже проверен на длинну и если они не соответствует, то выбросится исключение
								bufferSize+=DataBytes.Length;								
				
							//инициализируем байтовый массив
								buffer=new Byte[bufferSize];
								lengthLow=Convert.ToByte(bufferSize);//инициализируем младший байт длинны команды, т.к. команда маленькая, то её длинна влезет в 1 байт
							//заполняем массив
								Int32 currentByte=0;
									buffer[currentByte]=SyncByte;//заносим байт синхронизации
										currentByte+=CashCodeClass.QuantityOfSYNCBytes;
									buffer[currentByte]=AddressByte;//заносим адресный байт
										currentByte+=CashCodeClass.QuantityOfADRBytes;
									buffer[currentByte]=SeparatingBytes;//заносим байт разделителя
										currentByte+=CashCodeClass.QuantityOfSeparatingByte;
									buffer[currentByte]=CommandByte;//заносим командный байт 
										currentByte+=CommandByte.Equals(CashCodeClass.ByteNullValue) ? CashCodeClass.QuantityOfCMDByte : 0; //если командный байт присутствовал (его значение не было равно CashCodeClass.ByteNullValue), но смещаемся на CashCodeClass.QuantityOfCMDByte, в противном случае на 0
									buffer[currentByte]=SubCommandByte;//заносим байт подкоманды
										currentByte+=SubCommandByte.Equals(CashCodeClass.ByteNullValue) ? CashCodeClass.QuantityOfSUBCMDByte : 0; //если байт подкоманды присутствовал (его значение не было равно CashCodeClass.ByteNullValue), но смещаемся на CashCodeClass.QuantityOfSUBCMDByte, в противном случае на 0
									buffer[currentByte]=lengthHigh;
										currentByte+=CashCodeClass.QuantityOfLNGByte;//размер байтов LNG одинаков и для LNG HIGH и для LNG LOW
									buffer[currentByte]=lengthLow;
										currentByte+=CashCodeClass.QuantityOfLNGByte;//размер байтов LNG одинаков и для LNG HIGH и для LNG LOW
									DataBytes.CopyTo(buffer, currentByte);//скопировать массив DataBytes в buffer по индексу currentByte
										currentByte+=DataBytes.Length;
				
								//генерируем массив crc-кода
									crcBytes=BitConverter.GetBytes(
																		ServiceClass.GetCRC16(buffer, currentByte)
																	);//получаем тип Int32 с CRC-кодом массива buffer до элемента currentByte
				
								crcBytes.CopyTo(buffer, currentByte);//скопировать массив CRCBytes в buffer по индексу currentByte
							//массив заполнен
						//массив создан
			
						//вызываем метод для отправки байтов на порт			
							this.SendCommand_Main(buffer);
				}
			#endregion
		
			#region Метод для отправки команды на порт в формате case 2, размером не более 250 байт(SYNC-ADR-0-CMD-SYBCMD-LNG HIGH-LNG LOW-DATA-CRC)
				protected void SendCommand_Case2LARGE(Byte SyncByte, Byte AddressByte, Byte SeparatingBytes,
		                                Byte CommandByte, Byte SubCommandByte, Byte[] DataBytes)
				{
					//генерируем исключение, т.к. нужно еще разбираться каким образом передавать поток байтов длинной более 250 байт
					throw new System.NotImplementedException ();
				}
			#endregion
		
		
		
			#region Метод для отправки команды на порт в формате case 3, размером не более 250 байт(SYNC-ADR-0-LNG HIGH-LNG LOW-DATA-CRC)
				protected void SendCommand_Case3SMALL(Byte SyncByte, Byte AddressByte, Byte SeparatingByte, Byte[] DataBytes)
				{
					//проверка на соответствие данных требуемому формату делается в методе-оболочке SendCommand
						//создание массива байт для отправки на порт
							Byte lengthHigh=0, lengthLow;	//т.к. команда маленькая(длинна умещается в 250 байт, то старший байт длинны не нужен и поэтому инициализируем его нулем. Инициализация младшего байта будет ниже)
							Byte[] crcBytes=null;//CRC-байты представляют собой crc-код для байтов, начиная с SYNC и заканчивая концом блока DATA
				
							Byte[] buffer = null;
							Int32 bufferSize=0;
							//в buffer одназначно должны быть выделены байты для SYNC, ADR, SeparatingBytes и CRC
								bufferSize=CashCodeClass.QuantityOfSYNCBytes + 
											CashCodeClass.QuantityOfADRBytes + 
											CashCodeClass.QuantityOfSeparatingByte +
											2*CashCodeClass.QuantityOfLNGByte+
											CashCodeClass.QuantityOfCRCBytes;;
						
							//DataByte и CRCByte уже проверены на длинну и если они не соответствуют, то выбросится исключение
								bufferSize+=DataBytes.Length;								
				
							//инициализируем байтовый массив и длинну команды
								buffer=new Byte[bufferSize];
								lengthLow=Convert.ToByte(bufferSize);//инициализируем младший байт длинны команды
							//заполняем массив
								Int32 currentByte=0;
									buffer[currentByte]=SyncByte;//заносим байт синхронизации
										currentByte+=CashCodeClass.QuantityOfSYNCBytes;
									buffer[currentByte]=AddressByte;//заносим адресный байт
										currentByte+=CashCodeClass.QuantityOfADRBytes;
									buffer[currentByte]=SeparatingByte;//заносим байт разделителя
										currentByte+=CashCodeClass.QuantityOfSeparatingByte;
									buffer[currentByte]=lengthHigh;
										currentByte+=CashCodeClass.QuantityOfLNGByte;//размер байтов LNG одинаков и для LNG HIGH и для LNG LOW
									buffer[currentByte]=lengthLow;
										currentByte+=CashCodeClass.QuantityOfLNGByte;//размер байтов LNG одинаков и для LNG HIGH и для LNG LOW
									DataBytes.CopyTo(buffer, currentByte);//скопировать массив DataBytes в buffer по индексу currentByte
										currentByte+=DataBytes.Length;
				
								//генерируем массив crc-кода
									crcBytes=BitConverter.GetBytes(
																		ServiceClass.GetCRC16(buffer, currentByte)
																	);//получаем тип Int32 с CRC-кодом массива buffer до элемента currentByte
												
				
								crcBytes.CopyTo(buffer, currentByte);//скопировать массив crcBytes в buffer по индексу currentByte
							//массив заполнен
						//массив создан
			
						//вызываем метод для отправки байтов на порт			
							this.SendCommand_Main(buffer);
				}
			#endregion
		
			#region Метод для отправки команды на порт в формате case 3, размером не более 250 байт(SYNC-ADR-0-LNG HIGH-LNG LOW-DATA-CRC)
				protected void SendCommand_Case3LARGE(Byte SyncByte, Byte AddressByte, Byte SeparatingByte, Byte[] DataByte)
				{
					//генерируем исключение, т.к. нужно еще разбираться каким образом передавать поток байтов длинной более 250 байт
					throw new System.NotImplementedException ();
				}
			#endregion
		
			
			#region Метод для установки DataArray в отсылаемом пакете
				protected void SetDataArrayForPackage(ref Byte[] Buffer, ref Int32 CurrentIndex, Byte[] DataArray)
				{
					//если размер передаваемых данных больше CashCodeClass.MaxSizeOfSingleFrame, то
					if(Buffer.Length>CashCodeClass.MaxSizeOfSingleFrame)
					{
						//согласно мануаллу, если размер передаваемых данных больше CashCodeClass.MaxSizeOfSingleFrame, т.е. 250 байт, то в байт LNG устанавливается 0, а длинна команды указывается в:
							//a)если нет подкоманд, то в нулевом и первом байтах;
							//b)если подкоманды присутствуют, то длинна указывается в первом и втором байтах.
						//в начале двухбайтного LNG всегда должен идти бит MSB
				
						//инициализируем переменные, отвечающие за длинну передаваемого пакета
							Byte lengthHight=BitConverter.GetBytes(Buffer.Length)[0],	//старший байт в lengthHight
								lengthLow=BitConverter.GetBytes(Buffer.Length)[1];	//младший байт в lengthLow
							
							
							
							//инициализируем временный массив с пересылаемыми данными плюс два байта команд
							Byte[] tmpDataArray=new Byte[DataArray.Length+2];
								tmpDataArray[0]=Convert.ToByte(lengthHight|CashCodeClass.MSBByte);//устанавливаем байт MSB, т.к. он всегда требуется для двухбайтовой длинны
								tmpDataArray[1]=lengthLow;
								DataArray.CopyTo(tmpDataArray, 2);
							
							tmpDataArray.CopyTo(Buffer, CurrentIndex);
								CurrentIndex+=tmpDataArray.Length;
					}
					//в противном случае
					else
					{
						//инициализируем переменные, отвечающие за длинну передаваемого пакета
							//Byte lengthHigh=0, 
								Byte lengthLow=Convert.ToByte (Buffer.Length);
							//инициализируем пересылаемый буффер
							//Buffer[CurrentIndex]=lengthHigh;
								//CurrentIndex+=1;
							Buffer[CurrentIndex]=lengthLow;
								CurrentIndex+=1;
						//скопировать содержимое DataArray в Buffer по индексу CurrentIndex
						DataArray.CopyTo(Buffer, CurrentIndex);
							CurrentIndex+=DataArray.Length;
					}
				}
			#endregion
		
		
			#region Главный метод для отправки байт на порт(используется в case 1, case 2, case 3)
				protected void SendCommand_Main(Byte[] SentBytes)
				{								
					//проверяем открыт ли порт this.SerialPort
					if(this.SerialPort.IsOpen==false)
					{
						//если закрыт, то пытаемся открыть и если открытие не получилось
						if(!this.OpenPort())
						{
							//генерируем Exception
							throw new MemberAccessException("Невозможно получить доступ к устройству на порту" + this.SerialPort.PortName + "(порт не открывается)");
						}
					}
					
					//отправляем на порт все что есть в массиве buffer со смещением в CashCodeClass.WriteOffset
					this.SerialPort.Write(SentBytes,CashCodeClass.WriteOffset, SentBytes.Length-CashCodeClass.WriteOffset);
			
					if(!this.ClosePort())
					{
						throw new MemberAccessException("Ошибка при закрытии порта " + this.SerialPort.PortName);
					}
				}
			#endregion
		
		

			#region Метод для ожидания ответа от SerialPort
				protected Boolean WaitForResponse(Int32 ResponseTime, Int32 NonResponseTime)
				{
					Boolean returnedValue;
			
					//приостанавливаем поток на время ResponseTime
						System.Threading.Thread.Sleep(ResponseTime);
					
					//проверяем поле this.DataReceived, которое сигнализирует о приходе сообщения
					if(this.DataReceived==true)
					{
						this.DataReceived=false;
						returnedValue=true;
					}
					//если не true, то ответ еще не пришел, продолжаем ждать
					else
					{
						//приостанавливаем поток на время NonResponseTime, если ответ не придет и сейчас, то тогда с устройством проблемы
							System.Threading.Thread.Sleep(NonResponseTime);
						if(this.DataReceived==true)
						{
							this.DataReceived=false;
							returnedValue=true;
						}
						//если вновь не true, то устройство не ответило в течении заданного времени - имеются проблемы с ним
						else
						{
							returnedValue=false;
						}
					}
					
					return returnedValue;
				}
			#endregion
		

		}
	#endregion
}

