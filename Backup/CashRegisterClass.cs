using System;

namespace ElectronicPay
{
	#region Перечислитель, содержащий все управляющие символы протокола контрольно-кассовой машины(ККМ)
		public enum CharacteControlledProtocolOfCashRegister : byte	{ ENQ=0x05, ACK=0x06, STX=0x02, ETX=0x03,
																		EOT=0x04, NAK=0x15, DLE=0x10}
	#endregion
	
	#region Класс, необходимый для работы с ККМ
		public class CashRegisterClass
		{
			#region Конструктор класса CashRegisterClass [пустой]
				public CashRegisterClass ()
				{
				}
			#endregion


			#region Метод для распечатки данных
				public static void PrintData (String Data)
				{
					
				}
			#endregion
		}
	#endregion
}

